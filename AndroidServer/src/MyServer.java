import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.commons.io.FileUtils;


public class MyServer {
	private static String exePath = "C:\\Users\\Wild\\Desktop\\CompassCamera\\bin\\Release\\Compass.exe";
	private static String exeFolder="C:\\Users\\Wild\\Desktop\\CompassCamera\\bin\\Release\\";
	private static String initFilesFolder="C:\\Users\\Wild\\Desktop\\CompassCamera\\bin\\Release\\output\\";
	private static String videoDirectory="C:\\Users\\Wild\\Desktop\\VideoData\\";
	public static void main(String[] args) throws InterruptedException{
		  ServerSocket serverSocket = null;
		  Socket socket = null;
		  DataInputStream dataInputStream = null;
		  DataOutputStream dataOutputStream = null;
		  
		  try {
		   serverSocket = new ServerSocket(8888);
		   System.out.println("Listening :8888");
		  } catch (IOException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
		  }
		  
		  while(true){
		   try {
		    socket = serverSocket.accept();
		    dataInputStream = new DataInputStream(socket.getInputStream());
		    dataOutputStream = new DataOutputStream(socket.getOutputStream());
		    System.out.println("ip: " + socket.getInetAddress());
		    int flag = Integer.parseInt(dataInputStream.readUTF());
		    System.out.println("Connected!!! Flag is now  "+flag);
		    if (flag == 1){
		    	System.out.println("Executing the exe... ");
		    	Runtime.getRuntime().exec(exePath, null, new File(exeFolder));
		    	//dataOutputStream.writeUTF("Data is recorded, Thank you :)");
		    	System.out.println("Recording video, please wait... ");
				Thread.sleep(32000);
				System.out.println("Done... ");
				
				//create a random directory to store the video
				long currentTime = System.currentTimeMillis();
				String resDirectory =videoDirectory+currentTime;
				File file = new File(resDirectory);
				if (!file.exists()) {
					if (file.mkdir()) {
						System.out.println(resDirectory + "     Directory is created!");
					} else {
						System.out.println("Failed to create directory!");
					}
				}
				
				//move to newly created dir
				
				File mainDir=new File(initFilesFolder);
				
				File[] allImageFiles=mainDir.listFiles();
				
				for (File  source : allImageFiles) {
			    	  
			    	  File dest = new File(resDirectory+"//"+source.getName());
			    	  try {
			    	      FileUtils.copyFile(source, dest);
			    	      } 
			    	  catch (IOException e) {
			    	    	  e.printStackTrace();
			    	      }
			    	  Thread.sleep(500);
			  		
			    	  
			      }
				System.out.println("Data is recorded...");
				
		    }
//		    System.out.println("message: " + dataInputStream.readUTF());
		    
		   } catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		   }
		   finally{
		    if( socket!= null){
		     try {
		      socket.close();
		     } catch (IOException e) {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
		     }
		    }
		    
		    if( dataInputStream!= null){
		     try {
		      dataInputStream.close();
		     } catch (IOException e) {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
		     }
		    }
		    
		    if( dataOutputStream!= null){
		     try {
		      dataOutputStream.close();
		     } catch (IOException e) {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
		     }
		    }
		   }
		  }
		 }
}