
//=================================================================================-----
//== NaturalPoint (c)
//== Camera Library SDK Sample
//==
//== This sample brings up a connected camera and displays it's MJPEG output using a
//== GLSL shader that performs edge detection on the camera's image.
//=================================================================================-----

#include "supportcode.h"       //== Boiler-plate code for application window init ===---
#include "cameralibrary.h"     //== Camera Library header file ======================---
#include <math.h>
#include <vector>
#include <stdio.h>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include <thread>
#include<time.h>

#include <windows.h>		// Header File For Windows
//#include "glew.h"           // Header File for OpenGL Extensions
//#include <gl\gl.h>			// Header File For The OpenGL32 Library
//#include <gl\glu.h>			// Header File For The GLu32 Library
#include "cameramanager.h"

using namespace CameraLibrary;
using namespace std;
const int NUM_MSECONDS = 40000;

void shutdownCameras(Camera* cameras[], int nCameras) {
	//== Release cameras ==--
	for (int i = 0; i < nCameras; i++) {
		cameras[i]->Release();
	}

	//== Shutdown Camera Library ==--
	CameraManager::X().Shutdown();
	printf("Camera and camera manager: shutdown\n");
}





void recordVideo(Camera* camera, int index) {
	int cameraWidth = 640;
	int cameraHeight = 480;

	Surface  Texture(cameraWidth, cameraHeight);
	Bitmap * framebuffer = new Bitmap(cameraWidth, cameraHeight, Texture.PixelSpan() * 4,
		Bitmap::ThirtyTwoBit, Texture.GetBuffer());

	string filename = "output/out" + std::to_string(index + 1) + ".avi";
	cv::VideoWriter video(filename, CV_FOURCC('P', 'I', 'M', '1'), 60, cv::Size(cameraWidth, cameraHeight), false);

	double time_counter = 0;
	clock_t this_time = clock();
	clock_t last_time = this_time;

	while (1)
	{
		this_time = clock();

		time_counter += (double)(this_time - last_time);

		last_time = this_time;
		//== Fetch a new frame from the camera ===---
		Frame *videoframe = camera->GetFrame();

		if (videoframe)
		{
			//== Ok, we've received a new frame, lets do something
			//== with it.

			//== Lets have the Camera Library raster the camera's
			//== image into our texture.

			videoframe->Rasterize(framebuffer);

			cv::Mat matFrame(cv::Size(cameraWidth, cameraHeight), CV_8UC1);

			const int BACKBUFFER_BITSPERPIXEL = 8;

			videoframe->Rasterize(cameraWidth, cameraHeight, matFrame.step, BACKBUFFER_BITSPERPIXEL, matFrame.data);
			
			video.write(matFrame);
			//== Display Camera Image ============--
			/*
			if (!DrawGLScene(&Texture, videoframe->Width(), videoframe->Height(), 0.2))
				break;

			//== Escape key to exit application ==--

			if (keys[VK_ESCAPE])
				break;
			*/
			//== Release frame =========--

			videoframe->Release();
		}//end of if

		Sleep(2);
		//== Service Windows Message System ==--
		//printf("Time difference %f \n", time_counter);
		if (time_counter>NUM_MSECONDS)
			break;
	}//end of while

	camera->Release();
}//end of recordVideo
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	int nCameras = 0;
	int count = 0;
	Camera* cameras[4];

	int cameraWidth = 640;
	int cameraHeight = 480;

	//== For OptiTrack Ethernet cameras, it's important to enable development mode if you
	//== want to stop execution for an extended time while debugging without disconnecting
	//== the Ethernet devices.  Lets do that now:
	CameraLibrary_EnableDevelopment();

	//== Initialize connected cameras ==========----
	CameraManager::X().WaitForInitialization();

	printf("Cameras:\n");

	CameraList list;
	nCameras = list.Count();

	//== If no device connected, warn and exit ==--
	if (nCameras <= 0) {
		printf("Please connect a camera - No Device Connected");
		return 1;
	}


	if (nCameras > 0) {
		for (int i = 0; i < nCameras; i++) {
			printf("Camera %d >> %s\n", i, list[i].Name());
			//== Get a connected camera ================----
			cameras[i] = CameraManager::X().GetCameraBySerial(list[i].Serial());
			//1 - 205450
			//printf("%d", list[i].Serial());

			//== Determine camera resolution to image size ==----
			if (i == 0) {
				cameraWidth = cameras[0]->Width();
				cameraHeight = cameras[0]->Height();
			}

			Sleep(2);

			//== MJPEG Mode ==--
			cameras[i]->SetIRFilter(true);
			cameras[i]->SetVideoType(MJPEGMode);  //== Select MJPEG Video ===================---
			cameras[i]->SetMJPEGQuality(100);
			cameras[i]->SetExposure(480);
			//cameras[i]->SetIntensity(400);
			//cameras[i]->SetAEC(true); 
			cameras[i]->SetFrameRate(60);//== Enable Automatic Exposure Control ====---
			cameras[i]->SetAGC(true);             //== Enable Automatic Gain Control ========---

			//== Start camera output ==--
			cameras[i]->Start();

		}
	}


	switch (nCameras){
	case 0:{
		printf("Please connect a camera - No Device Connected");
		return 1;
		}
		break;
		case 1:
		{
			std::thread t0(recordVideo, cameras[0], 0);
			t0.join();
		}
			break;
		case 2:
		{
			std::thread t0(recordVideo, cameras[0], 0);
			std::thread t1(recordVideo, cameras[1], 1);
			t0.join();
			t1.join();
		}
			break;
		case 3:
		{
			std::thread t0(recordVideo, cameras[0], 0);
			std::thread t1(recordVideo, cameras[1], 1);
			std::thread t2(recordVideo, cameras[2], 2);
			t0.join();
			t1.join();
			t2.join();
		}
			break;
		case 4:
		{
			std::thread t0(recordVideo, cameras[0], 0);
			std::thread t1(recordVideo, cameras[1], 1);
			std::thread t2(recordVideo, cameras[2], 2);
			std::thread t3(recordVideo, cameras[3], 3);
			t0.join();
			t1.join();
			t2.join();
			t3.join();
		}
			break;
		default:
		{
			printf("Please connect 4 or less cameras. \n");
			return 1;
		}
	}
		

	CameraManager::X().Shutdown();
	printf("Camera and camera manager: shutdown\n");
	//== Exit the application.  Simple! ==--
	//getchar();
	return 1;
}

