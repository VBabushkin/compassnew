package shoeFittingAlgorithm;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;  
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;
import java.awt.Rectangle;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.highgui.Highgui;

public class testTwoCamsVsOneCam {
	 static String exePath = "C:\\Users\\Wild\\Desktop\\MyTests\\CompassCamera\\bin\\Release\\CompassOld.exe";
	 static String exePath1 = "C:\\Users\\Wild\\Desktop\\MyTests\\CompassCamera\\bin\\Release\\Compass.exe";
	 private static String exeFolder="C:\\Users\\Wild\\Desktop\\MyTests\\CompassCamera\\bin\\Release\\";
	 private static StringBuffer sb;
	
	public static void main(String args[]) throws IOException, InterruptedException{
	
		
		//get  a frame from two cams and combine them to one picture:
		long startTime = System.currentTimeMillis();
		
		Runtime.getRuntime().exec(exePath, null, new File(exeFolder));
		Thread.sleep(800);
		
		
		
		
		String path1="C:/Users/Wild/Desktop/MyTests/CompassCamera/bin/Release/output/now1.jpg";
		String path2="C:/Users/Wild/Desktop/MyTests/CompassCamera/bin/Release/output/now2.jpg";
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		//read image 1
		Mat piece1 = Highgui.imread(path1, Highgui.CV_LOAD_IMAGE_COLOR);
		//read image 1
		Mat piece2 = Highgui.imread(path2, Highgui.CV_LOAD_IMAGE_COLOR);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(piece1), "First Image");
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(piece2), "Second Image");
		
		Mat twoPieces = new Mat(2*piece1.rows(), piece1.cols(), piece1.type());
		twoPieces = addToVertically(piece1,piece2);
		
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(twoPieces), "Combined Image");
		
		deletefile(path1);
		deletefile(path2);
		
		
		long estimatedTime = System.currentTimeMillis() - startTime;
		
		System.out.println("TIME ELAPSED MULTIPLE CAMERAS MILLISECONDS "+ estimatedTime);
		System.out.println("TIME ELAPSED MULTIPLE CAMERAS SECONDS "+ TimeUnit.SECONDS.convert(estimatedTime, TimeUnit.MILLISECONDS));
		
		
		
		

		//take a picture with one camera
		
		
		startTime = System.currentTimeMillis();
		
		Runtime.getRuntime().exec(exePath1, null, new File(exeFolder));
		Thread.sleep(1000);
		
		
		
		
		
		Mat source = Highgui.imread(path1, Highgui.CV_LOAD_IMAGE_COLOR);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(source), "One single camera frame");
		
		estimatedTime = System.currentTimeMillis() - startTime;
		
		System.out.println("TIME ELAPSED SINGLE CAMERA MILLISECONDS "+ estimatedTime);
		System.out.println("TIME ELAPSED SINGLE CAMERA SECONDS "+ TimeUnit.SECONDS.convert(estimatedTime, TimeUnit.MILLISECONDS));
		deletefile(path1);
		
				
	}
	/**
	 * 
	 * @param matA
	 * @param matB
	 * @return
	 */
	private static Mat addToVertically(Mat matA, Mat matB) {
	    Mat m = new Mat(matA.rows()+matB.rows(), matA.cols(), matA.type());
	    int aCols = matA.cols();
	    int aRows = matA.rows();
	    for (int i = 0; i < aRows; i++) {
	        for (int j = 0; j < aCols; j++) {
	            m.put(i, j, matA.get(i, j));
	        }
	    }
	    for (int i = 0; i < matB.rows(); i++) {
	        for (int j = 0; j < matB.cols(); j++) {
	            m.put(aRows+i, j, matB.get(i, j));
	        }
	    }
	    return m;
	}
	
	
	/**
	 * 
	 * @param fileName
	 */
	private static void deletefile(String fileName) {
		File file = new File(fileName);
		boolean success = file.delete();
		if (!success) {
			System.out.println(fileName + " Deletion failed.");
		} else {
			System.out.println(fileName + " File deleted.");
		}
	}
	
//	
//	static private Mat addToHorizontally(Mat matA, Mat matB) {
//	    Mat m = new Mat(matA.rows(), matA.cols() +  matB.cols(), matA.type());
//	    int aCols = matA.cols();
//	    int aRows = matA.rows();
//	    for (int i = 0; i < aRows; i++) {
//	        for (int j = 0; j < aCols; j++) {
//	            m.put(i, j, matA.get(i, j));
//	        }
//	    }
//	    for (int i = 0; i < matB.rows(); i++) {
//	        for (int j = 0; j < matB.cols(); j++) {
//	            m.put(i, aCols + j, matB.get(i, j));
//	        }
//	    }
//	    return m;
//	}
	
	
	
	
	
}
