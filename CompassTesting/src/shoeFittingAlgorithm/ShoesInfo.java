package shoeFittingAlgorithm;

import java.awt.image.BufferedImage;

/**
 * contains shoes data related to one pair when reads from the db
 * @author nimesha
 *
 */
public class ShoesInfo {

	private int id = -1;
	private BufferedImage cannyImage = null;
	private AttributesMatrix[][] argmatrix  = null;
	
	
	private String sDirection = "";
	private int[] iarrlinecircleinfo = new int[4];
	

	public ShoesInfo(int id, AttributesMatrix[][] argmatrix,  int[] iarrlinecircleinfo) {
		this.setId(id);
		//this.setCannyImage(cannyImage);
		this.setArgmatrix(argmatrix);
		//this.setDirection(sDirection);
		this.setLinecircleinfo(iarrlinecircleinfo);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public BufferedImage getCannyImage() {
		return cannyImage;
	}
	public void setCannyImage(BufferedImage cannyImage) {
		this.cannyImage = cannyImage;
	}



	public AttributesMatrix[][] getArgmatrix() {
		return argmatrix;
	}

	public void setArgmatrix(AttributesMatrix[][] argmatrix) {
		this.argmatrix = argmatrix;
	}

	public String getDirection() {
		return sDirection;
	}

	public void setDirection(String sDirection) {
		this.sDirection = sDirection;
	}
	
	public int[] getLinecircleinfo() {
		return iarrlinecircleinfo;
	}

	public void setLinecircleinfo(int[] iarrlinecircleinfo) {
		this.iarrlinecircleinfo = iarrlinecircleinfo;
	}
	
	
}
