package shoeFittingAlgorithm;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;

public class testLineDetection {
	private static final int iLineDistance = 20;
	private static final int iBoundaryPixels = 5;
	
	private ArrayList <ArrayList<int[]>> iarrLines = null;
	private static ArrayList <int[]> iarrPixelLocations = null;
	
	double delta                       = 0.0;
	double slope                           = 0.0;
	
	private static Graphics2D graph = null;
	
	private static BufferedImage outputImage = null;
	private static String sPixelBasedLineDetectionImagePath = ".//results//pixelBasedLinesDetection.jpg";
	private static String edgeDetectedImagePath = ".//results//cannyedgedetectedshoes_now1.jpg";
	
	//image dimensions
	static int sizeX = 0;
	static int sizeY = 0;
	
	static //pixel arrays
	int [][] iarrImageColors           = null;
	int [][] iarrImageColorsBackup     = null;
	
//	static String initialImagePath1 = ".//results//extracted//shoes_21_45_RIGHT.png";
//	static String initialImagePath2 = ".//results//extracted//rotated.png";
	
	static String initialImagePath1 = ".//results//extracted//1.png";
	static String initialImagePath2 = ".//results//extracted//2.png";
	
	
	public static void main(String args[]) throws IOException{
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		
		//read image
		Mat source1 = Highgui.imread(initialImagePath1, Highgui.CV_LOAD_IMAGE_COLOR);
		Mat source2 = Highgui.imread(initialImagePath2, Highgui.CV_LOAD_IMAGE_COLOR);
		
		
		Mat tempImage1= source1.clone();
		Mat tempImage2= source2.clone();
		

		//original image
		processAndDisplayLines(tempImage1);
		//rotated image
		processAndDisplayLines(tempImage2);
	    
	    
		
	}
	
	/**
	 * 
	 * @param tempImage
	 */
	public static void processAndDisplayLines(Mat tempImage){
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempImage),"Initial Image");
		//optimal parameters for Canny edge extractor
		//TODO:
		//find optimal parameters
		
		
		int lowThreshold=50;
		int highThreshold=50;
		int kernelSize=3;
		
		Mat destination0=tempImage.clone();//imgToProcess1.clone();
		//extract edges
		Mat cannyFilteredSourceMat= AnalyzeImage.applyCannyEdgeDetectorOpenCV_Mat(destination0,lowThreshold, highThreshold, kernelSize);
		//display the image with extracted edges
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(cannyFilteredSourceMat),"Canny edge extracting algorith applied");
		
		//Extract circles
		Mat destination1=cannyFilteredSourceMat.clone();
		
		Mat destination2=cannyFilteredSourceMat.clone();

		Mat destination3=cannyFilteredSourceMat.clone();
		
		BufferedImage destinationBuffered = ProcessImages.Mat2BufferedImage(destination2);
		ArrayList<Rectangle>listOfDetectedCircleDimensions =new ArrayList<Rectangle>();

		//now detect lines on the image
		
		ArrayList<int[][]>listOfDetectedLinesCoordinates =new ArrayList<int[][]>();
		
		//specify the length of shortest detected line in pixels
	    int currentLengthThresholdValue = 20;
	    listOfDetectedLinesCoordinates=PixelBasedLineDetection_T.detectLinesAndAddToARG_v2(destinationBuffered,currentLengthThresholdValue);
	   
		//draw detected lines and circles
	    
	    destination3=AnalyzeImage.drawLinesCirclesArrayList(destination1,listOfDetectedLinesCoordinates,listOfDetectedCircleDimensions);
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination3),"Lines and circles detected for shoe ");
	    
	}
	
	
	/**
	 * repairs canny edge detected image and makes the wire-frame model
	 * @param cannyEdgeDetectedImagePath edge detected image
	 * @throws Exception
	 */
	public static ArrayList<int[][]>  detectLinesAndAddToARG_v2(BufferedImage inputImage, int currentLengthThresholdValue) {//int[][]
		
		//read canny edge detected image
		//BufferedImage inputImage = ImageIO.read(new File(edgeDetectedImagePath));
		sizeX = inputImage.getWidth();
		sizeY = inputImage.getHeight();

		//to save image pixel color data
		iarrImageColors             = new int[sizeX][sizeY];
		//iarrImageColorsBackup       = new int[sizeX][sizeY];
		//String [][] sarrDetectedLineSegments = new String[sizeX][sizeY];

		//new image to be drawn - repaired image
		outputImage = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_RGB);
		graph = outputImage.createGraphics();
		graph.drawImage(inputImage, 0, 0, null);
		graph.setStroke(new BasicStroke(1));
		graph.setColor(Color.WHITE);
		
		//list of point coordinates
		ArrayList<int[][]>result =new ArrayList<int[][]>();
		
		//get B & W pixel data = thresholding
		for (int x = 0; x < sizeX; x++) {
			for (int y = 0; y < sizeY; y++) {
				int rgb = inputImage.getRGB(x, y);
				// if the RGB value at given point x,y is greater than 16 then set that pixel to white else set to black
				int r = (rgb >> 16) & 0xFF;
				int g = (rgb >> 8) & 0xFF;
				int b = (rgb & 0xFF);
				int gray = (r + g + b) / 3;

				if (gray > 230) iarrImageColors[x][y] = 1; //white  
				else            iarrImageColors[x][y] = 0; //black  
			}
		}
		//ProcessImages.displayImage(outputImage,"BlackWhite");
		//System.out.println("iarrImageColors[0].length " + iarrImageColors[0].length); //height 448
		//System.out.println("iarrImageColors.length " + iarrImageColors.length);       //width  456

		//before process save original
		//iarrImageColorsBackup = deepCopy(iarrImageColors);
		
		
		
		for (int r = 0; r < iarrImageColors.length; r++) {
            for (int c = 0; c < iarrImageColors[r].length; c++) {
            	
            	//if white pixel found
            	if (iarrImageColors[r][c] == 1) {
            		result.add(detectLines_v1(r, c,iarrImageColors, currentLengthThresholdValue));
            	}
            	
            }
            
		}
		
		graph.dispose();
		//ImageIO.write(outputImage, "PNG", new File(sPixelBasedLineDetectionImagePath));
		//ProcessImages.displayImage(outputImage,"Lines Detected");
		return result;
		//return iarrImageColors;
	}
	
	
	
	
	
	/**
	 * get the adjacent matrix of pixels of a given pixel location 
	 * @param row original pixel row value
	 * @param col original pixel column value
	 * @return 2D array of adjacent matrix of pixels
	 */
	static int[][] getAdjacentPixelMatrix(int row, int col) {
		
		int [][] iarrAdjacentPixelMatrix = null;
		
		try {
			if ( (row < 5) || (col < 5) || (row >= (sizeX-5)) || (col >= (sizeY-5)) )
				return null;
			
			iarrAdjacentPixelMatrix = new int[3][3];
			int i = 0, j = 0;
			for (int r = (row-1); r < (row+2); r++) {
			    for (int c = (col-1); c < (col+2); c++) {
			    	int value = iarrImageColors[r][c];
			    	iarrAdjacentPixelMatrix[i][j] = value; 
			    	j++;
			    }
			    i++; j = 0;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return iarrAdjacentPixelMatrix;
	}
	
	
	/**
	 * check a given pixel dimensions are already in the array (already processed pixels)
	 * @param iarrAllPixelsDetected current processed array of pixels
	 * @param tempR row coordinates of the pixel to be checked
	 * @param tempC column coordinates of the pixel to be checked
	 * @return
	 */
	private static boolean isPixelExistsInArray(ArrayList<int[]> iarrAllPixelsDetected, int tempR, int tempC) {
		for (int i = 0; i < iarrAllPixelsDetected.size(); i++) {
			int [] pixelrc = iarrAllPixelsDetected.get(i);
			if ( (pixelrc[0] == tempR) && (pixelrc[1] == tempC) )
				return true;
		}
		return false;
	}
	
	/**
	 * get the pixel locations between two pixels
	 * @param startpixelxy start point of the first pixel
	 * @param endpixelxy end point of the second pixel
	 * @return pixel array between two points
	 */
	private static ArrayList<int[]> getPixelsOfTheIdealLineBetweenTwoPoints(int startpixelxy[], int endpixelxy[]) {
		//Bresenham Algorithm
		ArrayList<int[]> iarrPixelsInLine = new ArrayList<int[]>();
		int x  = startpixelxy[0],  y  = startpixelxy[1];
		int x2 = endpixelxy[0],    y2 = endpixelxy[1];
		
		int w = x2 - x;
	    int h = y2 - y;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
	    if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
	    if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
	    if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
	    int longest = Math.abs(w);
	    int shortest = Math.abs(h);
	    
	    if (!(longest > shortest)) {
	        longest = Math.abs(h);
	        shortest = Math.abs(w) ;
	        if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
	        dx2 = 0;            
	    }
	    
	    int numerator = longest >> 1;
		
	    for (int i = 0; i <= longest; i++) {
	        iarrPixelsInLine.add(new int[]{x, y});
	        numerator += shortest;
	        if (!(numerator < longest)) {
	            numerator -= longest;
	            x += dx1;
	            y += dy1;
	        } else {
	            x += dx2;
	            y += dy2;
	        }
	    }
	    
		return iarrPixelsInLine;
	}
	
	/**
	 * calculate the Area Between two Lines (same starting and end points)
	 * @param iarrPixelsOnActualLine
	 * @param iarrPixelsOnIdealLine
	 * @return
	 */
	private static double calculateAreaBetweenLinesAndGetThreshold(ArrayList<int[]> iarrPixelsOnActualLine, ArrayList<int[]> iarrPixelsOnIdealLine) {
		
		//ArrayList<int[]> matchedPixels = new ArrayList<int[]>();
		ArrayList<Double> matchedPixels = new ArrayList<Double>();
		double dAreaPixelCount = 0;
		int actualLineUncommonPixelsCount = 0;
		int actualLineCommonPixelsCount = 0;

		int[] startpixelxy = iarrPixelsOnIdealLine.get(0);
		int[] endpixelxy   = iarrPixelsOnIdealLine.get(iarrPixelsOnIdealLine.size()-1);

		//get orientation and slope
		int x  = startpixelxy[0],  y  = startpixelxy[1];
		int x2 = endpixelxy[0],    y2 = endpixelxy[1];
		int w = x2 - x;
		int h = y2 - y;
		double slope = (double) ( ((double) h) / w);

		for (int i = 1; i < (iarrPixelsOnIdealLine.size()-1); i++) {
			int [] pixelxyOnIdeal = iarrPixelsOnIdealLine.get(i);
			for (int j = 1; j < (iarrPixelsOnActualLine.size()-1); j++) {
				int [] pixelxyOnActual = iarrPixelsOnActualLine.get(j);
				
				if (h == 0) {//horizontal
					if (Math.abs(pixelxyOnIdeal[1]) == Math.abs(pixelxyOnActual[1]))
					{
						Point p1 = new Point(pixelxyOnActual[0], pixelxyOnActual[1]);
						Point p2 = new Point(pixelxyOnIdeal[0], pixelxyOnIdeal[1]);
						double dd = p1.distance(p2);
						
						//int idd = new Double(dd).intValue();
						if (dd > 0) {
							//matchedPixels.add(new int[]{pixelxyOnActual[0], pixelxyOnActual[1], pixelxyOnIdeal[0], pixelxyOnIdeal[1], idd});
							matchedPixels.add(dd);
							break;
						}
					}
				}
				else if (w == 0) {//vertical
					if (Math.abs(pixelxyOnIdeal[0]) == Math.abs(pixelxyOnActual[0]))
					{
						Point p1 = new Point(pixelxyOnActual[0], pixelxyOnActual[1]);
						Point p2 = new Point(pixelxyOnIdeal[0], pixelxyOnIdeal[1]);
						double dd = p1.distance(p2);
						
						if (dd > 0) {
							matchedPixels.add(dd);
							break;
						}
					}
				}
				else if ( (slope != 0)  ) {//sloped
					Point p1 = new Point(pixelxyOnActual[0], pixelxyOnActual[1]);
					Point p2 = new Point(pixelxyOnIdeal[0], pixelxyOnIdeal[1]);
					
					int w2 = p2.x - p1.x;
					int h2 = p2.y - p1.y;
					//if ( (w2 != 0) || (h2 != 0) )
					{
						double slope2 = (double) ( ((double) h2) / w2);
						double dmIsPerpendicular = slope * slope2;
						
						//if ( (dmIsPerpendicular > -1.8) && (dmIsPerpendicular < -0.2) ) //check if perpendicular 
						if ( (dmIsPerpendicular > -1.8) && (dmIsPerpendicular < -0.2) )
						{
							double ddist = p1.distance(p2);
							if (ddist > 0) {
								matchedPixels.add(ddist); 
								//System.out.println("dmIsPerpendicular: " + dmIsPerpendicular + " dist: " + ddist);
								break;
							}
						}
						
					}
				}
				
			}
		}
		
		for (int i = 0; i < matchedPixels.size(); i++) {
			double matchedPixelWithDistance = matchedPixels.get(i);
			dAreaPixelCount = dAreaPixelCount + matchedPixelWithDistance;
		}
		
		//counting actual line pixels which are not common on the ideal line
		for (int i = 0; i < iarrPixelsOnActualLine.size(); i++) {
			int[] actualxy = iarrPixelsOnActualLine.get(i);
			Point actualPixel = new Point(actualxy[0], actualxy[1]);
			
			for (int j = 0; j < iarrPixelsOnIdealLine.size(); j++) {
				int[] idealxy = iarrPixelsOnIdealLine.get(j);
				Point idealPixel = new Point(idealxy[0], idealxy[1]);
				
				//if (actualPixel.equals(idealPixel)) 
				if ( (actualPixel.x == idealPixel.x) && (actualPixel.y == idealPixel.y) )
					actualLineCommonPixelsCount++;
				
			}
		}
		actualLineUncommonPixelsCount = iarrPixelsOnActualLine.size() - actualLineCommonPixelsCount; 
		dAreaPixelCount = dAreaPixelCount + actualLineUncommonPixelsCount;
		return dAreaPixelCount; 
		
	}
	
	
	
	/**
	 * traverse through all the edge lines and detect straight lines
	 * also add to the ARG and prints the lines into the new image
	 * @param r row of the white pixel found 
	 * @param c column of the white pixel found 
	 */
	private static int[][] detectLines_v1(int r, int c, int[][] iarrImageColors, int currentLengthThresholdValue) {
		int result [][]= new int[2][2];
		int nextR = r, nextC = c;
		boolean bPixelFound = false;
		//double previousslope = 0, currentslope = 0, slope_pixels = 0, slope = 0;
		iarrPixelLocations = new ArrayList<int[]>();
		iarrPixelLocations.add(new int[]{r, c}); //add original pixel
		int iLoop = 0;
		int exitcode = 40;
		double dNoOfPixels = 0.0;
		double dNoOfPixelsThreshold = 0.0;
		ArrayList<int[]> pixelsonline = null;
		ArrayList<int[]> iarrLineSegments = new ArrayList<int[]>();
		
		//Random rand = new Random();
		//graph.setColor(new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat()));
		graph.setColor(Color.RED);
		
			int [][] iarrPrimaryAdjacentPixelMatrix = getAdjacentPixelMatrix(r, c);
			int tempCC = 0, tempRR = 0;

			for (int aa = 0; aa < 3; aa++) { //original matrix
				if (aa==0) tempRR=(r - 1);
				if (aa==1) tempRR=(r);
				if (aa==2) tempRR=(r + 1);
				for (int bb = 0; bb < 3; bb++) {
					if (bb==0) tempCC = (c - 1);
					if (bb==1) tempCC = (c);
					if (bb==2) tempCC = (c + 1);

					if ( (iarrPrimaryAdjacentPixelMatrix != null) && (iarrPrimaryAdjacentPixelMatrix[aa][bb] == 1) ) {
						if ( !(isPixelExistsInArray(iarrPixelLocations, tempRR, tempCC)) ) {
							iarrPixelLocations.add(0, new int[]{tempRR, tempCC}); //add next pixel - in one direction from the original pixel matrix
							
							while ( (iLoop < exitcode) ) {
								iLoop++;
								//out of bounds control
								if ( (nextR >= (sizeX-iBoundaryPixels)) || (nextC >= (sizeY-iBoundaryPixels)) ) break;
								if ( (nextC <= iBoundaryPixels) || (nextR <= iBoundaryPixels) ) break;

								int [][] iarrAdjacentPixelMatrix = getAdjacentPixelMatrix(nextR, nextC);
								int tempC = 0, tempR = 0;

								for (int a = 0; a < 3; a++) { //sub matrices when line grows
									if (a==0) tempR=(nextR - 1);
									if (a==1) tempR=(nextR);
									if (a==2) tempR=(nextR + 1);
									for (int b = 0; b < 3; b++) {
										if (b==0) tempC = (nextC - 1);
										if (b==1) tempC = (nextC);
										if (b==2) tempC = (nextC + 1);

										bPixelFound = false;

										if ( (iarrAdjacentPixelMatrix != null) && (iarrAdjacentPixelMatrix[a][b] == 1) ) {
											if ( !(isPixelExistsInArray(iarrPixelLocations, tempR, tempC)) ) {//if not a mid pixel or already processed pixel
												if (iarrLineSegments.size() <= 0)
													iarrPixelLocations.add(new int[]{tempR, tempC}); //if the first line segment, append at the end
												else 
													iarrPixelLocations.add(0, new int[]{tempR, tempC}); //else append at the beginning as an extended line
													
												pixelsonline = getPixelsOfTheIdealLineBetweenTwoPoints(iarrPixelLocations.get(0), iarrPixelLocations.get(iarrPixelLocations.size()-1));
												dNoOfPixels = calculateAreaBetweenLinesAndGetThreshold(iarrPixelLocations, pixelsonline);
												dNoOfPixelsThreshold = (dNoOfPixels / pixelsonline.size());

												//if (iarrPixelLocations.size() > 20)
												//System.out.println("20");

												if (dNoOfPixelsThreshold <= 1.0) {
													bPixelFound = true;
													break;
												}
												else 
													iLoop = exitcode;//exit from the while loop 
											}
										}

									}
									if (bPixelFound) {
										nextR = tempR; nextC = tempC; //next
										break;
									}
								}
							}
							iarrLineSegments.addAll(iarrPixelLocations);
						}
					}
				}
			}
			
			/// print the lines into the new image
			int [] firstpixelxy= iarrPixelLocations.get(0);
			int [] lastpixelxy = iarrPixelLocations.get(iarrPixelLocations.size()-1); //get the last entry

			ArrayList<int[]> iarrPixelArrayOfTheBestLine = getPixelsOfTheIdealLineBetweenTwoPoints(firstpixelxy, lastpixelxy);

			
			//graph.setColor(Color.GREEN);
			//graph.drawLine(firstpixelxy[0],firstpixelxy[1],lastpixelxy[0],lastpixelxy[1]);
			//System.out.println("Line size: "+iarrPixelArrayOfTheBestLine.size());
			
			
			
			if (iarrPixelArrayOfTheBestLine.size() >= currentLengthThresholdValue){ //get if the line segment is at least 20 pixels longer
				//add to the relational graph
				//makeRelationalGraph.addNewLine(firstpixelxy[0],firstpixelxy[1],lastpixelxy[0],lastpixelxy[1], dNoOfPixelsThreshold);
				
//				System.out.println("First and last pixels: ");
//				System.out.println(firstpixelxy[0]+"  "+firstpixelxy[1]);
//				System.out.println(lastpixelxy[0]+"  "+lastpixelxy[1]);
				
				result[0][0]=firstpixelxy[0];
				result[0][1]=firstpixelxy[1];
				result[1][0]=lastpixelxy[0];
				result[1][1]=lastpixelxy[1];
				
				graph.setColor(Color.GREEN);
				
				//graph.setColor(new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat()));

				for (int i = 1; i < iarrPixelArrayOfTheBestLine.size(); i++) {
					int [] pixelxy = iarrPixelArrayOfTheBestLine.get(i);
					graph.drawLine(pixelxy[0],pixelxy[1],pixelxy[0],pixelxy[1]);
				}
				
				//clear all processed pixels in this loop
				for (int i = 0; i < iarrPixelLocations.size(); i++) {
					int [] pixelxy = iarrPixelLocations.get(i);
					iarrImageColors[pixelxy[0]][pixelxy[1]] = 0;
				}
				
			}
			//graph.drawLine(r,c,nextR,nextC);
			//graph.dispose();
			//ProcessImages.displayImage(outputImage,"Lines Detected");
			//ImageIO.write(outputImage, "PNG", new File(sPixelBasedLineDetectionImagePath));
			return result;
		
		
	}
	
	
	
}
