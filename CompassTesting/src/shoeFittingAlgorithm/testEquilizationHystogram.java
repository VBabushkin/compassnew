package shoeFittingAlgorithm;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class testEquilizationHystogram {
	
	static String initialImagePath=".//CollectedShoePictures//";
	
	public  static void main(String args[]){
		int userID=2;
		int angle=135;
		
		String filename= "shoes_"+userID+"_"+angle;
		String backgroundFileName="background"+userID;
		String initPath=initialImagePath+filename+".jpg";
		String bckgImagePath=initialImagePath+backgroundFileName+".jpg";
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		//read image
		Mat source = Highgui.imread(initPath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//read the background image
		Mat bckg = Highgui.imread(bckgImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//to store image with removed background
		Mat backgroundRemoved = new Mat();
				
		//REMOVE BACKGROUND
		org.opencv.core.Core.subtract(source.clone(), bckg, backgroundRemoved);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(source),"Original without removing background");
		
		Mat onlyShoes=new Mat();
		//remove black color of background 
		//TODO: needs to be parametrized and checked for different gray background color ranges
		org.opencv.core.Core.inRange(backgroundRemoved, new Scalar(0, 0, 0), new Scalar(14, 14, 14), onlyShoes);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoes),"Shoes only");
		
	
		
		
		Mat imgH =  new Mat();
		backgroundRemoved.convertTo(imgH, -1, 5, 0);//increase the contrast (double)
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgH),"High contrast");
		
		Mat imgL=new Mat();
		backgroundRemoved.convertTo(imgL, -1, 0.5, 0); //decrease the contrast (halve)
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgL),"Low contrast");
		
		//laplasian gradient for high contrast shoe:
		Mat lapImg = new Mat();
		Imgproc.Laplacian(imgH, lapImg, org.bytedeco.javacpp.opencv_core.CV_8UC1, 5,0.5,0.5);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(lapImg),"Laplasian");
		
		
		
		Imgproc.cvtColor(backgroundRemoved,backgroundRemoved,Imgproc.COLOR_BGR2GRAY);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved),"before equalization");

		//source.convertTo(source, org.bytedeco.javacpp.opencv_core.CV_8UC1);
		Mat dst = new Mat();
		//Imgproc.cvtColor(backgroundRemoved,backgroundRemoved,Imgproc.COLOR_BGR2GRAY);
		
		Imgproc.equalizeHist(backgroundRemoved, dst);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(dst),"after equalization");		
		
		//Imgproc.erode(dst, dst, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2,2)));
		Imgproc.morphologyEx(dst, dst, Imgproc.MORPH_CLOSE,Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(dst),"after erosion and dilation");	
		
		Mat onlyShoes1=new Mat();
		//remove black color of background 
		org.opencv.core.Core.inRange(dst, new Scalar(0, 0, 0), new Scalar(128, 128, 128), onlyShoes1);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoes1),"Shoes only another representation");

//		dst.convertTo(imgL, -1, 0.5, 0); //decrease the contrast (halve)
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgL),"Low contrast");
	}
}
