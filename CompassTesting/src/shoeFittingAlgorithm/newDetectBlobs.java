package shoeFittingAlgorithm;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;
import blobDetection.Blob;
import blobDetection.BlobDetection;

public class newDetectBlobs {
	
	private static int iShoesDetected = -1;
	private static BlobDetection theBlobDetection;
	private static final int iExtraSpce = 40; //20 pixels border
	public static boolean bExtreamOn     = false;//don't know what it means yet
	
	//Constructor of the class. Paramaters are the dimensions of the image on which the detection is going to be applied.	
	public static void detectBlobs(BufferedImage imgBuffer, float threshold) {
			int width = imgBuffer.getWidth(null);
			int height = imgBuffer.getHeight(null);
			
			int[] pixels = new int[width*height];
			
			//Returns an array of integer pixels in the default RGB color model
			imgBuffer.getRGB(0, 0, width, height, pixels, 0, width);
			theBlobDetection = new BlobDetection(imgBuffer.getWidth(), imgBuffer.getHeight());
				
			//If passed boolean value is true, the blob detection will attempt to find bright areas. 
			//Otherwise, it will detect dark areas (default mode). 
			//This setting is very useful when you decide to compute polygons for each blob.
			theBlobDetection.setPosDiscrimination(false);
			
			//BlobDetection analyzes image for finding dark or bright areas, depending on the selected mode. 
			//The threshold is a float number between 0.0f and 1.0f used by the blobDetection to find blobs which contains pixels 
			//whose luminosity is inferior (for dark areas) or superior (for bright areas) to this value.
			theBlobDetection.setThreshold(threshold); //0.28f
			
			//Compute the blobs in the image.
			theBlobDetection.computeBlobs(pixels);
			
			//A call to this function will tell the BlobDetection instance to store polygon informations of each detected blob. 
			//By default, polygons are not computed.
			theBlobDetection.computeTriangles();
			
			//Returns the numbers of blobs detected in an image.
			int nblobs = theBlobDetection.getBlobNb();
			
			//System.out.println("Number of detected blobs: "+nblobs);
	}
	
	public int isShoesDetected() {
		return iShoesDetected;
	}
	
	
	/**
	 * detect the shoe blobs and extract shoes only region from the image and save it
	 * (put all the blobs larger than 100 x 200 into an array in descending order of their heights)
	 * @param croppedImagePath cropped original image of the shoes
	 * @param outlineDetectedImagePath outline detected image with sobel edge detector
	 * @param shoesOnlyImagePath shoes only image to be saved
	 * @throws IOException
	 */
	public static int[] extractShoesOnlyRegion(BufferedImage imgOriginalCroppedBuffer, BufferedImage imgEdgedBuffer, BufferedImage resultImage) throws IOException {

		int iOrientationOfShoes = -1; //0 = horizontal, 1 = vertical (shoes are pointing towards display panel)
		int hBlobs = 0, vBlobs = 0;
		
		//BufferedImage imgOriginalCroppedBuffer = ImageIO.read(new File(croppedImagePath));
		//BufferedImage imgEdgedBuffer = ImageIO.read(new File(outlineDetectedImagePath));
		//detect blobs
		detectBlobs(imgEdgedBuffer, 0.9f); 
		
		Blob b;
		int[][] iBlobInfo = new int[4][theBlobDetection.getBlobNb()/4]; // nblob rows and four columns (x y w h) 
		int blobIndex = 0;

		int bcount = theBlobDetection.getBlobNb();
		
		//save shoe blobs for testing
		BufferedImage shoeBlobsImage = new BufferedImage(imgOriginalCroppedBuffer.getWidth(), imgOriginalCroppedBuffer.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics2D graph = shoeBlobsImage.createGraphics();
		graph.setColor(Color.blue);

		//put all the blobs larger than 100 x 200 into an array in descending order of their heights
		for (int n=0 ; n < theBlobDetection.getBlobNb() ; n++) {
			b=theBlobDetection.getBlob(n);
			if (b!=null)
			{
					int x = Math.round(b.xMin*600); int y = Math.round(b.yMin*450);
					int w = Math.round(b.w*600);    int h = Math.round(b.h*450);
					
					if ( (h > 200) && (w > 100) && (h > w) ) { // to detect union of all blobs
						graph.fillRect(x, y, w, h);
					}
					
					if ( (h > 200) && (w > 100) && (h > w) ) { // to detect vertical orientation
						vBlobs++;
					}
					if ( (w > 200) && (h > 100) && (w > h) ) { // to detect horizontal orientation
						hBlobs++;
					}
					
					if ( ( (h > 200) && (w > 100) ) || ( (w > 200) && (h > 100) ) ) {
						if (blobIndex == 0) {
							iBlobInfo[0][blobIndex] = x;
							iBlobInfo[1][blobIndex] = y;
							iBlobInfo[2][blobIndex] = w;
							iBlobInfo[3][blobIndex] = h;
						}
						if (blobIndex > 0) {
							int i = blobIndex - 1;
							if (iBlobInfo[3][i] <= h) {
								for (int j = i; j >= 0; j--) {
									if (iBlobInfo[3][j] >= h) 
									{
										for (int k = i; k > j; k--) {
											iBlobInfo[0][(k+1)] = iBlobInfo[0][k];
											iBlobInfo[1][(k+1)] = iBlobInfo[1][k];
											iBlobInfo[2][(k+1)] = iBlobInfo[2][k];
											iBlobInfo[3][(k+1)] = iBlobInfo[3][k]; 
										}
										iBlobInfo[0][j+1] = x;
										iBlobInfo[1][j+1] = y;
										iBlobInfo[2][j+1] = w;
										iBlobInfo[3][j+1] = h;
										break;
									}
									if (iBlobInfo[3][j] < h) 
									{
										for (int k = i; k >= j; k--) {
											iBlobInfo[0][(k+1)] = iBlobInfo[0][k];
											iBlobInfo[1][(k+1)] = iBlobInfo[1][k];
											iBlobInfo[2][(k+1)] = iBlobInfo[2][k];
											iBlobInfo[3][(k+1)] = iBlobInfo[3][k]; 
										}
										iBlobInfo[0][j] = x;
										iBlobInfo[1][j] = y;
										iBlobInfo[2][j] = w;
										iBlobInfo[3][j] = h;
										break;
									}
								}
							}
							else {
								iBlobInfo[0][blobIndex] = x;
								iBlobInfo[1][blobIndex] = y;
								iBlobInfo[2][blobIndex] = w;
								iBlobInfo[3][blobIndex] = h;
							}
						}
						
						blobIndex++;
					}
			}
		}
		
		if ( (theBlobDetection.getBlobNb() > 800) && (bExtreamOn) ) {
			iShoesDetected = 2;
			return null;
		}
		//no shoes detected - return
		if (blobIndex < 1) {
			iShoesDetected = 0;
			
			imgOriginalCroppedBuffer.flush();
			imgEdgedBuffer.flush();
			
			imgOriginalCroppedBuffer = null;
			imgEdgedBuffer           = null;
			
			return null;
		}
		
		//save both shoe area
		
		//detect min x y
		int [] xvals = iBlobInfo[0];
		int [] yvals = iBlobInfo[1];
		Arrays.sort(xvals, 0, blobIndex);
		Arrays.sort(yvals, 0, blobIndex);
		int minx = xvals[0];
		int miny = yvals[0];
		
		//detect max x
		int [] xwidths = new int[blobIndex];
		for (int i=0; i < blobIndex; i++) {
			xwidths[i] = iBlobInfo[0][i] + iBlobInfo[2][i];
		}
		Arrays.sort(xwidths, 0, blobIndex);
		int maxx = xwidths[blobIndex-1];
		
		//detect max y
		int [] yheights = new int[blobIndex];
		for (int i=0; i < blobIndex; i++) {
			yheights[i] = iBlobInfo[1][i] + iBlobInfo[3][i];
		}
		Arrays.sort(yheights, 0, blobIndex);
		int maxy = yheights[blobIndex-1];
		
		System.out.println("Shoes only region - min x: " + minx + "min y: " + miny + " max x: "+ maxx + "max y: " + maxy);
		
		//prepare dimensions: 20 pixel spare space around the shoes
		minx = minx - iExtraSpce;
		miny = miny - iExtraSpce;
		int width = (maxx-minx)  + iExtraSpce;
		int height = (maxy-miny) + iExtraSpce;
		
		//to compensate over/under shooting
		if (minx < 0) minx = 0;
		if (miny < 0) miny = 0;
		
		if ( ( (maxx + iExtraSpce) > imgOriginalCroppedBuffer.getWidth() ) || (width  > imgOriginalCroppedBuffer.getWidth()) )
			width = imgOriginalCroppedBuffer.getWidth() - minx;
		
		if ( ( (maxy + iExtraSpce) > imgOriginalCroppedBuffer.getHeight() ) || (height > imgOriginalCroppedBuffer.getHeight()) )
			height = imgOriginalCroppedBuffer.getHeight() - miny;
		
		//save both shoes
		BufferedImage bBothShoes = imgOriginalCroppedBuffer.getSubimage(minx, miny, width, height);
		//ImageIO.write(bBothShoes, "PNG", new File(shoesOnlyImagePath)); 
		//resultImage=bBothShoes.getSubimage(0,0,bBothShoes.getWidth(), bBothShoes.getHeight());
		ProcessImages.displayImage(bBothShoes,"Both Shoes Only Region");
		
		iShoesDetected = 1;
		
		//orientation
		if (hBlobs >= vBlobs) iOrientationOfShoes = 0;
		if (hBlobs < vBlobs)  iOrientationOfShoes = 1;
		
		//save shoe blobs
		graph.dispose();
		//ImageIO.write(shoeBlobsImage, "PNG", new File(Properties.sUnionOfShoeBlobs)); 
		resultImage=shoeBlobsImage.getSubimage(0,0,shoeBlobsImage.getWidth(), shoeBlobsImage.getHeight());
		ProcessImages.displayImage(shoeBlobsImage,"Shoes Only Region");	
		return new int[]{minx, miny, width, height, iOrientationOfShoes};
		
	}
	
	
	
	
	
	
	/**
	 * detect the shoe blobs and extract shoes only region from the image and save it
	 * (put all the blobs larger than 100 x 200 into an array in descending order of their heights)
	 * @param croppedImagePath cropped original image of the shoes
	 * @param outlineDetectedImagePath outline detected image with sobel edge detector
	 * @param shoesOnlyImagePath shoes only image to be saved
	 * @throws IOException
	 */
	public static int[] extractShoesOnlyRegion(BufferedImage imgEdgedBuffer) throws IOException {

		int iOrientationOfShoes = -1; //0 = horizontal, 1 = vertical (shoes are pointing towards display panel)
		int hBlobs = 0, vBlobs = 0;
		
		//BufferedImage imgOriginalCroppedBuffer = ImageIO.read(new File(croppedImagePath));
		//BufferedImage imgEdgedBuffer = ImageIO.read(new File(outlineDetectedImagePath));
		//detect blobs
		detectBlobs(imgEdgedBuffer, 0.9f); 
		
		Blob b;
		int[][] iBlobInfo = new int[4][theBlobDetection.getBlobNb()/4]; // nblob rows and four columns (x y w h) 
		int blobIndex = 0;

		int bcount = theBlobDetection.getBlobNb();
		
		//save shoe blobs for testing
		//BufferedImage shoeBlobsImage = new BufferedImage(imgOriginalCroppedBuffer.getWidth(), imgOriginalCroppedBuffer.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics2D graph = imgEdgedBuffer.createGraphics();
		graph.setColor(Color.blue);

		//put all the blobs larger than 100 x 200 into an array in descending order of their heights
		for (int n=0 ; n < theBlobDetection.getBlobNb() ; n++) {
			b=theBlobDetection.getBlob(n);
			if (b!=null)
			{
					int x = Math.round(b.xMin*600); int y = Math.round(b.yMin*450);
					int w = Math.round(b.w*600);    int h = Math.round(b.h*450);
					
					if ( (h > 200) && (w > 100) && (h > w) ) { // to detect union of all blobs
						//graph.fillRect(x, y, w, h);
					}
					
					if ( (h > 200) && (w > 100) && (h > w) ) { // to detect vertical orientation
						vBlobs++;
					}
					if ( (w > 200) && (h > 100) && (w > h) ) { // to detect horizontal orientation
						hBlobs++;
					}
					
					if ( ( (h > 200) && (w > 100) ) || ( (w > 200) && (h > 100) ) ) {
						if (blobIndex == 0) {
							iBlobInfo[0][blobIndex] = x;
							iBlobInfo[1][blobIndex] = y;
							iBlobInfo[2][blobIndex] = w;
							iBlobInfo[3][blobIndex] = h;
						}
						if (blobIndex > 0) {
							int i = blobIndex - 1;
							if (iBlobInfo[3][i] <= h) {
								for (int j = i; j >= 0; j--) {
									if (iBlobInfo[3][j] >= h) 
									{
										for (int k = i; k > j; k--) {
											iBlobInfo[0][(k+1)] = iBlobInfo[0][k];
											iBlobInfo[1][(k+1)] = iBlobInfo[1][k];
											iBlobInfo[2][(k+1)] = iBlobInfo[2][k];
											iBlobInfo[3][(k+1)] = iBlobInfo[3][k]; 
										}
										iBlobInfo[0][j+1] = x;
										iBlobInfo[1][j+1] = y;
										iBlobInfo[2][j+1] = w;
										iBlobInfo[3][j+1] = h;
										break;
									}
									if (iBlobInfo[3][j] < h) 
									{
										for (int k = i; k >= j; k--) {
											iBlobInfo[0][(k+1)] = iBlobInfo[0][k];
											iBlobInfo[1][(k+1)] = iBlobInfo[1][k];
											iBlobInfo[2][(k+1)] = iBlobInfo[2][k];
											iBlobInfo[3][(k+1)] = iBlobInfo[3][k]; 
										}
										iBlobInfo[0][j] = x;
										iBlobInfo[1][j] = y;
										iBlobInfo[2][j] = w;
										iBlobInfo[3][j] = h;
										break;
									}
								}
							}
							else {
								iBlobInfo[0][blobIndex] = x;
								iBlobInfo[1][blobIndex] = y;
								iBlobInfo[2][blobIndex] = w;
								iBlobInfo[3][blobIndex] = h;
							}
						}
						
						blobIndex++;
					}
			}
		}
		
		if ( (theBlobDetection.getBlobNb() > 800) && (bExtreamOn) ) {
			iShoesDetected = 2;
			return null;
		}
		//no shoes detected - return
		if (blobIndex < 1) {
			iShoesDetected = 0;
			
			//imgOriginalCroppedBuffer.flush();
			imgEdgedBuffer.flush();
			
			//imgOriginalCroppedBuffer = null;
			imgEdgedBuffer           = null;
			
			return null;
		}
		
		//save both shoe area
		
		//detect min x y
		int [] xvals = iBlobInfo[0];
		int [] yvals = iBlobInfo[1];
		Arrays.sort(xvals, 0, blobIndex);
		Arrays.sort(yvals, 0, blobIndex);
		int minx = xvals[0];
		int miny = yvals[0];
		
		//detect max x
		int [] xwidths = new int[blobIndex];
		for (int i=0; i < blobIndex; i++) {
			xwidths[i] = iBlobInfo[0][i] + iBlobInfo[2][i];
		}
		Arrays.sort(xwidths, 0, blobIndex);
		int maxx = xwidths[blobIndex-1];
		
		//detect max y
		int [] yheights = new int[blobIndex];
		for (int i=0; i < blobIndex; i++) {
			yheights[i] = iBlobInfo[1][i] + iBlobInfo[3][i];
		}
		Arrays.sort(yheights, 0, blobIndex);
		int maxy = yheights[blobIndex-1];
		
		System.out.println("Shoes only region - min x: " + minx + "min y: " + miny + " max x: "+ maxx + "max y: " + maxy);
		
		//prepare dimensions: 20 pixel spare space around the shoes
		minx = minx - iExtraSpce;
		miny = miny - iExtraSpce;
		int width = (maxx-minx)  + iExtraSpce;
		int height = (maxy-miny) + iExtraSpce;
		
		//to compensate over/under shooting
		if (minx < 0) minx = 0;
		if (miny < 0) miny = 0;
		
		if ( ( (maxx + iExtraSpce) > imgEdgedBuffer.getWidth() ) || (width  > imgEdgedBuffer.getWidth()) )
			width = imgEdgedBuffer.getWidth() - minx;
		
		if ( ( (maxy + iExtraSpce) > imgEdgedBuffer.getHeight() ) || (height > imgEdgedBuffer.getHeight()) )
			height = imgEdgedBuffer.getHeight() - miny;
		
		//save both shoes
		BufferedImage bBothShoes = imgEdgedBuffer.getSubimage(minx, miny, width, height);
		//ImageIO.write(bBothShoes, "PNG", new File(shoesOnlyImagePath)); 
		
		iShoesDetected = 1;
		
		//orientation
		if (hBlobs >= vBlobs) iOrientationOfShoes = 0;
		if (hBlobs < vBlobs)  iOrientationOfShoes = 1;
		
		//save shoe blobs
		graph.dispose();
		//ImageIO.write(shoeBlobsImage, "PNG", new File(Properties.sUnionOfShoeBlobs)); 
		imgEdgedBuffer=imgEdgedBuffer.getSubimage(0,0,imgEdgedBuffer.getWidth(), imgEdgedBuffer.getHeight());
		ProcessImages.displayImage(imgEdgedBuffer,"Processed Image with blobs");	
		return new int[]{minx, miny, width, height, iOrientationOfShoes};
		
	}
		
	
	/**
	 * to display all detected blobs
	 * @param img
	 * @param threshold
	 */
	public static void drawAllBlobsDetected(BufferedImage img, float threshold){
		detectBlobs(img, threshold); 
		
		Blob b;
		//EdgeVertex eA, eB;
		int imgW = img.getWidth();
		int imgH = img.getHeight();
		
		BufferedImage outputimg = new BufferedImage(imgW, imgH, BufferedImage.TYPE_INT_RGB);
		Graphics2D graph = outputimg.createGraphics();
		graph.drawImage(img, 0, 0, null);
		graph.setStroke(new BasicStroke(1));
		graph.setColor(Color.RED);
		
		//to verify circles and strips
		VerifyCircle verifyCircle       = new VerifyCircle();
		//VerifyStrip verifyStrip         = new VerifyStrip();
		//VerifyRectangle verifyRectangle = new VerifyRectangle();
		
		for (int n=0 ; n < theBlobDetection.getBlobNb() ; n++) {
			b=theBlobDetection.getBlob(n); 
			if (b!=null)
			{
				// Blobs
				int x = Math.round(b.xMin*imgW); int y = Math.round(b.yMin*imgH);
				int w = Math.round(b.w*imgW);    int h = Math.round(b.h*imgH);
				graph.setColor(Color.RED);
				graph.drawRect(x,y,w,h);
			}
		}
		
		img=outputimg.getSubimage(0,0,outputimg.getWidth(), outputimg.getHeight());
		ProcessImages.displayImage(outputimg,"All Blobs Detected");	
		graph.dispose();
	}
	
	
//		/**
//		 * draw blobs and edged on the image and detect circles
//		 * @param drawBlobs if true draw blobs
//		 * @param drawEdges if true draw edges
//		 * @param img buffered image to process
//		 * @throws IOException 
//		 */
//		public static void detectBlobsAddCirclesToARG(BufferedImage img, float threshold) throws IOException {
//			
//			
//			detectBlobs(img, threshold); 
//			
//			Blob b;
//			//EdgeVertex eA, eB;
//			int imgW = img.getWidth();
//			int imgH = img.getHeight();
//			
//			BufferedImage outputimg = new BufferedImage(imgW, imgH, BufferedImage.TYPE_INT_RGB);
//			Graphics2D graph = outputimg.createGraphics();
//			graph.drawImage(img, 0, 0, null);
//			graph.setStroke(new BasicStroke(1));
//			graph.setColor(Color.RED);
//			
//			//to verify circles and strips
//			VerifyCircle verifyCircle       = new VerifyCircle();
//			VerifyStrip verifyStrip         = new VerifyStrip();
//			VerifyRectangle verifyRectangle = new VerifyRectangle();
//			
//			for (int n=0 ; n < theBlobDetection.getBlobNb() ; n++) {
//				b=theBlobDetection.getBlob(n); 
//				if (b!=null)
//				{
//					// Blobs
//					int x = Math.round(b.xMin*imgW); int y = Math.round(b.yMin*imgH);
//					int w = Math.round(b.w*imgW);    int h = Math.round(b.h*imgH);
//					graph.setColor(Color.RED);
//					
////					System.out.println("STRIP "+ verifyStrip.isStrip(img, "horizontal", x, y, w, h));
////					System.out.println("CIRCLE "+ verifyCircle.isCircle(x,y,w,h,img, graph));
////					System.out.println("HORIZONTAL RECTANGLE "+verifyRectangle.isRectangle(x, y, w, h, img, "horizontal"));
////					System.out.println("VERTICAL RECTANGLE "+verifyRectangle.isRectangle(x, y, w, h, img, "vertical"));
//					
//					if ( (w > 10) && (h > 10) ) {
//						
//						graph.setColor(Color.RED);
//						//graph.drawRect(x,y,w,h);
//						
//						////////////// detect strips ///////////////
//						//horizontal strips
//						if ( (h < w) && ( h < 15) && (w < 100) && (w > 20 ) ) {
//							//ImageIO.write(img.getSubimage(x, y, w, h), "PNG", new File(".\\output\\lines\\" + n + ".PNG"));
//							graph.setColor(Color.RED);
//							if (verifyStrip.isStrip(img, "horizontal", x, y, w, h)) 
//							{
//								graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewStrip(n, x, y, w, h);
//								//System.out.println("hstrip: " + x + "-" + y + "-" + w + "-" + h + " - n: " + n);
//							}
//						}
//						//vertical strips
//						if ( (w < h) && ( w < 15) && (h < 100) && (h > 20 ) ) {
//							//ImageIO.write(img.getSubimage(x, y, w, h), "PNG", new File(".\\output\\lines\\" + n + ".PNG"));
//							graph.setColor(Color.MAGENTA);
//							if (verifyStrip.isStrip(img, "vertical", x, y, w, h)) 
//							{
//								graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewStrip(n, x, y, w, h);
//								//System.out.println("vstrip: " + x + "-" + y + "-" + w + "-" + h + " - n: " + n);
//							}
//						}
//						
//						////////////// detect circles ///////////////
//						if ( (h > 10) && (w > 10) && (Math.abs(w-h) < 10)) {
//							//ImageIO.write(img.getSubimage(x, y, w, h), "PNG", new File(".\\output\\blobs_cc\\" + n + ".PNG"));
//							
//							if (verifyCircle.isCircle(x,y,w,h,img, graph))
//							{
//								graph.setColor(Color.GREEN);
//								//makeRelationalGraphsForBothShoes.addNewCircle(x, y, w, h, verifyCircle.getErrorThreshold());
//								graph.drawRect(x,y,w,h);
//								//System.out.println("square or circle: " + x + "-" + y + "-" + w + "-" + h + " - n: " + n);
//							}
//						}
//						//detect big horizontal rectangles
//						if ( (h < w) && (h > 15) && ( h < 120) && (w < 120) && (Math.abs(w-h) > 15)) {
//							graph.setColor(Color.BLUE);
//							if (verifyRectangle.isRectangle(x, y, w, h, img, "horizontal")) {
//								graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewRectangle(n, x, y, w, h);
//								//System.out.println("big horizontal rectangle: " + x + "-" + y + "-" + w + "-" + h);
//							}
//						}
//						//detect big vertical rectangles
//						if ( (h > w) && (w > 15) && ( h < 120) && (w < 120) && (Math.abs(h-w) > 15)) {
//							graph.setColor(Color.YELLOW);
//							if (verifyRectangle.isRectangle(x, y, w, h, img, "vertical")) {
//								graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewRectangle(n, x, y, w, h);
//								//System.out.println("big horizontal rectangle: " + x + "-" + y + "-" + w + "-" + h);
//							}
//						}
//					}
//				}
//			}
//			
//			
//			img=outputimg.getSubimage(0,0,outputimg.getWidth(), outputimg.getHeight());
//			ProcessImages.displayImage(outputimg,"Processed Image with blobs");		
//			graph.dispose();
//		}
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//
		//
		//   NEW VERSION FOR RECTANGLES
		//
		//
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		/**
		 * draw blobs and edged on the image and detect circles
		 * @param drawBlobs if true draw blobs
		 * @param drawEdges if true draw edges
		 * @param threshold -- The threshold is a float number between 0.0f and 1.0f used by the blobDetection to find blobs which contains pixels whose luminosity is inferior (for dark areas) or superior (for bright areas) to this value.
		 * @param img buffered image to process
		 * @throws IOException 
		 */
		public  static ArrayList<Rectangle> detectBlobsAddCirclesToARG1(BufferedImage img, float threshold,double dLineThresholdValue, double dBadPixelThresholdValue, int heightThresholdValue, int widthThresholdValue, int differenceValue) throws IOException {
			
			
			detectBlobs(img, threshold); 
			
			Blob b;
			//EdgeVertex eA, eB;
			int imgW = img.getWidth();
			int imgH = img.getHeight();
			
			BufferedImage outputimg = new BufferedImage(imgW, imgH, BufferedImage.TYPE_INT_RGB);
			Graphics2D graph = outputimg.createGraphics();
			graph.drawImage(img, 0, 0, null);
			graph.setStroke(new BasicStroke(1));
			graph.setColor(Color.RED);
			
			//to verify circles and strips
			VerifyCircle verifyCircle       = new VerifyCircle();
			//VerifyStrip verifyStrip         = new VerifyStrip();
			//VerifyRectangle verifyRectangle = new VerifyRectangle();
			
			//list of point coordinates
			ArrayList<Rectangle>result =new ArrayList<Rectangle>();
			
			for (int n=0 ; n < theBlobDetection.getBlobNb() ; n++) {
				b=theBlobDetection.getBlob(n); 
				if (b!=null)
				{
					// Blobs
					int x = Math.round(b.xMin*imgW); int y = Math.round(b.yMin*imgH);
					int w = Math.round(b.w*imgW);    int h = Math.round(b.h*imgH);
					graph.setColor(Color.RED);
					
					if ( (w > widthThresholdValue) && (h > heightThresholdValue) ) {
						
						graph.setColor(Color.RED);
						//graph.drawRect(x,y,w,h);
						
						////////////// detect strips ///////////////
						//horizontal strips
						if ( (h < w) && ( h < 15) && (w < 100) && (w > 20 ) ) {
							//ImageIO.write(img.getSubimage(x, y, w, h), "PNG", new File(".\\output\\lines\\" + n + ".PNG"));
							graph.setColor(Color.RED);
//							if (verifyStrip.isStrip(img, "horizontal", x, y, w, h)) 
//							{
//								//graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewStrip(n, x, y, w, h);
//								//System.out.println("hstrip: " + x + "-" + y + "-" + w + "-" + h + " - n: " + n);
//							}
						}
						//vertical strips
						if ( (w < h) && ( w < 15) && (h < 100) && (h > 20 ) ) {
							//ImageIO.write(img.getSubimage(x, y, w, h), "PNG", new File(".\\output\\lines\\" + n + ".PNG"));
							graph.setColor(Color.MAGENTA);
//							if (verifyStrip.isStrip(img, "vertical", x, y, w, h)) 
//							{
//								//graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewStrip(n, x, y, w, h);
//								//System.out.println("vstrip: " + x + "-" + y + "-" + w + "-" + h + " - n: " + n);
//							}
						}
						
						////////////// detect circles ///////////////
						if ( (h > heightThresholdValue) && (w > widthThresholdValue) && (Math.abs(w-h) < differenceValue)) {
							//ImageIO.write(img.getSubimage(x, y, w, h), "PNG", new File(".\\output\\blobs_cc\\" + n + ".PNG"));
							//TODO: CNAHGE whichShoe -- NO NEED TO HAVE IT
							if (verifyCircle.isCircle(x,y,w,h,img, graph, dLineThresholdValue, dBadPixelThresholdValue))
							{
								graph.setColor(Color.GREEN);
								
								result.add(new Rectangle(x,y,w,h));
								//makeRelationalGraphsForBothShoes.addNewCircle(x, y, w, h, verifyCircle.getErrorThreshold());
								graph.drawRect(x,y,w,h);
								
								//System.out.println("square or circle: " + x + "-" + y + "-" + w + "-" + h + " - n: " + n);
							}
						}
						//detect big horizontal rectangles
						if ( (h < w) && (h > 15) && ( h < 120) && (w < 120) && (Math.abs(w-h) > 15)) {
							graph.setColor(Color.BLUE);
//							if (verifyRectangle.isRectangle(x, y, w, h, img, "horizontal")) {
//								//graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewRectangle(n, x, y, w, h);
//								//System.out.println("big horizontal rectangle: " + x + "-" + y + "-" + w + "-" + h);
//							}
						}
						//detect big vertical rectangles
						if ( (h > w) && (w > 15) && ( h < 120) && (w < 120) && (Math.abs(h-w) > 15)) {
							graph.setColor(Color.YELLOW);
//							if (verifyRectangle.isRectangle(x, y, w, h, img, "vertical")) {
//								//graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewRectangle(n, x, y, w, h);
//								//System.out.println("big horizontal rectangle: " + x + "-" + y + "-" + w + "-" + h);
//							}
						}
					}
				}
			}
			
			graph.dispose();
			img=outputimg.getSubimage(0,0,outputimg.getWidth(), outputimg.getHeight());
			//ProcessImages.displayImage(outputimg,"Processed Image with blobs");		
			return result;
		}
		
		
//		/**
//		 * draw blobs and edged on the image and detect circles
//		 * @param drawBlobs if true draw blobs
//		 * @param drawEdges if true draw edges
//		 * @param img buffered image to process
//		 * @throws IOException 
//		 */
//		public  static ArrayList<Rectangle> detectBlobsAddCirclesToARG1(BufferedImage img, float threshold) throws IOException {
//			
//			
//			detectBlobs(img, threshold); 
//			
//			Blob b;
//			//EdgeVertex eA, eB;
//			int imgW = img.getWidth();
//			int imgH = img.getHeight();
//			
//			BufferedImage outputimg = new BufferedImage(imgW, imgH, BufferedImage.TYPE_INT_RGB);
//			Graphics2D graph = outputimg.createGraphics();
//			graph.drawImage(img, 0, 0, null);
//			graph.setStroke(new BasicStroke(1));
//			graph.setColor(Color.RED);
//			
//			//to verify circles and strips
//			VerifyCircle verifyCircle       = new VerifyCircle();
//			VerifyStrip verifyStrip         = new VerifyStrip();
//			VerifyRectangle verifyRectangle = new VerifyRectangle();
//			
//			//list of point coordinates
//			ArrayList<Rectangle>result =new ArrayList<Rectangle>();
//			
//			for (int n=0 ; n < theBlobDetection.getBlobNb() ; n++) {
//				b=theBlobDetection.getBlob(n); 
//				if (b!=null)
//				{
//					// Blobs
//					int x = Math.round(b.xMin*imgW); int y = Math.round(b.yMin*imgH);
//					int w = Math.round(b.w*imgW);    int h = Math.round(b.h*imgH);
//					graph.setColor(Color.RED);
//					
//					if ( (w > 10) && (h > 10) ) {
//						
//						graph.setColor(Color.RED);
//						//graph.drawRect(x,y,w,h);
//						
//						////////////// detect strips ///////////////
//						//horizontal strips
//						if ( (h < w) && ( h < 15) && (w < 100) && (w > 20 ) ) {
//							//ImageIO.write(img.getSubimage(x, y, w, h), "PNG", new File(".\\output\\lines\\" + n + ".PNG"));
//							graph.setColor(Color.RED);
//							if (verifyStrip.isStrip(img, "horizontal", x, y, w, h)) 
//							{
//								//graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewStrip(n, x, y, w, h);
//								//System.out.println("hstrip: " + x + "-" + y + "-" + w + "-" + h + " - n: " + n);
//							}
//						}
//						//vertical strips
//						if ( (w < h) && ( w < 15) && (h < 100) && (h > 20 ) ) {
//							//ImageIO.write(img.getSubimage(x, y, w, h), "PNG", new File(".\\output\\lines\\" + n + ".PNG"));
//							graph.setColor(Color.MAGENTA);
//							if (verifyStrip.isStrip(img, "vertical", x, y, w, h)) 
//							{
//								//graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewStrip(n, x, y, w, h);
//								//System.out.println("vstrip: " + x + "-" + y + "-" + w + "-" + h + " - n: " + n);
//							}
//						}
//						
//						////////////// detect circles ///////////////
//						if ( (h > 10) && (w > 10) && (Math.abs(w-h) < 10)) {
//							//ImageIO.write(img.getSubimage(x, y, w, h), "PNG", new File(".\\output\\blobs_cc\\" + n + ".PNG"));
//							//TODO: CNAHGE whichShoe -- NO NEED TO HAVE IT
//							if (verifyCircle.isCircle(x,y,w,h,img, graph))
//							{
//								graph.setColor(Color.GREEN);
//								
//								result.add(new Rectangle(x,y,w,h));
//								//makeRelationalGraphsForBothShoes.addNewCircle(x, y, w, h, verifyCircle.getErrorThreshold());
//								graph.drawRect(x,y,w,h);
//								//System.out.println("square or circle: " + x + "-" + y + "-" + w + "-" + h + " - n: " + n);
//							}
//						}
//						//detect big horizontal rectangles
//						if ( (h < w) && (h > 15) && ( h < 120) && (w < 120) && (Math.abs(w-h) > 15)) {
//							graph.setColor(Color.BLUE);
//							if (verifyRectangle.isRectangle(x, y, w, h, img, "horizontal")) {
//								//graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewRectangle(n, x, y, w, h);
//								//System.out.println("big horizontal rectangle: " + x + "-" + y + "-" + w + "-" + h);
//							}
//						}
//						//detect big vertical rectangles
//						if ( (h > w) && (w > 15) && ( h < 120) && (w < 120) && (Math.abs(h-w) > 15)) {
//							graph.setColor(Color.YELLOW);
//							if (verifyRectangle.isRectangle(x, y, w, h, img, "vertical")) {
//								//graph.drawRect(x,y,w,h);
//								//makeRelationalGraph.addNewRectangle(n, x, y, w, h);
//								//System.out.println("big horizontal rectangle: " + x + "-" + y + "-" + w + "-" + h);
//							}
//						}
//					}
//				}
//			}
//			
//			//graph.dispose();
//			img=outputimg.getSubimage(0,0,outputimg.getWidth(), outputimg.getHeight());
//			ProcessImages.displayImage(outputimg,"Processed Image with blobs");		
//			return result;
//		}
	
		
		
		
		/**
		 * get the rotated image, detect blobs, get width
		 * @param edgeDetectedImagePath
		 * @param threshold
		 * @return
		 * @throws IOException
		 */
		public static int[] detectBlobsAndShoeSize(BufferedImage img, float threshold) throws IOException {

			detectBlobs(img,threshold);//0.9f 
			
			Blob b;
			
			int imgW = img.getWidth();
			int imgH = img.getHeight();
			
			////// shoe size ////////
			int[] minx = new int[2];
			int[] miny = new int[2];
			int[] maxx = new int[2];
			int[] maxy = new int[2];
			boolean bFirstTime = true;

			BufferedImage outputimg = new BufferedImage(imgW, imgH, BufferedImage.TYPE_INT_RGB);
			Graphics2D graph = outputimg.createGraphics();
			graph.drawImage(img, 0, 0, null);
			graph.setStroke(new BasicStroke(1));
			graph.setColor(Color.RED);
			
			for (int n=0 ; n < theBlobDetection.getBlobNb() ; n++) {
				b=theBlobDetection.getBlob(n); 
				if (b!=null)
				{
					// Blobs
					int x = Math.round(b.xMin*imgW); int y = Math.round(b.yMin*imgH);
					int w = Math.round(b.w*imgW);    int h = Math.round(b.h*imgH);

					if ( (w > 10) && (h > 10) ) {
						
						if (bFirstTime) {
							minx[0] = x;   minx[1] = y;
							miny[1] = y;   miny[0] = x;
							maxx[0] = x+w; maxx[1] = y;
							maxy[1] = y+h; maxy[0] = x;
						}
						
						bFirstTime = false; 
						
						if (minx[0] > x)     minx[0] = x;   minx[1] = y;
						if (miny[1] > y)     miny[1] = y;   miny[0] = x;
						if (maxx[0] < (x+w)) maxx[0] = x+w; maxx[1] = y;
						if (maxy[1] < (y+h)) maxy[1] = y+h; maxy[0] = x;
						
					}
					
					
				}
			}
			graph.setColor(Color.RED);
			graph.drawLine(minx[0], minx[1], maxx[0], maxx[1]);
			graph.drawLine(miny[0], miny[1], maxy[0], maxy[1]);
			
			int iShoeWidth  = getPixelsOfTheIdealLineBetweenTwoPoints(minx, maxx);
			int iShoeHeight = getPixelsOfTheIdealLineBetweenTwoPoints(miny, maxy);
			
//			graph.dispose();
//			
//			try {
//				ImageIO.write(outputimg, "PNG", new File(".\\output\\shoesize\\"+ outputName +"_blobs.png"));
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
			
			img=outputimg.getSubimage(0,0,outputimg.getWidth(), outputimg.getHeight());
			ProcessImages.displayImage(outputimg,"Processed Image with DETECTED blobs");	
			ImageIO.write(outputimg, "PNG", new File(".//results//now2.jpg"));
			return new int[]{minx[0],miny[1],iShoeWidth, iShoeHeight};
			
		}
		
		
		/**
		 * get the pixel locations between two pixels
		 * @param startpixelxy start point of the first pixel
		 * @param endpixelxy end point of the second pixel
		 * @return pixel array between two points
		 */
		private static int getPixelsOfTheIdealLineBetweenTwoPoints(int startpixelxy[], int endpixelxy[]) {
			//Bresenham Algorithm
			int iPixels = 0;
			int x  = startpixelxy[0],  y  = startpixelxy[1];
			int x2 = endpixelxy[0],    y2 = endpixelxy[1];
			
			int w = x2 - x;
		    int h = y2 - y;
		    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
		    if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
		    if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
		    if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
		    int longest = Math.abs(w);
		    int shortest = Math.abs(h);
		    
		    if (!(longest > shortest)) {
		        longest = Math.abs(h);
		        shortest = Math.abs(w) ;
		        if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
		        dx2 = 0;            
		    }
		    
		    int numerator = longest >> 1;
			
		    for (int i = 0; i <= longest; i++) {
		    	iPixels++;
		        numerator += shortest;
		        if (!(numerator < longest)) {
		            numerator -= longest;
		            x += dx1;
		            y += dy1;
		        } else {
		            x += dx2;
		            y += dy2;
		        }
		    }
		    
			return iPixels;
		}

		
}
