package shoeFittingAlgorithm;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

public class testCompareShoeContours {
	
	final static int LOWER_CONTOUR_AREA_THRESHOLD=10000;
	final static int UPPER_CONTOUR_AREA_THRESHOLD=150000;
	final static int SHOE_SIZE_AREA_DIFFERENCE=14000;
	final static double UPPER_RATIO_VALUE=2.8;
	final static double LOWER_RATIO_VALUE=2.1;
	final static int EDGE_LENGTH=20;
	final static int DELTA=40; //80 pixels square around the center of gravity
	
	static String initialImagePath=".//CollectedShoePictures//";//".//results//in.jpg";
	static String processedImagesPath=".//CollectedShoePictures//Processed//";
	static String resultImagesPath=".//CollectedShoePictures//SizeExtracted//";
	static String bckgImage =".//CollectedShoePictures//background.jpg";//".//results//bckg.jpg";
	
	
	public static void main(String args[]) throws IOException, ClassNotFoundException, SQLException{
		
		List<shoesInfo2> shoesOutlineContours = new ArrayList<shoesInfo2>();
		
		//iterate over all shoes, determine orientation, left or right, rotate and center correspondingly
				//and store shoe outline images in storedProcessedImages list
//				
//				for(int userID=1;userID<=22;userID++){
//					for(int angle=0;angle<=315;angle+=45){
						
						int userID=8;
						int angle=45;
		
						String filename= "shoes_"+userID+"_"+angle;
						//String filename="randomAngle_"+userID;
						//String filename= "shoes_"+userID+"_half_"+3;
						String backgroundFileName="background"+userID;
						String initPath=initialImagePath+filename+".jpg";
						String bckgImagePath=initialImagePath+backgroundFileName+".jpg";
						
						//Split camera image by two
						ArrayList<Mat>correctCroppedImages=ExtractShoeContours.extractCorrectlyCroppedImages(initPath, bckgImagePath);
						if(correctCroppedImages.size()==0){
							System.out.println("NOTHING HAS BEEN DETECTED");
						}
						else{
							for(int index=0;index<2;index++){//ASSUMING ONLY 2 CONTOURS ARE EXTRACTED!!!
								//current image
								Mat tempImage= correctCroppedImages.get(index);
								
								Mat copyOfTempImage=tempImage.clone();
								
								//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempImage),"Shoe number "+index);
								
								//extract shoe outline
								Mat processedImage= ExtractShoeContours.processImageForContoursDetection(tempImage);
//								ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(processedImage),"Outline extracted "+index);
							
								
								//get a contour corresponding the extracted shoe outline
								MatOfPoint tempContour= ExtractShoeContours.extractLargestContour(processedImage);
								
								//find the area of this contour
								double area=Math.abs(Imgproc.contourArea(tempContour));
								System.out.println("THE AREA OF LARGEST CONTOUR FOR SHOE "+index+" DETECTED: "+ area);
								if(area<1000){
									System.out.println("NOTHING HAS BEEN DETECTED");
									continue;
								}
								else{
									//extract points from contour and draw them on image:
									
									Point[] outlinePoints = tempContour.toArray();
									
									//Draw the outline on the white background
									Mat outlinePointsOnly = new Mat(copyOfTempImage.rows(),copyOfTempImage.cols(), CvType.CV_8U, new Scalar(255,255,255));
									
									for(int i=0;i<outlinePoints.length-1; i++){
										Point start=outlinePoints[i];
										Point end= outlinePoints[i+1];
										Core.line(outlinePointsOnly, start, end, new Scalar(0,255,0), 1);
									}
									
//									ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnly), "Outline Contour Extracted for processing");

									///////////////////////////////////////////////////////////////////////////////////////////////////
									double perimeter =Imgproc.arcLength(new MatOfPoint2f( tempContour.toArray() ), true);
									System.out.println("THE PERIMETER OF LARGEST CONTOUR FOR SHOE "+index+" DETECTED: "+ perimeter);
									
									//calculate moments, center of gravity and axis tilt
									Moments m = Imgproc.moments(tempContour, true);
									// center of gravity
									
									double m00 =m.get_m00();
									double m10 = m.get_m10();
									double m01 =m.get_m01();
									
									int xCenter =0;
									int yCenter=0;
									
									if (m00 != 0) {   // calculate center
										xCenter = (int) Math.round(m10/m00);
										yCenter = (int) Math.round(m01/m00);
										
										}
									
									
									System.out.println("THE COORDINATES OF CENTER OF GRAVITY OF LARGEST CONTOUR FOR SHOE "+index+" ARE: X = "+xCenter+" Y = "+yCenter);
									
									
									
									//working with image and contour data
									
									//first fill the extracted large area contour with black color
									Mat finalImgForProcessing = ExtractShoeContours.fillLargestAreaContour(processedImage);
//									ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Large Area contour filed with black color "+index);
									
									//convert it to binary data
									int[][] iarrImageColors=ExtractShoeFeatures.extractBinaryDataFromImage(finalImgForProcessing);
									
								
									//detect longest/shortest horizontal lines near the center
									//FIRST FIND AND CORRECT THE ORIENTATION, THEN DETERMINE THE SHOE
									int[][] iarrImageColorsTransp= CompareShoeContours.transposeMatrix(iarrImageColors);
									System.out.println(iarrImageColorsTransp.length+"   "+iarrImageColorsTransp[0].length);
									List<Point[]> allHorizontalLines = ExtractShoeFeatures.extractLines(iarrImageColorsTransp);
									  
									List<Point[]> horizontalLinesNearCenter= ExtractShoeFeatures.findHorizontalLinesWithinBounds(allHorizontalLines, new Point(xCenter,yCenter), 40);
									  
									double maxHLimDist=Math.abs(horizontalLinesNearCenter.get(0)[0].x-horizontalLinesNearCenter.get(0)[1].x);
									double minHLimDist=Math.abs(horizontalLinesNearCenter.get(1)[0].x-horizontalLinesNearCenter.get(1)[1].x);
								  
									
									double yMaxH=horizontalLinesNearCenter.get(0)[0].y; //max length horizontal line y coordinate
									double yMinH=horizontalLinesNearCenter.get(1)[0].y; //min length horizontal line y coordinate
									
									//rotate the image to bring it to correct orientation
									//Mat rotated = copyOfTempImage.clone();
//									ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Finally Determined Before");
//									
//									//if shoes are oriented down -- flip them to face up
									String orientation="";
									if(yMaxH<yMinH)
										orientation="UP";
									else{
										orientation="DOWN";
										Core.flip(finalImgForProcessing, finalImgForProcessing, -1);
										Core.flip(copyOfTempImage, copyOfTempImage, -1);
									}
									
									System.out.println("ORIENTATION "+ orientation);
									
									
									
									
									ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Finally Determined After");
									
									
									
									//THEN DETERMINE WHICH SHOE IS IT -- LEFT OR RIGHT:
									
									//find shortest/longest vertical distance around center of mass
									iarrImageColors=ExtractShoeFeatures.extractBinaryDataFromImage(finalImgForProcessing);
									iarrImageColorsTransp= CompareShoeContours.transposeMatrix(iarrImageColors);
				 
									List<Point[]> allVerticalLines = ExtractShoeFeatures.extractLines(iarrImageColors);
									
									List<Point[]> verticalLinesNearCenter= ExtractShoeFeatures.findVerticalLinesWithinBounds(allVerticalLines, new Point(xCenter,yCenter), DELTA);
									System.out.println(verticalLinesNearCenter.get(0)[0]);
									double maxVLimDist=Math.abs(verticalLinesNearCenter.get(0)[0].y-verticalLinesNearCenter.get(0)[1].y);
									double minVLimDist=Math.abs(verticalLinesNearCenter.get(1)[0].y-verticalLinesNearCenter.get(1)[1].y);
								  
									//vertical longest part detection near the center of masses
									System.out.println("Longest Vertical near the center "+verticalLinesNearCenter.get(0)[0]+" "+verticalLinesNearCenter.get(0)[1]+" distance "+maxVLimDist);
									System.out.println("Shortest Vertical near the center "+verticalLinesNearCenter.get(1)[0]+" "+verticalLinesNearCenter.get(1)[1]+" distance "+minVLimDist);
								
									//determine whether the shoe is left or right
									double xMaxV=verticalLinesNearCenter.get(0)[0].x;
									double xMinV=verticalLinesNearCenter.get(1)[0].x;
									
									String whichShoe="";
									if(xMinV<xMaxV)
										whichShoe="LEFT";
									else{
										whichShoe="RIGHT";
									}
									
									Core.putText(copyOfTempImage, whichShoe+" "+orientation, new Point(10,30), Core.FONT_ITALIC,new Double(0.6), new Scalar(0,0,255));
								   	
									shoesOutlineContours.add(new shoesInfo2(whichShoe+filename, userID, whichShoe, finalImgForProcessing));
									
									
									
									//WORKING WITH DB:
									
									System.out.println();
									System.out.println("//////////////////////////////////////////////////////////////////////////////////////////////////////////////");
									
									
									//if DB is empty -- write to db
									
									communicateShoesOutlinesTestDB dbconnection = new communicateShoesOutlinesTestDB();
									
									Map<Integer, Double> res = new HashMap<>();
									//
									//populate the hashmap
									res=dbconnection.compareEntry(finalImgForProcessing.clone());
									//if the DB is empty -- add shoes to DB
									if(res.isEmpty()){
										System.out.println();
										System.out.println("The DB is empty");
										System.out.println("ADDING TO DATABASE....");
										System.out.println();
										dbconnection.setShoeInfoWriteToDB(userID,filename,whichShoe,finalImgForProcessing.clone());
										dbconnection.writeShoeInfoToDB();
									}
									else{
										
										shoesInfo2 matchedShoeInfo = null;
										
										//Calculate distances to all shoes in DB, save results in HashMap, sort this map and select the top (smallest) distance
//										System.out.println();
//										tmpNumOfCircles
										
										System.out.println("Unsort Map......");
										dbconnection.printMap(res);
//										
//										System.out.println();
//										
//										System.out.println("\nSorted Map......");
										Map<Integer, Double> sortedMap = dbconnection.sortByComparator(res);
//										printMap(sortedMap);
										
										System.out.println();
										System.out.println("For the following entry from file "+filename+ " labeled as "+whichShoe);
										System.out.println();
										System.out.println("THE CLOSES RECORD IN DB HAS AN ID: "+((int)sortedMap.keySet().toArray()[0]+1));
										
										int closestID=(int)sortedMap.keySet().toArray()[0];
										
										double closestValue=(double) sortedMap.values().toArray()[0];
										System.out.println("The closest distance is "+closestValue);
										
										
										
										//Search DB for closest shoe and output its records
										//May be needed to add a chekc in case if ID is not found?
										matchedShoeInfo=dbconnection.searchDBforID(closestID);
										
										System.out.println();
										System.out.println("THE CLOSEST SHOE FEATURES in DB HAS USER ID "+matchedShoeInfo.userID+" FILE ID "+matchedShoeInfo.fileID+ " AND LABELED AS "+matchedShoeInfo.label);
										System.out.println();
										//if the distance is greater then a specified threshold it might be a new shoe -- add it as a new entry
										//TODO: analyze threshold
										if(closestValue>0.35){
											System.out.println();
											System.out.println("For the following entry from file "+filename+ " labeled as "+whichShoe);
											System.out.println("NO MATCH HAS FOUND -- NEW UNIQUE ENTRY");
											System.out.println();
											System.out.println("ADDING TO DATABASE....");
											System.out.println();
											dbconnection.setShoeInfoWriteToDB(userID,filename,whichShoe,finalImgForProcessing.clone());
											dbconnection.writeShoeInfoToDB();
										}
										
									}//end of else
									
									
									
//									BufferedImage bufferedFinal=ProcessImages.Mat2BufferedImage(finalImgForProcessing);
//									ProcessImages.displayImage(bufferedFinal,"Original");
//									
//									byte[] data = ((DataBufferByte) bufferedFinal.getRaster().getDataBuffer()).getData();
//									
//									
//									Mat mat = new Mat(bufferedFinal.getHeight(),bufferedFinal.getWidth(), CvType.CV_8U);
//								    mat.put(0, 0, data);
//									
//									ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(mat),"Converted back");
									
									
//									dbconnection.setShoeInfoWriteToDB(userID, filename, tempSize,  iLines,  iCircles, currentAttributesMatrix);
//									dbconnection.writeShoeInfoToDB();
									//dbconnection.outputDB();
									
//									ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(copyOfTempImage),"Finally Determined");
									
								}//end of else case of testing whether the area is sufficient for considering it to be a good contour
							}//end of for iterating over 2 best contours extracted
						}//end of else case for checking the size of extracted contours is greater than 2
						
						
						
						
						
//					}//end of inner for iteration over all angles
//				}//end of outer for iteration over all user IDs
				
				
				
				
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//TEST CONTOUR DETECTION BASED ON THE CONTOUR OUTLINES
//				
//				
//				//set the shoe which will be compared with others
//				shoesInfo2 contourOfInterest=shoesOutlineContours.get(22);
//				
//		
//				
//				//iterate over all arrayList of shoes records to find a match:
//				for(int index=0; index<shoesOutlineContours.size(); index++){
//					shoesInfo2 currentContour=shoesOutlineContours.get(index);
//					
//					System.out.println("////////////////////////////////////////////////////////////////////////////////");
//					System.out.println("Current contour fileID is "+currentContour.fileID);
//					System.out.println("Current contour userID is "+currentContour.userID);
//					System.out.println("Current contour label is "+currentContour.label);
//					
//					
//					boolean contoursMatched=CompareShoeContours.contoursMatched(contourOfInterest,currentContour);//
//					
//					System.out.println("MATCHED WITH USER'S "+contourOfInterest.userID+"  "+ contourOfInterest.label+" SHOE "+contoursMatched);
//					
//				}
//				
//				System.out.println();
//				System.out.println("Number of shoes extracted "+shoesOutlineContours.size());
//				System.out.println("contour fileID is "+contourOfInterest.fileID);
//				System.out.println("contour userID is "+contourOfInterest.userID);
//				System.out.println("contour label is "+contourOfInterest.label);
		
		
	}//END OF MAIN
	
	
	
	

}//END OF CLASS testCompareShoeContours
