package shoeFittingAlgorithm;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.Rectangle;

import javax.imageio.ImageIO;

/**
 * make relational graphs for shoes with shape elements such as lines and circles
 * @author nimesha
 *
 */
public class MakeRelationalGraphsForBothShoes {
	
	//status flags
	private boolean bProcessShoe  = true;
	
	
	private int inLines   = 0;
	private int inCircles = 0;
	
	private ArrayList<double[]> lines       = new ArrayList<double[]>();
	private ArrayList<double[]> circles     = new ArrayList<double[]>();
	private ArrayList<double[]> shapes      = new ArrayList<double[]>(); //lines and circles with errors

//	
	//relational Graph Matrix - all in one
	private AttributesMatrix[][] distanceMatrix         = null;
	
	
	private BufferedImage relationalGraphImage   = null;

	private Graphics2D relationalGraphGraphics  = null;
	
	private String sRelationalGraphImagePath        = "./output//relationalGraph.jpg";

	
	
	public MakeRelationalGraphsForBothShoes(BufferedImage relationalGraphImage) throws IOException {
		//left shoe
		this.relationalGraphImage = relationalGraphImage;
		int w = this.relationalGraphImage.getWidth();
		int h = this.relationalGraphImage.getHeight();
		this.relationalGraphImage      = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		shapes    = new ArrayList<double[]>();
		initializeStructures();
//		shapes_Right     = new ArrayList<double[]>();
		
	}
	
	public void initializeStructures() {
		lines      = new ArrayList<double[]>();
		circles    = new ArrayList<double[]>();
//		strips     = new ArrayList<int[]>();
//		rectangles = new ArrayList<int[]>();
		
	}
	

	/**
	 * add new line to the array-list
	 * @param x1 starting X coordinates of the line
	 * @param y1 starting Y coordinates of the line
	 * @param x2 ending X coordinates of the line
	 * @param y2 ending Y coordinates of the line
	 * @param error lines error during line detection algorithm
	 */
	public void addNewLine(int[][] line,int error) {
		double [] lineinfo = new double[] {line[0][0], line[0][1], line[1][0], line[1][1], error, 0}; //last element 0 says it is a line
		//int[][] lineinfo= {{x1, y1},{ x2, y2}};
		lines.add(lineinfo);
	}
	
	/**
	 * add new circle to the array-list
	 * @param x x coordinates of the center
	 * @param y y coordinates of the center
	 * @param r radius
	 */
	public void addNewCircle(Rectangle rect) {
		int w=rect.width;
		int h=rect.height;
		int r=rect.width/2;
		double [] circleinfo = new double[] {rect.x+w/2,rect.y+h/2, r, h, 1, 1};//last element 1 says it is a circle - add additional 1's to be same array size as lines
		circles.add(circleinfo);
	}
	
	
	/**
	 * replace circles in the image with dots and remove the detected ones (clear-rect)
	 * @param graph graphics object of the image
	 */
	private void addLinesToGraphGraphics(Graphics2D graph) {
		for (int i = 0; i < lines.size(); i++) {
			double [] lineinfo = lines.get(i);
			graph.setColor(Color.BLACK);
			int x1 = (int)lineinfo[0]; int y1 = (int)lineinfo[1];
			int x2 = (int)lineinfo[2]; int y2 = (int)lineinfo[3];
//			graph.setColor(Color.GREEN);
//			graph.drawLine(x1, y1, x2, y2);
			graph.setColor(Color.RED);
			graph.fillRect( ((x1 + x2) / 2 ), ((y1 + y2) / 2 ), 2, 2);
		}
		
	}
	
	/**
	 * replace circles in the image with dots and remove the detected ones (clear-rect)
	 * @param graph graphics object of the image
	 */
	private void addCirclesToGraphGraphics(Graphics2D graph) {
		
		for (int i = 0; i < circles.size(); i++) {
			double [] circleinfo = circles.get(i);
			int x = (int)circleinfo[0]; int y = (int)circleinfo[1];
			int w  = (int)circleinfo[2]; int h  = (int)circleinfo[3];
			graph.clearRect(x, y, w, h);
			graph.setColor(Color.YELLOW);
			graph.fillRect( ((x + (w/2)) ), ((y + (h/2)) ), 2, 2);
		}
	}
	
	
	/**
	 * remove detected circles from canny image
	 * @param sImageWithCircles
	 * @param sCirclesRemovedImagePath
	 */
	public void removeCircleBlobsFromEdgeDetectedImage(String sImageWithCircles, String sCirclesRemovedImagePath) {
		
		//read canny edge detected image
		BufferedImage inputImage;
		try {
			inputImage = ImageIO.read(new File(sImageWithCircles));
			
			Graphics2D graph = inputImage.createGraphics();
			graph.setColor(Color.BLACK);
			
			for (int i = 0; i < circles.size(); i++) {
				double [] circleinfo = circles.get(i);
				int x = (int)circleinfo[0]; int y = (int)circleinfo[1];
				int r  = (int)circleinfo[2]; 
				graph.clearRect( (x-r), (y-r), (2*r), (2*r));
			}
			
			graph.dispose();
			ImageIO.write(inputImage, "PNG", new File(sCirclesRemovedImagePath));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	

	
	/**
	 * build the graph by adding circles and strips and save the image
	 */
	public void buildGraphAndSave() {
		
		System.out.println("circles.size(): " + circles.size() + " - " + "lines.size(): " + lines.size());
		
		Graphics2D graph = getRelationalGraph();
		addLinesToGraphGraphics(graph);
		addCirclesToGraphGraphics(graph);
		saveRelationalGraphImage();
	}
	
	/**
	 * save the final relational image
	 */
	private void saveRelationalGraphImage() {
		try {
			if (relationalGraphGraphics != null) 
				relationalGraphGraphics.dispose();
			ProcessImages.displayImage(getRelationalGraphImage(),"Relational graph");
			//TODO: remove saving to make it work faster
			ImageIO.write(getRelationalGraphImage(), "JPG", new File(getRelationalGraphImagePath()));
			relationalGraphGraphics = null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * run selection sort to sort lines and circle based on their length and radius
	 */
	public void sortAndPrepareShapeArrayWithErrors() { //Selection Sort
		//shapes = lines (length) + circles(radius) // sorted: (large > small)
		
		//sort lines
		for (int i = 0; i < (lines.size()-1); i++) {
	        for (int j = i + 1; j < lines.size(); j++) {
	        	//line 1 distance
	        	double [] lineinfo1 = lines.get(i);
				int x1 = (int)lineinfo1[0]; int y1 = (int)lineinfo1[1];
				int x2 = (int)lineinfo1[2]; int y2 = (int)lineinfo1[3];
				Point startPoint1 = new Point(x1, y1);
				Point endPoint1 = new Point(x2, y2);
				double distance1 = startPoint1.distance(endPoint1);
				
				//line 2 distance
				double [] lineinfo2 = lines.get(j);
				x1 = (int)lineinfo2[0]; y1 = (int)lineinfo2[1];
				x2 = (int)lineinfo2[2]; y2 = (int)lineinfo2[3];
				Point startPoint2 = new Point(x1, y1);
				Point endPoint2 = new Point(x2, y2);
				double distance2 = startPoint2.distance(endPoint2);
	        	
	            if (distance1 > distance2) {
	                //Exchange elements
	            	lines.set(j, lineinfo1);
	            	lines.set(i, lineinfo2);
	            }
	        }
	    }
		
		//sort circles
		for (int i = 0; i < (circles.size()-1); i++) {
	        for (int j = i + 1; j < circles.size(); j++) {
	        	//circle 1 radius
	        	double [] circleinfo1 = circles.get(i);
				int r1 = (int)(circleinfo1[2]); 
				
				//circle 2 radius
				double [] circleinfo2 = circles.get(j);
				int r2 = (int)(circleinfo2[2]); 
	        	
	            if (r1 > r2) {
	                //Exchange elements
	            	circles.set(j, circleinfo1);
	            	circles.set(i, circleinfo2);
	            }
	        }
	    }
		
		//add sorted lines and circles to shapes array list
		if (bProcessShoe) {
			shapes.addAll(lines);
			shapes.addAll(circles);
		}
//		if (bProcessRightShoe) {
//			shapes_Right.addAll(lines);
//			shapes_Right.addAll(circles);
//		}
//		
		setLinesCount(lines.size());
		setCirclesCount(circles.size());
		
		setShapes(shapes);
		setShapes(shapes);
		
		lines.clear();     lines = null;
		circles.clear(); circles = null;
		
	}
	

	/**
	 * calculate node and edge attributes and save them in the distance matrix 
	 * these matrices will be matched later to identify similar shoe-soles
	 */
	public void calculateLineAndCircleAttributes(ArrayList<double[]> shapes) {
		
		//boolean bPreviousFileFound = true;
		//AttributesMatrix[][] previousDistanceMatrix = null; 
		//int iMatchedShapes = 0;
		
		//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		//System.out.println("strips.size(): " + strips.size() + " circles.size(): " + circles.size() + " rectangles.size(): " + rectangles.size());
		
		if (distanceMatrix == null)
			distanceMatrix          = new AttributesMatrix[shapes.size()][shapes.size()];

		
		CalculateNodeAndEdgeAttributes calculateNodeAndEdgeAttributes = new CalculateNodeAndEdgeAttributes(distanceMatrix);
		calculateNodeAndEdgeAttributes.calculate_OrientationRelativeAndPerpendicularDistances(shapes);
		this.setAttributesMatrix(distanceMatrix);


	}
	
	
	
	/////////////////////////// getters and setters methods /////////////////////////////////////
	


	public void setShapes(ArrayList<double[]> shapes) {
		this.shapes = shapes;
	}
	
	public ArrayList<double[]> getShapes() {
		return shapes;
	}
	
	public AttributesMatrix[][] getDistanceMatrix() {
		return distanceMatrix;
	}

	public void setDistanceMatrix(AttributesMatrix[][] distanceMatrix) {
		this.distanceMatrix= distanceMatrix;
	}
	
	public void setAttributesMatrix(AttributesMatrix[][] distanceMatrix) {
		this.distanceMatrix = distanceMatrix;
	}
	
	public AttributesMatrix[][] getAttributesMatrix() {
		return distanceMatrix;
	}

	public void setAttributesMatrix_Right(AttributesMatrix[][] distanceMatrix) {
		this.distanceMatrix = distanceMatrix;
	}
	
	/**
	 * getter for image path
	 * @return path of the relational image to be saved
	 */
	private String getRelationalGraphImagePath() {
		
			return this.sRelationalGraphImagePath;
			
	}
	
	/**
	 * getter for image graphics
	 * @return
	 */
	private Graphics2D getRelationalGraph() {
		if (relationalGraphGraphics == null) {
				relationalGraphGraphics = relationalGraphImage.createGraphics();
		}
		return relationalGraphGraphics;
	}
	
	/**
	 * getter for buffered image for relational graphics
	 * @return
	 */
	private BufferedImage getRelationalGraphImage() {
			return this.relationalGraphImage;
	}

	public int getLinesCount() {
		return inLines;
	}
	
	public void setLinesCount(int inLines) {
		this.inLines = inLines;
	}
	
	public int getCirclesCount() {
		return inCircles;
	}
	
	public void setCirclesCount(int inCircles) {
		this.inCircles = inCircles;
	}
	
	
}






//save shoes info in DB
//try {
//	BufferedImage imgCanny = ImageIO.read(new File(Properties.cannyEdgeDetectedImagePath));
//	DatabaseReadWrite dbcon = new DatabaseReadWrite();
//	dbcon.writeShoeInfoToDB(imgCanny, distanceMatrix_Left, distanceMatrix_Right, "test2");
//	
//	//read from db
//	ArrayList arrShoesInfo = dbcon.readShoeInfoFromDB();
//	arrShoesInfo.size();
//	
//} catch(Exception ex) {
//	ex.printStackTrace();
//}





//////////////////////// relational graph comparison /////////////////////////
//SaveAndReadRelationalGraphData saveRelationalGraphData = new SaveAndReadRelationalGraphData();
//VerifyRelationalGraphs verifyRelationalGraphs = new VerifyRelationalGraphs();
//
//File src = new File(Properties.getCurrentDirectory() + "\\output\\ARG\\");
//
//File[] files = src.listFiles();
////looping through all the image files
//for (File file : files) {
//    if ( (file.isFile()) ) { //&& (file.getName().contains("mix"))
//    	bPreviousFileFound = true;
//    	//check if any saved instances
//		try {
//			previousDistanceMatrix = saveRelationalGraphData.readFromDisk(file.getAbsolutePath());
//		} catch (IOException e) {
//			bPreviousFileFound = false;
//			e.printStackTrace();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		//if ( file.getName().contains("mix") )
//			verifyRelationalGraphs.verifyGraph(verifyGraphGraphics_l2l, previousDistanceMatrix, off_Image_l2l, ".\\output\\verify_new_graph_pre.PNG");
//		
//		//compare previous and current matrices // previousDistanceMatrix VS distanceMatrix 
//		//if (bPreviousFileFound)
//			//iMatchedShapes = compareDistanceMatrices(distanceMatrix, previousDistanceMatrix);
//		
//		System.out.println(file.getName() + " - " + iMatchedShapes);
//		System.out.println("---------------------------------------------");
//		
//		try {
//			saveRelationalGraphData.saveToDisk(distanceMatrix, Properties.sDistanceMatrices);
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		
//    }
//}

//////////////////////////////////////////////////////////////////////////////

//verifyRelationalGraphs.verifyGraph(verifyGraphGraphics_l2l, distanceMatrix, off_Image_l2l, ".\\output\\verify_new_graph.PNG");



//private int compareDistanceMatrices(AttributesMatrix[][] currentDistanceMatrix, AttributesMatrix[][] previousDistanceMatrix) {
////Distances[][] mainMatrix = null; //larger
////Distances[][] subMatrix  = null; //smaller
////
////if (previousDistanceMatrix.length > currentDistanceMatrix.length) {
////	mainMatrix = previousDistanceMatrix;
////	subMatrix = currentDistanceMatrix;
////}
////else {
////	mainMatrix = currentDistanceMatrix;
////	subMatrix = previousDistanceMatrix;
////}
//
//ArrayList <Integer> iarrMatchedRows    = new ArrayList<Integer>();
//ArrayList <Integer> iarrMatchedColumns = null;
//
////System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
////System.out.println("rowOfCurrentMatrix.length: " + rowOfCurrentMatrix.length + " rowOfPreviousMatrix.length: " + rowOfPreviousMatrix.length);
//
//for (int a = 0; a < currentDistanceMatrix.length; a++) {
//	if (a >= previousDistanceMatrix.length) break;
//	AttributesMatrix[] rowOfCurrentMatrix   = currentDistanceMatrix[a];
//	
//	for (int b = 0; b < previousDistanceMatrix.length; b++) {
//		
//		if (iarrMatchedRows.contains(new Integer(b))) continue;
//		
//		AttributesMatrix[] rowOfPreviousMatrix  = previousDistanceMatrix[b];
//		iarrMatchedColumns = new ArrayList<Integer>();
//		
//		
//		for (int i = 0; i < rowOfCurrentMatrix.length; i++) {//starting from 1 since 0th position has '-1'
//			for (int j = 0; j < rowOfPreviousMatrix.length; j++) {
//				
//				 
//				if ( iarrMatchedColumns.contains(new Integer(j)) )
//					continue;
//				
//				if ( !( (b == i) || (b == j) ) )
//				{
//					if (    (rowOfCurrentMatrix[i].getsShapeMethod().equalsIgnoreCase("L2L")) && (rowOfPreviousMatrix[j].getsShapeMethod().equalsIgnoreCase("L2L")) ||
//							(rowOfCurrentMatrix[i].getsShapeMethod().equalsIgnoreCase("C2C")) && (rowOfPreviousMatrix[j].getsShapeMethod().equalsIgnoreCase("C2C")) //||
//							//(rowOfCurrentMatrix[i].getsShapeMethod().equalsIgnoreCase("C2L")) && (rowOfPreviousMatrix[j].getsShapeMethod().equalsIgnoreCase("C2L")) ||
//							//(rowOfCurrentMatrix[i].getsShapeMethod().equalsIgnoreCase("L2C")) && (rowOfPreviousMatrix[j].getsShapeMethod().equalsIgnoreCase("L2C"))  
//					   ) {
//
//						double pddiff = Math.abs( Math.abs(rowOfCurrentMatrix[i].getPdistance())    - Math.abs(rowOfPreviousMatrix[j].getPdistance())    );
//						double rddiff = Math.abs( Math.abs(rowOfCurrentMatrix[i].getRdistance())    - Math.abs(rowOfPreviousMatrix[j].getRdistance())    );
//						double ordiff = Math.abs( Math.abs(rowOfCurrentMatrix[i].getRorientation()) - Math.abs(rowOfPreviousMatrix[j].getRorientation()) );
//
//						if ( (pddiff < 10) && (rddiff < 10) && (ordiff < 10) ) { //&& (ordiff < 10)
//							
//							iarrMatchedColumns.add(new Integer(j));
//							break;
//						}
//					}
//				}
//			}
//		}
//		
//		double percentage = (double) ( (iarrMatchedColumns.size() * 100.0) / ( (double) (currentDistanceMatrix.length-1)) );
//		//System.out.println("iMPoints: " + iMPoints + " rowOfCurrentMatrix.length: " + rowOfCurrentMatrix.length + " %: " + percentage);
//		
//		if (percentage > 70.0) {
//			iarrMatchedRows.add( new Integer(b) ); //a match found
//			break;
//		}
//		
//		
//	}
//	
//	//if (a == 1) break;
//	System.out.print(a + ", ");
//
//}
//
//double rows_percentage = (double) ( (iarrMatchedRows.size() * 100.0) / ( (double) (currentDistanceMatrix.length)) );
//System.out.println("\n%%%%%%%%%%%%: " + rows_percentage);
//
//
//return (int) rows_percentage;
//
//}





//double emdd = new CalculateEMD().simpleEMD(darrSubElement, darrMainElement);
//if (emdd < 2) 
//	System.out.println("111111111111 J: " + j + " I: " + i + " EMD: " + emdd);


//L2L - relative sizes
//private double [][] relativesizes_l2l = null;
////L2L - perpendicular distance
//private double [][] perpendiculardistances_l2l = null;
////L2L - relative distance
//private double [][] relativedistances_l2l      = null;
////L2L - relative orientation
//private double [][] relativeorientations_l2l   = null;
//
//
////C2C - relative sizes
//private double [][] relativesizes_c2c = null;
////C2C - perpendicular distance
//private double [][] perpendiculardistances_c2c = null;
////C2C - relative distance
//private double [][] relativedistances_c2c      = null;
////C2C - relative orientation
//private double [][] relativeorientations_c2c   = null;
//
//
////C2L - relative sizes
//private double [][] relativesizes_c2l = null;
////C2L - perpendicular distance
//private double [][] perpendiculardistances_c2l = null;
////C2L - relative distance
//private double [][] relativedistances_c2l      = null;
////C2L - relative orientation
//private double [][] relativeorientations_c2l   = null;
//
//
////L2C - relative sizes
//private double [][] relativesizes_l2c = null;
////L2C - perpendicular distance
//private double [][] perpendiculardistances_l2c = null;
////L2C - relative distance
//private double [][] relativedistances_l2c      = null;
////L2C - relative orientation
//private double [][] relativeorientations_l2c   = null;
//
//
////R2R - relative sizes
//private double [][] relativesizes_r2r = null;
////R2R - perpendicular distance
//private double [][] perpendiculardistances_r2r = null;
////R2R - relative distance
//private double [][] relativedistances_r2r      = null;
////R2R - relative orientation
//private double [][] relativeorientations_r2r   = null;
//
//









//public void calculateLineAndCircleAttributes() {
////initialize line 2 line structures
//relativesizes_l2l          = new double[strips.size()][strips.size()];
//perpendiculardistances_l2l = new double[strips.size()][strips.size()];
//relativedistances_l2l      = new double[strips.size()][strips.size()];
//relativeorientations_l2l   = new double[strips.size()][strips.size()];
//
////initialize circle 2 circle structures
//relativesizes_c2c          = new double[circles.size()][circles.size()];
//perpendiculardistances_c2c = new double[circles.size()][circles.size()];
//relativedistances_c2c      = new double[circles.size()][circles.size()];
//relativeorientations_c2c   = new double[circles.size()][circles.size()];
//
////initialize circle 2 line structures
//relativesizes_c2l          = new double[circles.size()][strips.size()];
//perpendiculardistances_c2l = new double[circles.size()][strips.size()];
//relativedistances_c2l      = new double[circles.size()][strips.size()];
//relativeorientations_c2l   = new double[circles.size()][strips.size()]; 
//
////initialize line to circle structures
//relativesizes_l2c          = new double[strips.size()][circles.size()];
//perpendiculardistances_l2c = new double[strips.size()][circles.size()];
//relativedistances_l2c      = new double[strips.size()][circles.size()];
//relativeorientations_l2c   = new double[strips.size()][circles.size()]; 
//
////initialize rectangle 2 rectangle structures
//relativesizes_r2r          = new double[rectangles.size()][rectangles.size()];
//perpendiculardistances_r2r = new double[rectangles.size()][rectangles.size()];
//relativedistances_r2r      = new double[rectangles.size()][rectangles.size()];
//relativeorientations_r2r   = new double[rectangles.size()][rectangles.size()];
//
//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
//System.out.println("strips.size(): " + strips.size() + " circles.size(): " + circles.size() + " rectangles.size(): " + rectangles.size());
//
////detect largest sizes and total number of detected shapes
//int ixSize = 0, iySize = 0;
//if (strips.size()     > circles.size()) ixSize = strips.size();
//if (circles.size()    > strips.size())  ixSize = circles.size();
//if (rectangles.size() > ixSize)         ixSize = rectangles.size();
//
//iySize = (strips.size()*8) + (circles.size()*8) + (rectangles.size()*4);
//System.out.println("ixSize: " + ixSize + " iySize: " + iySize);
//relationalGraphMatrix      = new double[iySize][ixSize]; //r c
//
//
//CalculateNodeAndEdgeAttributes calculateNodeAndEdgeAttributes = new CalculateNodeAndEdgeAttributes(relationalGraphMatrix); 
//
////L2L - Relative Sizes
//calculateNodeAndEdgeAttributes.calculate_RelativeSizes_L2LC2CR2R(strips);
////C2C - Relative Sizes
//calculateNodeAndEdgeAttributes.calculate_RelativeSizes_L2LC2CR2R(circles);
////R2R - Relative Sizes
//calculateNodeAndEdgeAttributes.calculate_RelativeSizes_L2LC2CR2R(rectangles);
////C2L - Relative Sizes
//calculateNodeAndEdgeAttributes.calculate_RelativeSizes_C2LorL2C(strips, circles);
////L2C - Relative Sizes
//calculateNodeAndEdgeAttributes.calculate_RelativeSizes_C2LorL2C(circles, strips);
//		
//
////L2L - Calculate Relative Distances, Perpendicular Distances, and Relative Orientations
//calculateNodeAndEdgeAttributes.calculate_OrientationRelativeAndPerpendicularDistances_L2LC2CR2R(strips, relativedistances_l2l, perpendiculardistances_l2l, relativeorientations_l2l);
//
//
////C2C - Calculate Relative Distances, Perpendicular Distances, and Relative Orientations
//calculateNodeAndEdgeAttributes.calculate_OrientationRelativeAndPerpendicularDistances_L2LC2CR2R(circles, relativedistances_c2c, perpendiculardistances_c2c, relativeorientations_c2c);
//
//
////R2R - Calculate Relative Distances, Perpendicular Distances, and Relative Orientations
//calculateNodeAndEdgeAttributes.calculate_OrientationRelativeAndPerpendicularDistances_L2LC2CR2R(rectangles, relativedistances_r2r, perpendiculardistances_r2r, relativeorientations_r2r);
//
//
//
////C2C - Calculate Relative Distances, Perpendicular Distances, and Relative Orientations
//calculateNodeAndEdgeAttributes.calculate_OrientationRelativeAndPerpendicularDistances_C2L(circles, strips, relativedistances_c2l, perpendiculardistances_c2l, relativeorientations_c2l);
//
//
////L2C - Calculate Relative Distances, Perpendicular Distances, and Relative Orientations
//calculateNodeAndEdgeAttributes.calculate_OrientationRelativeAndPerpendicularDistances_L2C(circles, strips, relativedistances_l2c, perpendiculardistances_l2c, relativeorientations_l2c);



//verification images
//System.out.println("\n!!!!!!!!!!!!!! VERIFY !!!!!!!!!!!!!!!!!!!!!");
//VerifyRelationalGraphs verifyRelationalGraphs = new VerifyRelationalGraphs();
//verifyRelationalGraphs.verifyGraph_l2l(verifyGraphGraphics_l2l, relativedistances_l2l, relativeorientations_l2l, off_Image_l2l);
//verifyRelationalGraphs.verifyGraph_c2c(verifyGraphGraphics_c2c, relativedistances_c2c, relativeorientations_c2c, off_Image_c2c);
//verifyRelationalGraphs.verifyGraph_r2r(verifyGraphGraphics_r2r, relativedistances_r2r, relativeorientations_r2r, off_Image_r2r);
//
//verifyRelationalGraphs.verifyGraph_c2l(verifyGraphGraphics_c2l, relativedistances_c2l, relativeorientations_c2l, off_Image_c2l); 
//verifyRelationalGraphs.verifyGraph_l2c(verifyGraphGraphics_l2c, relativedistances_l2c, relativeorientations_l2c, off_Image_l2c); 


//////////// saving ARG data ///////
/*System.out.println("\n!!!!!!!!!!!!!! SAVING !!!!!!!!!!!!!!!!!!!!!");
SaveRelationalGraphData saveRelationalGraphData = new SaveRelationalGraphData();
saveRelationalGraphData.saveToDisk(perpendiculardistances_l2l, Properties.sPerpendicularDistances_l2l);
saveRelationalGraphData.saveToDisk(perpendiculardistances_c2c, Properties.sPerpendicularDistances_c2c);
saveRelationalGraphData.saveToDisk(perpendiculardistances_r2r, Properties.sPerpendicularDistances_r2r);
saveRelationalGraphData.saveToDisk(perpendiculardistances_l2c, Properties.sPerpendicularDistances_l2c);
saveRelationalGraphData.saveToDisk(perpendiculardistances_c2l, Properties.sPerpendicularDistances_c2l);

saveRelationalGraphData.saveToDisk(relativedistances_l2l, Properties.sRelativeDistances_l2l);
saveRelationalGraphData.saveToDisk(relativedistances_c2c, Properties.sRelativeDistances_c2c);
saveRelationalGraphData.saveToDisk(relativedistances_r2r, Properties.sRelativeDistances_r2r);
saveRelationalGraphData.saveToDisk(relativedistances_l2c, Properties.sRelativeDistances_l2c);
saveRelationalGraphData.saveToDisk(relativedistances_c2l, Properties.sRelativeDistances_c2l);

saveRelationalGraphData.saveToDisk(relativeorientations_l2l, Properties.sRelativeOrientations_l2l);
saveRelationalGraphData.saveToDisk(relativeorientations_c2c, Properties.sRelativeOrientations_c2c);
saveRelationalGraphData.saveToDisk(relativeorientations_r2r, Properties.sRelativeOrientations_r2r);
saveRelationalGraphData.saveToDisk(relativeorientations_l2c, Properties.sRelativeOrientations_l2c);
saveRelationalGraphData.saveToDisk(relativeorientations_c2l, Properties.sRelativeOrientations_c2l);

saveRelationalGraphData.saveToDisk(relativesizes_l2l, Properties.sRelativeSizes_l2l);
saveRelationalGraphData.saveToDisk(relativesizes_c2c, Properties.sRelativeSizes_c2c);
saveRelationalGraphData.saveToDisk(relativesizes_r2r, Properties.sRelativeSizes_r2r);
saveRelationalGraphData.saveToDisk(relativesizes_l2c, Properties.sRelativeSizes_l2c);
saveRelationalGraphData.saveToDisk(relativesizes_c2l, Properties.sRelativeSizes_c2l);*/


//////////////////// EMD ///////////////////////
//CalculateEMD calculateEMD = new CalculateEMD();
//calculateEMD.EMD(perpendiculardistances_l2l, Properties.sPerpendicularDistances_l2l);
//calculateEMD.EMD(perpendiculardistances_c2c, Properties.sPerpendicularDistances_c2c);
//calculateEMD.EMD(perpendiculardistances_r2r, Properties.sPerpendicularDistances_r2r);
//calculateEMD.EMD(perpendiculardistances_l2c, Properties.sPerpendicularDistances_l2c);
//calculateEMD.EMD(perpendiculardistances_c2l, Properties.sPerpendicularDistances_c2l);
//
//calculateEMD.EMD(relativedistances_l2l, Properties.sRelativeDistances_l2l);
//calculateEMD.EMD(relativedistances_c2c, Properties.sRelativeDistances_c2c);
//calculateEMD.EMD(relativedistances_r2r, Properties.sRelativeDistances_r2r);
//calculateEMD.EMD(relativedistances_l2c, Properties.sRelativeDistances_l2c);
//calculateEMD.EMD(relativedistances_c2l, Properties.sRelativeDistances_c2l);
//
//calculateEMD.EMD(relativeorientations_l2l, Properties.sRelativeOrientations_l2l);
//calculateEMD.EMD(relativeorientations_c2c, Properties.sRelativeOrientations_c2c);
//calculateEMD.EMD(relativeorientations_r2r, Properties.sRelativeOrientations_r2r);
//calculateEMD.EMD(relativeorientations_l2c, Properties.sRelativeOrientations_l2c);
//calculateEMD.EMD(relativeorientations_c2l, Properties.sRelativeOrientations_c2l);
//
//calculateEMD.EMD(relativesizes_l2l, Properties.sRelativeSizes_l2l);
//calculateEMD.EMD(relativesizes_c2c, Properties.sRelativeSizes_c2c);
//calculateEMD.EMD(relativesizes_r2r, Properties.sRelativeSizes_r2r);
//calculateEMD.EMD(relativesizes_l2c, Properties.sRelativeSizes_l2c);
//calculateEMD.EMD(relativesizes_c2l, Properties.sRelativeSizes_c2l);
//
//}
