package shoeFittingAlgorithm;

import org.opencv.core.Point;

//to store detected lines

public class Line {
	
	Point start;
	Point end;
	
	public Line(Point start, Point end){
		this.start=start;
		this.end=end;
	}
	
	public double getLength(){
		return Math.sqrt((this.start.x-this.end.x)*(this.start.x-this.end.x)+(this.start.y-this.end.y)*(this.start.y-this.end.y));
	}
	
	public Point getMidpoint(){
		return new Point((this.start.x+this.end.x)/2,(this.start.y+this.end.y)/2);
	}
	
	 /**
	 * calculate angle from the Y axis to second point - mid point of second object //array elements: n, x, y, w, h
	 * @param p1 source point - root
	 * @param p2 secondary point - relative to root
	 * @return degrees from Y axis
	 */
	public double GetAngleBetweenTwoPoints(Point p1, Point p2, String sMeasurement) { 
		double xDiff = p2.x - p1.x; 
		double yDiff = p2.y - p1.y; 
		double radians  = Math.atan2(yDiff, xDiff);
		double degrees = Math.toDegrees(radians); 
		
		//From the Y Axis
		double ydegrees = -1;

		if ( (degrees <= 90) && (degrees >= 0) ) {
			ydegrees = 90-degrees;
		}
		if ( (degrees <= 180) && (degrees > 90) ) {
			ydegrees = (180 - degrees) + 90; //360 - degrees + 90;
		}
		if ( (degrees < 0) && (degrees >= -90) ) {
			ydegrees = 90 + Math.abs(degrees);
		}
		if ( (degrees <= -90) && (degrees > -180) ) {
			ydegrees = (90 + Math.abs(degrees) ) - 180;
		}
		
		if (sMeasurement.equalsIgnoreCase("radians"))
			return radians;
		if (sMeasurement.equalsIgnoreCase("degrees"))
			return ydegrees;
		return 0;
	}
}
