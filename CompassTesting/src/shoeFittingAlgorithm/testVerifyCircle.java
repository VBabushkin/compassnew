package shoeFittingAlgorithm;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import blobDetection.Blob;
import blobDetection.BlobDetection;

public class testVerifyCircle {
	static float threshold=0.2f;
	static float dLineThresholdValue=0.2f;
	static float dBadPixelThresholdValue=0.02f;
	static BlobDetection theBlobDetection;
	public static void main(String args[]){
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
				
		String initialImagePath=".//results//circleTest.jpg";
		//read image
		Mat source = Highgui.imread(initialImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		int lowThreshold=50;
		int highThreshold=50;
		int kernelSize=3;
		
		Mat cannyFilteredSourceMat= AnalyzeImage.applyCannyEdgeDetectorOpenCV_Mat(source,lowThreshold, highThreshold, kernelSize);
		
		
		
		BufferedImage img=ProcessImages.Mat2BufferedImage(cannyFilteredSourceMat);


		detectBlobs(img, threshold);
		
		ProcessImages.displayImage(img,"Test image circle");
		
		Blob b;
		
		int imgW = img.getWidth();
		int imgH = img.getHeight();
		
		BufferedImage outputimg = new BufferedImage(imgW, imgH, BufferedImage.TYPE_INT_RGB);
		Graphics2D graph = outputimg.createGraphics();
		graph.drawImage(img, 0, 0, null);
		graph.setStroke(new BasicStroke(1));
		graph.setColor(Color.RED);
		
		
		VerifyCircle verifyCircle       = new VerifyCircle();
		System.out.println("BLOBS DETECTED "+theBlobDetection.getBlobNb());
		for (int n=0 ; n < theBlobDetection.getBlobNb() ; n++) {
			b=theBlobDetection.getBlob(n); 
			if (b!=null)
			{
				// Blobs
				int x = Math.round(b.xMin*imgW); int y = Math.round(b.yMin*imgH);
				int w = Math.round(b.w*imgW);    int h = Math.round(b.h*imgH);
				graph.setColor(Color.RED);
				
				Rect roi = new Rect(x,y,w,h);
				
				Mat currentBlob=ProcessImages.BufferedImage2Mat(img).submat(roi);
				Imgproc.cvtColor(currentBlob, currentBlob, Imgproc.COLOR_GRAY2BGR);
				BufferedImage buffCurrentBlob =ProcessImages.Mat2BufferedImage(currentBlob);
				
				
				
				Graphics2D graphCurrentBlob = buffCurrentBlob.createGraphics();
				graphCurrentBlob.drawImage(buffCurrentBlob, 0, 0, null);
				graphCurrentBlob.setStroke(new BasicStroke(1));
				graphCurrentBlob.setColor(Color.RED);
				
				if (verifyCircle.isCircle(x,y,w,h,img, graph, dLineThresholdValue, dBadPixelThresholdValue))
				{
					graph.setColor(Color.GREEN);

					graph.drawRect(x,y,w,h);
			
				}
				
				if (verifyCircle.isCircle(0,0,w,h,buffCurrentBlob, graphCurrentBlob, dLineThresholdValue, dBadPixelThresholdValue))
				{
					
					graphCurrentBlob.setColor(Color.GREEN);
					
					graphCurrentBlob.drawRect(0,0,w,h);
					
				}
				
				
				ProcessImages.displayImage(buffCurrentBlob,"Blob # "+n);
			}
			
		}
		
		ProcessImages.displayImage(outputimg,"Final Image");
		
	}

	
	
	
	
	public static void detectBlobs(BufferedImage imgBuffer, float threshold) {
		int width = imgBuffer.getWidth(null);
		int height = imgBuffer.getHeight(null);
		
		int[] pixels = new int[width*height];
		
		//Returns an array of integer pixels in the default RGB color model
		imgBuffer.getRGB(0, 0, width, height, pixels, 0, width);
		theBlobDetection = new BlobDetection(imgBuffer.getWidth(), imgBuffer.getHeight());
			
		//If passed boolean value is true, the blob detection will attempt to find bright areas. 
		//Otherwise, it will detect dark areas (default mode). 
		//This setting is very useful when you decide to compute polygons for each blob.
		theBlobDetection.setPosDiscrimination(false);
		
		//BlobDetection analyzes image for finding dark or bright areas, depending on the selected mode. 
		//The threshold is a float number between 0.0f and 1.0f used by the blobDetection to find blobs which contains pixels 
		//whose luminosity is inferior (for dark areas) or superior (for bright areas) to this value.
		theBlobDetection.setThreshold(threshold); //0.28f
		
		//Compute the blobs in the image.
		theBlobDetection.computeBlobs(pixels);
		
		//A call to this function will tell the BlobDetection instance to store polygon informations of each detected blob. 
		//By default, polygons are not computed.
		theBlobDetection.computeTriangles();
		
		//Returns the numbers of blobs detected in an image.
		int nblobs = theBlobDetection.getBlobNb();
		
		//System.out.println("Number of detected blobs: "+nblobs);
}
}
