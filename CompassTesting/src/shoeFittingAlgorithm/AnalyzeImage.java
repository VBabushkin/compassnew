package shoeFittingAlgorithm;

import static org.bytedeco.javacpp.opencv_core.CV_FILLED;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class AnalyzeImage {
	
	
	//optimal parameters for circle detection
	int currentBlobDetectionThresholdValue=15;
	int currentLineDetectionThresholdValue=20;
	int currentbadPixelThresholdValue=2;
	int heightThresholdValue=10;
	int widthThresholdValue =10;
	int differenceValue=10;
    
	/**
	 * Applies Canny edge detection algorithm
	 * @param Mat image
	 * @return Mat
	 */
	public static Mat applyCannyEdgeDetectorOpenCV_Mat(Mat image,int lowThreshold,int ratio, int kernelSize) {
		//TODO
		//all these parameters have to be investigated
		Mat result=image.clone();
	    ///////////////////////////////////////////////////////////////////
	    Imgproc.blur(result, result,new Size(2,2));
	    Imgproc.Canny(result, result, lowThreshold, ratio, kernelSize,true);
	    
	    return result;
	}
	
	
	
	/**
	 * 
	 * @param img -- image where circles have to be removed
	 * @param allDetectedCicles -- ArrayList of all detected circles information
	 * @param color -- color to be filled in place of detected circles
	 * @return -- an image with removed circles to simplify the detection of lines
	 */
	public static BufferedImage removeDetectedCircles(BufferedImage img, ArrayList allDetectedCicles, Scalar color) {
		//convert back to rgb
		 Mat destination=ProcessImages.BufferedImage2Mat(img);
	     Imgproc.cvtColor(destination, destination, Imgproc.COLOR_GRAY2BGR);
	    
	     for (int lin = 0; lin< allDetectedCicles.size(); lin++) 
	      {
	            Rectangle vec =  (Rectangle) allDetectedCicles.get(lin);
	            int x=vec.x;
	            int y=vec.y;
	            int w=vec.width;
	            int h=vec.height;
	            //fill detected circle with given color
	            //the extra 2 pixels are added to radius to ensure that all area around the detected circle is covered
	            int extraPx=2;
	            Core.circle(destination, new Point(x+w/2,y+h/2), w/2+extraPx, color,CV_FILLED);
	      }
	      return ProcessImages.Mat2BufferedImage(destination);
	}
	
	
	
	/**
	 * Draw both lines and circles
	 * @param destination
	 * @param lines
	 * @param circles
	 * @return
	 */
	public static Mat drawLinesCirclesArrayList(Mat destination, ArrayList allDetectedLines,ArrayList allDetectedCicles) {
		//convert back to rgb
		
	     Imgproc.cvtColor(destination, destination, Imgproc.COLOR_GRAY2BGR);
	     //Imgproc.threshold(result,result,64, 254,Imgproc.THRESH_BINARY_INV);
	     //display lines
	     
	     int numOfLinesDetected=0;

	     //removing lines of 0 length
	     ArrayList lines=removeLinesOfZeroLength(allDetectedLines);
	     for (int lin = 0; lin< lines.size(); lin++) //lines.size()
	      {
	            int[][] vec = (int[][]) lines.get(lin);
	            int  x1 = vec[0][0], 
	                 y1 = vec[0][1],
	                 x2 = vec[1][0],
	                 y2 = vec[1][1];

	            Point start = new Point(x1, y1);
	            Point end = new Point(x2, y2);
	            
	            //System.out.println("POINTS DETECTED: ");
	            //System.out.println(x1+" "+y1+" "+x2+" "+y2);
	            
	            Core.line(destination, start, end, new Scalar(0,255,0), 1);
	            Core.circle(destination,end, 2, new Scalar(0,0,255), 1);
	            Core.circle(destination, start, 2, new Scalar(0,255,255), 1);
	            numOfLinesDetected++;

	            
	      }
	      System.out.println("Number of lines found "+numOfLinesDetected);
	     // numOfLines.setText(String.valueOf(lines.size()));
	      
	      
	      int numOfCirclesDetected=0;

		     for (int lin = 0; lin< allDetectedCicles.size(); lin++) //lines.size()
		      {
		            Rectangle vec =  (Rectangle) allDetectedCicles.get(lin);
		            int x=vec.x;
		            int y=vec.y;
		            int w=vec.width;
		            int h=vec.height;
		            Core.rectangle(destination, new Point(x,y), new Point(x+w,y+h), new Scalar(255,255,0));
		            Core.circle(destination, new Point(x+w/2,y+h/2), w/2, new Scalar(0,0,255));
		            numOfCirclesDetected++;

		            
		      }
		      //System.out.println("Number of Circles found "+numOfCirclesDetected);
		      //numOfCircles.setText(String.valueOf(allDetectedCicles.size()));
		      return destination;
	}

	/**
	 * removes lines of zero length (points)
	 * @param lines
	 * @return
	 */
	public static ArrayList removeLinesOfZeroLength(ArrayList lines){
		 ArrayList<int[][]>result =new ArrayList<int[][]>();
		
		for (int lin = 0; lin< lines.size(); lin++) //lines.size()
	      {
	            int[][] vec = (int[][]) lines.get(lin);
	            int  x1 = vec[0][0], 
	                 y1 = vec[0][1],
	                 x2 = vec[1][0],
	                 y2 = vec[1][1];
	            if(lineLength(x1,x2,y1,y2)==0){
	            	continue;
	            }
	            else{
	            	result.add(vec);
	            }
	            
	      }
		return result;
		
	}
	
	/**
	 * Calculates euclidean distance between two points with coordinates x1, y1, x2, y2
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	private static double lineLength(int x1, int y1, int x2, int y2){
		return Math.sqrt((x1-x2)*(x1-x2)+(y1-y1)*(y1-y1));
	}
	
	
	
	
}
