package shoeFittingAlgorithm;


import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.KeyPoint;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractorMOG2;

public class testContourExtractionDevice {
	final static int DIFF = 10;
	final static int LOWER_CONTOUR_AREA_THRESHOLD=6000;//20000
	final static int UPPER_CONTOUR_AREA_THRESHOLD=100000;//50000
	//final static int SHOE_SIZE_AREA_DIFFERENCE=14000;
	//final static double UPPER_RATIO_VALUE=2.8;
	//final static double LOWER_RATIO_VALUE=2.1;
	//final static int EDGE_LENGTH=20;
	//final static int DELTA=40; //80 pixels square around the center of gravity
	
	static String initialImagePath=".//RawPhotos//";//".//results//in.jpg";
	static String processedImagesPath=".//RawPhotos//";
	static String resultImagesPath=".//RawPhotos//";
	static String bckgImage =".//RawPhotos//background.jpg";//".//results//bckg.jpg";
	
	
	
	
	
	public static void main(String args[]){
		int userID= 21;
		int angle=878;
		//names after undistorting is performed
		String filename= "shoes_"+userID+"_"+angle;
		String backgroundFileName="bckg_"+userID;
		String initPath=initialImagePath+filename+".jpg";
		String bckgImagePath=initialImagePath+backgroundFileName+".jpg";
		
		//Split camera image by two
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		
		//read image
		Mat source = Highgui.imread(initPath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(source),"Initial Image");
		
		//read the background image
		Mat bckg = Highgui.imread(bckgImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bckg),"bckg");
		
		//convert to one-channel gray
		Imgproc.cvtColor(source,source,Imgproc.COLOR_BGR2GRAY);
		Imgproc.cvtColor(bckg,bckg,Imgproc.COLOR_BGR2GRAY);


		

	     
	     Mat dst = new Mat();
	     Mat bckgDist = new Mat();
	     
	    // 1. increase the contrast
	    source.convertTo(dst, -1, 6, 0);//increase the contrast (double)
	    bckg.convertTo(bckgDist, -1, 6, 0);//increase the contrast (double)
	    
	    
//	    dst = source.clone();
//	    bckgDist = bckg.clone();
	    // 2. blur
	    Imgproc.blur(dst, dst, new Size(10,10));
	    Imgproc.blur(bckgDist, bckgDist, new Size(10,10));
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(dst),"BLURRED");
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bckgDist),"BLURRED");
	
	    //COMPARE BACKGROUNDS
	    Mat bckg2 = Highgui.imread(initialImagePath+"bckg_3"+".jpg", Highgui.CV_LOAD_IMAGE_COLOR);
	    bckg2.convertTo(bckg2, -1, 6, 0);
	    Imgproc.blur(bckg2, bckg2, new Size(10,10));
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bckg2),"bckg2");
		System.out.println("two backgrounds are the same "+compareImages(bckgDist,bckgDist,15));
//		
	    
	    // 3. remove background
	    
	    Mat newResImg = removeBackground(dst,bckgDist, DIFF);
	    
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(newResImg),"RESULTING IMAGE");
		
		
		
		
		Mat newResImg_1=new Mat();
		org.opencv.core.Core.subtract(dst, bckgDist, newResImg_1);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(newResImg_1),"COMPARISON RESULTING IMAGE");
		
		//4. equalize histograms
		Imgproc.equalizeHist(newResImg, newResImg);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(newResImg),"after equalization");	
//		Highgui.imwrite(initialImagePath+"withoutBckg"+filename+".jpg", newResImg);
		//5. erode/dilate
		
		Imgproc.erode(newResImg, newResImg, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
		Imgproc.dilate(newResImg, newResImg, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
	    
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(newResImg),"RESULTING IMAGE AFTER MORPH TRANSFORMS");
		
		//6. apply binary threshold
		Imgproc.threshold(newResImg, newResImg, 254.8, 255, Imgproc.THRESH_BINARY);
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(newResImg),"THRESHOLDED");
		
	    //remove background in ordinary way
	  //REMOVE BACKGROUND
	    Mat backgroundRemoved =  new Mat();
	  	org.opencv.core.Core.subtract(source.clone(), bckg, backgroundRemoved);
	  	ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved),"Original with removed background");
	  		 
	    
	  	
	  	
	  	
	  	
	    newResImg = backgroundRemoved;
	    
	    
	    org.opencv.core.Core.inRange(backgroundRemoved, new Scalar(0, 0, 0), new Scalar(17, 17, 17), newResImg);
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(newResImg),"THRESHOLDED");
	    

	    Mat image= new Mat();
	    Imgproc.dilate(newResImg, image, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)),new Point(0,0), 3);
	    
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(image),"DILATED");
	    
	    
	    /*
	     * USING BACKGROUND SUBTRACTOR
	     */
	    
	    BackgroundSubtractorMOG2 bg=new BackgroundSubtractorMOG2();	
	    Mat fgmask = new Mat();
	    double learningRate = 0.05;
	    bg.apply(bckg, source.clone(),learningRate);
	    bg.apply(source.clone(), fgmask,learningRate);
	    
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(fgmask),"REMOVED   "+learningRate);
	    
		//7. draw contours
	    
	    //first add border both to initial and resulting images
	    
        Mat finalResultingImage = addBorder(newResImg, 0.2, new Scalar(255,255,255));
        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalResultingImage),"finalResultingImage ENLARGED");
        
        
       
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
        //contour extraction
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
	    //to store the recognized contours
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		 
		//recognize contours
		Imgproc.findContours(finalResultingImage.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
	     
		 
		//keep the destination to draw the extracted rectangles on it
	
	     
	     
//	     
//	     Imgproc.drawContours(destination, contours, -1, new Scalar(0,0,255));
//		
//	     ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"CONTOURS");
//			
//	    
		///////////////////////////////////////////////////////////////////////////////////////////////////////
	     
	   //to save contour areas for further analysis
	     
//	     ArrayList<RotatedRect> shoesRectangle = new ArrayList<RotatedRect>();
//	     
//	     ArrayList<Double> contourAreas = new ArrayList<Double>();
//	     
//	     for(int i=0;i<contours.size()-1;i++){
//	    	 
//	    	 MatOfPoint2f approx =  new MatOfPoint2f();
//	    	 
//	    	 //get current contour elements
//	    	 MatOfPoint tempContour=contours.get(i);
//	    	 
//	    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
//	    	 
//	    	 int contourSize = (int)tempContour.total();
//	    	 
//	    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
//	    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
//	    	 Float[] tempArray=new Float[2];
//
//	    	 contourAreas.add(Math.abs(Imgproc.contourArea(tempContour)));
//
//	    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
//	    		 	
//	    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
//	    		 	
//	    		 	shoesRectangle.add(r);
//	    		 	
//	    		 	//DRAW THE BOUNDING RECTANGLE
//	    		 	
//  			   		//area of the approximated polygon
//  			   		double area = Imgproc.contourArea(newMat);
//  			   		Imgproc.drawContours(destination, contours,i, new Scalar(0, 0, 250),2);
//  			   		Point points[] = new Point[4];
//  			   		r.points(points);
//  			   		
//  			   		if(r.size.height>r.size.width){
//  			   			
//  			   			tempArray[0]=(float) r.size.height;
//  			   			tempArray[1]=(float) r.size.width;
//  			   			
//  			   		}
//  			   		else{
//  			   		
//  			   			tempArray[0]=(float) r.size.width;
//  			   			tempArray[1]=(float) r.size.height;
//  			   			
//  			   		}
//  			   	
//  			   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
//  			   		String textH=" h = "+String.format("%.2f",tempArray[1]);
//  			   		
//  			   		
//  			   		
//  			   		for(int pt=0; pt<4; ++pt){
//  			   			Core.line(destination, points[pt], points[(pt+1)%4], new Scalar(0,255,0),2);
//  			   			}
//  			   		Core.putText(destination, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
//  			   		Core.putText(destination, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
//	    	 		}//end of if (checking the contours area)
//
//  			   		
//  			}//end of for loop
//	     
//	    	     
//	     ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"Shoes only with contours without morphological transform");
//	     
	   
	     
	     
	  Mat destination = drawShoeRectangles(contours, finalResultingImage.clone());
	  ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"Shoes only with contours without morphological transform");
//	  Highgui.imwrite(initialImagePath+"contours"+filename+".jpg", destination);   
	     
	     
	   //remove background of the source image
	     
	   //to store image with removed background
		//Mat backgroundRemoved = new Mat();
				
//		//REMOVE BACKGROUND
//		org.opencv.core.Core.subtract(source.clone(), bckg, backgroundRemoved);
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved),"Original with removed background");
//		 
	    //add a border to picture with removed background 
	     
		Mat newSource=addBorder(backgroundRemoved, 0.2,new Scalar(0,0,0));

        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(newSource),"SOURCE ENLARGED");
	     
	     
        
        //display extracted rectangles
        
        ArrayList<RotatedRect> shoesRectangle = findShoesBoundingRectangles( contours);
        
        System.out.println(shoesRectangle.size());
        
        ArrayList<Mat> shoesOnlyExtracted = cropShoesFromImage(newSource, shoesRectangle);
        
        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(shoesOnlyExtracted.get(0)),"SHOES");
//        Highgui.imwrite(initialImagePath+"shoes1"+filename+".jpg", shoesOnlyExtracted.get(0)); 
//        Highgui.imwrite(initialImagePath+"shoes2"+filename+".jpg", shoesOnlyExtracted.get(1)); 
        
        Mat copyOfTempImage = shoesOnlyExtracted.get(0);
        
        if(copyOfTempImage.rows()<copyOfTempImage.cols()){
        	Core.transpose(copyOfTempImage, copyOfTempImage);
        	Core.flip(copyOfTempImage, copyOfTempImage, 0);
        }
        	
		
		
        
        Imgproc.resize(copyOfTempImage, copyOfTempImage, new Size(300,500));
        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(copyOfTempImage),"SHOES");
        Highgui.imwrite(initialImagePath+"/extracted_9/"+filename+".jpg", copyOfTempImage);
		
	}//end of main
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * compares images
	 * @param image
	 * @param background
	 * @return
	 */
	public static boolean compareImages(Mat image, Mat background, int pixelThreshold){

		boolean result = false;
		Size sizeSource = image.size();
		int total=0;
		int check=0;
		for (int i = 0; i < sizeSource.height; i++)
		    for (int j = 0; j < sizeSource.width; j++) {
		    	total+=1;
		        double[] dataSrc = image.get(i, j);
		        double[] dataBckg = background.get(i, j);
		        if(java.lang.Math.abs(dataSrc[0]-dataBckg[0])>= pixelThreshold){
		        	check+=1;
		        }  
		    }
		if((double)check/total<=0.1)
			result=true;
		System.out.println((double)check/total);
		return result;
	}
	
	
	
	
	
	
	
	/**
	 * removes background
	 * @param image
	 * @param background
	 * @return
	 */
	public static Mat removeBackground(Mat image, Mat background, int pixelThreshold){

		 Mat result = new Mat(image.rows(),image.cols(), CvType.CV_8UC1, new Scalar(255,255,255));
			Size sizeSource = image.size();
			for (int i = 0; i < sizeSource.height; i++)
			    for (int j = 0; j < sizeSource.width; j++) {
			        double[] dataSrc = image.get(i, j);
			        double[] dataBckg = background.get(i, j);
			        if(java.lang.Math.abs(dataSrc[0]-dataBckg[0])>= pixelThreshold){
			        	result.put(i, j, dataSrc[0]);
			        }  
			    }

		 return result;
	}
	
	
	/**
	 * adds border to image
	 * @param image
	 * @return
	 */
	public static Mat addBorder(Mat image, double borderSize, Scalar color){
		int top, bottom, left, right;
		/// Initialize arguments for the filter
        top = (int) (borderSize*image.rows()); 
        bottom = (int) (borderSize*image.rows());
        left = (int) (borderSize*image.cols()); 
        right = (int) (borderSize*image.cols());

        Mat result=new Mat();
        //add border to resulting image with black shoes contours
        Imgproc.copyMakeBorder(image, result, top, bottom, left, right, Imgproc.BORDER_CONSTANT,color);
        
        return result;
        
	}
	
	
	
	/**
	 * returns found shoe rectangles
	 * @param contours
	 * @return
	 */
	
	public static ArrayList<RotatedRect> findShoesBoundingRectangles(List <MatOfPoint> contours){
		ArrayList<RotatedRect> shoesRectangle = new ArrayList<RotatedRect>();
	     
	     for(int i=0;i<contours.size()-1;i++){
	    	 
	    	 MatOfPoint2f approx =  new MatOfPoint2f();
	    	 
	    	 //get current contour elements
	    	 MatOfPoint tempContour=contours.get(i);
	    	 
	    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
	    	 
	    	 int contourSize = (int)tempContour.total();
	    	 
	    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
	    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
	    	 Float[] tempArray=new Float[2];

	    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
	    		 	
	    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
	    		 	
	    		 	shoesRectangle.add(r);
	    		 	}
	    	 }//end of for loop
	     return shoesRectangle;
	}
	
	/**
	 * Draw bounding rectangles for determined shoes on the picture
	 * @param contours
	 * @param image
	 * @return
	 */
	public static Mat drawShoeRectangles(List <MatOfPoint> contours, Mat image){
		Mat result =  new Mat();
		Imgproc.cvtColor(image, result, Imgproc.COLOR_GRAY2BGR);
		ArrayList<RotatedRect> shoesRectangle = new ArrayList<RotatedRect>();
		for(int i=0;i<contours.size()-1;i++){
	    	 
	    	 MatOfPoint2f approx =  new MatOfPoint2f();
	    	 
	    	 //get current contour elements
	    	 MatOfPoint tempContour=contours.get(i);
	    	 
	    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
	    	 
	    	 int contourSize = (int)tempContour.total();
	    	 
	    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
	    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
	    	 Float[] tempArray=new Float[2];

	    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
	    		 	
	    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
	    		 	
	    		 	shoesRectangle.add(r);
	    		 	
	    		 	//DRAW THE BOUNDING RECTANGLE
	    		 	
 			   		//area of the approximated polygon
 			   		double area = Imgproc.contourArea(newMat);
 			   		Imgproc.drawContours(result, contours,i, new Scalar(0, 0, 250),2);
 			   		Point points[] = new Point[4];
 			   		r.points(points);
 			   		
 			   		if(r.size.height>r.size.width){
 			   			
 			   			tempArray[0]=(float) r.size.height;
 			   			tempArray[1]=(float) r.size.width;
 			   			
 			   		}
 			   		else{
 			   		
 			   			tempArray[0]=(float) r.size.width;
 			   			tempArray[1]=(float) r.size.height;
 			   			
 			   		}
 			   	
 			   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
 			   		String textH=" h = "+String.format("%.2f",tempArray[1]);
 			   		
 			   		
 			   		
 			   		for(int pt=0; pt<4; ++pt){
 			   			Core.line(result, points[pt], points[(pt+1)%4], new Scalar(0,255,0),2);
 			   			}
 			   		Core.putText(result, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
 			   		Core.putText(result, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
	    	 		}//end of if (checking the contours area)

 			   		
 			}//end of for loop
	    
		return result;
	}
	
	
	
	
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 
	 * @param index
	 * @param masks
	 * @param image
	 * @return
	 */
	public static ArrayList<Mat> cropShoesFromImage(Mat image, ArrayList<RotatedRect> masks){
		ArrayList<Mat> result=new ArrayList<Mat>();
		//ArrayList<RotatedRect> masks= extractShoesOnly_v1(image);
		
	
		Mat rotated=new Mat();
		int top, bottom, left, right;
		/// Initialize arguments for the filter
        top = (int) (0.1*image.rows()); 
        bottom = (int) (0.1*image.rows());
        left = (int) (0.1*image.cols()); 
        right = (int) (0.1*image.cols());
        Mat src=new Mat();
        //destination = source;
        Imgproc.copyMakeBorder(image, src, top, bottom, left, right, Imgproc.BORDER_CONSTANT, new Scalar(0,0,0));
        
        
		for(int i=0;i<masks.size();i++){
			RotatedRect rect= masks.get(i);

			// matrices we'll use
			
			// get angle and size from the bounding box
			float angle = (float) rect.angle;
			Size rect_size = rect.size;
			// thanks to http://felix.abecassis.me/2011/10/opencv-rotation-deskewing/
			if (rect.angle < -45.) {
			    angle += 90.0;
			    rect_size=swap(rect_size.width, rect_size.height);
			}
			
			rect.center=new Point(rect.center.x+left, rect.center.y+top);
			Mat M=image.clone();
			
			Mat cropped=new Mat();
			// get the rotation matrix
			M = org.opencv.imgproc.Imgproc.getRotationMatrix2D(rect.center, angle, 1.0);
			// perform the affine transformation on your image in src,
			// the result is the rotated image in rotated. I am doing
			// cubic interpolation here
			org.opencv.imgproc.Imgproc.warpAffine(src, rotated, M, src.size(), org.opencv.imgproc.Imgproc.INTER_LINEAR);
			
			// crop the resulting image, which is then given in cropped
			org.opencv.imgproc.Imgproc.getRectSubPix(rotated, rect_size, rect.center, cropped);
			result.add(cropped);
			//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(rotated),"rotated");
			//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(cropped),"Cropped");
			
		}
		
		return result;
		
	}
	
	
	
	
	
	/**
	 * 
	 * @param w
	 * @param h
	 * @return
	 */
	public static Size  swap(double w, double h){
		Size rect_size=new Size(h,w);
		return rect_size;

	}
	
	
	
	
}





//
//
////to store image with removed background
//Mat backgroundRemoved = new Mat();
//
//////increase the contrast of background
////Mat imgBckgHigh =  new Mat();
////finImg.convertTo(imgBckgHigh, -1, 5, 0);//increase the contrast (double)
////ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgBckgHigh),"High contrast background");
////
//////increase the contrast of image
////Mat imgH =  new Mat();
////source.convertTo(imgH, -1, 5, 0);//increase the contrast (double)
////ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgH),"High contrast image");
////
////
//
//
//		
//////REMOVE BACKGROUND
////org.opencv.core.Core.subtract(source.clone(), finImg, backgroundRemoved);
////
////ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved),"difference btw main image and background using subtract");
//
//backgroundRemoved=resImg;
//Mat onlyShoes= new Mat();
//
//org.opencv.core.Core.inRange(backgroundRemoved, new Scalar(0, 0, 0), new Scalar(15, 15, 15), onlyShoes);
//
//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoes),"onlyShoes");
//
//
//Mat mIntermediateMat = onlyShoes;
//
//
//// 4) Dilate -> fill the image using the MORPH_DILATE
//Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_DILATE, new Size(3,3), new Point(1,1));
//Imgproc.dilate(mIntermediateMat, mIntermediateMat, kernel);
//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(mIntermediateMat)," after MORPH_DILATE");
//
//Imgproc.erode(mIntermediateMat, mIntermediateMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5,5)));
//
//
//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(mIntermediateMat)," after erode");
//
////org.opencv.core.Core.absdiff(source.clone(), bckg, backgroundRemoved);
////ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved),"difference btw main image and background using Abs Diff");
//
//
//
//
//// //to store the recognized contours
//// List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
//// 
//// //recognize contours
//// Imgproc.findContours(onlyShoes.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
//// 
//// 
//////keep the destination to draw the extracted rectangles on it
//// Mat destination=onlyShoes.clone();
//// 
//// Imgproc.cvtColor(destination, destination, Imgproc.COLOR_GRAY2BGR);
//// 
//// Imgproc.drawContours(destination, contours, -1, new Scalar(0,0,255));
////
//// ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"CONTOURS");
////	
//
// //////////////////////////////////////////////////////////////////////////////////////////////////
 