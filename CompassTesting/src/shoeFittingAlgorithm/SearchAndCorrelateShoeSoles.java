package shoeFittingAlgorithm;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * search db and match existing shoe pairs
 * @author nimesha
 *
 */
public class SearchAndCorrelateShoeSoles implements Runnable {
	
	private int inLines    = 0;
	private int inCircles  = 0;
	
	private int fileID;
	private String filename;
	
	private int[] shoeSize  = null;
	
	
	private AttributesMatrix[][] attributesMatrix  = null;
	
	
	private ArrayList<double[]> arrTotalMatch   = null;
	private ArrayList<double[]> arrPartialMatch = null;
	
	private ArrayList<Thread> arrCompareRelationalGraphsOfshoes_T = null;
	private ArrayList<ShoesInfo> arrDbShoesInfo = null;
	
	public SearchAndCorrelateShoeSoles() {
		
	}
	
	@Override
	public void run() {
		try {
			readExistingShoesFromDB();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * takes number of lines and circles in both shoes and attribute matrices related to both shoes.
	 * @param inLinesLeft number of lines in left shoe
	 * @param inCirclesLeft number of circles in left shoe
	 * @param inLinesRight number of lines in right shoe
	 * @param inCirclesRight number of circles in right shoe
	 * @param attributesMatrix_Left attribute matrix in left shoe
	 * @param attributesMatrix_Right attribute matrix in right shoe
	 */
	public void SetDataForSearchAndCorrelateShoeSoles(int fileID, String filename, int[] shoeSize,  int inLines, int inCircles, AttributesMatrix[][] attributesMatrix) {
		
		this.fileID=fileID;
		this.filename=filename;
		this.attributesMatrix  = attributesMatrix;
		
		
		this.inLines    = inLines;
		this.inCircles  = inCircles;
		
		
		this.shoeSize  = shoeSize;
	
		arrTotalMatch   = new ArrayList<double[]>();
		arrPartialMatch = new ArrayList<double[]>();
		
		arrCompareRelationalGraphsOfshoes_T = new ArrayList<Thread>();
	}
	
	/**
	 * start shoe comparison algorithm.
	 * Initialize a thread per row in the database for comparison.
	 * Before compare matrices it selects possible candidates by comparing the no of lines and circles up to some threshold
	 */
	public void startShoeMatching(boolean bUpdateAllShoesDB) {
		try {
			////////////////////////////////////////////
			long lStartTime = new Date().getTime(); // start time
			////////////////////////////////////////////	

			ArrayList<ShoesInfo> arrExistingShoesInfo = getArrDbShoesInfo();
			System.out.println("arrExistingShoesInfo "+arrExistingShoesInfo.size());
			if (arrExistingShoesInfo.size() > 0) {
				for (int i = 0; i < arrExistingShoesInfo.size(); i++) {
					ShoesInfo shoesInfo = arrExistingShoesInfo.get(i);
					
					//System.out.println(compareLinesAndCircles(shoesInfo));
					
					if (compareLinesAndCircles(shoesInfo)) {//simple comparison test with lines and circles
						CompareRelationalGraphsOfshoes_T compareRelationalGraphsOfshoes_T = new CompareRelationalGraphsOfshoes_T(shoesInfo, attributesMatrix, arrTotalMatch, arrPartialMatch);
						Thread threadCompareGraphs = new Thread(compareRelationalGraphsOfshoes_T);
						threadCompareGraphs.start();
						arrCompareRelationalGraphsOfshoes_T.add(threadCompareGraphs);
					}
				}
				//join threads
				for (int i = 0; i < arrCompareRelationalGraphsOfshoes_T.size(); i++) {
					Thread tCompareGraphs = arrCompareRelationalGraphsOfshoes_T.get(i);
					tCompareGraphs.join();
				}
				
			}
			
			///////////////////////////////////////////
			long lEndTime = new Date().getTime(); // end time
			long difference = lEndTime - lStartTime; // check difference
			System.out.println("^^^^^^^^^^^^^^^^ individual timings milliseconds: " + difference);
			lStartTime = new Date().getTime(); // start time
			///////////////////////////////////////////

			selectBestMatch(bUpdateAllShoesDB); 


			}catch(Exception ex) {
				ex.printStackTrace();
			}
	}
	
	/**
	 * once the entries in the DB are compared and returned with most likely matches this function selects the best match.
	 * And if no match found it calls the DB to update as a new pair of shoes
	 * @throws IOException
	 * @throws SQLException
	 */
	private void selectBestMatch(boolean bUpdateAllShoesDB) throws IOException, SQLException {
		double largeValue = 0;
		double shoeID = -1;
		boolean bMatchFound = false;
		System.out.println("arrTotalMatch.size() "+arrTotalMatch.size());
		System.out.println("arrPartialMatch.size() "+arrPartialMatch.size());
		if (arrTotalMatch.size() > 0) {
			for (int i = 0; i < arrTotalMatch.size(); i++) {
				double[] arrMatched = arrTotalMatch.get(i);
				
				System.out.println("##### MATCHED WITH: " + (int)arrMatched[0] + " Accuracy: " + (arrMatched[1]) + "%");
				
				if (i == 0) {
					largeValue = (arrMatched[1]);
					shoeID = arrMatched[0];
				}
				//TODO:  
				//don't understand this logic
				if (largeValue < arrMatched[1] ) {
					largeValue = arrMatched[1];
					shoeID = arrMatched[0];
				}
				
			}
			if (shoeID != -1) {
//				
//				String sDestination = getDirectionFromDB((int)shoeID).trim();
//				Compass.sCurrentDestination = sDestination;
//				if ( (!bUpdateAllShoesDB) || (Compass.iSomeoneThere == 1) )
//					Compass.setMessageToSend("DESTINATION#" + sDestination + "#UIMESSAGE#" + Properties.msgSelectNewDestination);
				bMatchFound = true;
				System.out.println("BEST MATCH: " + (int)shoeID);// + " you are going to: " + sDestination + " Accuracy: " + largeValue + "%");
//				Compass.iCurrentShoeID = (int)shoeID;
//				
//				if (Compass.iCurrentAllShoesID > 0) { //update matched time-stamp
//					BackupData backupData = new BackupData();
//					backupData.updateMatchedTimestamp(Compass.iCurrentAllShoesID);
//				}
			}
		}
		else if (arrPartialMatch.size() > 0) {
			//to handle partial match
		}
		//System.out.println("Destination:::::::::" + Compass.getMessageFromServer());
		if (!bMatchFound) { //new pair
			//String sDestinationFromServer = Compass.getMessageFromServer();
			//System.out.println("@@@@@@@@@@@@@@@@@@@@@@@ 1: " + Compass.getMessageFromServer());
			updateDB();
			
		}
		
	}
	
	/**
	 * compare no of lines and circles against the db entries to pre-select candidates
	 * @param iShoeID
	 * @return
	 * @throws SQLException
	 */
	private boolean compareLinesAndCircles(ShoesInfo shoesInfo) throws SQLException {
		
		//DatabaseReadWrite dbconnection = new DatabaseReadWrite();
		//read from db
		//int[] iarrlinecircleinfo = dbconnection.readLinesCirclesAndShoeSizeInfoFromDB(iShoeID);
		int[] iarrlinecircleinfo = shoesInfo.getLinecircleinfo();
//		System.out.println(shoeSize[0]);
//		System.out.println(shoeSize[1]);
//		System.out.println(inLines);
//		System.out.println(inCircles);
//		System.out.println("Math.abs(iarrlinecircleinfo[0] - shoeSize[0] ) "+Math.abs(iarrlinecircleinfo[0] - shoeSize[0] ));
		if (    ( Math.abs(iarrlinecircleinfo[0] - shoeSize[0] ) < 50 ) && //shoe sizes
				( Math.abs(iarrlinecircleinfo[1] - shoeSize[1] ) < 50 ) &&
				
				( Math.abs(iarrlinecircleinfo[2] - inLines    )  < 40 ) && //number of lines and circles
				( Math.abs(iarrlinecircleinfo[3] - inCircles  )  < 30 ) 
				
				//( iarrlinecircleinfo[8] == Compass.iBlackPlainDetected )     
				
				)  {
			
			return true;
		}
		
		return false;
	}
	
//	/**
//	 * get direction from the DB using the shoeid
//	 * @param shoeID
//	 * @return
//	 * @throws SQLException
//	 */
//	private String getDirectionFromDB(int shoeID) throws SQLException {
//		DatabaseReadWrite dbconnection = new DatabaseReadWrite();
//		//read from db
//		String sDirection = dbconnection.readDirectionFromDB(shoeID);
//		return sDirection;
//	}
	
	/**
	 * read all the existing shoes from DB
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException
	 */
	private void readExistingShoesFromDB() throws ClassNotFoundException, SQLException, IOException {
		DatabaseReadWrite dbconnection = new DatabaseReadWrite();
		//read from db
		setArrDbShoesInfo(dbconnection.readShoeInfoFromDB());
		//return arrShoesInfo;
	}
	
	/**
	 * update db when a new pair is detected
	 * @throws IOException
	 * @throws SQLException
	 */
	private void updateDB() throws IOException, SQLException {
		
		DatabaseReadWrite dbcon = new DatabaseReadWrite();
		dbcon.setShoeInfoWriteToDB(fileID, filename, shoeSize, inLines, inCircles, attributesMatrix);
		dbcon.writeShoeInfoToDB();
//		Thread tUpdateShoeInfo = new Thread(dbcon);
//		tUpdateShoeInfo.start();
	}
	
	public ArrayList<ShoesInfo> getArrDbShoesInfo() {
		return arrDbShoesInfo;
	}
	
	public void setArrDbShoesInfo(ArrayList<ShoesInfo> arrDbShoesInfo) {
		this.arrDbShoesInfo = arrDbShoesInfo;
	}

	
	
}


