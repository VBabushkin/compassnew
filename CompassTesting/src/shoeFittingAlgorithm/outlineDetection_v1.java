package shoeFittingAlgorithm;

import blobDetection.BlobDetection;
import static org.bytedeco.javacpp.opencv_core.CV_FILLED;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY;
import static org.bytedeco.javacpp.opencv_imgproc.cvThreshold;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_highgui.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Point2f;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.video.BackgroundSubtractorMOG;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opencv.imgproc.Imgproc;

import com.mysql.jdbc.PreparedStatement;

import blobDetection.Blob;
import blobDetection.BlobDetection;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import org.opencv.imgproc.Moments;


public class outlineDetection_v1 {
	
final static int EDGE_LENGTH=20;
	
	//threshold to set the minimum area of contours that can be considered as shoes
	//TODO: needs to be analyzed and selected the optimal value
	final static int LOWER_CONTOUR_AREA_THRESHOLD=10000;
	final static int UPPER_CONTOUR_AREA_THRESHOLD=150000;
	final static int SHOE_SIZE_AREA_DIFFERENCE=14000;
	final static double UPPER_RATIO_VALUE=2.8;
	final static double LOWER_RATIO_VALUE=2.1;
	
	
	static String initialImagePath=".//CollectedShoePictures//";//".//results//in.jpg";
	static String processedImagesPath=".//CollectedShoePictures//Processed//";
	static String resultImagesPath=".//CollectedShoePictures//SizeExtracted//";
	static String bckgImage =".//CollectedShoePictures//background.jpg";//".//results//bckg.jpg";
	
	
	
	static BlobDetection theBlobDetection;
	static float threshold= 0.88f;
	
	
	
	/**
	 * 
	 * @param image -- a processed image with removed background
	 * @return an ArrayList of 2D arays containing shoes ROI
	 */
	public static ArrayList<RotatedRect> extractShoesOnly_v1(Mat image){
		Mat onlyShoes=new Mat();
		//remove black color of background 
		//TODO: needs to be parametrized and checked for different gray background color ranges
		org.opencv.core.Core.inRange(image, new Scalar(0, 0, 0), new Scalar(14, 14, 14), onlyShoes);
		Mat onlyShoesCopy = onlyShoes.clone();
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoesCopy),"Shoes only");
		
		
		 //to store the recognized contours
		 List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		 
		 
		 //FIRST CHECK IF TWO CONTOURS ARE CLEARLY SEPARATED FROM INITIAL IMAGE:
		 
		 //recognize contours
	     Imgproc.findContours(onlyShoes.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
	     
	     //keep the destination to draw the extracted rectangles on it
	     Mat destination=onlyShoes.clone();
	     
	     Imgproc.cvtColor(destination, destination, Imgproc.COLOR_GRAY2BGR);
	     
	     Imgproc.drawContours(destination, contours, -1, new Scalar(0,0,255));
	     
	     //store extracted sizes in the list of arrays of two elements [height][width]
	     //height is bigger then width by assumption 
	     ArrayList<RotatedRect> shoesOnly =new ArrayList<RotatedRect>();
	     
	     //to save contour areas for further analysis
	     ArrayList<Double> contourAreas = new ArrayList<Double>();
	     
	     for(int i=0;i<contours.size()-1;i++){
	    	 
	    	 MatOfPoint2f approx =  new MatOfPoint2f();
	    	 
	    	 //get current contour elements
	    	 MatOfPoint tempContour=contours.get(i);
	    	 
	    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
	    	 
	    	 int contourSize = (int)tempContour.total();
	    	 
	    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
	    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
	    	 Float[] tempArray=new Float[2];
	    	 
	    	 //System.out.println("CURRENT CONTOUR AREA: "+Math.abs(Imgproc.contourArea(tempContour)));
	    	//if contour is big enough
	    	 //TODO check the threshold for min area
	    	//System.out.println("Countour "+i+ " area is "+Math.abs(Imgproc.contourArea(tempContour)));
	    	 contourAreas.add(Math.abs(Imgproc.contourArea(tempContour)));
	    	//Imgproc.drawContours(destination, contours, i, new Scalar(10*i,10*i,0),3);
	    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
	    		 	
	    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
	    		 	
	    		 	shoesOnly.add(r);
	    		 	
	    		 	//DRAW THE BOUNDING RECTANGLE
	    		 	
  			   		//area of the approximated polygon
  			   		double area = Imgproc.contourArea(newMat);
  			   		Imgproc.drawContours(destination, contours,i, new Scalar(0, 0, 250),2);
  			   		Point points[] = new Point[4];
  			   		r.points(points);
  			   		System.out.println();
  			   		System.out.println("Size of the shoe is:\nWidth "+ r.size.width+"\nHeight "+r.size.height);
  			   		
  			   		if(r.size.height>r.size.width){
  			   			
  			   			tempArray[0]=(float) r.size.height;
  			   			tempArray[1]=(float) r.size.width;
  			   			
  			   		}
  			   		else{
  			   		
  			   			tempArray[0]=(float) r.size.width;
  			   			tempArray[1]=(float) r.size.height;
  			   			
  			   		}
  			   	
  			   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
  			   		String textH=" h = "+String.format("%.2f",tempArray[1]);
  			   		
  			   		
  			   		
  			   		for(int pt=0; pt<4; ++pt){
  			   			Core.line(destination, points[pt], points[(pt+1)%4], new Scalar(0,255,0),2);
  			   			}
  			   		Core.putText(destination, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
  			   		Core.putText(destination, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
	    	 		}//end of if (checking the contours area)

  			   		
  			}//end of for loop
	     
	    	     
	     ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"Shoes only with contours without morphological transform");
	     System.out.println("Without morphological transform found contours areas: ");
	     
	     Collections.sort(contourAreas);
	     
//	     for(int j=0;j<contourAreas.size();j++)
//	    	 System.out.println("THE CONTOUR AREA IS: "+ contourAreas.get(j));
	     
	     System.out.println("NUMBER OF CONTOURS FOUND WITHOUT MORPHOLOGICAL TRANSFORM "+shoesOnly.size());

	     //if one contour found but it's area is within the allowed limits --just return it to split by 2
	     if(shoesOnly.size()==1 && shoesOnly.get(0).size.width*shoesOnly.get(0).size.height>LOWER_CONTOUR_AREA_THRESHOLD &&shoesOnly.get(0).size.width*shoesOnly.get(0).size.height <UPPER_CONTOUR_AREA_THRESHOLD){
	    	 return shoesOnly;
	     }
	    
	     
	     
	     
	     
	     //Apply morphological transform for better detection
	     
	   //to save contour areas for further analysis
	     ArrayList<Double> contourAreas_1 = new ArrayList<Double>();
	     
	     //Add something here to discriminate based on areas
	     //if 0 or more then 2 contours found...
	     //or if the sizes of found two contours are different (areas differ by difference value)
	     //TODO:
	     int difference =SHOE_SIZE_AREA_DIFFERENCE;
	     if(shoesOnly.size()==0 || shoesOnly.size()>2 || (shoesOnly.size()==2 
	    		 && (shoesOnly.get(0).size.width*shoesOnly.get(0).size.height-shoesOnly.get(1).size.width*shoesOnly.get(1).size.height> difference)
	    		 ||((shoesOnly.get(0).size.width/shoesOnly.get(0).size.height<LOWER_RATIO_VALUE && shoesOnly.get(0).size.width/shoesOnly.get(0).size.height>UPPER_RATIO_VALUE) 
	    		|| (shoesOnly.get(1).size.width/shoesOnly.get(1).size.height<LOWER_RATIO_VALUE && shoesOnly.get(1).size.width/shoesOnly.get(1).size.height>UPPER_RATIO_VALUE)))){
	    	 contours = new ArrayList<MatOfPoint>();
	    	 shoesOnly =new ArrayList<RotatedRect>();
	    	 Imgproc.erode(onlyShoes, onlyShoes, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15,15)));
	    	 ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoes),"After morphological closing");
	    	 //TODO: MAKE IT AS FUNCTION
	    	 
	    	//recognize contours
		     Imgproc.findContours(onlyShoes.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
		     
		     //keep the destination to draw the extracted rectangles on it
		     destination=onlyShoes.clone();
		     
		     Imgproc.cvtColor(destination, destination, Imgproc.COLOR_GRAY2BGR);
		     
		     Imgproc.drawContours(destination, contours, -1, new Scalar(0,0,255));
		     
		     //store extracted sizes in the list of arrays of two elements [height][width]
		     //height is bigger then width by assumption 
		     
		  
		     for(int i=0;i<contours.size()-1;i++){
		    	 
		    	 MatOfPoint2f approx =  new MatOfPoint2f();
		    	 
		    	 //get current contour elements
		    	 MatOfPoint tempContour=contours.get(i);
		    	 
		    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
		    	 
		    	 int contourSize = (int)tempContour.total();
		    	 
		    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
		    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
		    	 Float[] tempArray=new Float[2];
		    	 
		    	 //System.out.println("CURRENT CONTOUR AREA: "+Math.abs(Imgproc.contourArea(tempContour)));
		    	//if contour is big enough
		    	 //TODO check the threshold for min area
		    	//System.out.println("Countour "+i+ " area is "+Math.abs(Imgproc.contourArea(tempContour)));
		    	 
		    	 contourAreas_1.add(Math.abs(Imgproc.contourArea(tempContour)));
		    	//Imgproc.drawContours(destination, contours, i, new Scalar(10*i,10*i,0),3);
		    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
		    		 	
		    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
		    		 	shoesOnly.add(r);
		    		 	
		    		 	//DRAW THE BOUNDING RECTANGLE
		    		 	
	  			   		//area of the approximated polygon
	  			   		double area = Imgproc.contourArea(newMat);
	  			   		Imgproc.drawContours(destination, contours,i, new Scalar(0, 0, 250),2);
	  			   		Point points[] = new Point[4];
	  			   		r.points(points);
	  			   		System.out.println();
	  			   		System.out.println("Size of the shoe is:\nWidth "+ r.size.width+"\nHeight "+r.size.height);
	  			   		
	  			   		if(r.size.height>r.size.width){
	  			   			
	  			   			tempArray[0]=(float) r.size.height;
	  			   			tempArray[1]=(float) r.size.width;
	  			   			
	  			   		}
	  			   		else{
	  			   		
	  			   			tempArray[0]=(float) r.size.width;
	  			   			tempArray[1]=(float) r.size.height;
	  			   			
	  			   		}
	  			   	
	  			   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
	  			   		String textH=" h = "+String.format("%.2f",tempArray[1]);
	  			   		
	  			   		
	  			   		
	  			   		for(int pt=0; pt<4; ++pt){
	  			   			Core.line(destination, points[pt], points[(pt+1)%4], new Scalar(0,255,0),2);
	  			   			}
	  			   		Core.putText(destination, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
	  			   		Core.putText(destination, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
		    	 		}//end of if (checking the contours area)

	  			   		
	  			}
	    	 
	     }
	    
	     
	     
	     
	     
	     ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"Shoes only with contours after morphological transform");
	     //ImageIO.write(ProcessImages.Mat2BufferedImage(destination), "JPG", new File(extractedSizeFilePath));
	     System.out.println("Size of Shoes Only "+ shoesOnly.size());
	     
	     
	     System.out.println();
	     System.out.println();
	     Collections.sort(contourAreas_1);
	     
//	     for(int j=0;j<contourAreas_1.size();j++)
//	    	 System.out.println("THE CONTOUR AREA IS: "+ contourAreas_1.get(j));
	     
	     
	     return shoesOnly;
	     
	}
	
	
	
	
	
	/**
	 * 
	 * @param index
	 * @param masks
	 * @param image
	 * @return
	 */
	public static ArrayList<Mat> cropShoesFromImage(Mat image){
		ArrayList<Mat> result=new ArrayList<Mat>();
		ArrayList<RotatedRect> masks= extractShoesOnly_v1(image);
		Mat rotated=new Mat();
		int top, bottom, left, right;
		/// Initialize arguments for the filter
        top = (int) (0.1*image.rows()); 
        bottom = (int) (0.1*image.rows());
        left = (int) (0.1*image.cols()); 
        right = (int) (0.1*image.cols());
        Mat src=new Mat();
        //destination = source;
        Imgproc.copyMakeBorder(image, src, top, bottom, left, right, Imgproc.BORDER_CONSTANT, new Scalar(0,0,0));
        
        
		for(int i=0;i<masks.size();i++){
			RotatedRect rect= masks.get(i);

			// matrices we'll use
			
			// get angle and size from the bounding box
			float angle = (float) rect.angle;
			Size rect_size = rect.size;
			// thanks to http://felix.abecassis.me/2011/10/opencv-rotation-deskewing/
			if (rect.angle < -45.) {
			    angle += 90.0;
			    rect_size=swap(rect_size.width, rect_size.height);
			}
			
			rect.center=new Point(rect.center.x+left, rect.center.y+top);
			Mat M=image.clone();
			
			Mat cropped=new Mat();
			// get the rotation matrix
			M = org.opencv.imgproc.Imgproc.getRotationMatrix2D(rect.center, angle, 1.0);
			// perform the affine transformation on your image in src,
			// the result is the rotated image in rotated. I am doing
			// cubic interpolation here
			org.opencv.imgproc.Imgproc.warpAffine(src, rotated, M, src.size(), org.opencv.imgproc.Imgproc.INTER_LINEAR);
			
			// crop the resulting image, which is then given in cropped
			org.opencv.imgproc.Imgproc.getRectSubPix(rotated, rect_size, rect.center, cropped);
			result.add(cropped);
//			ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(rotated),"rotated");
			ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(cropped),"Cropped");
		}
		
		return result;
		
	}
	/**
	 * 
	 * @param w
	 * @param h
	 * @return
	 */
	public static Size  swap(double w, double h){
		Size rect_size=new Size(h,w);
		return rect_size;

	}
	
	
	
	public static int[] getSize(Mat image){
		int result[] = new int[2];
		if(image.height()> image.width()){
			result[0]=image.height();
			result[1]=image.width();
		}
		else{
			result[1]=image.height();
			result[0]=image.width();
		}
		return result;
	}
	
	
	
	
	 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * 
		 * @param args
		 * @throws IOException
		 */
		public static void main(String args[]) throws IOException{
			//TODO:
			//devise a logic to determie whether the taken pisture valid
			//devise a function to sort records according to shoe sizes categorization -- small, medium, large 
			//make everything tidy and accurate
			//create a relational DB to store all records
			//devise a mechanism to calculate a distance between new and old records and find a better match
			//create an interface may be with parametrized values to simplify the analysis
			//
			//
			//TODO: test
			int userID=16;
			int id=1;
			String filename= "shoes_"+userID+"_135";
			//String filename="randomAngle_"+userID;
			//String filename= "shoes_"+userID+"_half_"+3;
			String backgroundFileName="background"+userID;
			String initPath=initialImagePath+filename+".jpg";
			String bckgImagePath=initialImagePath+backgroundFileName+".jpg";
			String backgroundRemovedFilePath=processedImagesPath+"proc_"+filename+".jpg";
			String extractedSizeFilePath=resultImagesPath+"size_"+filename+".jpg";
			
			//load library
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
			
			//read image
			Mat source = Highgui.imread(initPath, Highgui.CV_LOAD_IMAGE_COLOR);
			
			//crop image
			Mat source0= ProcessImages.cropImageHorizontal(source,EDGE_LENGTH);
			
			//show initial image
			ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(source),"Initial Image");
			
			//read the background image
			Mat bckg0 = Highgui.imread(bckgImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
			
			//crop the background image
			Mat bckg=ProcessImages.cropImageHorizontal(bckg0,EDGE_LENGTH);
			
			//show the background image
			ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bckg),"bckg");
			
			//to store image with removed background
			Mat backgroundRemoved = new Mat();
			
			//REMOVE BACKGROUND
			org.opencv.core.Core.subtract(source0.clone(), bckg, backgroundRemoved);
			
			//show image with background removed 
			ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved),"difference btw main image and background using subtract");
			
			//save it to file (optional)
			//ImageIO.write(ProcessImages.Mat2BufferedImage(backgroundRemoved), "JPG", new File(backgroundRemovedFilePath));
			
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//							Croping the extracted shoe regions only
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			ArrayList<Mat> croppedImages = cropShoesFromImage(backgroundRemoved);
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////
			System.out.println("NUMBER OF IMAGES EXTRACTED: "+croppedImages.size());
			
			
			ArrayList<Mat> correctCroppedImages=new ArrayList<Mat>();
			//TODO: Correct the logic
			//
//			
			
			if(croppedImages.size()==1){ //only one contour is extracted -- split it by half
				Mat tempCropped=croppedImages.get(0);
				
				//System.out.println("HEIGHT "+tempCropped.height());
				//System.out.println("WIDTH "+tempCropped.width());
				int w;
				int h;
				if(tempCropped.width()> tempCropped.height()){
//					w=tempCropped.height();
//					h=tempCropped.width();
//				}
//				else{
//					w=tempCropped.width();
//					h=tempCropped.height();
					Core.transpose(tempCropped, tempCropped);
					Core.flip(tempCropped, tempCropped, 1);
				}
				w=tempCropped.width();
				h=tempCropped.height();
				if(w>100 && w<400 && h>120 && h<500){
					
					Mat uncropped = tempCropped.clone(); // if you want to rotate by 90 degrees
					
//					if(uncropped.width()>uncropped.height()){
//						
//					}
					
					
					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(uncropped),"ROTATED");
					//uncropped=dist.clone();
					//split by two images
					Rect roiOne = new Rect(0, 0, uncropped.cols()/2, uncropped.rows());
					Rect roiTwo = new Rect(uncropped.cols()/2, 0, uncropped.cols()/2, uncropped.rows());
					
					//System.out.println("uncropped.cols()   "+uncropped.cols());
					//System.out.println("uncropped.rows()   "+uncropped.rows());
					
					correctCroppedImages.add(new Mat(uncropped.clone(), roiOne));
					correctCroppedImages.add(new Mat(uncropped.clone(), roiTwo));
					
				}//internal if
			}
			else{ // if more than 1 rectangle is returned
				for(int idx=0;idx<croppedImages.size();idx++){
					Mat tempCropped=croppedImages.get(idx);
					
					if(tempCropped.width()> tempCropped.height()){
						Core.transpose(tempCropped, tempCropped);
						Core.flip(tempCropped, tempCropped, 1);
					}
					correctCroppedImages.add(tempCropped);
					//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempCropped),"Cropped Correctly  "+idx);
				}//end of for
			}//end of else
			
			 List<MatOfPoint> tempContours = new ArrayList<MatOfPoint>();
			 List<Mat> extractedImages = new ArrayList<Mat>();
			 List <double[]> extractedFeatures = new ArrayList<double[]>();
			 //parameters to save
			 double area=0;
			 double perimeter=0;
			 double tilt=0;
			 double maxVDist=0;
			 double minVDist=0;
			 double maxHDist=0;
			 double minHDist=0;
//			 double centerX=0;
//			 double centerY=0;
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			for(int index=0;index<2;index++){
					//current image
					Mat tempImage= correctCroppedImages.get(index);
					
					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempImage),"Shoe number "+index);
					
					
					//TODO:
					//Extract contours
					Imgproc.blur(tempImage, tempImage, new Size(5,5));
					
					org.opencv.core.Core.inRange(tempImage, new Scalar(0, 0, 0), new Scalar(6, 6, 6), tempImage);
					
					
					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempImage),"Black color removed ");
					//Imgproc.erode(tempImage, tempImage, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5,5)));
					//Imgproc.dilate(tempImage, tempImage, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5,5)));
					
					////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					//Extend image border to avoid merging the large area contour with image edges, which results 
					//in several white contours detection
					////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					
					
		//			// 1) Apply gaussian blur to remove noise
		//			Imgproc.GaussianBlur(tempImage, tempImage, new Size(11,11), 0);
		//			
		//			// 2) AdaptiveThreshold -> classify as either black or white
		//			Imgproc.adaptiveThreshold(tempImage, tempImage, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY, 5, 2);
		//			// 3) Invert the image -> so most of the image is black
		//			Core.bitwise_not(tempImage, tempImage);
		//			// 4) Dilate -> fill the image using the MORPH_DILATE
		//			Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_DILATE, new Size(3,3), new Point(1,1));
		//			Imgproc.dilate(tempImage, tempImage, kernel);
					
					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempImage),"Current Image ");
					
					
					
					 Mat destination = new Mat(tempImage.rows(),tempImage.cols(),tempImage.type());
					 
					 int top, bottom, left, right;
			         
		
			         /// Initialize arguments for the filter
			         top = (int) (0.05*tempImage.rows()); 
			         bottom = (int) (0.05*tempImage.rows());
			         left = (int) (0.05*tempImage.cols()); 
			         right = (int) (0.05*tempImage.cols());
					 
			         //destination = source;
			         Imgproc.copyMakeBorder(tempImage, destination, top, bottom, left, right, Imgproc.BORDER_CONSTANT, new Scalar(255,255,255));
			        
					
					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"ENLARGED IMAGE ");
					
					////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					
					
					
		
					//to store the recognized contours
					 List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
					 
					 
					 //
					 
					//recognize contours
				    Imgproc.findContours(destination.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
				    
				    //keep the destination to draw the extracted rectangles on it
				    Mat destination1=destination.clone();
				     
				    Imgproc.cvtColor(destination1, destination1, Imgproc.COLOR_GRAY2BGR);
			
					System.out.println("CONTOURS SIZE: "+contours.size());
					
					
					
					int largestAreaContourID=0;
					//TODO: ADD logic for selecting of proper contour
					for(int cntID=0; cntID<contours.size();cntID++){
						if(Math.abs(Imgproc.contourArea(contours.get(cntID)))>20000 && Math.abs(Imgproc.contourArea(contours.get(cntID)))<60000){
							largestAreaContourID=cntID;
							break;
						}
							
					}
					
					MatOfPoint tempContour= contours.get(largestAreaContourID);
					area=Math.abs(Imgproc.contourArea(tempContour));
					System.out.println("THE AREA OF LARGEST CONTOUR DETECTED: "+ area);
					
					
					//fill the detected largest contour with black to remove internal structure
					Mat finalImgForProcessing = new Mat(destination.rows(),destination.cols(), CvType.CV_8U, new Scalar(255,255,255));
					Imgproc.drawContours(finalImgForProcessing, contours, largestAreaContourID, new Scalar(0,0,0),CV_FILLED);
					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Filled Contours ");
					
					
					extractedImages.add(finalImgForProcessing);
					
					tempContours.add(tempContour);
					
					
					
					
					
					
					//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					// To Approximate with polyline
					MatOfPoint2f approxCurve =  new MatOfPoint2f();
					
					MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
					
					int contourSize = (int)tempContour.total();
					System.out.println("Current contour size: "+contourSize);
					
					Imgproc.approxPolyDP(newMat, approxCurve, contourSize*0.005, true);
					
					MatOfPoint points = new MatOfPoint( approxCurve.toArray() ); 
					
					System.out.println("Size of points "+points.size());
					System.out.println(points.toString());
		
					List<MatOfPoint> currentPolyLine = new ArrayList<MatOfPoint>();
					  
					 currentPolyLine.add(points);
					  
					 Imgproc.drawContours(destination1, currentPolyLine, -1, new Scalar(0, 255, 0), 2);
					  
					 
					 
					 
					 double exactPerimeter =Imgproc.arcLength(new MatOfPoint2f( tempContour.toArray() ), true);
					 double approxPerimeter = Imgproc.arcLength(approxCurve, true);
					 
					 
					 
					 //TODO:Perimeter can be one of the features
					 System.out.println("Approximate Perimeter  "+approxPerimeter);
					 System.out.println("Exact Perimeter  "+exactPerimeter);
					 perimeter=exactPerimeter;
					 
					 //fill the approximated shoe contour with black (to remove details inside the contour)
					 
					 Mat mainSource = new Mat(destination.rows(),destination.cols(), CvType.CV_8U, new Scalar(255,255,255));
					 
					 //ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(mainSource),"initial background");
					 
					 Core.fillConvexPoly(mainSource, points, new Scalar(0,0,0));
					 
					// ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(mainSource),"POLYLINE APPROXIMATION ");
					 
					 
					 
					
					  
					  double[] featuresArray= new double[2];
					  featuresArray[0]=area;
					  featuresArray[1]=perimeter;
					  extractedFeatures.add(featuresArray);
					  
		  }//end of for loop
			
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////		  
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			  
		  
			
			
			
			
			
		  
		  
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//MOMENTS
			MatOfPoint currentContour=tempContours.get(id);
			
			Moments m = Imgproc.moments(currentContour, true);
			// center of gravity
			
			double m00 =m.get_m00();
			double m10 = m.get_m10();
			double m01 =m.get_m01();
			
			int xCenter =0;
			int yCenter=0;
			
			
			
			
			
			
			double m11=m.get_m11();
			double m20=m.get_m20();
			double m02=m.get_m02();
			double contourAxisAngle = calculateTilt(m11, m20, m02);
			System.out.println("Axis tilt angle "+contourAxisAngle);
		  
		  
		  
		  
		  
		MatOfPoint contour1=tempContours.get(0);
		MatOfPoint contour2=tempContours.get(1);
		  double match0= Imgproc.matchShapes(contour1, contour2, CV_CONTOURS_MATCH_I1, 0);
		  double match1= Imgproc.matchShapes(contour1, contour2, CV_CONTOURS_MATCH_I2, 0);
		  double match2= Imgproc.matchShapes(contour1, contour2, CV_CONTOURS_MATCH_I3, 0);
		  System.out.println("contours are matched with accuracy of "+match0+"  "+match1+" "+match2);
		//			  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  
		  /////
		  ////
		  //
		  //    Shortest and longest parts detection
		  
		  
		  //get a Mat image
		  Mat dst=extractedImages.get(id);
		  
		  //convert it to binary data
		  int[][] iarrImageColors=extractBinaryDataFromImage(dst);
		  
		  System.out.println(iarrImageColors.length+"   "+iarrImageColors[0].length);
		  
		  //find shortest/longest vertical distance around center of mass
		  
		  List<Point[]> allVerticalLines = extractLines(iarrImageColors);
		  if (m00 != 0) {   // calculate center
				xCenter = (int) Math.round(m10/m00);
				yCenter = (int) Math.round(m01/m00);
				Core.circle(dst,new Point(xCenter,yCenter), 2, new Scalar(255,0,255), 3);
				}
		  
		  
		  List<Point[]> verticalLinesNearCenter= findVerticalLinesWithinBounds(allVerticalLines, new Point(xCenter,yCenter), 40);
		  
		  System.out.println(verticalLinesNearCenter.get(0)[0]);
		  double maxVLimDist=Math.abs(verticalLinesNearCenter.get(0)[0].y-verticalLinesNearCenter.get(0)[1].y);
		  double minVLimDist=Math.abs(verticalLinesNearCenter.get(1)[0].y-verticalLinesNearCenter.get(1)[1].y);
		  
		  //vertical longest part detection near the center of masses
		  System.out.println("Longest  "+verticalLinesNearCenter.get(0)[0]+" "+verticalLinesNearCenter.get(0)[1]+" distance "+maxVLimDist);
		  System.out.println("Shortest "+verticalLinesNearCenter.get(1)[0]+" "+verticalLinesNearCenter.get(1)[1]+" distance "+minVLimDist);
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  
		  
		  int[][] iarrImageColorsTransp= transposeMatrix(iarrImageColors);
		  System.out.println(iarrImageColorsTransp.length+"   "+iarrImageColorsTransp[0].length);
		  List<Point[]> allHorizontalLines = extractLines(iarrImageColorsTransp);
		  
		  //TODO
		  
		  
		  List<Point[]> horizontalLinesNearCenter= findHorizontalLinesWithinBounds(allHorizontalLines, new Point(xCenter,yCenter), 40);
		  
		  System.out.println(horizontalLinesNearCenter.get(0)[0]);
		  double maxHLimDist=Math.abs(horizontalLinesNearCenter.get(0)[0].y-horizontalLinesNearCenter.get(0)[1].y);
		  double minHLimDist=Math.abs(horizontalLinesNearCenter.get(1)[0].y-horizontalLinesNearCenter.get(1)[1].y);
		  
		  //vertical longest part detection near the center of masses
		  System.out.println("Longest  horizontal "+horizontalLinesNearCenter.get(0)[0]+" "+horizontalLinesNearCenter.get(0)[1]+" distance "+maxHLimDist);
		  System.out.println("Shortest  horizontal"+horizontalLinesNearCenter.get(1)[0]+" "+horizontalLinesNearCenter.get(1)[1]+" distance "+minHLimDist);
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  
		 
		  
		  //find vertical longest/shortest distance
		  List<Point[]> verticalLines = findVerticalLines(iarrImageColors);

		  maxVDist=Math.abs(verticalLines.get(0)[0].y-verticalLines.get(0)[1].y);
		  minVDist=Math.abs(verticalLines.get(1)[0].y-verticalLines.get(1)[1].y);
		  
		  //vertical longest part detection
		  System.out.println("Longest  "+verticalLines.get(0)[0]+" "+verticalLines.get(0)[1]+" distance "+maxVDist);
		  System.out.println("Shortest "+verticalLines.get(1)[0]+" "+verticalLines.get(1)[1]+" distance "+minVDist);

		  
		  
		  
		
		  
		  
		 //find horizontal longest/shortest distance
		  List<Point[]> horizontalLines = findHorizontalLines(iarrImageColorsTransp);
		  System.out.println(horizontalLines.get(0)[0]);
		  System.out.println(horizontalLines.get(0)[1]);
		  System.out.println(horizontalLines.get(1)[0]);
		  System.out.println(horizontalLines.get(1)[0]);
		  
		  maxHDist=Math.abs(horizontalLines.get(0)[0].x-horizontalLines.get(0)[1].x);
		  minHDist=Math.abs(horizontalLines.get(1)[0].x-horizontalLines.get(1)[1].x);
		  //vertical longest part detection
		  System.out.println("Longest  "+horizontalLines.get(0)[0]+" "+horizontalLines.get(0)[1]+" distance "+maxHDist);
		  System.out.println("Shortest "+horizontalLines.get(1)[0]+" "+horizontalLines.get(1)[1]+" distance "+minHDist);
		 
		  Imgproc.cvtColor(dst, dst, Imgproc.COLOR_GRAY2BGR); 

		  Core.line(dst, verticalLinesNearCenter.get(0)[0],verticalLinesNearCenter.get(0)[1], new Scalar(255,255,0), 2);
		  Core.line(dst, verticalLinesNearCenter.get(1)[0],verticalLinesNearCenter.get(1)[1], new Scalar(255,255,0), 2);
		  Core.line(dst, horizontalLinesNearCenter.get(0)[0],horizontalLinesNearCenter.get(0)[1], new Scalar(255,0,255), 2);
		  Core.line(dst, horizontalLinesNearCenter.get(1)[0],horizontalLinesNearCenter.get(1)[1], new Scalar(0,255,255), 2);
		  drawLine(verticalLines.get(0)[0],verticalLines.get(0)[1],dst);
		  drawLine(verticalLines.get(1)[0],verticalLines.get(1)[1],dst);
		  drawLine(horizontalLines.get(0)[0],horizontalLines.get(0)[1],dst);
		  drawLine(horizontalLines.get(1)[0],horizontalLines.get(1)[1],dst);
		  
		  
		  
		  
		  
		  
		  
		  ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(dst),"Vertical/Horizontal lines extracted");
		  
		  //TODO
		  
		//test shortest/longest horizontal detection to determine orientation 
		//-- if longest horizontal up and shortest is down -- orientation is up
		//-- if longest horizontal down and shortest is up -- orientation is down
		  
		 //test ability to predict left and right shoes 
		 //-- if the longest vertical is left and shortest vertical is right -- it is a left shoe
		 //-- if the longest vertical is right  and shortest vertical is left -- it is a right shoe
		  
		  
		  
		  
		  
		  
		//			  //CONVEX HULL
		//			  
		//			  
		//			  MatOfInt hull =  new MatOfInt();
		//			  Imgproc.convexHull(tempContour, hull, false);
		//			  
		//			  //http://stackoverflow.com/questions/17586948/convex-hull-on-java-android-opencv-2-3
		//			  MatOfPoint mopOut = new MatOfPoint();
		//			  mopOut.create((int)hull.size().height,1,CvType.CV_32SC2);
		//
		//			  for(int i = 0; i < hull.size().height ; i++)
		//			  {
		//			      int k = (int)hull.get(i, 0)[0];
		//			      double[] point = new double[] {
		//			    		  tempContour.get(index, 0)[0], tempContour.get(k, 0)[1]
		//			      };
		//			      mopOut.put(i, 0, point);
		//			  }           
		//			  
		//			  
		//			  
		//			  
		//			  List<MatOfPoint> currentHull = new ArrayList<MatOfPoint>();
		//			  
		//			  currentHull.add(mopOut);
		//			  Imgproc.drawContours(destination1, currentHull, -1, new Scalar(0, 255, 0), 2);
		//			  ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination1),"Convex Hull extracted "+index);
		  
		  
		  
		//			  //Another way to do it from http://www.javacodegeeks.com/2012/12/hand-and-finger-detection-using-javacv.html
		//			  //does not work
		//			  IplImage iplDestination = IplImage.createFrom(ProcessImages.Mat2BufferedImage(destination)); 
		//			  
		//			  ProcessImages.displayImage(iplDestination.getBufferedImage(),"IPL image");
		//			  
		//			  CvSeq bigContour = null;
		//			
		//			    // generate all the contours in the threshold image as a list
		//			
		//			    CvSeq contours1 = new CvSeq();
		//			    CvMemStorage contourStorage = new CvMemStorage(null);
		//
		//			    cvFindContours(iplDestination, contourStorage, contours1,Loader.sizeof(CvContour.class), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
		  
		  
		  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//		  	//Write collected info to file
//		  	File file = new File(".\\output\\txtdata\\shoesFeatures.txt"); 
//
//			// if file doesnt exists, then create it
//			if (!file.exists()) {
//				file.createNewFile();
//			}
//			
//			FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
//			BufferedWriter bw = new BufferedWriter(fw);
//			String dataline = filename + ", area " + extractedFeatures.get(id)[0]+ ", perimeter " + extractedFeatures.get(id)[1] + ", contourAxisAngle " + extractedFeatures.get(id)[2] + ", maxVDist " + maxVDist + ", minVDist " + minVDist +", maxHDist " + maxHDist + ", minHDist " + minHDist +"\r\n";
//			
//			bw.write(dataline);
//			bw.flush();
//			bw.close();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////
		  
		  
		  
		  
		}//END OF MAIN
		
		
	
		/**
		 * http://www.javacodegeeks.com/2012/12/hand-and-finger-detection-using-javacv.html
		 * @param m11
		 * @param m20
		 * @param m02
		 * @return
		 */
		private static int calculateTilt(double m11, double m20, double m02)
		{
			double diff = m20 - m02;
			if (diff == 0) {
				if (m11 == 0)
					return 0;
				else if (m11 > 0)
					return 45;
				else   // m11 < 0
					return -45;
				}
			double theta = 0.5 * Math.atan2(2*m11, diff);
			int tilt = (int) Math.round( Math.toDegrees(theta));
			if ((diff > 0) && (m11 == 0))
				return 0;
			else if ((diff < 0) && (m11 == 0))
				return -90;
			else if ((diff > 0) && (m11 > 0))  // 0 to 45 degrees
				return tilt;
			else if ((diff > 0) && (m11 < 0))  // -45 to 0
				return (180 + tilt);   // change to counter-clockwise angle
			else if ((diff < 0) && (m11 > 0))   // 45 to 90
				return tilt;
			else if ((diff < 0) && (m11 < 0))   // -90 to -45
			    return (180 + tilt);  // change to counter-clockwise angle
			System.out.println("Error in moments for tilt angle");
			return 0;
			}  // end of calculateTilt()

		
	/**
	 * takes a Mat image as an output and re	
	 * @param image
	 * @return
	 */
	static int[][] extractBinaryDataFromImage(Mat image){
		  BufferedImage inputImage = ProcessImages.Mat2BufferedImage(image);
		  int sizeX = inputImage.getWidth();
		  int sizeY = inputImage.getHeight();
		  int [][] iarrImageColors             = new int[sizeX][sizeY];
		  //get B & W pixel data = thresholding
		  for (int x = 0; x < sizeX; x++) {
				for (int y = 0; y < sizeY; y++) {
					int rgb = inputImage.getRGB(x, y);
					// if the RGB value at given point x,y is greater than 16 then set that pixel to white else set to black
					int r = (rgb >> 16) & 0xFF;
					int g = (rgb >> 8) & 0xFF;
					int b = (rgb & 0xFF);
					int gray = (r + g + b) / 3;

					if (gray > 230) iarrImageColors[x][y] = 1; //white  
					else            iarrImageColors[x][y] = 0; //black  
				}
				
			}
		  return iarrImageColors;
	}
	
	/**
	 * 
	 * @param iarrImageColors
	 * @return
	 */
	public static List<Point[]> findHorizontalLines(int[][] iarrImageColors){
		  List<Point[]> lines =new ArrayList<Point[]>();
		  List<Point[]> res = new ArrayList<Point[]>();
		  for (int r = 0; r < iarrImageColors.length; r++) {
			  Point [] tmpLine = new Point[2];
			  for (int c = 0; c < iarrImageColors[r].length; c++) {  
		      //if black pixel found (change to 1 in case of inverted image (black background))
		       if (iarrImageColors[r][c] == 0){
		    	   if(tmpLine[0]==null){
		    		   tmpLine[0] = new Point(c,r);
		    	   }
		    		   
		    	   while(iarrImageColors[r][c] == 0){
		    		   if(c >= iarrImageColors[r].length-1)
		    			   break;
		    			c++;
		    			
		    	   }
		    	   tmpLine[1] = new Point(c,r);
		    	   lines.add(tmpLine);
		    	   }
			  }//end of internal for loop
		
		  }//end of external for loop
		  
		  Point[] longestLine=new Point[2];
		  Point[] shortestLine=new Point[2];
		  double maxDist = 0;
		  double minDist = 1000000;
		  for(int l=0;l<lines.size();l++){
			  Point start = lines.get(l)[0];
		      Point end = lines.get(l)[1];
		       double distance=Math.abs(lines.get(l)[0].x-lines.get(l)[1].x);
		       if(distance > maxDist){
		    	   maxDist=distance;
		    	   longestLine[0]=start;
		    	   longestLine[1]=end;
		       }  
		       if(distance<minDist){
		    	   minDist =distance;
		    	   shortestLine[0]=start;
		    	   shortestLine[1]=end;
		       }
		  }
		  res.add(longestLine);
		  res.add(shortestLine);
		  return res;
	}
	
	/**
	 * 
	 * @param iarrImageColors
	 * @return
	 */
	public static List<Point[]> findVerticalLines(int[][] iarrImageColors){
		  List<Point[]> lines =new ArrayList<Point[]>();
		  List<Point[]> res = new ArrayList<Point[]>();
		  for (int r = 0; r < iarrImageColors.length; r++) {
			  Point [] tmpLine = new Point[2];
			  for (int c = 0; c < iarrImageColors[r].length; c++) {  
		      //if black pixel found (change to 1 in case of inverted image (black background))
		       if (iarrImageColors[r][c] == 0){
		    	   if(tmpLine[0]==null){
		    		   tmpLine[0] = new Point(r,c);
		    		   }
		    	   while(iarrImageColors[r][c] == 0){
		    		   if(c >= iarrImageColors[r].length-1)
		    			   break;
		    			c++;
		    			
		    	   }
		    	   tmpLine[1] = new Point(r,c);
		    	   lines.add(tmpLine);
		    	   }
			  }//end of internal for loop
		
		  }//end of external for loop
		  
		  Point[] longestLine=new Point[2];
		  Point[] shortestLine=new Point[2];
		  double maxDist = 0;
		  double minDist = 1000000;
		  for(int l=0;l<lines.size();l++){
			  Point start = lines.get(l)[0];
		      Point end = lines.get(l)[1];
		       double distance=Math.abs(lines.get(l)[0].y-lines.get(l)[1].y);
		       if(distance > maxDist){
		    	   maxDist=distance;
		    	   longestLine[0]=start;
		    	   longestLine[1]=end;
		       }  
		       if(distance<minDist){
		    	   minDist =distance;
		    	   shortestLine[0]=start;
		    	   shortestLine[1]=end;
		       }
		  }
		  res.add(longestLine);
		  res.add(shortestLine);
		  return res;
	}
	/**
	 * Draw a line between two given points
	 * @param start
	 * @param end
	 * @param image
	 */
	public static void drawLine(Point start, Point end, Mat image){
		Core.line(image, start, end, new Scalar(0,255,0), 1);
        Core.circle(image,end, 2, new Scalar(0,0,255), 1);
        Core.circle(image, start, 2, new Scalar(0,255,255), 1);
	}
	/**
	 * transpose int[][] matrix of binary image data
	 * @param m
	 * @return
	 */
	public static int[][] transposeMatrix(int [][] m){
		int[][] temp = new int[m[0].length][m.length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[0].length; j++)
                temp[j][i] = m[i][j];
        return temp;
    }
	
	
	/**
	 * 
	 * @param iarrImageColors
	 * @return
	 */
	public static List<Point[]> extractLines(int[][] iarrImageColors){
		  List<Point[]> lines =new ArrayList<Point[]>();
		  for (int r = 0; r < iarrImageColors.length; r++) {
			  Point [] tmpLine = new Point[2];
			  for (int c = 0; c < iarrImageColors[r].length; c++) {  
		      //if black pixel found (change to 1 in case of inverted image (black background))
		       if (iarrImageColors[r][c] == 0){
		    	   if(tmpLine[0]==null){
		    		   tmpLine[0] = new Point(r,c);
		    	   }
		    		   
		    	   while(iarrImageColors[r][c] == 0){
		    		   if(c >= iarrImageColors[r].length-1)
		    			   break;
		    			c++;
		    			
		    	   }
		    	   tmpLine[1] = new Point(r,c);
		    	   lines.add(tmpLine);
		    	   }
			  }//end of internal for loop
		
		  }//end of external for loop
		  return lines;
	}
	
	
	
	/**
	 * Find vertical lines around the center of mass within the region x-delta x+delta
	 * @param iarrImageColors
	 * @return
	 */
	public static List<Point[]> findVerticalLinesWithinBounds(List<Point[]> lines, Point center, int delta){	  
		  List<Point[]> res = new ArrayList<Point[]>();
		  Point[] longestLine=new Point[2];
		  Point[] shortestLine=new Point[2];
		  double maxDist = 0;
		  double minDist = 1000000;
		  for(int l=0;l<lines.size();l++){
			  if( (Math.abs(lines.get(l)[0].x-center.x)<delta) && (Math.abs(lines.get(l)[1].x-center.x)<delta)){
				  
				  Point start = lines.get(l)[0];
			      Point end = lines.get(l)[1];
			       double distance=Math.abs(lines.get(l)[0].y-lines.get(l)[1].y);
			       if(distance > maxDist){
			    	   maxDist=distance;
			    	   longestLine[0]=start;
			    	   longestLine[1]=end;
			       }  
			       if(distance<minDist){
			    	   minDist =distance;
			    	   shortestLine[0]=start;
			    	   shortestLine[1]=end;
			       }
			  }
			  
		  }
		  res.add(longestLine);
		  res.add(shortestLine);
		  return res;
	}

	/**
	 * Find horizontal lines around the center of mass within the region x-delta x+delta
	 * @param iarrImageColors
	 * @return
	 */
	public static List<Point[]> findHorizontalLinesWithinBounds(List<Point[]> lines, Point center, int delta){	  
		  List<Point[]> res = new ArrayList<Point[]>();
		  Point[] longestLine=new Point[2];
		  Point[] shortestLine=new Point[2];
		  double maxDist = 0;
		  double minDist = 1000000;
		  for(int l=0;l<lines.size();l++){
//			  System.out.println(lines.get(l)[0].x);
//			  System.out.println(lines.get(l)[1].x);
//			  System.out.println(lines.get(l)[0].y);
//			  System.out.println(lines.get(l)[1].y);
//			  System.out.println("Center coords: x = "+center.x+ "  y = "+center.y);
//			  System.out.println("dist "+Math.abs(lines.get(l)[0].x-center.y));
//			  System.out.println("dist btw lines: "+Math.abs(lines.get(l)[0].x-lines.get(l)[1].x));
			  if( (Math.abs(lines.get(l)[0].x-center.y)<delta) && (Math.abs(lines.get(l)[1].x-center.y)<delta)){
				  
				  Point start =new Point (lines.get(l)[0].y,lines.get(l)[0].x);
			      Point end = new Point (lines.get(l)[1].y,lines.get(l)[1].x);
			       double distance=Math.abs(lines.get(l)[0].y-lines.get(l)[1].y);
			       if(distance > maxDist){
			    	   maxDist=distance;
			    	   longestLine[0]=start;
			    	   longestLine[1]=end;
			       }  
			       if(distance<minDist){
			    	   minDist =distance;
			    	   shortestLine[0]=start;
			    	   shortestLine[1]=end;
			       }
			  }
			  
		  }
		  res.add(longestLine);
		  res.add(shortestLine);
		  return res;
	}
	
	
	
}//end of class
