package shoeFittingAlgorithm;

import static org.bytedeco.javacpp.opencv_core.CV_FILLED;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import blobDetection.BlobDetection;

public class ExtractShoeContours {

	
	//threshold to set the minimum area of contours that can be considered as shoes
	//TODO: needs to be analyzed and selected the optimal value
	final static int LOWER_CONTOUR_AREA_THRESHOLD=10000;
	final static int UPPER_CONTOUR_AREA_THRESHOLD=150000;
	final static int SHOE_SIZE_AREA_DIFFERENCE=14000;
	final static double UPPER_RATIO_VALUE=2.8;
	final static double LOWER_RATIO_VALUE=2.1;
	final static int EDGE_LENGTH=20;
	
	static String initialImagePath=".//CollectedShoePictures//";//".//results//in.jpg";
	static String processedImagesPath=".//CollectedShoePictures//Processed//";
	static String resultImagesPath=".//CollectedShoePictures//SizeExtracted//";
	static String bckgImage =".//CollectedShoePictures//background.jpg";//".//results//bckg.jpg";
	
	
	
	static BlobDetection theBlobDetection;
	static float threshold= 0.88f;
	
	
	
	/**
	 * 
	 * @param image -- a processed image with removed background
	 * @return an ArrayList of 2D arays containing shoes ROI
	 */
	public static ArrayList<RotatedRect> extractShoesOnly_v1(Mat image){
		Mat onlyShoes=new Mat();
		//remove black color of background 
		//TODO: needs to be parametrized and checked for different gray background color ranges
		org.opencv.core.Core.inRange(image, new Scalar(0, 0, 0), new Scalar(14, 14, 14), onlyShoes);
		Mat onlyShoesCopy = onlyShoes.clone();
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoesCopy),"Shoes only");
		
		
		 //to store the recognized contours
		 List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		 
		 //TODO
//		 Mat rotated=new Mat();
//		 int top, bottom, left, right;
//		 /// Initialize arguments for the filter
//	     top = (int) (0.1*image.rows()); 
//	     bottom = (int) (0.1*image.rows());
//	     left = (int) (0.1*image.cols()); 
//	     right = (int) (0.1*image.cols());
//	     Mat src=new Mat();
//	     //destination = source;
//	     Imgproc.copyMakeBorder(image, src, top, bottom, left, right, Imgproc.BORDER_CONSTANT, new Scalar(0,0,0));
//	     System.out.println(src.toString());
//	     Imgproc.cvtColor(src,src,Imgproc.COLOR_BGR2GRAY);
//	     System.out.println(src.toString());
		 //FIRST CHECK IF TWO CONTOURS ARE CLEARLY SEPARATED FROM INITIAL IMAGE:
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 //FIRST CHECK IF TWO CONTOURS ARE CLEARLY SEPARATED FROM INITIAL IMAGE:
		 
		 //recognize contours
	     Imgproc.findContours(onlyShoes.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
	     
	     //keep the destination to draw the extracted rectangles on it
	     Mat destination=onlyShoes.clone();
	     
	     Imgproc.cvtColor(destination, destination, Imgproc.COLOR_GRAY2BGR);
	     
	     Imgproc.drawContours(destination, contours, -1, new Scalar(0,0,255));
	     
	     //store extracted sizes in the list of arrays of two elements [height][width]
	     //height is bigger then width by assumption 
	     ArrayList<RotatedRect> shoesOnly =new ArrayList<RotatedRect>();
	     
	     //to save contour areas for further analysis
	     ArrayList<Double> contourAreas = new ArrayList<Double>();
	     
	     for(int i=0;i<contours.size()-1;i++){
	    	 
	    	 MatOfPoint2f approx =  new MatOfPoint2f();
	    	 
	    	 //get current contour elements
	    	 MatOfPoint tempContour=contours.get(i);
	    	 
	    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
	    	 
	    	 int contourSize = (int)tempContour.total();
	    	 
	    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
	    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
	    	 Float[] tempArray=new Float[2];
	    	 
	    	 //System.out.println("CURRENT CONTOUR AREA: "+Math.abs(Imgproc.contourArea(tempContour)));
	    	//if contour is big enough
	    	 //TODO check the threshold for min area
	    	//System.out.println("Countour "+i+ " area is "+Math.abs(Imgproc.contourArea(tempContour)));
	    	 contourAreas.add(Math.abs(Imgproc.contourArea(tempContour)));
	    	//Imgproc.drawContours(destination, contours, i, new Scalar(10*i,10*i,0),3);
	    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
	    		 	
	    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
	    		 	
	    		 	shoesOnly.add(r);
	    		 	
	    		 	//DRAW THE BOUNDING RECTANGLE
	    		 	
  			   		//area of the approximated polygon
  			   		double area = Imgproc.contourArea(newMat);
  			   		Imgproc.drawContours(destination, contours,i, new Scalar(0, 0, 250),2);
  			   		Point points[] = new Point[4];
  			   		r.points(points);
  			   		System.out.println();
  			   		System.out.println("Size of the shoe is:\nWidth "+ r.size.width+"\nHeight "+r.size.height);
  			   		
  			   		if(r.size.height>r.size.width){
  			   			
  			   			tempArray[0]=(float) r.size.height;
  			   			tempArray[1]=(float) r.size.width;
  			   			
  			   		}
  			   		else{
  			   		
  			   			tempArray[0]=(float) r.size.width;
  			   			tempArray[1]=(float) r.size.height;
  			   			
  			   		}
  			   	
  			   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
  			   		String textH=" h = "+String.format("%.2f",tempArray[1]);
  			   		
  			   		
  			   		
  			   		for(int pt=0; pt<4; ++pt){
  			   			Core.line(destination, points[pt], points[(pt+1)%4], new Scalar(0,255,0),2);
  			   			}
  			   		Core.putText(destination, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
  			   		Core.putText(destination, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
	    	 		}//end of if (checking the contours area)

  			   		
  			}//end of for loop
	     
	    	     
//	     ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"Shoes only with contours without morphological transform");
	     System.out.println("Without morphological transform found contours areas: ");
	     
	     Collections.sort(contourAreas);
	     
//	     for(int j=0;j<contourAreas.size();j++)
//	    	 System.out.println("THE CONTOUR AREA IS: "+ contourAreas.get(j));
	     
	     System.out.println("NUMBER OF CONTOURS FOUND WITHOUT MORPHOLOGICAL TRANSFORM "+shoesOnly.size());

	     //if one contour found but it's area is within the allowed limits --just return it to split by 2
	     if(shoesOnly.size()==1 && shoesOnly.get(0).size.width*shoesOnly.get(0).size.height>LOWER_CONTOUR_AREA_THRESHOLD &&shoesOnly.get(0).size.width*shoesOnly.get(0).size.height <UPPER_CONTOUR_AREA_THRESHOLD){
	    	 return shoesOnly;
	     }
	    
	     
	     
	     
	     
	     //Apply morphological transform for better detection
	     
	   //to save contour areas for further analysis
	     ArrayList<Double> contourAreas_1 = new ArrayList<Double>();
	     
	     //Add something here to discriminate based on areas
	     //if 0 or more then 2 contours found...
	     //or if the sizes of found two contours are different (areas differ by difference value)
	     //TODO:
	     int difference =SHOE_SIZE_AREA_DIFFERENCE;
	     if(shoesOnly.size()==0 || shoesOnly.size()>2 || (shoesOnly.size()==2 
	    		 && (shoesOnly.get(0).size.width*shoesOnly.get(0).size.height-shoesOnly.get(1).size.width*shoesOnly.get(1).size.height> difference)
	    		 ||((shoesOnly.get(0).size.width/shoesOnly.get(0).size.height<LOWER_RATIO_VALUE && shoesOnly.get(0).size.width/shoesOnly.get(0).size.height>UPPER_RATIO_VALUE) 
	    		|| (shoesOnly.get(1).size.width/shoesOnly.get(1).size.height<LOWER_RATIO_VALUE && shoesOnly.get(1).size.width/shoesOnly.get(1).size.height>UPPER_RATIO_VALUE)))){
	    	 contours = new ArrayList<MatOfPoint>();
	    	 shoesOnly =new ArrayList<RotatedRect>();
	    	 Imgproc.erode(onlyShoes, onlyShoes, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15,15)));
//	    	 ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoes),"After morphological closing");
	    	 //TODO: MAKE IT AS FUNCTION
	    	 
	    	//recognize contours
		     Imgproc.findContours(onlyShoes.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
		     
		     //keep the destination to draw the extracted rectangles on it
		     destination=onlyShoes.clone();
		     
		     Imgproc.cvtColor(destination, destination, Imgproc.COLOR_GRAY2BGR);
		     
		     Imgproc.drawContours(destination, contours, -1, new Scalar(0,0,255));
		     
		     //store extracted sizes in the list of arrays of two elements [height][width]
		     //height is bigger then width by assumption 
		     
		  
		     for(int i=0;i<contours.size()-1;i++){
		    	 
		    	 MatOfPoint2f approx =  new MatOfPoint2f();
		    	 
		    	 //get current contour elements
		    	 MatOfPoint tempContour=contours.get(i);
		    	 
		    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
		    	 
		    	 int contourSize = (int)tempContour.total();
		    	 
		    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
		    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
		    	 Float[] tempArray=new Float[2];
		    	 
		    	 //System.out.println("CURRENT CONTOUR AREA: "+Math.abs(Imgproc.contourArea(tempContour)));
		    	//if contour is big enough
		    	 //TODO check the threshold for min area
		    	//System.out.println("Countour "+i+ " area is "+Math.abs(Imgproc.contourArea(tempContour)));
		    	 
		    	 contourAreas_1.add(Math.abs(Imgproc.contourArea(tempContour)));
		    	//Imgproc.drawContours(destination, contours, i, new Scalar(10*i,10*i,0),3);
		    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
		    		 	
		    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
		    		 	shoesOnly.add(r);
		    		 	
		    		 	//DRAW THE BOUNDING RECTANGLE
		    		 	
	  			   		//area of the approximated polygon
	  			   		double area = Imgproc.contourArea(newMat);
	  			   		Imgproc.drawContours(destination, contours,i, new Scalar(0, 0, 250),2);
	  			   		Point points[] = new Point[4];
	  			   		r.points(points);
	  			   		System.out.println();
	  			   		System.out.println("Size of the shoe is:\nWidth "+ r.size.width+"\nHeight "+r.size.height);
	  			   		
	  			   		if(r.size.height>r.size.width){
	  			   			
	  			   			tempArray[0]=(float) r.size.height;
	  			   			tempArray[1]=(float) r.size.width;
	  			   			
	  			   		}
	  			   		else{
	  			   		
	  			   			tempArray[0]=(float) r.size.width;
	  			   			tempArray[1]=(float) r.size.height;
	  			   			
	  			   		}
	  			   	
	  			   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
	  			   		String textH=" h = "+String.format("%.2f",tempArray[1]);
	  			   		
	  			   		
	  			   		
	  			   		for(int pt=0; pt<4; ++pt){
	  			   			Core.line(destination, points[pt], points[(pt+1)%4], new Scalar(0,255,0),2);
	  			   			}
	  			   		Core.putText(destination, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
	  			   		Core.putText(destination, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
		    	 		}//end of if (checking the contours area)

	  			   		
	  			}
	    	 
	     }
	    
	     
	     
	     
	     
//	     ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"Shoes only with contours after morphological transform");
	     //ImageIO.write(ProcessImages.Mat2BufferedImage(destination), "JPG", new File(extractedSizeFilePath));
	     System.out.println("Size of Shoes Only "+ shoesOnly.size());
	     
	     
	     
	     System.out.println();
	     System.out.println();
	     Collections.sort(contourAreas_1);
	     
//	     for(int j=0;j<contourAreas_1.size();j++)
//	    	 System.out.println("THE CONTOUR AREA IS: "+ contourAreas_1.get(j));
	     
	     
	     return shoesOnly;
	     
	}
	
	
	
	
	
	/**
	 * 
	 * @param index
	 * @param masks
	 * @param image
	 * @return
	 */
	public static ArrayList<Mat> cropShoesFromImage(Mat image){
		ArrayList<Mat> result=new ArrayList<Mat>();
		ArrayList<RotatedRect> masks= extractShoesOnly_v1(image);
		
	
		Mat rotated=new Mat();
		int top, bottom, left, right;
		/// Initialize arguments for the filter
        top = (int) (0.1*image.rows()); 
        bottom = (int) (0.1*image.rows());
        left = (int) (0.1*image.cols()); 
        right = (int) (0.1*image.cols());
        Mat src=new Mat();
        //destination = source;
        Imgproc.copyMakeBorder(image, src, top, bottom, left, right, Imgproc.BORDER_CONSTANT, new Scalar(0,0,0));
        
        
		for(int i=0;i<masks.size();i++){
			RotatedRect rect= masks.get(i);

			// matrices we'll use
			
			// get angle and size from the bounding box
			float angle = (float) rect.angle;
			Size rect_size = rect.size;
			// thanks to http://felix.abecassis.me/2011/10/opencv-rotation-deskewing/
			if (rect.angle < -45.) {
			    angle += 90.0;
			    rect_size=swap(rect_size.width, rect_size.height);
			}
			
			rect.center=new Point(rect.center.x+left, rect.center.y+top);
			Mat M=image.clone();
			
			Mat cropped=new Mat();
			// get the rotation matrix
			M = org.opencv.imgproc.Imgproc.getRotationMatrix2D(rect.center, angle, 1.0);
			// perform the affine transformation on your image in src,
			// the result is the rotated image in rotated. I am doing
			// cubic interpolation here
			org.opencv.imgproc.Imgproc.warpAffine(src, rotated, M, src.size(), org.opencv.imgproc.Imgproc.INTER_LINEAR);
			
			// crop the resulting image, which is then given in cropped
			org.opencv.imgproc.Imgproc.getRectSubPix(rotated, rect_size, rect.center, cropped);
			result.add(cropped);
			//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(rotated),"rotated");
			//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(cropped),"Cropped");
			
		}
		
		return result;
		
	}
	
	/**
	 * 
	 * @param w
	 * @param h
	 * @return
	 */
	public static Size  swap(double w, double h){
		Size rect_size=new Size(h,w);
		return rect_size;

	}
	
	
	/**
	 * 
	 * @param image
	 * @return
	 */
	public static int[] getSize(Mat image){
		int result[] = new int[2];
		if(image.height()> image.width()){
			result[0]=image.height();
			result[1]=image.width();
		}
		else{
			result[1]=image.height();
			result[0]=image.width();
		}
		return result;
	}
	
	/**
	 * 
	 * @param initialImagePath
	 * @param backgroundImagePath
	 * @return
	 */
	public static ArrayList<Mat> extractCorrectlyCroppedImages(String initialImagePath, String backgroundImagePath){
		ArrayList<Mat> correctCroppedImages=new ArrayList<Mat>();
		ArrayList<Mat> result=new ArrayList<Mat>();
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		
		
		
		//read image
		Mat source = Highgui.imread(initialImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//crop image
		Mat source0= ProcessImages.cropImageHorizontal(source,EDGE_LENGTH);
		
		//show initial image
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(source),"Initial Image");
		
		//read the background image
		Mat bckg0 = Highgui.imread(backgroundImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//crop the background image
		Mat bckg=ProcessImages.cropImageHorizontal(bckg0,EDGE_LENGTH);
		
		//show the background image
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bckg),"bckg");
		
		//to store image with removed background
		Mat backgroundRemoved = new Mat();
		
		//REMOVE BACKGROUND
		org.opencv.core.Core.subtract(source0.clone(), bckg, backgroundRemoved);
		
		//show image with background removed 
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved),"difference btw main image and background using subtract");

		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//							Croping the extracted shoe regions only
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		ArrayList<Mat> croppedImages = cropShoesFromImage(backgroundRemoved);
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("NUMBER OF IMAGES EXTRACTED: "+croppedImages.size());
		
		
		
		
		if(croppedImages.size()==1){ //only one contour is extracted -- split it by half
			Mat tempCropped=croppedImages.get(0);
			int w;
			int h;
			//if it is rotated make it perpendicular
			if(tempCropped.width()> tempCropped.height()){
				Core.transpose(tempCropped, tempCropped);
				Core.flip(tempCropped, tempCropped, 1);
			}
			w=tempCropped.width();
			h=tempCropped.height();
			//if it has a reasonable size
			if(w>100 && w<500 && h>120 && h<500){
				Mat uncropped = tempCropped.clone();
//				ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(uncropped),"ROTATED");

				//split by two images
				Rect roiOne = new Rect(0, 0, uncropped.cols()/2, uncropped.rows());
				Rect roiTwo = new Rect(uncropped.cols()/2, 0, uncropped.cols()/2, uncropped.rows());
				correctCroppedImages.add(new Mat(uncropped.clone(), roiOne));
				correctCroppedImages.add(new Mat(uncropped.clone(), roiTwo));
				
			}//internal if
		}
		else{ // if more than 1 rectangle is returned
			for(int idx=0;idx<croppedImages.size();idx++){
				Mat tempCropped=croppedImages.get(idx);
				
				if(tempCropped.width()> tempCropped.height()){
					Core.transpose(tempCropped, tempCropped);
					Core.flip(tempCropped, tempCropped, 1);
				}
				correctCroppedImages.add(tempCropped);
				
			}//end of for
		}//end of else
		
		//bring all extracted images to standard size:
		
		for(int i=0;i<correctCroppedImages.size();i++){
			result.add(ProcessImages.bringImageToStdSize(correctCroppedImages.get(i)));
		}
		return result;
	}//end of extractCorrectlyCroppedImages
	
	
	
	/**
	 * 
	 * @param image
	 * @return
	 */
	public static Mat processImageForContoursDetection(Mat image){
		Mat destination = new Mat(image.rows(),image.cols(),image.type());
		//Extract contours
		Imgproc.blur(image, image, new Size(5,5));
		//remove black background
		org.opencv.core.Core.inRange(image, new Scalar(0, 0, 0), new Scalar(6, 6, 6), image);
//		int top, bottom, left, right;
//        
//		//add white border to apply contour detection correctly
//		
//        // Initialize arguments for the filter
//        top = (int) (0.05*image.rows()); 
//        bottom = (int) (0.05*image.rows());
//        left = (int) (0.05*image.cols()); 
//        right = (int) (0.05*image.cols());
//		 
//        //add border
//        Imgproc.copyMakeBorder(image, destination, top, bottom, left, right, Imgproc.BORDER_CONSTANT, new Scalar(255,255,255));
//        
//		
//		return destination;
		return image;
	}//end of processImageForContoursDetection
	
	/**
	 * 
	 * @param image
	 * @return
	 */
	public static MatOfPoint extractLargestContour(Mat image){
		//to store the recognized contours
		 List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		//recognize contours
		 Imgproc.findContours(image, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
		  
		 int largestAreaContourID=0;
			//TODO: ADD logic for selecting of proper contour
			for(int cntID=0; cntID<contours.size();cntID++){
				if(Math.abs(Imgproc.contourArea(contours.get(cntID)))>20000 && Math.abs(Imgproc.contourArea(contours.get(cntID)))<60000){
					largestAreaContourID=cntID;
					break;
				}
					
			}
			
			MatOfPoint resultingContour= contours.get(largestAreaContourID);

			return resultingContour;
	}//end of extractShoeContour
	/**
	 * 
	 * @param image
	 * @param contours
	 * @return
	 */
	public static Mat fillLargestAreaContour(Mat image){
		//to store the recognized contours
		 List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		//recognize contours
		 Imgproc.findContours(image, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
		//fill the detected largest contour with black to remove internal structure
		Mat finalImgForProcessing = new Mat(image.rows(),image.cols(), CvType.CV_8U, new Scalar(255,255,255));
		
		int largestAreaContourID=0;
		//TODO: ADD logic for selecting of proper contour
		for(int cntID=0; cntID<contours.size();cntID++){
			if(Math.abs(Imgproc.contourArea(contours.get(cntID)))>20000 && Math.abs(Imgproc.contourArea(contours.get(cntID)))<60000){
				largestAreaContourID=cntID;
				break;
			}
				
		}
		
		Imgproc.drawContours(finalImgForProcessing, contours, largestAreaContourID, new Scalar(0,0,0),CV_FILLED);
		
		return finalImgForProcessing;
	}
	
	
}//END OF THE CLASS
