package shoeFittingAlgorithm;

import static org.bytedeco.javacpp.opencv_core.CV_FILLED;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import blobDetection.Blob;
import blobDetection.BlobDetection;

public class testVerifyCircle_v2 {
	static float threshold=0.2f;
	static float dLineThresholdValue=0.4f;
	static float dBadPixelThresholdValue=0.02f;
	static BlobDetection theBlobDetection;
	private static ArrayList<Integer> iarrErrors = null;
	private static ArrayList<Integer> iBadPixels = null;
	private double dErrorThreshold        = 0.0;
	
	
	
	
	public static void main(String args[]) throws IOException{
		
/**
 * 		//TEST THE PERFORMANCE OF getAdjacentPixelMatrix2() FUNCTION
 */
		
//		int[][] matrix= new int[10][10];
//				
//		for(int i=0; i<matrix.length;i++){
//			for(int j=0; j<matrix[0].length;j++){
//				matrix[i][j] = (int)(Math.random()*10);
//				System.out.print(matrix[i][j]+"    ");
//			}
//				
//			System.out.println();
//		}
//		
//		
//		int[][] adjPixelmatrix = getAdjacentPixelMatrix2(8,7,matrix,3);
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		for(int i=0; i<adjPixelmatrix.length;i++){
//			for(int j=0; j<adjPixelmatrix[0].length;j++)
//				System.out.print(adjPixelmatrix[i][j]+"    ");
//			System.out.println();
//		}
//		//END OF TEST CASES
		
		
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
				
		String initialImagePath=".//results//circleTest.jpg";
		//read image
		Mat source = Highgui.imread(initialImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		int lowThreshold=50;
		int highThreshold=50;
		int kernelSize=3;
		
		Mat cannyFilteredSourceMat= AnalyzeImage.applyCannyEdgeDetectorOpenCV_Mat(source,lowThreshold, highThreshold, kernelSize);
		
		BufferedImage img=ProcessImages.Mat2BufferedImage(cannyFilteredSourceMat);

		detectBlobs(img, threshold);
		
		ProcessImages.displayImage(img,"Test image circle");
		
		Blob b;
		
		int imgW = img.getWidth();
		int imgH = img.getHeight();
		
		BufferedImage outputimg = new BufferedImage(imgW, imgH, BufferedImage.TYPE_INT_RGB);
		Graphics2D graph = outputimg.createGraphics();
		graph.drawImage(img, 0, 0, null);
		graph.setStroke(new BasicStroke(1));
		graph.setColor(Color.RED);
		
		
		System.out.println("BLOBS DETECTED "+theBlobDetection.getBlobNb());
		for (int n=0 ; n < theBlobDetection.getBlobNb() ; n++) {
			b=theBlobDetection.getBlob(n); 
			if (b!=null)
			{
				// Blobs
				int x = Math.round(b.xMin*imgW); int y = Math.round(b.yMin*imgH);
				int w = Math.round(b.w*imgW);    int h = Math.round(b.h*imgH);
				graph.setColor(Color.RED);
				
				Rect roi = new Rect(x,y,w,h);
				
				Mat currentBlob=ProcessImages.BufferedImage2Mat(img).submat(roi);
				
				Imgproc.cvtColor(currentBlob, currentBlob, Imgproc.COLOR_GRAY2BGR);
				BufferedImage buffCurrentBlob =ProcessImages.Mat2BufferedImage(currentBlob);
				
				
				
				Graphics2D graphCurrentBlob = buffCurrentBlob.createGraphics();
				graphCurrentBlob.drawImage(buffCurrentBlob, 0, 0, null);
				graphCurrentBlob.setStroke(new BasicStroke(1));
				graphCurrentBlob.setColor(Color.RED);
				
				drawAllCircleVerificationTestDetails(x,y,w,h,cannyFilteredSourceMat, dLineThresholdValue, dBadPixelThresholdValue);
				
				
				if (isCircle(x,y,w,h,img, graph, dLineThresholdValue, dBadPixelThresholdValue))
				{
					graph.setColor(Color.GREEN);

					graph.drawRect(x,y,w,h);
			
				}
				
//				if (verifyCircle.isCircle(0,0,w,h,buffCurrentBlob, graphCurrentBlob, dLineThresholdValue, dBadPixelThresholdValue))
//				{
//					
//					graphCurrentBlob.setColor(Color.GREEN);
//					
//					graphCurrentBlob.drawRect(0,0,w,h);
//					
//				}
				
				
				//ProcessImages.displayImage(buffCurrentBlob,"Blob # "+n);
			}
			
			cannyFilteredSourceMat= AnalyzeImage.applyCannyEdgeDetectorOpenCV_Mat(source,lowThreshold, highThreshold, kernelSize);
			
		}
		
		ProcessImages.displayImage(outputimg,"Final Image");
		
	}

	
	/**
	 * detect circle like structures based on pixel color and generate a distance/error array
	 * then selects best matches based on error threshold
	 * @param r radius of the circle
	 * @param squaredCircleToCheck BufferedImage of the circle image
	 * @param iarrImaginaryCirclePixels pixels of the imaginary circle
	 * @return boolean value to indicate this is a circle based on a given threshold
	 */
	private static boolean passedCircleTest(int r, BufferedImage squaredCircleToCheck, ArrayList<int[]> iarrImaginaryCirclePixels, double dLineThresholdValue, double dBadPixelThresholdValue) {
		iarrErrors = new ArrayList<Integer>();
		iBadPixels = new ArrayList<Integer>();
		int width  = squaredCircleToCheck.getWidth();
		int height = squaredCircleToCheck.getHeight();
		Point pCenter = new Point(r, r);
		//int iSumOfError = 0;
		int iPixelError = -1;
		
		double dLineThreshold = 0.0;
		double dBadPixelThreshold = 0.0;
		double circumference = (double) 2 * Math.PI * r;
		Mat resImage=ProcessImages.BufferedImage2Mat(squaredCircleToCheck);
		
		//draw center
		
		Core.circle(resImage, new org.opencv.core.Point(r, r), 2,new Scalar(255,0,255), 1);
		
		for (int i = 0; i < iarrImaginaryCirclePixels.size(); i++) {
			int x1 = iarrImaginaryCirclePixels.get(i)[0];
			int y1 = iarrImaginaryCirclePixels.get(i)[1];
			Core.circle(resImage, new org.opencv.core.Point(x1, y1), 2,new Scalar(0,0,255), 1);
		}
		
		
		
		
		for (int i = 0; i < iarrImaginaryCirclePixels.size(); i++) {
			int x1 = iarrImaginaryCirclePixels.get(i)[0];
			int y1 = iarrImaginaryCirclePixels.get(i)[1];
			
			ArrayList<int[]> iarrPixelsOnTheLine_InsideCircle  = getPixelsOnTheLineBetweenTwoPoints (new int[]{pCenter.x, pCenter.y}, new int[]{x1, y1});
			ArrayList<int[]> iarrPixelsOnTheLine_OutsideCircle = getNextPixelsOnTheLineOutsideCircle(new int[]{pCenter.x, pCenter.y}, new int[]{x1, y1}, width, height);    
			
			for (int j = (iarrPixelsOnTheLine_InsideCircle.size() - 1), k = 0; j >= 0; j--, k++) {
				int xj = iarrPixelsOnTheLine_InsideCircle.get(j)[0];
				int yj = iarrPixelsOnTheLine_InsideCircle.get(j)[1];
				
				if (!isBlackPixel(xj, yj, squaredCircleToCheck)) {//check inside the circle portion
					iarrErrors.add(new Integer(k));
					iPixelError = k;
					break;
				}
				if (k < iarrPixelsOnTheLine_OutsideCircle.size()) {//check outside the circle portion
					int xk = iarrPixelsOnTheLine_OutsideCircle.get(k)[0];
					int yk = iarrPixelsOnTheLine_OutsideCircle.get(k)[1];
					
					if (!isBlackPixel(xk, yk, squaredCircleToCheck)) {
						iarrErrors.add(new Integer(k));
						iPixelError = k;
						break;
					}
				}
			}
			
			if (iPixelError == -1) 
				iBadPixels.add(new Integer(iPixelError));
			else {
				dLineThreshold = (double) ((double)iPixelError) / r;
				//System.out.println("dLineThreshold: " + dLineThreshold);
				if (dLineThreshold > dLineThresholdValue)//0.2
					iBadPixels.add(new Integer(iPixelError));
			}
			
		}
		
		
		dBadPixelThreshold = iBadPixels.size() / circumference;
		//System.out.println(iBadPixels.size()+"    "+dBadPixelThreshold);
		
		//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(resImage),"lines casted");
		
		
		if (dBadPixelThreshold <= dBadPixelThresholdValue) { //0.01
			
//			try {
//				graphics.dispose();
//				ImageIO.write(squaredCircleToCheck, "jpg", new File(".\\output\\blobs\\" + iImageNo + ".jpg"));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			return true;
		}
		
		return false;
		
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param img
	 * @param graph
	 * @param dLineThresholdValue
	 * @param dBadPixelThresholdValue
	 * @throws IOException 
	 */
	public static void drawAllCircleVerificationTestDetails(int x,int y,int w,int h, Mat img, double dLineThresholdValue, double dBadPixelThresholdValue) throws IOException{
		
		int r = 0;
		
		int wh = w;
		if (w > h) wh = w;
		if (h > w) wh = h;

		Rect roi = new Rect(x,y,wh,wh);
		
		
		//here throws exception because cannot submat if width higher then height TODO
		
		Mat blobCircleToCheck=img.submat(roi);
		BufferedImage bufferBlobCircleToCheck=ProcessImages.Mat2BufferedImage(blobCircleToCheck);
//		ProcessImages.displayImage(bufferBlobCircleToCheck,"Blob # ");
		
		Rect roiExpanded = new Rect(x-5,y-5,wh+10,wh+10);
		
		Mat expandedBlobCircleToCheck=img.submat(roiExpanded);
		BufferedImage expandedBufferBlobCircleToCheck=ProcessImages.Mat2BufferedImage(expandedBlobCircleToCheck);
		
		
//		ProcessImages.displayImage(expandedBufferBlobCircleToCheck,"Initial blob");
		//ImageIO.write(blobCircleToCheck, "jpg", new File(".\\output\\blobs_cc\\" + n + ".jpg"));
		
		//extract blob's contours:
		
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		//recognize contours
		Imgproc.findContours(expandedBlobCircleToCheck, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);

		r = (blobCircleToCheck.cols()/2)-1;
		
		ArrayList<int[]> iarrImaginaryCirclePixels = generateImaginaryCirclePixelsArray(r+5, r+5, r);
		
		/////////// verify imaginary circles ////////////

		
		Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(r+5, r+5), 2,new Scalar(255,0,255), 1);

		int width  = expandedBlobCircleToCheck.cols();
		int height = expandedBlobCircleToCheck.rows();
		Point pCenter = new Point(r+5, r+5);
	
	
		Mat invertcolormatrix= new Mat(expandedBlobCircleToCheck.rows(),expandedBlobCircleToCheck.cols(), expandedBlobCircleToCheck.type(), new Scalar(255,255,255));
		Core.subtract(invertcolormatrix, expandedBlobCircleToCheck, expandedBlobCircleToCheck);
		
		
		
		Imgproc.cvtColor(expandedBlobCircleToCheck, expandedBlobCircleToCheck, Imgproc.COLOR_GRAY2BGR);
		
		//draw center
		
		Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(r+5, r+5), 1,new Scalar(255,0,255), 1);
		
		//convert to binary image leaving only black and white pixels
		int [][] binaryImage= transposeMatrix(convertImageToBlackAndWhite(expandedBufferBlobCircleToCheck));
		
		
		
//		//to print the binary image
//		for(int i = 0;i < binaryImage.length;i++){
//			for(int j = 0; j <binaryImage[0].length; j++){
//				System.out.print(binaryImage[i][j]);
//			}
//			System.out.println();
//		}
		
		
		//int[][] binaryImage=ProcessImages.extractBinaryDataFromImage(expandedBlobCircleToCheck);
		
		

//		//show the produced buffered image
//		BufferedImage bi= Binary2BufferedImage(binaryImage);
//		ProcessImages.displayImage(bi,"BINARY IMAGE");

		/////////////////////////////////////////////////////////////////////////////////////////////////
		
		//set the sliding window size
		int windowSize=3;
		
		//windowSizeXwindowSize window around the given pixel to check whether there are any black pixels in neighborhood
		
		int tempAdjacentPixelMatrix[][]= new int[windowSize][windowSize];
		
		for (int i = 0; i < iarrImaginaryCirclePixels.size(); i++) {
			int x1 = iarrImaginaryCirclePixels.get(i)[0];
			int y1 = iarrImaginaryCirclePixels.get(i)[1];
			Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(x1, y1), 1,new Scalar(0,0,255), 1);
		}
		
		//DRAW INSIDE THE CIRCLE
		for (int i = 0; i < iarrImaginaryCirclePixels.size(); i++) {
			int x1 = iarrImaginaryCirclePixels.get(i)[0];
			int y1 = iarrImaginaryCirclePixels.get(i)[1];
		
			ArrayList<int[]> iarrPixelsOnTheLine_InsideCircle  = getPixelsOnTheLineBetweenTwoPoints (new int[]{pCenter.x, pCenter.y}, new int[]{x1, y1});
			
			for (int j = (iarrPixelsOnTheLine_InsideCircle.size() - 1), k = 0; j >= 0; j--, k++) {
				int xj = iarrPixelsOnTheLine_InsideCircle.get(j)[0];
				int yj = iarrPixelsOnTheLine_InsideCircle.get(j)[1];
				
				
				//draw adjacent pixel window around given pixel
				tempAdjacentPixelMatrix=getAdjacentPixelMatrix2(xj,yj,binaryImage,windowSize);
				
				//tempAdjacentPixelMatrix=getAdjacentPixelMatrix1(xj,yj,binaryImage);
				
				
				if(tempAdjacentPixelMatrix==null)
					break;
				else{
//					System.out.println(tempAdjacentPixelMatrix.length);
//					System.out.println(tempAdjacentPixelMatrix[0].length);
//					for(int row =0;row<windowSize;row++){
//						for(int col=0;col<windowSize;col++)
//							System.out.print(tempAdjacentPixelMatrix[row][col]);
//						System.out.println();
//					}
//					System.out.println("Contains neighboring black pixels: "+ containsNeighboringPixels(tempAdjacentPixelMatrix));
//					
					//Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(xj, yj), 1,new Scalar(255,0,0), 1);
					
					//Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(xj, yj), 1,new Scalar(255,0,0), 1);
					Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(xj, yj), 1,new Scalar(255,0,0), 1);
					
					if (!isBlackPixel(xj, yj, expandedBufferBlobCircleToCheck) ){
						break;
						}
				   	else if(containsNeighboringPixels(tempAdjacentPixelMatrix)){
						Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(xj, yj), 1,new Scalar(255,0,0), 1);
					}
					
				}//end of else case	
//			
				
			}//END INSIDE THE CIRCLE FOR LOOP
			
			
			//DRAW OUTSIDE THE CIRCLE
			for (int j = (iarrPixelsOnTheLine_InsideCircle.size() - 1), k = 0; j >= 0; j--, k++) {
				int xj = iarrPixelsOnTheLine_InsideCircle.get(j)[0];
				int yj = iarrPixelsOnTheLine_InsideCircle.get(j)[1];
				ArrayList<int[]> iarrPixelsOnTheLine_OutsideCircle = getNextPixelsOnTheLineOutsideCircle(new int[]{pCenter.x, pCenter.y}, new int[]{x1, y1}, width, height);    
				
				if (k < iarrPixelsOnTheLine_OutsideCircle.size()) {//check outside the circle portion
					int xk = iarrPixelsOnTheLine_OutsideCircle.get(k)[0];
					int yk = iarrPixelsOnTheLine_OutsideCircle.get(k)[1];
					Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(xk, yk), 1,new Scalar(0,255,255), 1);
					
					if (!isBlackPixel(xk, yk, expandedBufferBlobCircleToCheck)) {

						break;
					}
					else if(containsNeighboringPixels(tempAdjacentPixelMatrix)){
						Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(xk, yk), 1,new Scalar(0,255,255), 1);
					}
				}
			}//END OF OUTSIDE THE CIRCLE FOR LOOP
			
		}
		
		
		//DRAW IMAGINARY CIRCLE
		for (int i = 0; i < iarrImaginaryCirclePixels.size(); i++) {
			int x1 = iarrImaginaryCirclePixels.get(i)[0];
			int y1 = iarrImaginaryCirclePixels.get(i)[1];
			Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(x1, y1), 1,new Scalar(0,0,255), 1);
		}
		Core.circle(expandedBlobCircleToCheck, new org.opencv.core.Point(r+5, r+5), 3,new Scalar(255,0,255), 1);
		
		
		Imgproc.drawContours(expandedBlobCircleToCheck, contours, -1, new Scalar(0,0,0),CV_FILLED);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(expandedBlobCircleToCheck),"lines casted");
		
		int n=(int) (Math.random()*100);
		//ImageIO.write(ProcessImages.Mat2BufferedImage(blobCircleToCheck), "jpg", new File(".\\results\\blobs\\" + n + ".jpg"));
		
		 String filename = n + ".jpg";
		  File file = new File(".\\results\\blobs\\", filename);

		  Boolean bool = null;
		  filename = file.toString();
		  bool = Highgui.imwrite(filename, expandedBlobCircleToCheck);

		
	}
	
	
	/**
	 * get the adjacent matrix of any size for a given pixel location
	 * @param row original pixel row value
	 * @param col original pixel column value
	 * @return 2D array of adjacent matrix of pixels
	 */
	static int[][] getAdjacentPixelMatrix2(int row, int col, int[][] squaredCircleToCheck, int windowSize) {
		
		int [][] iarrAdjacentPixelMatrix = null;
		int sizeX=squaredCircleToCheck.length;
		int sizeY=squaredCircleToCheck[0].length;
		iarrAdjacentPixelMatrix = new int[windowSize][windowSize];
		int i = 0, j = 0;
	
		
		if ( (row < windowSize/2) &&  (col < windowSize/2)){
				for (int r = 0; r <=windowSize-1; r++) {
				    for (int c = 0; c <=windowSize-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}///////////////////////////
			
		if ( (row < windowSize/2) &&  (col<sizeY-windowSize/2) && (col>=windowSize/2)){
			for (int r = 0; r <=windowSize-1; r++) {
			    for (int c = (col-windowSize/2); c < (col+windowSize/2); c++) {
			    	int value = squaredCircleToCheck[r][c];
			    	iarrAdjacentPixelMatrix[i][j] = value; 
			    	j++;
			    }
			    i++; j = 0;
			}
		}///////////////////////////
			if((row < windowSize/2) &&(col >= (sizeY-windowSize/2)) ){
				for (int r = 0; r <=windowSize-1; r++) {
				    for (int c = sizeY-windowSize; c <= sizeY-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			if((row >=(sizeX-windowSize/2)) &&(col >= (sizeY-windowSize/2)) ){
				for (int r = sizeX-windowSize; r <= sizeX-1; r++) {
				    for (int c = sizeY-windowSize; c <= sizeY-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >=windowSize/2) && (row <(sizeX-windowSize/2)) &&(col >= (sizeY-windowSize/2)) ){
				
				for (int r = row-windowSize/2; r <= row+windowSize/2; r++) {
				    for (int c = sizeY-windowSize; c <= sizeY-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >=windowSize/2) && (row < (sizeX-windowSize/2)) && (col < windowSize/2) ){
				
				for (int r = row-windowSize/2; r <= row+windowSize/2; r++) {
				    for (int c = 0; c <= windowSize-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >= sizeX-windowSize/2)  && (col < windowSize/2) ){
				
				for (int r = sizeX-windowSize; r <= sizeX-1; r++) {
				    for (int c = 0; c <= windowSize-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >=(sizeX-windowSize/2)) && (col >= windowSize/2)  && (col< (sizeY-windowSize/2) )){
				
				for (int r = sizeX-windowSize; r <=sizeX-1; r++) {
					for (int c = col-windowSize/2; c <=col+windowSize/2; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			if(row>=windowSize/2 && row< sizeX-windowSize/2 && col>=windowSize/2 && col< sizeY-windowSize/2){
				for (int r = (row-windowSize/2); r <= (row+windowSize/2); r++) {
				    for (int c = (col-windowSize/2); c <= (col+windowSize/2); c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}
			
			
		return iarrAdjacentPixelMatrix;
	}
	
	/**
	 * Convert binaryImage back to BufferedImage to test the correctness of output
	 * @param binaryImage
	 * @return
	 */
	public static BufferedImage Binary2BufferedImage(int[][]binaryImage){
		/////////////////////////////////////////////////////////////////////////////////////////////////
		//to test the output
		//// create the binary mapping
		byte BLACK = (byte)0, WHITE = (byte)255;
		byte[] map = {BLACK, WHITE};
		IndexColorModel icm = new IndexColorModel(1, map.length, map, map, map);
		
		
		int w1=binaryImage.length;
		int h1=binaryImage[0].length;
		System.out.println("rows "+ w1 +" columns "+h1);
		// create image from color model and data
		WritableRaster raster = icm.createCompatibleWritableRaster(h1, w1);
		
		int[] data = new int[w1*h1];
		int m=0;
		for(int i = 0;i < w1;i++){
			for(int j = 0; j < h1; j++){
			data[m]=binaryImage[i][j];
			m++;
			}
		}
		
		raster.setPixels(0, 0, h1, w1, data);
		BufferedImage result = new BufferedImage(icm, raster, false, null);
		return result;
	}
	
	/**
	 * get the adjacent matrix of pixels of a given pixel location 
	 * @param row original pixel row value
	 * @param col original pixel column value
	 * @return 2D array of adjacent matrix of pixels
	 */
	static int[][] getAdjacentPixelMatrix1(int row, int col, int[][] squaredCircleToCheck) {
		
		int [][] iarrAdjacentPixelMatrix = null;
		int sizeX=squaredCircleToCheck.length;
		int sizeY=squaredCircleToCheck[0].length;
		iarrAdjacentPixelMatrix = new int[3][3];
		int i = 0, j = 0;
	
		
		if ( (row < 2) &&  (col < 2)){
				for (int r = 0; r <=2; r++) {
				    for (int c = 0; c <=2; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}///////////////////////////
			
		if ( (row < 2) &&  (col<sizeY-1) && (col>=2)){
			for (int r = 0; r <=2; r++) {
			    for (int c = (col-1); c < (col+2); c++) {
			    	int value = squaredCircleToCheck[r][c];
			    	iarrAdjacentPixelMatrix[i][j] = value; 
			    	j++;
			    }
			    i++; j = 0;
			}
		}///////////////////////////
			if((row < 2) &&(col >= (sizeY-1)) ){
				for (int r = 0; r <=2; r++) {
				    for (int c = sizeY-3; c <= sizeY-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			if((row >=(sizeX-1)) &&(col >= (sizeY-1)) ){
				for (int r = sizeX-3; r <= sizeX-1; r++) {
				    for (int c = sizeY-3; c <= sizeY-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >=2) && (row <(sizeX-1)) &&(col >= (sizeY-1)) ){
				for (int r = row-1; r <= row+1; r++) {
				    for (int c = sizeY-3; c <= sizeY-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >=2) && (row < (sizeX-1)) && (col < 2) ){
				System.out.println("HERE!!!");
				for (int r = row-1; r <= row+1; r++) {
				    for (int c = 0; c <= 2; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >= sizeX-1)  && (col < 2) ){
				for (int r = sizeX-3; r <= sizeX-1; r++) {
				    for (int c = 0; c <= 2; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >=(sizeX-1)) && (col >= 2)  && (col< (sizeY-1) )){
				for (int r = sizeX-3; r <=sizeX-1; r++) {
					for (int c = col-1; c <=col+1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			if(row>=2 && row< sizeX-1 && col>=2 && col< sizeY-1){
				for (int r = (row-1); r < (row+2); r++) {
				    for (int c = (col-1); c < (col+2); c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}
			
			
		return iarrAdjacentPixelMatrix;
	}
	
	/**
	 * get the adjacent matrix of pixels of a given pixel location 
	 * @param row original pixel row value
	 * @param col original pixel column value
	 * @return 2D array of adjacent matrix of pixels
	 */
	static int[][] getAdjacentPixelMatrix(int row, int col, int[][] squaredCircleToCheck) {
		
		int [][] iarrAdjacentPixelMatrix = null;
		int sizeX=squaredCircleToCheck.length;
		int sizeY=squaredCircleToCheck[0].length;
		iarrAdjacentPixelMatrix = new int[3][3];
		int i = 0, j = 0;
	
		
		if ( (row < 2) &&  (col < 2)){
				for (int r = 0; r <=2; r++) {
				    for (int c = 0; c <=2; c++) {
				    	int value = squaredCircleToCheck[c][r];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}///////////////////////////
			
			if((row < 2) &&(col >= (sizeY-2)) ){
				for (int r = 0; r <=2; r++) {
				    for (int c = sizeY-3; c < sizeY-1; c++) {
				    	int value = squaredCircleToCheck[c][r];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			if((row >=(sizeX-2)) &&(col >= (sizeY-2)) ){
				for (int r = sizeX-3; r < sizeX-1; r++) {
				    for (int c = sizeY-3; c < sizeY-1; c++) {
				    	int value = squaredCircleToCheck[c][r];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			if((row >=(sizeX-2)) &&(col >= 2) ){
				for (int r = sizeX-3; r < sizeX-1; r++) {
					for (int c = 0; c <=2; c++) {
				    	int value = squaredCircleToCheck[c][r];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			for (int r = (row-1); r < (row+2); r++) {
			    for (int c = (col-1); c < (col+2); c++) {
			    	int value = squaredCircleToCheck[c][r];
			    	iarrAdjacentPixelMatrix[i][j] = value; 
			    	j++;
			    }
			    i++; j = 0;
			}
			
			
		return iarrAdjacentPixelMatrix;
	}
	
	
	/**
	 * 
	 * @param window
	 * @return
	 */
	public static boolean containsNeighboringPixels(int[][] window){
		int result=0;
		int size=window.length;
		int row=size/2;
		int col=size/2;
		for (int r = (row-size/2); r <=(row+size/2); r++) {
		    for (int c = (col-size/2); c <= (col+size/2); c++) {
		    	if(r!=row && c!=col && window[r][c]==0)
		    		result+=1;
		    }
		}
		return (result>0?true:false);
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param img
	 * @param graph
	 * @param dLineThresholdValue
	 * @param dBadPixelThresholdValue
	 * @return
	 */
	public static boolean isCircle(int x,int y,int w,int h, BufferedImage img, Graphics2D graph, double dLineThresholdValue, double dBadPixelThresholdValue){
		
		BufferedImage blobCircleToCheck = null;
		int r = 0;
		
		int wh = w;
		if (w > h) wh = w;
		if (h > w) wh = h;

		if ( (x + wh) > img.getWidth())  return false;
		if ( (y + wh) > img.getHeight()) return false;
		
		blobCircleToCheck = img.getSubimage(x, y, wh, wh);	
		//ImageIO.write(blobCircleToCheck, "jpg", new File(".\\output\\blobs_cc\\" + n + ".jpg"));
		
		r = (blobCircleToCheck.getWidth()/2)-1;
		
		ArrayList<int[]> iarrImaginaryCirclePixels = generateImaginaryCirclePixelsArray(r, r, r);
		
		/////////// verify imaginary circles ////////////

		graph.setStroke(new BasicStroke(1));
		graph.setColor(Color.RED);
		
		for (int i = 0; i < iarrImaginaryCirclePixels.size(); i++) {
			int x1 = iarrImaginaryCirclePixels.get(i)[0]+x;
			int y1 = iarrImaginaryCirclePixels.get(i)[1]+y;
			graph.drawLine(x1, y1, x1, y1);
		}
//		iImageNo++;
		/////////////////////////////////////////////////
		
		return passedCircleTest(r, blobCircleToCheck, iarrImaginaryCirclePixels,dLineThresholdValue, dBadPixelThresholdValue);
		
	}
	
	
	
	
	
	/**
	 * Extract blobs
	 * @param imgBuffer
	 * @param threshold
	 */
	public static void detectBlobs(BufferedImage imgBuffer, float threshold) {
		int width = imgBuffer.getWidth(null);
		int height = imgBuffer.getHeight(null);
		
		int[] pixels = new int[width*height];
		
		//Returns an array of integer pixels in the default RGB color model
		imgBuffer.getRGB(0, 0, width, height, pixels, 0, width);
		theBlobDetection = new BlobDetection(imgBuffer.getWidth(), imgBuffer.getHeight());
			
		//If passed boolean value is true, the blob detection will attempt to find bright areas. 
		//Otherwise, it will detect dark areas (default mode). 
		//This setting is very useful when you decide to compute polygons for each blob.
		theBlobDetection.setPosDiscrimination(false);
		
		//BlobDetection analyzes image for finding dark or bright areas, depending on the selected mode. 
		//The threshold is a float number between 0.0f and 1.0f used by the blobDetection to find blobs which contains pixels 
		//whose luminosity is inferior (for dark areas) or superior (for bright areas) to this value.
		theBlobDetection.setThreshold(threshold); //0.28f
		
		//Compute the blobs in the image.
		theBlobDetection.computeBlobs(pixels);
		
		//A call to this function will tell the BlobDetection instance to store polygon informations of each detected blob. 
		//By default, polygons are not computed.
		theBlobDetection.computeTriangles();
		
		//Returns the numbers of blobs detected in an image.
		int nblobs = theBlobDetection.getBlobNb();
		
		//System.out.println("Number of detected blobs: "+nblobs);
	}
	
	/**
	 * Transposing binary image data
	 * @param m
	 * @return
	 */
	public static int[][] transposeMatrix(int [][] m){
        int[][] temp = new int[m[0].length][m.length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[0].length; j++)
                temp[j][i] = m[i][j];
        return temp;
    }
	
	/**
	 * 
	 * @param inputImage
	 * @return
	 */
	public static int[][] convertImageToBlackAndWhite(BufferedImage inputImage){
		int sizeX = inputImage.getWidth();
		int sizeY = inputImage.getHeight();
		//to save image pixel color data
		int [][] iarrImageColors             = new int[sizeX][sizeY];
		//get B & W pixel data = thresholding
		for (int x = 0; x < sizeX; x++) {
			for (int y = 0; y < sizeY; y++) {
				int rgb = inputImage.getRGB(x, y);
				// if the RGB value at given point x,y is greater than 16 then set that pixel to white else set to black
				int r = (rgb >> 16) & 0xFF;
				int g = (rgb >> 8) & 0xFF;
				int b = (rgb & 0xFF);
				int gray = (r + g + b) / 3;

				if (gray <5) iarrImageColors[x][y] = 1; //white  
				else         iarrImageColors[x][y] = 0; //black  
				}
			}
		return iarrImageColors;
	}
	
	
	/**
	 * return true if the pixel is white color
	 * @param x x cordinates of the pixel
	 * @param y y cordinates of the pixel
	 * @param squaredCircleToCheck buffered-image object of the circle segment
	 * @return true if the pixel is white color
	 */
	private static boolean isBlackPixel(int x , int y, BufferedImage squaredCircleToCheck) {
		try {
			int rgb = squaredCircleToCheck.getRGB(x, y);
			int r = (rgb >> 16) & 0xFF;
			int g = (rgb >> 8) & 0xFF;
			int b = (rgb & 0xFF);
			int gray = (r + g + b) / 3;
			//System.out.println("isBlackPixel - gray level: " + gray);
			if (gray < 5) { //almost black pixel //if (gray < 100) {
				//System.out.println("isBlackPixel: " + gray);
				return true;
			}
		} catch (ArrayIndexOutOfBoundsException arrex) {
			System.out.println("x: " + x + " y: " + y + " W: " + squaredCircleToCheck.getWidth() + " H: " + squaredCircleToCheck.getHeight());
			arrex.printStackTrace();
		}
		return false;
	}
	
	/**
	 * this function predicts pixels beyond the line until it reaches the image boundaries
	 * @param startpixelxy start pixel or the center of the circle
	 * @param circlepixelxy pixel on the circle
	 * @param width width of the image 
	 * @param height height of the image 
	 * @return array list with the pixel x,y coordinates 
	 */
	private static ArrayList<int[]> getNextPixelsOnTheLineOutsideCircle(int startpixelxy[], int circlepixelxy[], int width, int height) {
		int iExtraPixels = 20;
		//Bresenham Algorithm
		ArrayList<int[]> iarrPixelsOutLine = new ArrayList<int[]>();
		int x  = startpixelxy[0],  y  = startpixelxy[1];
		int x2 = circlepixelxy[0],    y2 = circlepixelxy[1];
		
		int w = x2 - x;
	    int h = y2 - y;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
	    if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
	    if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
	    if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
	    int longest = Math.abs(w);
	    int shortest = Math.abs(h);
	    
	    if (!(longest > shortest)) {
	        longest = Math.abs(h);
	        shortest = Math.abs(w) ;
	        if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
	        dx2 = 0;            
	    }
	    
	    int numerator = longest >> 1;
		
	    for (int i = 0; i <= (longest+iExtraPixels); i++) {
	    	if ( (x >= (width)) || (y >= (height)) ) break;
	    	if ( (x < 0) || (y < 0) ) break;
	    	if (i > longest)
	    		iarrPixelsOutLine.add(new int[]{x, y});
	        
	        numerator += shortest;
	        if (!(numerator < longest)) {
	            numerator -= longest;
	            x += dx1;
	            y += dy1;
	        } else {
	            x += dx2;
	            y += dy2;
	        }
	    }
	    
		return iarrPixelsOutLine;
		
	}
	
	/**
	 * get the pixel locations between two pixels
	 * @param startpixelxy start point of the first pixel
	 * @param endpixelxy end point of the second pixel
	 * @return pixel array between two points
	 */
	private static ArrayList<int[]> getPixelsOnTheLineBetweenTwoPoints(int startpixelxy[], int endpixelxy[]) {
		//Bresenham Algorithm
		ArrayList<int[]> iarrPixelsInLine = new ArrayList<int[]>();
		int x  = startpixelxy[0],  y  = startpixelxy[1];
		int x2 = endpixelxy[0],    y2 = endpixelxy[1];
		
		int w = x2 - x;
	    int h = y2 - y;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
	    if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
	    if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
	    if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
	    int longest = Math.abs(w);
	    int shortest = Math.abs(h);
	    
	    if (!(longest > shortest)) {
	        longest = Math.abs(h);
	        shortest = Math.abs(w) ;
	        if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
	        dx2 = 0;            
	    }
	    
	    int numerator = longest >> 1;
		
	    for (int i = 0; i <= (longest); i++) {
	        iarrPixelsInLine.add(new int[]{x, y});
	        numerator += shortest;
	        if (!(numerator < longest)) {
	            numerator -= longest;
	            x += dx1;
	            y += dy1;
	        } else {
	            x += dx2;
	            y += dy2;
	        }
	    }
	    
		return iarrPixelsInLine;
	}
	
	
	/**
	 * generate pixels on the circle based on center point and radius 
	 * @param centerX center X coordinates of the circle
	 * @param centerY center Y coordinates of the circle
	 * @param r radius of the circle
	 * @return pixel array of the circle
	 */
	private static ArrayList<int[]> generateImaginaryCirclePixelsArray(int centerX, int centerY, int r) {
		//if 0,0
		ArrayList<int[]> iarrImaginaryCirclePixels = new ArrayList<int[]>();
		
		if (r < 2) return null;
		
		int d = (5 - r * 4)/4;
		int x = 0;
		int y = r;
 
		do {
			iarrImaginaryCirclePixels.add(new int[]{centerX + x, centerY + y});
			iarrImaginaryCirclePixels.add(new int[]{centerX + x, centerY - y});
			iarrImaginaryCirclePixels.add(new int[]{centerX - x, centerY + y});
			iarrImaginaryCirclePixels.add(new int[]{centerX - x, centerY - y});
			
			iarrImaginaryCirclePixels.add(new int[]{centerX + y, centerY + x});
			iarrImaginaryCirclePixels.add(new int[]{centerX + y, centerY - x});
			iarrImaginaryCirclePixels.add(new int[]{centerX - y, centerY + x});
			iarrImaginaryCirclePixels.add(new int[]{centerX - y, centerY - x});
			
			if (d < 0) {
				d += 2 * x + 1;
			} else {
				d += 2* (x - y) + 1;
				y--;
			}
			x++;
		} while (x <= y);
		
		return iarrImaginaryCirclePixels;
	}

	
	//////////////// getters and setters for T //////////////////////
	public double getErrorThreshold() {
		return dErrorThreshold;
	}

	public void setErrorThreshold(double dErrorThreshold) {
		this.dErrorThreshold = dErrorThreshold;
	}
	
	
	
}
