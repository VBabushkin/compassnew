package shoeFittingAlgorithm;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;


public class TakePicture {
	static String savePath= ".//RawPhotos";
	private static String savedImageFromCamera="D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\output\\now1.jpg";
	private static String exePath = "D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\Compass.exe";
	private static String exeFolder="D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\";
	private static StringBuffer sb;
	
	private static String exePath_1 ="C:\\Program Files (x86)\\OptiTrack\\Camera SDK\\bin\\visualtest.exe";
	private static String exeFolder_1="C:\\Program Files (x86)\\OptiTrack\\Camera SDK\\bin\\";
			
	public static void main(String[] args) throws IOException, InterruptedException {
		//first run the native Optitrack SDK to arrange shoes
		//Runtime.getRuntime().exec(exePath_1, null, new File(exeFolder_1)).waitFor();
		
		
		Runtime.getRuntime().exec(exePath, null, new File(exeFolder));
		Thread.sleep(3500);
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		String filename=savedImageFromCamera;
		Mat source = Highgui.imread(filename, Highgui.CV_LOAD_IMAGE_COLOR);
		BufferedImage sourceBuffer = ProcessImages.Mat2BufferedImage(source.clone());
		ProcessImages.displayImage(sourceBuffer,"Captured Image");
		
		String number="26";
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(1000);
		
		//First take the picture of background
		//String outputName ="background"+number;
		String outputName ="shoes_"+number+"_"+randomInt;
		//
		//String outputName ="shoes_"+number+"_180";
		//String outputName ="shoes_"+number+"_half_5";
		
		ImageIO.write(sourceBuffer, "JPG", new File(savePath+"\\"+ outputName +".jpg"));
		Thread.sleep(500);
		deletefile(savedImageFromCamera); 
		Thread.sleep(1500);
		
	}
	
	private static void deletefile(String fileName) {
		File file = new File(fileName);
		boolean success = file.delete();
		if (!success) {
			System.out.println(fileName + " Deletion failed.");
		} else {
			System.out.println(fileName + " File deleted.");
		}
	}
}
