package shoeFittingAlgorithm;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

public class ExtractShoeFeatures {
	
	final static int DELTA=40; //80 pixels square around the center of gravity
	
	/**
	 * http://www.javacodegeeks.com/2012/12/hand-and-finger-detection-using-javacv.html
	 * @param m11
	 * @param m20
	 * @param m02
	 * @return
	 */
	public static int calculateTilt(double m11, double m20, double m02)
	{
		double diff = m20 - m02;
		if (diff == 0) {
			if (m11 == 0)
				return 0;
			else if (m11 > 0)
				return 45;
			else   // m11 < 0
				return -45;
			}
		double theta = 0.5 * Math.atan2(2*m11, diff);
		int tilt = (int) Math.round( Math.toDegrees(theta));
		if ((diff > 0) && (m11 == 0))
			return 0;
		else if ((diff < 0) && (m11 == 0))
			return -90;
		else if ((diff > 0) && (m11 > 0))  // 0 to 45 degrees
			return tilt;
		else if ((diff > 0) && (m11 < 0))  // -45 to 0
			return (180 + tilt);   // change to counter-clockwise angle
		else if ((diff < 0) && (m11 > 0))   // 45 to 90
			return tilt;
		else if ((diff < 0) && (m11 < 0))   // -90 to -45
		    return (180 + tilt);  // change to counter-clockwise angle
		System.out.println("Error in moments for tilt angle");
		return 0;
	}  // end of calculateTilt()
	
	/**
	 * takes a Mat image as an output and convert it to a binary array (0 for black and 1 for white pixel)	
	 * @param image
	 * @return
	 */
	static int[][] extractBinaryDataFromImage(Mat image){
		  BufferedImage inputImage = ProcessImages.Mat2BufferedImage(image);
		  int sizeX = inputImage.getWidth();
		  int sizeY = inputImage.getHeight();
		  int [][] iarrImageColors             = new int[sizeX][sizeY];
		  //get B & W pixel data = thresholding
		  for (int x = 0; x < sizeX; x++) {
				for (int y = 0; y < sizeY; y++) {
					int rgb = inputImage.getRGB(x, y);
					// if the RGB value at given point x,y is greater than 16 then set that pixel to white else set to black
					int r = (rgb >> 16) & 0xFF;
					int g = (rgb >> 8) & 0xFF;
					int b = (rgb & 0xFF);
					int gray = (r + g + b) / 3;

					if (gray > 230) iarrImageColors[x][y] = 1; //white  
					else            iarrImageColors[x][y] = 0; //black  
				}
				
			}
		  return iarrImageColors;
	}
	
	
	/**
	 * 
	 * @param iarrImageColors
	 * @return
	 */
	public static List<Point[]> extractLines(int[][] iarrImageColors){
		  List<Point[]> lines =new ArrayList<Point[]>();
		  for (int r = 0; r < iarrImageColors.length; r++) {
			  Point [] tmpLine = new Point[2];
			  for (int c = 0; c < iarrImageColors[r].length; c++) {  
		      //if black pixel found (change to 1 in case of inverted image (black background))
		       if (iarrImageColors[r][c] == 0){
		    	   if(tmpLine[0]==null){
		    		   tmpLine[0] = new Point(r,c);
		    	   }
		    		   
		    	   while(iarrImageColors[r][c] == 0){
		    		   if(c >= iarrImageColors[r].length-1)
		    			   break;
		    			c++;
		    			
		    	   }
		    	   tmpLine[1] = new Point(r,c);
		    	   lines.add(tmpLine);
		    	   }
			  }//end of internal for loop
		
		  }//end of external for loop
		  return lines;
	}
	
	
	
	/**
	 * Find vertical lines around the center of mass within the region x-delta x+delta
	 * @param iarrImageColors
	 * @return
	 */
	public static List<Point[]> findVerticalLinesWithinBounds(List<Point[]> lines, Point center, int delta){	  
		  List<Point[]> res = new ArrayList<Point[]>();
		  Point[] longestLine=new Point[2];
		  Point[] shortestLine=new Point[2];
		  double maxDist = 0;
		  double minDist = 1000000;
		  for(int l=0;l<lines.size();l++){
			  if( (Math.abs(lines.get(l)[0].x-center.x)<delta) && (Math.abs(lines.get(l)[1].x-center.x)<delta)){
				  
				  Point start = lines.get(l)[0];
			      Point end = lines.get(l)[1];
			       double distance=Math.abs(lines.get(l)[0].y-lines.get(l)[1].y);
			       if(distance > maxDist){
			    	   maxDist=distance;
			    	   longestLine[0]=start;
			    	   longestLine[1]=end;
			       }  
			       if(distance<minDist){
			    	   minDist =distance;
			    	   shortestLine[0]=start;
			    	   shortestLine[1]=end;
			       }
			  }
			  
		  }
		  res.add(longestLine);
		  res.add(shortestLine);
		  return res;
	}

	
	
	/**
	 * Find horizontal lines around the center of mass within the region x-delta x+delta
	 * @param iarrImageColors
	 * @return
	 */
	public static List<Point[]> findHorizontalLinesWithinBounds(List<Point[]> lines, Point center, int delta){	  
		  List<Point[]> res = new ArrayList<Point[]>();
		  Point[] longestLine=new Point[2];
		  Point[] shortestLine=new Point[2];
		  double maxDist = 0;
		  double minDist = 1000000;
		  for(int l=0;l<lines.size();l++){
			  if( (Math.abs(lines.get(l)[0].x-center.y)<delta) && (Math.abs(lines.get(l)[1].x-center.y)<delta)){
				  
				  Point start =new Point (lines.get(l)[0].y,lines.get(l)[0].x);
			      Point end = new Point (lines.get(l)[1].y,lines.get(l)[1].x);
			       double distance=Math.abs(lines.get(l)[0].y-lines.get(l)[1].y);
			       if(distance > maxDist){
			    	   maxDist=distance;
			    	   longestLine[0]=start;
			    	   longestLine[1]=end;
			       }  
			       if(distance<minDist){
			    	   minDist =distance;
			    	   shortestLine[0]=start;
			    	   shortestLine[1]=end;
			       }
			  }
			  
		  }
		  res.add(longestLine);
		  res.add(shortestLine);
		  return res;
	}
	
	
	
	
	/**
	 * Determine whether the shoe is left or right
	 * @param contour
	 * @return
	 */
	public static Mat finalizeImgForPatternDetection(Mat image){
		
		Mat tempImage=image.clone();
		Mat tempImage2=image.clone();
		Mat copyOfTempImage=ExtractShoeContours.processImageForContoursDetection(tempImage2);
		//get a contour corresponding the extracted shoe outline
		MatOfPoint contour= ExtractShoeContours.extractLargestContour(copyOfTempImage);
		
		//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(copyOfTempImage),"Loaded shoe");
		
		//extract points from contour and draw them on image:
		
		Point[] outlinePoints = contour.toArray();
		
		//Draw the outline on the white background
		Mat outlinePointsOnly = new Mat(copyOfTempImage.rows(),copyOfTempImage.cols(), CvType.CV_8U, new Scalar(255,255,255));
		
		for(int i=0;i<outlinePoints.length-1; i++){
			Point start=outlinePoints[i];
			Point end= outlinePoints[i+1];
			Core.line(outlinePointsOnly, start, end, new Scalar(0,255,0), 1);
		}
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnly), "Outline Contour Extracted for processing");

		///////////////////////////////////////////////////////////////////////////////////////////////////
		//double perimeter =Imgproc.arcLength(new MatOfPoint2f( tempContour.toArray() ), true);
		//System.out.println("THE PERIMETER OF LARGEST CONTOUR FOR SHOE "+index+" DETECTED: "+ perimeter);
		
		//calculate moments, center of gravity and axis tilt
		Moments m = Imgproc.moments(contour, true);
		// center of gravity
		
		double m00 =m.get_m00();
		double m10 = m.get_m10();
		double m01 =m.get_m01();
		
		int xCenter =0;
		int yCenter=0;
		
		if (m00 != 0) {   // calculate center
			xCenter = (int) Math.round(m10/m00);
			yCenter = (int) Math.round(m01/m00);
			
			}
		
		//working with image and contour data
		
		//first fill the extracted large area contour with black color
		Mat finalImgForProcessing = ExtractShoeContours.fillLargestAreaContour(copyOfTempImage);
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Large Area contour filed with black color "+index);
		
		//convert it to binary data
		int[][] iarrImageColors=ProcessImages.extractBinaryDataFromImage(finalImgForProcessing);
		
	
		//detect longest/shortest horizontal lines near the center
		//FIRST FIND AND CORRECT THE ORIENTATION, THEN DETERMINE THE SHOE
		int[][] iarrImageColorsTransp= CompareShoeContours.transposeMatrix(iarrImageColors);
		System.out.println(iarrImageColorsTransp.length+"   "+iarrImageColorsTransp[0].length);
		List<Point[]> allHorizontalLines = ExtractShoeFeatures.extractLines(iarrImageColorsTransp);
		  
		List<Point[]> horizontalLinesNearCenter= ExtractShoeFeatures.findHorizontalLinesWithinBounds(allHorizontalLines, new Point(xCenter,yCenter), 40);
		  
		double maxHLimDist=Math.abs(horizontalLinesNearCenter.get(0)[0].x-horizontalLinesNearCenter.get(0)[1].x);
		double minHLimDist=Math.abs(horizontalLinesNearCenter.get(1)[0].x-horizontalLinesNearCenter.get(1)[1].x);
	  
		
		double yMaxH=horizontalLinesNearCenter.get(0)[0].y; //max length horizontal line y coordinate
		double yMinH=horizontalLinesNearCenter.get(1)[0].y; //min length horizontal line y coordinate
		
		//rotate the image to bring it to correct orientation
		//Mat rotated = copyOfTempImage.clone();
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Finally Determined Before");
//		
//		//if shoes are oriented down -- flip them to face up
		String orientation="";
		if(yMaxH<yMinH)
			orientation="UP";
		else{
			orientation="DOWN";
			//Core.flip(finalImgForProcessing, finalImgForProcessing, -1);
			Core.flip(tempImage, tempImage, -1);
		}
		
//		System.out.println("ORIENTATION "+ orientation);
		
		
		
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempImage),"Finally Determined After");

		
		return tempImage;
	}//end of determineLeftOrRight
	
	
	
	
	
	/**
	 * Determine whether the shoe is left or right
	 * @param contour
	 * @return
	 */
	public static shoesInfo2 determineLeftOrRight(MatOfPoint contour, Mat image){
		
		
		
		Mat copyOfTempImage=image.clone();
		
		//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempImage),"Shoe number "+index);
		
		
	
		//extract points from contour and draw them on image:
		
		Point[] outlinePoints = contour.toArray();
		
		//Draw the outline on the white background
		Mat outlinePointsOnly = new Mat(copyOfTempImage.rows(),copyOfTempImage.cols(), CvType.CV_8U, new Scalar(255,255,255));
		
		for(int i=0;i<outlinePoints.length-1; i++){
			Point start=outlinePoints[i];
			Point end= outlinePoints[i+1];
			Core.line(outlinePointsOnly, start, end, new Scalar(0,255,0), 1);
		}
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnly), "Outline Contour Extracted for processing");

		///////////////////////////////////////////////////////////////////////////////////////////////////
		//double perimeter =Imgproc.arcLength(new MatOfPoint2f( tempContour.toArray() ), true);
		//System.out.println("THE PERIMETER OF LARGEST CONTOUR FOR SHOE "+index+" DETECTED: "+ perimeter);
		
		//calculate moments, center of gravity and axis tilt
		Moments m = Imgproc.moments(contour, true);
		// center of gravity
		
		double m00 =m.get_m00();
		double m10 = m.get_m10();
		double m01 =m.get_m01();
		
		int xCenter =0;
		int yCenter=0;
		
		if (m00 != 0) {   // calculate center
			xCenter = (int) Math.round(m10/m00);
			yCenter = (int) Math.round(m01/m00);
			
			}
		
		//working with image and contour data
		
		//first fill the extracted large area contour with black color
		Mat finalImgForProcessing = ExtractShoeContours.fillLargestAreaContour(image);
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Large Area contour filed with black color "+index);
		
		//convert it to binary data
		int[][] iarrImageColors=ProcessImages.extractBinaryDataFromImage(finalImgForProcessing);
		
	
		//detect longest/shortest horizontal lines near the center
		//FIRST FIND AND CORRECT THE ORIENTATION, THEN DETERMINE THE SHOE
		int[][] iarrImageColorsTransp= CompareShoeContours.transposeMatrix(iarrImageColors);
		System.out.println(iarrImageColorsTransp.length+"   "+iarrImageColorsTransp[0].length);
		List<Point[]> allHorizontalLines = ExtractShoeFeatures.extractLines(iarrImageColorsTransp);
		  
		List<Point[]> horizontalLinesNearCenter= ExtractShoeFeatures.findHorizontalLinesWithinBounds(allHorizontalLines, new Point(xCenter,yCenter), 40);
		  
		double maxHLimDist=Math.abs(horizontalLinesNearCenter.get(0)[0].x-horizontalLinesNearCenter.get(0)[1].x);
		double minHLimDist=Math.abs(horizontalLinesNearCenter.get(1)[0].x-horizontalLinesNearCenter.get(1)[1].x);
	  
		
		double yMaxH=horizontalLinesNearCenter.get(0)[0].y; //max length horizontal line y coordinate
		double yMinH=horizontalLinesNearCenter.get(1)[0].y; //min length horizontal line y coordinate
		
		//rotate the image to bring it to correct orientation
		//Mat rotated = copyOfTempImage.clone();
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Finally Determined Before");
//		
//		//if shoes are oriented down -- flip them to face up
		String orientation="";
		if(yMaxH<yMinH)
			orientation="UP";
		else{
			orientation="DOWN";
			Core.flip(finalImgForProcessing, finalImgForProcessing, -1);
			//Core.flip(copyOfTempImage, copyOfTempImage, -1);
		}
		
//		System.out.println("ORIENTATION "+ orientation);
		
		
		
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Finally Determined After");
		
		
		
		//THEN DETERMINE WHICH SHOE IS IT -- LEFT OR RIGHT:
		
		//find shortest/longest vertical distance around center of mass
		iarrImageColors=ProcessImages.extractBinaryDataFromImage(finalImgForProcessing);
		iarrImageColorsTransp= CompareShoeContours.transposeMatrix(iarrImageColors);

		List<Point[]> allVerticalLines = ExtractShoeFeatures.extractLines(iarrImageColors);
		
		List<Point[]> verticalLinesNearCenter= ExtractShoeFeatures.findVerticalLinesWithinBounds(allVerticalLines, new Point(xCenter,yCenter), DELTA);
//		System.out.println(verticalLinesNearCenter.get(0)[0]);
		double maxVLimDist=Math.abs(verticalLinesNearCenter.get(0)[0].y-verticalLinesNearCenter.get(0)[1].y);
		double minVLimDist=Math.abs(verticalLinesNearCenter.get(1)[0].y-verticalLinesNearCenter.get(1)[1].y);
	  
		//vertical longest part detection near the center of masses
//		System.out.println("Longest Vertical near the center "+verticalLinesNearCenter.get(0)[0]+" "+verticalLinesNearCenter.get(0)[1]+" distance "+maxVLimDist);
//		System.out.println("Shortest Vertical near the center "+verticalLinesNearCenter.get(1)[0]+" "+verticalLinesNearCenter.get(1)[1]+" distance "+minVLimDist);
	
		//determine whether the shoe is left or right
		double xMaxV=verticalLinesNearCenter.get(0)[0].x;
		double xMinV=verticalLinesNearCenter.get(1)[0].x;
		
		String whichShoe="";
		if(xMinV<xMaxV)
			whichShoe="LEFT";
		else{
			whichShoe="RIGHT";
		}
		
		
		
		
		System.out.println("label  " +whichShoe);
		shoesInfo2 res = new shoesInfo2("",0, whichShoe, finalImgForProcessing);
		System.out.println("label  " +res.label);
		
		
		//for output only
//		Imgproc.cvtColor(finalImgForProcessing, finalImgForProcessing, Imgproc.COLOR_GRAY2BGR);
//		Core.putText(finalImgForProcessing, whichShoe+" "+orientation, new Point(10,30), Core.FONT_ITALIC,new Double(0.6), new Scalar(0,0,255));
//	   	ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing), "Determined");
//		ImageIO.write(ProcessImages.Mat2BufferedImage(finalImgForProcessing), "JPG", new File("./output/result/"+whichShoe+"/"+filename+"_"+index+" "+whichShoe+".jpg"));
//		ImageIO.write(ProcessImages.Mat2BufferedImage(finalImgForProcessing), "JPG", new File("./output/result/"+filename+"_"+index+" "+whichShoe+".jpg"));
		
		return res;
	}//end of determineLeftOrRight
	
	
	
}//END OF CLASS
