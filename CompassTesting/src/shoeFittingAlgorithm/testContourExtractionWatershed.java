package shoeFittingAlgorithm;
//http://docs.opencv.org/master/d2/dbd/tutorial_distance_transform.html#gsc.tab=0
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractorMOG2;

public class testContourExtractionWatershed {
	final static int DIFF = 10;
	final static int LOWER_CONTOUR_AREA_THRESHOLD=6000;//20000
	final static int UPPER_CONTOUR_AREA_THRESHOLD=100000;//50000
	static String initialImagePath=".//RawPhotos//";//".//results//in.jpg";
	static String processedImagesPath=".//RawPhotos//";
	static String resultImagesPath=".//RawPhotos//";
	static String bckgImage =".//RawPhotos//background.jpg";//".//results//bckg.jpg";
	
	public static void main(String args[]){
		int userID= 4;
		int angle=809;
		//names after undistorting is performed
		String filename= "shoes_"+userID+"_"+angle;
		String backgroundFileName="bckg_"+userID;
		String initPath=initialImagePath+filename+".jpg";
		String bckgImagePath=initialImagePath+backgroundFileName+".jpg";
		
		//Split camera image by two
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		
		//read image
		Mat source = Highgui.imread(initPath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(source),"Initial Image");
		
		//read the background image
		Mat bckg = Highgui.imread(bckgImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bckg),"bckg");
		
		
		
//		//applying background remover
//		 BackgroundSubtractorMOG2 bg=new BackgroundSubtractorMOG2();	
//		 Mat fgmask = new Mat();
//		 Mat initialBck= bckg.clone();
//		 Mat initialImage = source.clone();
//		 
//		 double learningRate = 0.05;
//		 bg.apply(initialBck, initialImage.clone(),learningRate);
//		 bg.apply(initialImage.clone(), fgmask,learningRate);
//		    
//		 ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(fgmask),"REMOVED   "+learningRate);
		
		
		
		//REMOVE BACKGROUND OLD WAY
	    Mat backgroundRemoved =  new Mat();
	  	org.opencv.core.Core.subtract(source.clone(), bckg, backgroundRemoved);
	  	ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved),"Original with removed background");
	  		 
		
		
		// Create a kernel that we will use for accuting/sharpening our image
		int[][] intArray = new int[][]{{1, 1, 1},{1, -8, 1},{1,  1, 1}};
		Mat kernel  = new Mat(3,3,CvType.CV_32F);
		for(int row=0;row<3;row++){
		   for(int col=0;col<3;col++)
			   kernel.put(row, col, intArray[row][col]);
		}// an approximation of second derivative, a quite strong kernel
		
		
	    // do the laplacian filtering as it is
	    // well, we need to convert everything in something more deeper then CV_8U
	    // because the kernel has some negative values,
	    // and we can expect in general to have a Laplacian image with negative values
	    // BUT a 8bits unsigned int (the one we are working with) can contain values from 0 to 255
	    // so the possible negative number will be truncated
		Mat src=backgroundRemoved.clone();
		
		
		
		Mat imgLaplacian= new Mat();
	    Mat sharp = src.clone();
	    Imgproc.filter2D(sharp, imgLaplacian, CvType.CV_32F, kernel);
	    src.convertTo(sharp, CvType.CV_32F);
	    Mat imgResult = new Mat();
	    Core.subtract(sharp, imgLaplacian, imgResult);
	    // convert back to 8bits gray scale
	    imgResult.convertTo(imgResult, CvType.CV_8UC1);
	    imgLaplacian.convertTo(imgLaplacian, CvType.CV_8UC1);
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgLaplacian),"Laplace Filtered Image");
		
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgResult),"New Sharped Image");
		
	 // Create binary image from source image
	    Mat bw = new Mat();
	    Imgproc.cvtColor(src, bw, Imgproc.COLOR_BGR2GRAY);
	    Imgproc.threshold(bw, bw, 45, 225, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bw),"Binary Image");
		
	 // Perform the distance transform algorithm
	    Mat dist=new Mat();
	    Imgproc.distanceTransform(bw, dist, Imgproc.CV_DIST_L2, 3);
	    // Normalize the distance image for range = {0.0, 1.0}
	    // so we can visualize and threshold it
	    Core.normalize(dist, dist, 0, 1., Core.NORM_MINMAX);
	    Core.normalize(dist, dist, 0, 255, 32, CvType.CV_8U);
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(dist),"Distance Transform Image");
	    
	 // Threshold to obtain the peaks
	    // This will be the markers for the foreground objects
	    Imgproc.threshold(dist, dist, 15, 250, Imgproc.THRESH_BINARY);
	    // Dilate a bit the dist image
	    
	    Mat kernel1  = new Mat(3,3,CvType.CV_8UC1);
		for(int row=0;row<3;row++){
		   for(int col=0;col<3;col++)
			   kernel.put(row, col, 1);
		}

	    Imgproc.dilate(dist, dist, kernel1);
	    
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(dist),"Peaks");
	    
	    //TODO
	    // Create the CV_8U version of the distance image
	    // It is needed for findContours()
	    Mat dist_8u= new Mat();
	    dist.convertTo(dist_8u, CvType.CV_8U);
		
	    
	    // Find total markers
	    List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

		//	recognize contours
		Imgproc.findContours(dist_8u.clone(), contours, new Mat(), Imgproc.RETR_EXTERNAL,Imgproc.CHAIN_APPROX_SIMPLE);
		 
		// Create the marker image for the watershed algorithm
		
		Mat markers =Mat.zeros(dist.size(), CvType.CV_32SC1);
		
		// Draw the foreground markers
	    for (int i = 0; i < contours.size(); i++)
	    	Imgproc.drawContours(markers, contours, i, new Scalar(i+1), -1);

	 // Draw the background marker
	    Core.circle(markers,new Point(5,5), 3, new Scalar(255,255,255), -1);    
//	    Core.normalize(markers, markers, 0, 255, 32, CvType.CV_8U);
//	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(markers),"markers");
	    
	    
	 // Perform the watershed algorithm
	    Imgproc.watershed(src, markers);
	    
	    Mat mark = Mat.zeros(markers.size(), CvType.CV_8UC1);
	    markers.convertTo(mark, CvType.CV_8UC1);
	    
	    Core.bitwise_not(mark, mark);
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(mark),"markers");
	    
	    
	    Highgui.imwrite(initialImagePath+"watershed"+"_"+userID+"_"+angle+".jpg",mark);
		
	    
	    
	    
	}
	
	

}
