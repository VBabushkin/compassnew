package shoeFittingAlgorithm;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_highgui.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

import java.awt.image.BufferedImage;

import org.bytedeco.javacpp.opencv_core.IplImage;

import marvin.image.MarvinColorModelConverter;
import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;
import marvin.math.MarvinMath;
import marvin.plugin.MarvinImagePlugin;
import marvin.util.MarvinPluginLoader;

import com.jhlabs.image.*;

/**
 * this class apply all the filters to a given image
 * @author compass
 *
 */
public class ApplyImageFilters {

	MarvinImagePlugin erode    = null;
	MarvinImagePlugin dilate   = null;
	MarvinImagePlugin invert   = null;
	MarvinImagePlugin boundary = null;
	
	MarvinImagePlugin edgeSobel     = null;
	MarvinImagePlugin edge          = null;
	MarvinImagePlugin edgeroberts   = null;
	MarvinImagePlugin edgeprewitt   = null;
	
	MarvinImage image               = null;
	
	private String bgRemovedImagePath          = "";
	private String sobelEdgeDetectedPath       = ""; 
	private String binaryOutputPath            = ""; 
	//private String cannyEdgeDetectedImagePath  = ""; 
	
	public ApplyImageFilters() {
		this.sobelEdgeDetectedPath       = ".\\output\\edged_sobel_now1.png";
		this.binaryOutputPath            = ".\\output\\binary_now1.png";
		//this.cannyEdgeDetectedImagePath  = Compass.cannyEdgeDetectedImagePath;
	}
	
	
	
	/**
	 * apply image filters to remove the background
	 * this method has following functions:
	 * load plug-ins
	 * Set plug-ins attributes
	 * Load the image
	 * Process the image (color invert, convert to binary, apply erosion and dilation)
	 * @param imagePath path of the image to apply filters
	 * @param bgRemovedImagePath background removed image path
	 */
	public BufferedImage applyFilters1(BufferedImage img) {
		
			//// 1. Load plug-ins
			loadPlugIns();
			
			//// 2. Set plug-ins attributes
			//setPluginAttributes();
			
			//// 3. Load image
			image=new MarvinImage(img);
			//image = MarvinImageIO.loadImage(imagePath); //("imgs/nn-o.jpg"); 
			
			//// 4. process image
			
			//edge detection 
			applyEdgeDetectFilterAndSave();
			
			//convert to binary - set the threshold
			MarvinImage binImage = convertColorModel(254);
			
			//apply morphological operations - boundary
			MarvinImage results=applyMorphologicalOperationsAndSave(binImage);
			
			
			return results.getBufferedImage();

	}
	
	
	/**
	 * apply image filters to remove the background
	 * this method has following functions:
	 * load plug-ins
	 * Set plug-ins attributes
	 * Load the image
	 * Process the image (color invert, convert to binary, apply erosion and dilation)
	 * @param imagePath path of the image to apply filters
	 * @param bgRemovedImagePath background removed image path
	 */
	public void applyFilters(String imagePath, String bgRemovedImagePath) {
		
		this.bgRemovedImagePath = bgRemovedImagePath;
		
		try {
			//// 1. Load plug-ins
			loadPlugIns();
			
			//// 2. Set plug-ins attributes
			//setPluginAttributes();
			
			//// 3. Load image
			image = MarvinImageIO.loadImage(imagePath); //("imgs/nn-o.jpg"); 
			
			//// 4. process image
			
			//edge detection 
			applyEdgeDetectFilterAndSave();
			
			//convert to binary - set the threshold
			MarvinImage binImage = convertColorModel(254);
			
			//apply morphological operations - boundary
			applyMorphologicalOperationsAndSave(binImage);
			
			//ImageIO.write(binImage.getBufferedImage(), "PNG", new File(".\\output\\canny_edged_now1.png"));
			
//			MarvinImage image1 =  MarvinImageIO.loadImage(".\\output\\beforecanny_now1.jpg");
//			MarvinImage binImage1 = MarvinColorModelConverter.rgbToBinary(image1, 100);
//			MarvinImageIO.saveImage(binImage1, ".\\output\\beforecanny_threashold_now1.jpg");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * apply canny edge detector on the shoes only image
	 * @param showsOnlyImagePath shoes extracted image
	 * @param edgedAndInvertedImagePath edge detected and color inverted image 
	 */
	public void applyCannyEdgeDetector(String shoesOnlyImagePath,String cannyEdgeDetectedImagePath, String edgedAndInvertedImagePath) {
		try {
//			BufferedImage frame;
//
//			frame = ImageIO.read(new File(showsOnlyImagePath));
//
//			//create the detector
//			CannyEdgeDetector detector = new CannyEdgeDetector();
//			
//			//adjust its parameters as desired
//			//detector.setLowThreshold(5f);
//			//detector.setHighThreshold(10f);
//
//			//apply it to an image
//			detector.setSourceImage(frame);
//			detector.process();
//			BufferedImage edges = detector.getEdgesImage();
//			
//			MarvinImage medged = new MarvinImage(edges);
//			//dilate = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.morphological.dilation");
//			//dilate.process(medged.clone(), medged); //make it all black sole
//			MarvinImageIO.saveImage(medged, cannyEdgeDetectedImagePath); //canny_edged_now1
//			
//			applyInvertFilterAndSave(medged, edgedAndInvertedImagePath);
//			
//			//do not use ImageIO save; it will destroy the image
//			//ImageIO.write(edges, "jpg", new File(".\\output\\canny_edged_now1.jpg"));
			
			

			//openCV canny
			IplImage src = cvLoadImage(shoesOnlyImagePath, 0);
			IplImage thres = cvCreateImage(cvGetSize(src), src.depth(), 1);
			IplImage dst = cvCreateImage(cvGetSize(src), src.depth(), 1);
			IplImage colorDst = cvCreateImage(cvGetSize(src), src.depth(), 1);
			
			//cvCvtColor(src, src, CV_RGB2GRAY);
			
			cvSmooth(src, colorDst, CV_BLUR, 3, 0, 0 ,0);
			cvCanny(colorDst, dst, 50, 150, 3);
			cvThreshold(src, src, 64, 254, THRESH_BINARY);
			
			try {
				cvSaveImage(cannyEdgeDetectedImagePath, dst);
				applyInvertFilterAndSave(new MarvinImage(dst.getBufferedImage()), edgedAndInvertedImagePath);
				//ImageIO.write(dst.getBufferedImage(), "PNG", new File(".\\output\\canny_edged_now1.png"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load plug-ins for MarvinPluginLoader
	 */
	private void loadPlugIns() {
		// 1. Load plug-ins
		//erode  = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.morphological.erosion");
		//dilate = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.morphological.dilation");
		//invert = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.color.invert");
		boundary = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.morphological.boundary");
		
		//edge        = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.edge.edgeDetector");
		edgeSobel   = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.edge.sobel");
		//edgeroberts = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.edge.roberts");
		//edgeprewitt = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.edge.prewitt");
		
	}
	
	/**
	 * Set plug-ins attributes for morphological operations
	 */
	private void setPluginAttributes() {
		boolean[][] m = MarvinMath.getTrueMatrix(20, 20);//(15,15);
		erode.setAttribute("matrix", m);
		dilate.setAttribute("matrix", m);
	}
	
	/**
	 * apply sobel edge detection filter to brighten up the shoe sole to extract the shoe-area
	 */
	private void applyEdgeDetectFilterAndSave() {
		//Different edge detection methods try out
		
		/*MarvinImage edgeimg = image.clone();
		edge.process(edgeimg.clone(), edgeimg);
		MarvinImageIO.saveImage(edgeimg, ".\\output\\edged_now1.jpg");
		
		MarvinImage edgerobertsimg = image.clone();
		edgeroberts.process(edgerobertsimg.clone(), edgerobertsimg);
		MarvinImageIO.saveImage(edgerobertsimg, ".\\output\\edged_roberts_now1.jpg");
		
		MarvinImage edgeprewittimg = image.clone();
		edgeprewitt.process(edgeprewittimg.clone(), edgeprewittimg);
		MarvinImageIO.saveImage(edgeprewittimg, ".\\output\\edged_prewitt_now1.jpg");*/
		
		edgeSobel.process(image.clone(), image);
		if (newDetectBlobs.bExtreamOn) {//look for bExtreamOn
			edgeprewitt = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.edge.prewitt");
			edgeprewitt.process(image.clone(), image);
		}
		MarvinImageIO.saveImage(image, sobelEdgeDetectedPath);
		
		//applyCannyEdgeDetector(sobelEdgeDetectedPath, ".\\output\\canny_edgedetectedshoes_now1.jpg");
	}
	
	/**
	 * 
	 * apply invert filter and save the color inverter image
	 * @param cedImage MarvinImage to be inverted
	 */
	private void applyInvertFilterAndSave(MarvinImage cedImage, String edgedAndInvertedImagePath) {
		if (invert == null)
			invert = MarvinPluginLoader.loadImagePlugin("org.marvinproject.image.color.invert");
		invert.process(cedImage.clone(), cedImage);
		//cedImage = convertColorModel(cedImage.clone(), 254);
		MarvinImageIO.saveImage(cedImage, edgedAndInvertedImagePath);
	}
	
	/**
	 * convert the image to binary based on the threshold
	 * @param threshold integer which define the threshold of rbg to binary
	 * @return binary image
	 */
	private MarvinImage convertColorModel(int threshold) {
		MarvinImage binImage = MarvinColorModelConverter.rgbToBinary(image, threshold);
		MarvinImageIO.saveImage(binImage, binaryOutputPath); //"imgs/nn-temp.jpg"
		return binImage;
	}
	
	
	/**
	 * apply morphological operations - boundary OR (erosion and dilation)
	 * @param binImage binary image
	 */
	private MarvinImage applyMorphologicalOperationsAndSave(MarvinImage binImage) {
		
//		erode.process(binImage.clone(), binImage);MarvinImageIO.saveImage(binImage, ".\\output\\erode-now1.jpg");
//		dilate.process(binImage.clone(), binImage); //make it all black sole
//		MarvinImageIO.saveImage(binImage, ".\\output\\dilate-now1.jpg");
		boundary.process(binImage.clone(), binImage);
		//MarvinImageIO.saveImage(binImage, this.bgRemovedImagePath);
		return binImage;
	}
	
	/**
	 * change the exposure level of the buffered image
	 * @param imageToChangeExposure buffered image object
	 * @param exposureLevel new exposure level of the image
	 * @return
	 */
	public BufferedImage applyExposureFilter(BufferedImage imageToChangeExposure, float exposureLevel) {
		BufferedImage exposureChangedImage = null;
		ExposureFilter ef = new ExposureFilter();
		
		ef.setExposure(exposureLevel);
		return ef.filter(imageToChangeExposure, exposureChangedImage);
		
		//return exposureChangedImage;
	}
	
	
}
