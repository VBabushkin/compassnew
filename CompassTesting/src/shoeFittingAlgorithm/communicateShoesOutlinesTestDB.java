package shoeFittingAlgorithm;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import com.mysql.jdbc.PreparedStatement;

public class communicateShoesOutlinesTestDB {
	private int userID=1;
	private String fileID = null;
	private Mat shoesOutlines  = null;
	private String label = "";
	
	/**
	 * make the db connection and returns the connection
	 * @return
	 */
	private Connection getConnection() {
		Connection con = null;
//		String url = "jdbc:mysql://localhost:3306/";
//		String db = "compass";
//		String driver = "com.mysql.jdbc.Driver";
//		String user = "compass";
//		String pass = "compass";
		
		String url = "jdbc:mysql://localhost:3306/";
		String db = "testdb";
        String user = "testuser";
        String pass = "test623";
		try{
			//Class.forName(driver);
			con = DriverManager.getConnection(url+db, user, pass);
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return con;
	}
	
	
	
	/**
	 * update the db with the data if the shoes are detected for the first time
	 */
	public void run() {
		try {
			this.writeShoeInfoToDB();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		} 
	}
	
	
	
	/**
	 * set shoe info in the DB, if it did not match with existing records
	 * @param userID
	 * @param fileID
	 * @param label
	 * @param shoesContoursOutlines
	 */
	public void setShoeInfoWriteToDB(int userID, String fileID, String label, Mat shoesOutlines) throws SQLException, IOException {
		this.userID = userID;
		this.fileID = fileID;
		this.label= label;
		this.shoesOutlines    = shoesOutlines;


	}
	
	
	
	/**
	 * write data into the compass table
	 * @throws SQLException
	 * @throws IOException
	 */
	public void writeShoeInfoToDB() throws SQLException, IOException {
		Connection con = getConnection();
		
		int bufferSize = shoesOutlines.channels()*shoesOutlines.cols()*shoesOutlines.rows();
	    byte [] barrDistances = new byte[bufferSize];
	    
	    shoesOutlines.get(0,0,barrDistances); // get all the pixels

	    
		PreparedStatement stmt = (PreparedStatement) con.prepareStatement("INSERT INTO shoesOutlinesTest (userID, fileID, label,shoesOutlines) " + "VALUES (?, ?, ?, ?)");
		
		stmt.setInt(1, userID);
		stmt.setString(2, fileID);
		stmt.setString(3, label);
		stmt.setBytes(4, barrDistances);
	
		stmt.execute();

		stmt.close();
		con.close();
	}
	
	

	
	
	/**
	 * retrieves info about record with given ID
	 * @param filename -- name of the file
	 * @param shoeSize -- ArrayList of shoe sizes
	 * TODO: other parameters may be added later
	 */
	public static shoesInfo2 searchDBforID (int id){
		shoesInfo2 result=null;
		Connection con = null;
		Statement st = null;
	    ResultSet rs = null;
	    PreparedStatement pst = null;

	    String url = "jdbc:mysql://localhost:3306/testdb";
        String user = "testuser";
        String password = "test623";

        try {
            con = DriverManager.getConnection(url, user, password);
            
            
            pst = (PreparedStatement) con.prepareStatement("SELECT * FROM shoesOutlinesTest");
            rs = pst.executeQuery();
            
           //print the content of the table
            while (rs.next()) {
                int currentID= rs.getInt(1);
                if((id+1) == currentID){
                	//Print the headers of the table
                	System.out.print(String.format("%-5s" , "ID"));
                    System.out.print("| ");
                    System.out.print(String.format("%-5s" , "userID"));
                    System.out.print("| ");
                    System.out.print(String.format("%-15s" , "fileID"));
                    System.out.print("| ");
                    System.out.println(String.format("%-10s" , "Label"));
                    
                    System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
                    //print the content of the table
                    
                    int userID = rs.getInt("userID");
                    String fileID=rs.getString("fileID");
                    String label=rs.getString("label");
                    byte[] bmatrix = rs.getBytes("shoesOutlines");
                	Mat shoeOutline = new Mat(500,300, CvType.CV_8UC1);
                	shoeOutline.put(0, 0, bmatrix);
                    
                	result=new shoesInfo2(fileID, userID,label,shoeOutline);
                	
                    System.out.print(String.format("%-5d" , currentID));
                    System.out.print("| ");
                    System.out.print(String.format("%-6d" ,userID ));
                    System.out.print("| ");
                    System.out.print(String.format("%-15s" , fileID));
                    System.out.print("| ");
                    System.out.println(String.format("%-15s" , label));
                    break;   
                }

            }//end of while loop
            //System.out.println("No record for given ID is found");

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main2.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
            	if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Main2.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		return result;
       
		
	}
	
	
	
	

	

	/**
	 * iterator for sorting the hash map by values
	 * @param unsortMap
	 * @return
	 */
	public static Map<Integer, Double> sortByComparator(Map<Integer,Double> unsortMap) {
		 
		// Convert Map to List
		List<Map.Entry<Integer,Double>> list = 
			new LinkedList<Map.Entry<Integer,Double>>(unsortMap.entrySet());
 
		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<Integer,Double>>() {
			public int compare(Map.Entry<Integer,Double> o1,
                                           Map.Entry<Integer,Double> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});
 
		// Convert sorted map back to a Map
		Map<Integer,Double> sortedMap = new LinkedHashMap<Integer,Double>();
		for (Iterator<Map.Entry<Integer,Double>> it = list.iterator(); it.hasNext();) {
			Map.Entry<Integer,Double> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
	
	/**
	 * to print the map
	 * @param map
	 */
	public static void printMap(Map<Integer,Double> map) {
		for (Map.Entry<Integer,Double> entry : map.entrySet()) {
			System.out.println("[Key] : " + entry.getKey() 
                                      + " [Value] : " + entry.getValue());
		}
	}
	
	
	/**
	 * 
	 * @param filename -- name of the file
	 * @param shoeSize -- ArrayList of shoe sizes
	 * TODO: other parameters may be added later
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static Map<Integer, Double> compareEntry(Mat shoe1Outline) throws IOException, ClassNotFoundException{
		
		Map<Integer, Double> res = new HashMap<>();
		Connection con = null;
		Statement st = null;
	    ResultSet rs = null;
	    PreparedStatement pst = null;

	    String url = "jdbc:mysql://localhost:3306/testdb";
        String user = "testuser";
        String password = "test623";

        try {
            con = DriverManager.getConnection(url, user, password);
            
            
            pst = (PreparedStatement) con.prepareStatement("SELECT * FROM shoesOutlinesTest");
            rs = pst.executeQuery();
            
           //print the content of the table
            while (rs.next()) {
            	byte[] bmatrix = rs.getBytes("shoesOutlines");
            	Mat shoe2Outline = new Mat(500,300, CvType.CV_8UC1);
            	shoe2Outline.put(0, 0, bmatrix);
            	
            	
//            	ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(shoe2Outline),"Converted back");
            	
                double distance= CompareShoeContours.contoursMatchedDistanceValues(shoe1Outline, shoe2Outline.clone());
                
                //double tempArray[]={rs.getInt(1),distance};
                //result.add(tempArray);
                res.put(rs.getInt(1)-1,distance);
                
            }

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main2.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
            	if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Main2.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
        return res;
	}
	
	
	
	
	
	
}
