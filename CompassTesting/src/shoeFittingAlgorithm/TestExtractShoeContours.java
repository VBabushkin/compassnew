package shoeFittingAlgorithm;

import ij.io.FileSaver;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

public class TestExtractShoeContours {
	final static int LOWER_CONTOUR_AREA_THRESHOLD=10000;
	final static int UPPER_CONTOUR_AREA_THRESHOLD=150000;
	final static int SHOE_SIZE_AREA_DIFFERENCE=14000;
	final static double UPPER_RATIO_VALUE=2.8;
	final static double LOWER_RATIO_VALUE=2.1;
	final static int EDGE_LENGTH=20;
	final static int DELTA=40; //80 pixels square around the center of gravity
	
	static String initialImagePath=".//CollectedShoePictures//";//".//results//in.jpg";
	static String processedImagesPath=".//CollectedShoePictures//Processed//";
	static String resultImagesPath=".//CollectedShoePictures//SizeExtracted//";
	static String bckgImage =".//CollectedShoePictures//background.jpg";//".//results//bckg.jpg";
	public static void main(String args[]) throws IOException{
		//TODO:
		//devise a logic to determine whether the taken picture valid
		//devise a function to sort records according to shoe sizes categorization -- small, medium, large 
		//make everything tidy and accurate
		//create a relational DB to store all records
		//devise a mechanism to calculate a distance between new and old records and find a better match
		//create an interface may be with parametrized values to simplify the analysis
		//
		//
		//TODO: test
//		int userID=1;
//		int angle=45;
		
		List<MatOfPoint> shoesOutlineContours = new ArrayList<MatOfPoint>();
		List<Mat> storedProcessedImages = new ArrayList<Mat>();
		
		//iterate over all shoes, determine orientation, left or right, rotate and center correspondingly
		//and store shoe outline images in storedProcessedImages list
		int idx=1;	
		for(int userID=1;userID<=22;userID++){
			for(int angle=0;angle<=315;angle+=45){
//			
				
		String filename= "shoes_"+userID+"_"+angle;
		//String filename="randomAngle_"+userID;
		//String filename= "shoes_"+userID+"_half_"+3;
		String backgroundFileName="background"+userID;
		String initPath=initialImagePath+filename+".jpg";
		String bckgImagePath=initialImagePath+backgroundFileName+".jpg";
		
		//Split camera image by two
		ArrayList<Mat>correctCroppedImages=ExtractShoeContours.extractCorrectlyCroppedImages(initPath, bckgImagePath);
		if(correctCroppedImages.size()==0){
			System.out.println("NOTHING HAS BEEN DETECTED");
		}
		else{
			
			for(int index=0;index<2;index++){
				//current image
				Mat tempImage= correctCroppedImages.get(index);
				
				Mat copyOfTempImage=tempImage.clone();
				
				//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempImage),"Shoe number "+index);
				
				//extract shoe outline
				Mat processedImage= ExtractShoeContours.processImageForContoursDetection(tempImage);
//				ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(processedImage),"Outline extracted "+index);
				
				
				//Apply bilateral filter
				//Mat copy2OfTempImage=new Mat();
				//org.opencv.imgproc.Imgproc.bilateralFilter(copyOfTempImage, copy2OfTempImage,CvType.CV_8UC1, 39, 39);
				
				//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(copy2OfTempImage),"bilateral filter "+index);
				
				//get a contour corresponding the extracted shoe outline
				MatOfPoint tempContour= ExtractShoeContours.extractLargestContour(processedImage);
				
				//find the area of this contour
				double area=Math.abs(Imgproc.contourArea(tempContour));
				System.out.println("THE AREA OF LARGEST CONTOUR FOR SHOE "+index+" DETECTED: "+ area);
				if(area<1000){
					System.out.println("NOTHING HAS BEEN DETECTED");
					continue;
				}
				else{
					//extract points from contour and draw them on image:
					
					Point[] outlinePoints = tempContour.toArray();
					
					//Draw the outline on the white background
					Mat outlinePointsOnly = new Mat(copyOfTempImage.rows(),copyOfTempImage.cols(), CvType.CV_8U, new Scalar(255,255,255));
					
					for(int i=0;i<outlinePoints.length-1; i++){
						Point start=outlinePoints[i];
						Point end= outlinePoints[i+1];
						Core.line(outlinePointsOnly, start, end, new Scalar(0,255,0), 1);
					}
					
//					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnly), "Outline Contour Extracted for processing");
					
					
					shoesOutlineContours.add(tempContour);
					
					
					
					///////////////////////////////////////////////////////////////////////////////////////////////////
					double perimeter =Imgproc.arcLength(new MatOfPoint2f( tempContour.toArray() ), true);
					System.out.println("THE PERIMETER OF LARGEST CONTOUR FOR SHOE "+index+" DETECTED: "+ perimeter);
					
					//calculate moments, center of gravity and axis tilt
					Moments m = Imgproc.moments(tempContour, true);
					// center of gravity
					
					double m00 =m.get_m00();
					double m10 = m.get_m10();
					double m01 =m.get_m01();
					
					int xCenter =0;
					int yCenter=0;
					
					if (m00 != 0) {   // calculate center
						xCenter = (int) Math.round(m10/m00);
						yCenter = (int) Math.round(m01/m00);
						
						}
					
					
					System.out.println("THE COORDINATES OF CENTER OF GRAVITY OF LARGEST CONTOUR FOR SHOE "+index+" ARE: X = "+xCenter+" Y = "+yCenter);
					
					
					double m11=m.get_m11();
					double m20=m.get_m20();
					double m02=m.get_m02();
					double contourAxisAngle = ExtractShoeFeatures.calculateTilt(m11, m20, m02);
					System.out.println("THE AXIS TILT ANGLE OF LARGEST CONTOUR FOR SHOE "+index+" DETECTED: "+contourAxisAngle);
					System.out.println();
					
					
					//working with image and contour data
					
					//first fill the extracted large area contour with black color
					Mat finalImgForProcessing = ExtractShoeContours.fillLargestAreaContour(processedImage);
//					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Large Area contour filed with black color "+index);
					
					//convert it to binary data
					int[][] iarrImageColors=ExtractShoeFeatures.extractBinaryDataFromImage(finalImgForProcessing);
					
				
					//detect longest/shortest horizontal lines near the center
					//FIRST FIND AND CORRECT THE ORIENTATION, THEN DETERMINE THE SHOE
					int[][] iarrImageColorsTransp= transposeMatrix(iarrImageColors);
					System.out.println(iarrImageColorsTransp.length+"   "+iarrImageColorsTransp[0].length);
					List<Point[]> allHorizontalLines = ExtractShoeFeatures.extractLines(iarrImageColorsTransp);
					  
					List<Point[]> horizontalLinesNearCenter= ExtractShoeFeatures.findHorizontalLinesWithinBounds(allHorizontalLines, new Point(xCenter,yCenter), 40);
					  
					double maxHLimDist=Math.abs(horizontalLinesNearCenter.get(0)[0].x-horizontalLinesNearCenter.get(0)[1].x);
					double minHLimDist=Math.abs(horizontalLinesNearCenter.get(1)[0].x-horizontalLinesNearCenter.get(1)[1].x);
				  
					
					double yMaxH=horizontalLinesNearCenter.get(0)[0].y; //max length horizontal line y coordinate
					double yMinH=horizontalLinesNearCenter.get(1)[0].y; //min length horizontal line y coordinate
					
					//rotate the image to bring it to correct orientation
					//Mat rotated = copyOfTempImage.clone();
//					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Finally Determined Before");
//					
//					//if shoes are oriented down -- flip them to face up
					String orientation="";
					if(yMaxH<yMinH)
						orientation="UP";
					else{
						orientation="DOWN";
						Core.flip(finalImgForProcessing, finalImgForProcessing, -1);
						Core.flip(copyOfTempImage, copyOfTempImage, -1);
					}
					
					System.out.println("ORIENTATION "+ orientation);
					
					
					
					
//					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing),"Finally Determined After");
					storedProcessedImages.add(finalImgForProcessing);
					
					
					//THEN DETERMINE WHICH SHOE IS IT -- LEFT OR RIGHT:
					
					//find shortest/longest vertical distance around center of mass
					iarrImageColors=ExtractShoeFeatures.extractBinaryDataFromImage(finalImgForProcessing);
					iarrImageColorsTransp= transposeMatrix(iarrImageColors);
 
					List<Point[]> allVerticalLines = ExtractShoeFeatures.extractLines(iarrImageColors);
					
					List<Point[]> verticalLinesNearCenter= ExtractShoeFeatures.findVerticalLinesWithinBounds(allVerticalLines, new Point(xCenter,yCenter), DELTA);
					System.out.println(verticalLinesNearCenter.get(0)[0]);
					double maxVLimDist=Math.abs(verticalLinesNearCenter.get(0)[0].y-verticalLinesNearCenter.get(0)[1].y);
					double minVLimDist=Math.abs(verticalLinesNearCenter.get(1)[0].y-verticalLinesNearCenter.get(1)[1].y);
				  
					//vertical longest part detection near the center of masses
					System.out.println("Longest Vertical near the center "+verticalLinesNearCenter.get(0)[0]+" "+verticalLinesNearCenter.get(0)[1]+" distance "+maxVLimDist);
					System.out.println("Shortest Vertical near the center "+verticalLinesNearCenter.get(1)[0]+" "+verticalLinesNearCenter.get(1)[1]+" distance "+minVLimDist);
					
//					
					//determine whether the shoe is left or right
					double xMaxV=verticalLinesNearCenter.get(0)[0].x;
					double xMinV=verticalLinesNearCenter.get(1)[0].x;
					
					String whichShoe="";
					if(xMinV<xMaxV)
						whichShoe="LEFT";
					else{
						whichShoe="RIGHT";
					}
					///////////////////////////////////////////////////////////////////////////////////////////////////////
					//WRITE TO THE FILE
					///////////////////////////////////////////////////////////////////////////////////////////////////////
		
					Imgproc.cvtColor(copyOfTempImage, copyOfTempImage, Imgproc.COLOR_RGB2GRAY);
					Imgproc.resize(copyOfTempImage, copyOfTempImage, new Size(300,500));
					//Highgui.imwrite("./results/extracted/"+filename+"_"+whichShoe+".pgm", copyOfTempImage);
					//Highgui.imwrite("./results/extracted/"+filename+"_"+whichShoe+".png", copyOfTempImage);
					Highgui.imwrite("./results/extractedJPG/"+filename+"_"+whichShoe+".jpg", copyOfTempImage);
					//Highgui.imwrite("./results/extracted/"+idx+".bmp", copyOfTempImage);
					Core.putText(copyOfTempImage, whichShoe+" "+orientation, new Point(10,30), Core.FONT_ITALIC,new Double(0.6), new Scalar(0,0,255));
				   	
					idx++;
					
				}//END OF ELSE CASE WHEN THE AREA OF LARGEST CONTOUR IS DETECTED
				
			}//END OF FOR
			
		}//END OF ELSE CASE WHEN AT LEAST ONE CONTOUR HAS BEEN DETECTED
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
			}//end of test iteration over all angles
		}//end of test iteration over all ids
		
		
		
		System.out.println("Size of all storedProcessedImages ArrayList "+storedProcessedImages.size());
		
		Mat copyOfTempImageShoe1= storedProcessedImages.get(1);
		Mat copyOfTempImageShoe2= storedProcessedImages.get(0);
		
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(copyOfTempImageShoe2),"Initial outline ");
		
		
		MatOfPoint tempContourShoe1 =  ExtractShoeContours.extractLargestContour(copyOfTempImageShoe1);
		MatOfPoint tempContourShoe2 =  ExtractShoeContours.extractLargestContour(copyOfTempImageShoe2);
		
		Point[] outlinePointsShoe1 = tempContourShoe1.toArray();
		Point[] outlinePointsShoe2 = tempContourShoe2.toArray();
		System.out.println(outlinePointsShoe1.length);
		System.out.println(outlinePointsShoe2.length);
		//Draw the outline on the white background
		Mat outlinePointsOnlyShoe1 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat imageShoe1 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat outlinePointsOnlyShoe2 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		
		for(int i=0;i<outlinePointsShoe1.length-1; i++){
			Point start=outlinePointsShoe1[i];
			Point end= outlinePointsShoe1[i+1];
			Core.line(outlinePointsOnlyShoe1, start, end, new Scalar(0,255,0), 1);
		}
		
		imageShoe1=outlinePointsOnlyShoe1.clone();
		
		for(int i=0;i<outlinePointsShoe2.length-1; i++){
			Point start=outlinePointsShoe2[i];
			Point end= outlinePointsShoe2[i+1];
			Core.line(outlinePointsOnlyShoe2, start, end, new Scalar(0,255,0), 1);
		}
		
		
		
		Point center1=findGravityCenter(tempContourShoe1);
		Point center2=findGravityCenter(tempContourShoe2);
		
		Core.circle(outlinePointsOnlyShoe1, center1, 3, new Scalar(0,0,255));
		Core.circle(outlinePointsOnlyShoe2, center2, 3, new Scalar(0,0,255));
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe1),"Outline of tempContourShoe1 ");
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe2),"Outline of tempContourShoe2 ");
		
		
		
		
		ImageIO.write(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe1), "JPG", new File("./output/result/outlinePointsOnlyShoe1.jpg"));
		ImageIO.write(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe2), "JPG", new File("./output/result/outlinePointsOnlyShoe2.jpg"));
		
		
		Mat overlayImage= new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat shiftedImageShoe2= new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		
		Imgproc.cvtColor(overlayImage, overlayImage, Imgproc.COLOR_GRAY2BGR);
		for(int i=0;i<outlinePointsShoe1.length-1; i++){
			Point start=outlinePointsShoe1[i];
			Point end= outlinePointsShoe1[i+1];
			Core.line(overlayImage, start, end, new Scalar(255,0,0), 1);
		}
		
		//shift contour so their centers of gravity coincide
		Point[] outlinePointsShoe2Shifted = tempContourShoe2.toArray();
		int diffX= (int) (center1.x-center2.x);
		int diffY=(int) (center1.y-center2.y);
		for(int i=0;i<outlinePointsShoe2.length;i++){
			outlinePointsShoe2Shifted[i].x=outlinePointsShoe2[i].x+diffX;
			outlinePointsShoe2Shifted[i].y=outlinePointsShoe2[i].y+diffY;
		}
		
		//to get the diagonal
		RotatedRect minAreaRectShoe2=org.opencv.imgproc.Imgproc.minAreaRect(new MatOfPoint2f( outlinePointsShoe2Shifted ));
		Point[] pts=new Point[4];
				
		minAreaRectShoe2.points(pts);
		
		double diagonal = Math.sqrt((pts[0].x-pts[2].x)*(pts[0].x-pts[2].x)+(pts[0].y-pts[2].y)*(pts[0].y-pts[2].y));
		
		
		
		//draw the shifted image on the overlay background
		for(int i=0;i<outlinePointsShoe2Shifted.length-1; i++){
			Point start=outlinePointsShoe2Shifted[i];
			Point end= outlinePointsShoe2Shifted[i+1];
			Core.line(overlayImage, start, end, new Scalar(0,255,0), 1);
			Core.line(shiftedImageShoe2, start, end, new Scalar(0,0,0), 1);
		}
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imageShoe1),"imageShoe1 ");
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(shiftedImageShoe2),"shiftedImageShoe2");
		
		
		//add centers to the image
		Core.circle(overlayImage, center1, 2, new Scalar(255,255,0));
		Core.circle(overlayImage, new Point(center2.x+diffX, center2.y+diffY), 3, new Scalar(0,0,255));
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(overlayImage),"Two contours overlayed ");
//		
		
		
		//now convert both contour images to binary arrays
		
		
		//convert it to binary data
		int[][] binArrayShoe1=ExtractShoeFeatures.extractBinaryDataFromImage(imageShoe1);
		//int[][] binArrayShoe2=ExtractShoeFeatures.extractBinaryDataFromImage(shiftedImageShoe2);
		//BufferedImage squaredCircleToCheck = ProcessImages.Mat2BufferedImage(shiftedImageShoe2);
	
		int circumference=(int) Imgproc.arcLength(new MatOfPoint2f( outlinePointsShoe2Shifted ), true);
		
		int width  = binArrayShoe1.length;// squaredCircleToCheck.getWidth();
		int height = binArrayShoe1[0].length;//squaredCircleToCheck.getHeight();
		
	
		
		System.out.println(width);
		System.out.println(height);
		
		ArrayList<Integer> iarrErrors = new ArrayList<Integer>();
		ArrayList<Integer> iBadPixels = new ArrayList<Integer>();
		Point pCenter = center1;//the center of the common contour (first shoe's gravity center)
		
		int iPixelError = -1;
		
		double dLineThreshold = 0.0;
		double dBadPixelThreshold = 0.0;
		
		boolean match=false;
		
		//set thresholds:
		double dLineThresholdValue=0.02;
		double  dBadPixelThresholdValue=0.5;
	
		//set the sliding window size
		int windowSize=3;
				
		//windowSizeXwindowSize window around the given pixel to check whether there are any black pixels in neighborhood
				
		int tempAdjacentPixelMatrix[][]= new int[windowSize][windowSize];
		
		for (int i = 0; i < outlinePointsShoe2Shifted.length; i++) {
			int x1 = (int) outlinePointsShoe2Shifted[i].x;
			int y1 = (int) outlinePointsShoe2Shifted[i].y;
			ArrayList<int[]> iarrPixelsOnTheLine_InsideCircle  = getPixelsOnTheLineBetweenTwoPoints (new int[]{(int) pCenter.x, (int) pCenter.y}, new int[]{x1, y1});
			ArrayList<int[]> iarrPixelsOnTheLine_OutsideCircle = getNextPixelsOnTheLineOutsideCircle(new int[]{(int) pCenter.x, (int) pCenter.y}, new int[]{x1, y1}, width, height);    
			
			
			
			
			for (int j = (iarrPixelsOnTheLine_InsideCircle.size() - 1), k = 0; j >= 0; j--, k++) {
				int xj = iarrPixelsOnTheLine_InsideCircle.get(j)[0];
				int yj = iarrPixelsOnTheLine_InsideCircle.get(j)[1];
				
				//draw adjacent pixel window around given pixel
				tempAdjacentPixelMatrix=getAdjacentPixelMatrix2(xj,yj,binArrayShoe1,windowSize);
				
				Core.circle(overlayImage, new Point(xj, yj), 1,new Scalar(255,0,0), 1);
				//if contains black pixels
				if (binArrayShoe1[xj][yj]==0) {//check inside the circle portion
					iarrErrors.add(new Integer(k));
					iPixelError = k;
					break;
				}
				//check whether the neighboring pixels are black
				else if(containsNeighboringPixels(tempAdjacentPixelMatrix)){
					Core.circle(overlayImage, new Point(xj, yj), 1,new Scalar(255,0,0), 1);
					iarrErrors.add(new Integer(k));
					iPixelError = k;
					break;
					}
				
				/////////////////////////////////////////////////////////////////////////////////////////
				//
				//  Outside the shoe contour
				//
				if (k < iarrPixelsOnTheLine_OutsideCircle.size()) {//check outside the circle portion
					int xk = iarrPixelsOnTheLine_OutsideCircle.get(k)[0];
					int yk = iarrPixelsOnTheLine_OutsideCircle.get(k)[1];
					
					Core.circle(overlayImage, new Point(xk, yk), 1,new Scalar(0,255,255), 1);
					
					if (binArrayShoe1[xk][yk]==0) {
						iarrErrors.add(new Integer(k));
						iPixelError = k;
						break;
					}
					//check whether the neighboring pixels are black
					else if(containsNeighboringPixels(tempAdjacentPixelMatrix)){
						Core.circle(overlayImage, new Point(xk, yk), 1,new Scalar(0,255,255), 1);
						iarrErrors.add(new Integer(k));
						iPixelError = k;
						break;
						}
				}
			}
			
			if (iPixelError == -1) 
				iBadPixels.add(new Integer(iPixelError));
			else {
				dLineThreshold = (double) 2.0*((double)iPixelError)/ diagonal;
//				System.out.println("dLineThreshold: " + dLineThreshold);
				if (dLineThreshold > dLineThresholdValue)//0.2
					iBadPixels.add(new Integer(iPixelError));
			}
		}
		
		
		dBadPixelThreshold = (double)iBadPixels.size() / circumference;
		System.out.println("Circumference "+  circumference);
		System.out.println("iBadPixels.size() = "+iBadPixels.size()+"   dBadPixelThreshold = "+dBadPixelThreshold);
		if (dBadPixelThreshold <= dBadPixelThresholdValue) { //0.01
			
//			try {
//				graphics.dispose();
//				ImageIO.write(squaredCircleToCheck, "jpg", new File(".\\output\\blobs\\" + iImageNo + ".jpg"));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			match=true;
			System.out.println("Contours are the same: "+match);
		}
		
		match= false;
		
		
		System.out.println("Contours are the same: "+match);
		
		Core.line(overlayImage, pts[0], pts[2], new Scalar(255,0,255));
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(overlayImage),"Two contours overlayed ");
		
		
		
		//test
		
		Mat copyOfTempImageShoe3= storedProcessedImages.get(0);
		Mat copyOfTempImageShoe4= storedProcessedImages.get(1);
		
		
		boolean contoursMatched=contoursMatched(copyOfTempImageShoe3,copyOfTempImageShoe4);
		
		System.out.println("CONTOURS ARE THE SAME: "+contoursMatched);
		
		
		//TODO
		  
				//test shortest/longest horizontal detection to determine orientation 
				//-- if longest horizontal up and shortest is down -- orientation is up
				//-- if longest horizontal down and shortest is up -- orientation is down
				  
				 //test ability to predict left and right shoes 
				 //-- if the longest vertical is left and shortest vertical is right -- it is a left shoe
				 //-- if the longest vertical is right  and shortest vertical is left -- it is a right shoe
		
		
	}//END OF MAIN
	
	
	public static boolean contoursMatched(Mat copyOfTempImageShoe1, Mat copyOfTempImageShoe2){
		MatOfPoint tempContourShoe1 =  ExtractShoeContours.extractLargestContour(copyOfTempImageShoe1);
		MatOfPoint tempContourShoe2 =  ExtractShoeContours.extractLargestContour(copyOfTempImageShoe2);
		
		Point[] outlinePointsShoe1 = tempContourShoe1.toArray();
		Point[] outlinePointsShoe2 = tempContourShoe2.toArray();
		System.out.println(outlinePointsShoe1.length);
		System.out.println(outlinePointsShoe2.length);
		//Draw the outline on the white background
		Mat outlinePointsOnlyShoe1 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat imageShoe1 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat outlinePointsOnlyShoe2 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		
		for(int i=0;i<outlinePointsShoe1.length-1; i++){
			Point start=outlinePointsShoe1[i];
			Point end= outlinePointsShoe1[i+1];
			Core.line(outlinePointsOnlyShoe1, start, end, new Scalar(0,255,0), 1);
		}
		
		imageShoe1=outlinePointsOnlyShoe1.clone();
		
		for(int i=0;i<outlinePointsShoe2.length-1; i++){
			Point start=outlinePointsShoe2[i];
			Point end= outlinePointsShoe2[i+1];
			Core.line(outlinePointsOnlyShoe2, start, end, new Scalar(0,255,0), 1);
		}
		
		
		
		Point center1=findGravityCenter(tempContourShoe1);
		Point center2=findGravityCenter(tempContourShoe2);
		
		Core.circle(outlinePointsOnlyShoe1, center1, 3, new Scalar(0,0,255));
		Core.circle(outlinePointsOnlyShoe2, center2, 3, new Scalar(0,0,255));
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe1),"Outline of tempContourShoe1 ");
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe2),"Outline of tempContourShoe2 ");
		
		
		
		
		//ImageIO.write(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe1), "JPG", new File("./output/result/outlinePointsOnlyShoe1.jpg"));
		//ImageIO.write(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe2), "JPG", new File("./output/result/outlinePointsOnlyShoe2.jpg"));
		
		
		Mat overlayImage= new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat shiftedImageShoe2= new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		
		Imgproc.cvtColor(overlayImage, overlayImage, Imgproc.COLOR_GRAY2BGR);
		for(int i=0;i<outlinePointsShoe1.length-1; i++){
			Point start=outlinePointsShoe1[i];
			Point end= outlinePointsShoe1[i+1];
			Core.line(overlayImage, start, end, new Scalar(255,0,0), 1);
		}
		
		//shift contour so their centers of gravity coincide
		Point[] outlinePointsShoe2Shifted = tempContourShoe2.toArray();
		int diffX= (int) (center1.x-center2.x);
		int diffY=(int) (center1.y-center2.y);
		for(int i=0;i<outlinePointsShoe2.length;i++){
			outlinePointsShoe2Shifted[i].x=outlinePointsShoe2[i].x+diffX;
			outlinePointsShoe2Shifted[i].y=outlinePointsShoe2[i].y+diffY;
		}
		
		//to get the diagonal
		RotatedRect minAreaRectShoe2=org.opencv.imgproc.Imgproc.minAreaRect(new MatOfPoint2f( outlinePointsShoe2Shifted ));
		Point[] pts=new Point[4];
				
		minAreaRectShoe2.points(pts);
		
		double diagonal = Math.sqrt((pts[0].x-pts[2].x)*(pts[0].x-pts[2].x)+(pts[0].y-pts[2].y)*(pts[0].y-pts[2].y));
		
		
		
		//draw the shifted image on the overlay background
		for(int i=0;i<outlinePointsShoe2Shifted.length-1; i++){
			Point start=outlinePointsShoe2Shifted[i];
			Point end= outlinePointsShoe2Shifted[i+1];
			Core.line(overlayImage, start, end, new Scalar(0,255,0), 1);
			Core.line(shiftedImageShoe2, start, end, new Scalar(0,0,0), 1);
		}
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imageShoe1),"imageShoe1 ");
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(shiftedImageShoe2),"shiftedImageShoe2");
		
		
		//add centers to the image
		Core.circle(overlayImage, center1, 2, new Scalar(255,255,0));
		Core.circle(overlayImage, new Point(center2.x+diffX, center2.y+diffY), 3, new Scalar(0,0,255));
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(overlayImage),"Two contours overlayed ");

		//now convert both contour images to binary arrays

		//convert it to binary data
		int[][] binArrayShoe1=ExtractShoeFeatures.extractBinaryDataFromImage(imageShoe1);
		//int[][] binArrayShoe2=ExtractShoeFeatures.extractBinaryDataFromImage(shiftedImageShoe2);
		//BufferedImage squaredCircleToCheck = ProcessImages.Mat2BufferedImage(shiftedImageShoe2);
	
		int circumference=(int) Imgproc.arcLength(new MatOfPoint2f( outlinePointsShoe2Shifted ), true);
		
		int width  = binArrayShoe1.length;// squaredCircleToCheck.getWidth();
		int height = binArrayShoe1[0].length;//squaredCircleToCheck.getHeight();
	
		ArrayList<Integer> iarrErrors = new ArrayList<Integer>();
		ArrayList<Integer> iBadPixels = new ArrayList<Integer>();
		Point pCenter = center1;//the center of the common contour (first shoe's gravity center)
		
		int iPixelError = -1;
		
		double dLineThreshold = 0.0;
		double dBadPixelThreshold = 0.0;
		
		boolean match=false;
		
		//set thresholds:
		double dLineThresholdValue=0.02;
		double  dBadPixelThresholdValue=0.5;
	
		
		
		for (int i = 0; i < outlinePointsShoe2Shifted.length; i++) {
			int x1 = (int) outlinePointsShoe2Shifted[i].x;
			int y1 = (int) outlinePointsShoe2Shifted[i].y;
			ArrayList<int[]> iarrPixelsOnTheLine_InsideCircle  = getPixelsOnTheLineBetweenTwoPoints (new int[]{(int) pCenter.x, (int) pCenter.y}, new int[]{x1, y1});
			ArrayList<int[]> iarrPixelsOnTheLine_OutsideCircle = getNextPixelsOnTheLineOutsideCircle(new int[]{(int) pCenter.x, (int) pCenter.y}, new int[]{x1, y1}, width, height);    
			
			
			
			
			for (int j = (iarrPixelsOnTheLine_InsideCircle.size() - 1), k = 0; j >= 0; j--, k++) {
				int xj = iarrPixelsOnTheLine_InsideCircle.get(j)[0];
				int yj = iarrPixelsOnTheLine_InsideCircle.get(j)[1];
				Core.circle(overlayImage, new Point(xj, yj), 1,new Scalar(255,0,0), 1);
				
				if (binArrayShoe1[xj][yj]==0) {//check inside the circle portion
					iarrErrors.add(new Integer(k));
					iPixelError = k;
					break;
				}
				if (k < iarrPixelsOnTheLine_OutsideCircle.size()) {//check outside the circle portion
					int xk = iarrPixelsOnTheLine_OutsideCircle.get(k)[0];
					int yk = iarrPixelsOnTheLine_OutsideCircle.get(k)[1];
					
					Core.circle(overlayImage, new Point(xk, yk), 1,new Scalar(0,255,255), 1);
					
					if (binArrayShoe1[xk][yk]==0) {
						iarrErrors.add(new Integer(k));
						iPixelError = k;
						break;
					}
				}
			}
			
			if (iPixelError == -1) 
				iBadPixels.add(new Integer(iPixelError));
			else {
				dLineThreshold = (double) 2.0*((double)iPixelError)/ diagonal;
				//System.out.println("dLineThreshold: " + dLineThreshold);
				if (dLineThreshold > dLineThresholdValue)//0.2
					iBadPixels.add(new Integer(iPixelError));
			}
		}
		
		
		dBadPixelThreshold = (double)iBadPixels.size() / circumference;
		//System.out.println("Circumference "+  circumference);
		System.out.println("iBadPixels.size() = "+iBadPixels.size()+"   dBadPixelThreshold = "+dBadPixelThreshold);
		if (dBadPixelThreshold <= dBadPixelThresholdValue) { //0.01
			
//			try {
//				graphics.dispose();
//				ImageIO.write(squaredCircleToCheck, "jpg", new File(".\\output\\blobs\\" + iImageNo + ".jpg"));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			return true;
			
		}
		
		return false;
		
	}//END OF contoursMatched
	
	
	
	/**
	 * 
	 * @param contour
	 * @return
	 */
	public static Point findGravityCenter(MatOfPoint contour){
		//calculate moments, center of gravity and axis tilt
				Moments m = Imgproc.moments(contour, true);
				// center of gravity
				
				double m00 =m.get_m00();
				double m10 = m.get_m10();
				double m01 =m.get_m01();
				
				int xCenter =0;
				int yCenter=0;
				
				if (m00 != 0) {   // calculate center
					xCenter = (int) Math.round(m10/m00);
					yCenter = (int) Math.round(m01/m00);
					return new Point(xCenter,yCenter);
					}
				else 
					return null;		
	}
	
	/**
	 * transpose int[][] matrix of binary image data
	 * @param m
	 * @return
	 */
	public static int[][] transposeMatrix(int [][] m){
		int[][] temp = new int[m[0].length][m.length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[0].length; j++)
                temp[j][i] = m[i][j];
        return temp;
    }
	
	
	
	
	/**
	 * get the pixel locations between two pixels
	 * @param startpixelxy start point of the first pixel
	 * @param endpixelxy end point of the second pixel
	 * @return pixel array between two points
	 */
	private static ArrayList<int[]> getPixelsOnTheLineBetweenTwoPoints(int startpixelxy[], int endpixelxy[]) {
		//Bresenham Algorithm
		ArrayList<int[]> iarrPixelsInLine = new ArrayList<int[]>();
		int x  = startpixelxy[0],  y  = startpixelxy[1];
		int x2 = endpixelxy[0],    y2 = endpixelxy[1];
		
		int w = x2 - x;
	    int h = y2 - y;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
	    if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
	    if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
	    if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
	    int longest = Math.abs(w);
	    int shortest = Math.abs(h);
	    
	    if (!(longest > shortest)) {
	        longest = Math.abs(h);
	        shortest = Math.abs(w) ;
	        if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
	        dx2 = 0;            
	    }
	    
	    int numerator = longest >> 1;
		
	    for (int i = 0; i <= (longest); i++) {
	        iarrPixelsInLine.add(new int[]{x, y});
	        numerator += shortest;
	        if (!(numerator < longest)) {
	            numerator -= longest;
	            x += dx1;
	            y += dy1;
	        } else {
	            x += dx2;
	            y += dy2;
	        }
	    }
	    
		return iarrPixelsInLine;
	}
	
	
	
	/**
	 * this function predicts pixels beyond the line until it reaches the image boundaries
	 * @param startpixelxy start pixel or the center of the circle
	 * @param circlepixelxy pixel on the circle
	 * @param width width of the image 
	 * @param height height of the image 
	 * @return array list with the pixel x,y coordinates 
	 */
	private static ArrayList<int[]> getNextPixelsOnTheLineOutsideCircle(int startpixelxy[], int circlepixelxy[], int width, int height) {
		int iExtraPixels = 20;
		//Bresenham Algorithm
		ArrayList<int[]> iarrPixelsOutLine = new ArrayList<int[]>();
		int x  = startpixelxy[0],  y  = startpixelxy[1];
		int x2 = circlepixelxy[0],    y2 = circlepixelxy[1];
		
		int w = x2 - x;
	    int h = y2 - y;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
	    if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
	    if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
	    if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
	    int longest = Math.abs(w);
	    int shortest = Math.abs(h);
	    
	    if (!(longest > shortest)) {
	        longest = Math.abs(h);
	        shortest = Math.abs(w) ;
	        if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
	        dx2 = 0;            
	    }
	    
	    int numerator = longest >> 1;
		
	    for (int i = 0; i <= (longest+iExtraPixels); i++) {
	    	if ( (x >= (width)) || (y >= (height)) ) break;
	    	if ( (x < 0) || (y < 0) ) break;
	    	if (i > longest)
	    		iarrPixelsOutLine.add(new int[]{x, y});
	        
	        numerator += shortest;
	        if (!(numerator < longest)) {
	            numerator -= longest;
	            x += dx1;
	            y += dy1;
	        } else {
	            x += dx2;
	            y += dy2;
	        }
	    }
	    
		return iarrPixelsOutLine;
		
	}
	
	
	/**
	 * return true if the pixel is white color
	 * @param x x cordinates of the pixel
	 * @param y y cordinates of the pixel
	 * @param squaredCircleToCheck buffered-image object of the circle segment
	 * @return true if the pixel is white color
	 */
	private static boolean isBlackPixel(int x , int y, BufferedImage squaredCircleToCheck) {
		try {
			int rgb = squaredCircleToCheck.getRGB(x, y);
			int r = (rgb >> 16) & 0xFF;
			int g = (rgb >> 8) & 0xFF;
			int b = (rgb & 0xFF);
			int gray = (r + g + b) / 3;
			//System.out.println("isBlackPixel - gray level: " + gray);
			if (gray < 5) { //almost black pixel //if (gray < 100) {
				//System.out.println("isBlackPixel: " + gray);
				return true;
			}
		} catch (ArrayIndexOutOfBoundsException arrex) {
			System.out.println("x: " + x + " y: " + y + " W: " + squaredCircleToCheck.getWidth() + " H: " + squaredCircleToCheck.getHeight());
			arrex.printStackTrace();
		}
		return false;
	}
	
	
	
	/**
	 * get the adjacent matrix of any size for a given pixel location
	 * @param row original pixel row value
	 * @param col original pixel column value
	 * @return 2D array of adjacent matrix of pixels
	 */
	static int[][] getAdjacentPixelMatrix2(int row, int col, int[][] squaredCircleToCheck, int windowSize) {
		
		int [][] iarrAdjacentPixelMatrix = null;
		int sizeX=squaredCircleToCheck.length;
		int sizeY=squaredCircleToCheck[0].length;
		iarrAdjacentPixelMatrix = new int[windowSize][windowSize];
		int i = 0, j = 0;
	
		
		if ( (row < windowSize/2) &&  (col < windowSize/2)){
				for (int r = 0; r <=windowSize-1; r++) {
				    for (int c = 0; c <=windowSize-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}///////////////////////////
			
		if ( (row < windowSize/2) &&  (col<sizeY-windowSize/2) && (col>=windowSize/2)){
			for (int r = 0; r <=windowSize-1; r++) {
			    for (int c = (col-windowSize/2); c < (col+windowSize/2); c++) {
			    	int value = squaredCircleToCheck[r][c];
			    	iarrAdjacentPixelMatrix[i][j] = value; 
			    	j++;
			    }
			    i++; j = 0;
			}
		}///////////////////////////
			if((row < windowSize/2) &&(col >= (sizeY-windowSize/2)) ){
				for (int r = 0; r <=windowSize-1; r++) {
				    for (int c = sizeY-windowSize; c <= sizeY-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			if((row >=(sizeX-windowSize/2)) &&(col >= (sizeY-windowSize/2)) ){
				for (int r = sizeX-windowSize; r <= sizeX-1; r++) {
				    for (int c = sizeY-windowSize; c <= sizeY-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >=windowSize/2) && (row <(sizeX-windowSize/2)) &&(col >= (sizeY-windowSize/2)) ){
				
				for (int r = row-windowSize/2; r <= row+windowSize/2; r++) {
				    for (int c = sizeY-windowSize; c <= sizeY-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >=windowSize/2) && (row < (sizeX-windowSize/2)) && (col < windowSize/2) ){
				
				for (int r = row-windowSize/2; r <= row+windowSize/2; r++) {
				    for (int c = 0; c <= windowSize-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >= sizeX-windowSize/2)  && (col < windowSize/2) ){
				
				for (int r = sizeX-windowSize; r <= sizeX-1; r++) {
				    for (int c = 0; c <= windowSize-1; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			
			if((row >=(sizeX-windowSize/2)) && (col >= windowSize/2)  && (col< (sizeY-windowSize/2) )){
				
				for (int r = sizeX-windowSize; r <=sizeX-1; r++) {
					for (int c = col-windowSize/2; c <=col+windowSize/2; c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}/////////////////////////////
			
			if(row>=windowSize/2 && row< sizeX-windowSize/2 && col>=windowSize/2 && col< sizeY-windowSize/2){
				for (int r = (row-windowSize/2); r <= (row+windowSize/2); r++) {
				    for (int c = (col-windowSize/2); c <= (col+windowSize/2); c++) {
				    	int value = squaredCircleToCheck[r][c];
				    	iarrAdjacentPixelMatrix[i][j] = value; 
				    	j++;
				    }
				    i++; j = 0;
				}
			}
			
			
		return iarrAdjacentPixelMatrix;
	}
	
	/**
	 * 
	 * @param window
	 * @return
	 */
	public static boolean containsNeighboringPixels(int[][] window){
		int result=0;
		int size=window.length;
		int row=size/2;
		int col=size/2;
		for (int r = (row-size/2); r <=(row+size/2); r++) {
		    for (int c = (col-size/2); c <= (col+size/2); c++) {
		    	if(r!=row && c!=col && window[r][c]==0)
		    		result+=1;
		    }
		}
		return (result>0?true:false);
	}
	
	
}//END OF CLASS
