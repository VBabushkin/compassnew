package shoeFittingAlgorithm;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import org.imgscalr.*;

/**
 * this class executes basic image operations such as cropping and scaling 
 * @author nimesha
 *
 */
public class ImageSegmentation {

	/**
	 * image crop operation
	 * @param src source image in BufferedImage
	 * @param x starting cordinates X
	 * @param y starting cordinates Y
	 * @param w width of the sub-image to be extracted
	 * @param h height of the sub-image to be extracted
	 * ex: img1 = new ImageOperations().cropImage(ImageIO.read(new File("1.jpg")), 65, 30, 520, 445);
	 * @return new sub-image
	 */
	public BufferedImage cropImage(BufferedImage src, int x, int y, int w, int h) {
	      BufferedImage img = src.getSubimage(x, y, w, h); //fill in the corners of the desired crop location here
	      BufferedImage copyOfImage = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
	      Graphics g = copyOfImage.createGraphics();
	      g.drawImage(img, 0, 0, null);
	      
	      return copyOfImage; 
	} 
	
	/**
	 * image re-scale
	 * @param src source image in BufferedImage
	 * @param imageType buffered image type - ex: TYPE_INT_RGB
	 * @param dWidth new image width
	 * @param dHeight new image height
	 * @param fWidth  factor by which coordinates are scaled along the X axis direction
	 * @param fHeight  factor by which coordinates are scaled along the Y axis direction\\
	 * ex: img1 = rescaleImage(ImageIO.read(new File("1.jpg")), 1, 400, 300, 0.625, 0.625);
	 * @return the new re-scaled image
	 */
//	public BufferedImage rescaleImage1(BufferedImage src, int imageType, int dWidth, int dHeight, double fWidth, double fHeight) {
//		BufferedImage dbi = null;
//		if(src != null) {
//			dbi = new BufferedImage(dWidth, dHeight, imageType); 
//			Graphics2D g = dbi.createGraphics();
//			AffineTransform at = AffineTransform.getScaleInstance(fWidth, fHeight);
//			g.drawRenderedImage(src, at);
//		}
//		return dbi;
//	}
	
	/**
	 * rescale the buffered image
	 * @param src source image in BufferedImage
	 * @param width new image width
	 * @param height new image height
	 * @return the new re-scaled image
	 */
	public BufferedImage rescaleImage(BufferedImage src, int width, int height) {
		BufferedImage dbi = null;
		if(src != null) {
			dbi = Scalr.resize(src, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, width, height, Scalr.OP_BRIGHTER);
		}
		return dbi;
	}
	
	
}
