package shoeFittingAlgorithm;

import static org.bytedeco.javacpp.opencv_core.CV_32FC1;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_32F;
import static org.bytedeco.javacpp.opencv_core.cvCloneImage;
import static org.bytedeco.javacpp.opencv_core.cvCreateImage;
import static org.bytedeco.javacpp.opencv_core.cvCreateMat;
import static org.bytedeco.javacpp.opencv_core.cvGetSize;
import static org.bytedeco.javacpp.opencv_core.cvReleaseImage;
import static org.bytedeco.javacpp.opencv_core.cvScalarAll;
import static org.bytedeco.javacpp.opencv_imgproc.CV_INTER_LINEAR;
import static org.bytedeco.javacpp.opencv_imgproc.CV_WARP_FILL_OUTLIERS;
import static org.bytedeco.javacpp.opencv_imgproc.cvInitUndistortMap;
import static org.bytedeco.javacpp.opencv_imgproc.cvRemap;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.bytedeco.javacpp.opencv_core.CvMat;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.CanvasFrame;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;



public class CalibrateCamera {
	
	public static void main(String args[]) throws IOException{
		CanvasFrame undistortFrame = new CanvasFrame("Undistort");
		CanvasFrame rawFrame = new CanvasFrame("Raw Video");
		String filename="shoes_7_660";
		BufferedImage img =  ImageIO.read(new File("./RawPhotos/"+filename+".jpg"));//("./results/distorted.jpg") );
		IplImage image = IplImage.createFrom(img);
		
		rawFrame.showImage(image);
		//to measure the time taken
		long startTime = System.currentTimeMillis();
		
		
		CvMat intrinsic_matrix = cvCreateMat(3, 3, CV_32FC1);
		CvMat distortion_coeffs = cvCreateMat(4, 1, CV_32FC1);

		// At this point we have all of the chessboard corners we need.
		// Initialize the intrinsic matrix such that the two focal
		// lengths have a ratio of 1.0
		//
		// the CV_MAT_ELEM() macro corresponds to CvMat.put(row,col,val) and CvMat.get(row,col).
		// the put and get methods account for the primitive type lengths using pointer arithmetic.

		// CV_MAT_ELEM( *intrinsic_matrix, float, 0, 0 ) = 1.0f;
				intrinsic_matrix.put(0, 0, 320.0); // focal length x//240
				intrinsic_matrix.put(0, 1, 0.0);
				intrinsic_matrix.put(0, 2, 320.0); // center x//240

				intrinsic_matrix.put(1, 0, 0.0);
				intrinsic_matrix.put(1, 1, 240.0); // focal length y [= x * h / w]//320
				intrinsic_matrix.put(1, 2, 240.0); // center y//320

				intrinsic_matrix.put(2, 0, 0.0);
				intrinsic_matrix.put(2, 1, 0.0);
				intrinsic_matrix.put(2, 2, 1.0); // flat z

				
				double p_factor = 0.00;
				
				distortion_coeffs.put(0, 0, -0.085); // k1 * r^2 //for Tamron -0.055
				distortion_coeffs.put(1, 0, 0.0); // k2 * r^4
				distortion_coeffs.put(2, 0, -p_factor); // tangential p1
				distortion_coeffs.put(3, 0, p_factor); // tangential p2

				printMatrix("intrisic matrix",intrinsic_matrix);
				printMatrix("distortion coeffs",distortion_coeffs);

				// SAVE THE INTRINSICS AND DISTORTIONS
				System.out.println(" *** DONE!\n\nStoring Intrinsics.xml and Distortions.xml files\n\n");
				//todo: print out to xml file
//				cvSave("Intrinsics.xml", intrinsic_matrix);
//				cvSave("Distortion.xml", distortion_coeffs);
				//

				// EXAMPLE OF LOADING THESE MATRICES BACK IN:
//				CvMat intrinsic = (CvMat) cvLoad("Intrinsics.xml");
//				CvMat distortion = (CvMat) cvLoad("Distortion.xml");
				
				CvMat intrinsic = intrinsic_matrix;
				CvMat distortion = distortion_coeffs;

				printMatrix("intrisic",intrinsic);
				printMatrix("distortion",distortion);

				// Build the undistort map which we will use for all
				// subsequent frames.
				//
				IplImage mapx = cvCreateImage(cvGetSize(image), IPL_DEPTH_32F, 1);
				IplImage mapy = cvCreateImage(cvGetSize(image), IPL_DEPTH_32F, 1);
				cvInitUndistortMap(intrinsic, distortion, mapx, mapy);
				// Just run the camera to the screen, now showing the raw and
				// the undistorted image.
				//

				
				//while (image != null) {
					IplImage t = cvCloneImage(image);
					rawFrame.showImage(image); // Show raw image
					cvRemap(t, image, mapx, mapy, CV_INTER_LINEAR
							| CV_WARP_FILL_OUTLIERS, cvScalarAll(0));
					cvReleaseImage(t);
					undistortFrame.showImage(image); // Show corrected image

					//image = grabber.grab();// cvQueryFrame( capture );
				//}
					
				long estimatedTime = System.currentTimeMillis() - startTime;
				ImageIO.write(image.getBufferedImage(), "jpg", new File("./RawPhotos/u_"+filename+".jpg"));//".\\results\\undistorted.jpg"));	
				System.out.println("TIME ELAPSED FOR DISTORTION REMOVAL MILLISECONDS "+ estimatedTime);
				System.out.println("TIME ELAPSED FOR DISTORTION REMOVAL SECONDS "+ TimeUnit.SECONDS.convert(estimatedTime, TimeUnit.MILLISECONDS));

	}
	
	
	/**
	 * 
	 * @param label
	 * @param matrix
	 */
	private static void printMatrix(String label, CvMat matrix) {
		int rows = matrix.rows();
		int cols = matrix.cols();
		
		System.out.println("matrix "+label);
		for (int i=0; i<rows; i++) {
			String rowStr = "(";
			for (int j=0; j<cols; j++) {
				rowStr += matrix.get(i,j);
				if (j<cols-1) rowStr+=",";
			}
			rowStr += ")";
			System.out.println(rowStr);
		}
	}


}
