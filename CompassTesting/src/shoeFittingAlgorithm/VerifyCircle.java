package shoeFittingAlgorithm;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

/**
 * verify a circle based on a simple method
 * first get circle like blobs, runs through a test where it compares actual pixel locations with an imaginary circle
 * @author nimesha
 *
 */
public class VerifyCircle {

	private ArrayList<Integer> iarrErrors = null;
	private ArrayList<Integer> iBadPixels = null;
	private double dErrorThreshold        = 0.0;
	//private int iImageNo = 0;
	
	/**
	 * after extracting blobs each blob is undergoing a reviewing process to determine it contains a circle or not
	 * @param x blob x in main image
	 * @param y blob y in main image
	 * @param w width of the blob
	 * @param h height of the blob
	 * @param img main buffered image
	 * @param n blob index
	 * @return true if detected a circle like structure
	 * @throws IOException
	 */
	public boolean isCircle(int x,int y,int w,int h, BufferedImage img, Graphics2D graph, double dLineThresholdValue, double dBadPixelThresholdValue){
		
		BufferedImage blobCircleToCheck = null;
		int r = 0;
		
		int wh = w;
		if (w > h) wh = w;
		if (h > w) wh = h;

		if ( (x + wh) > img.getWidth())  return false;
		if ( (y + wh) > img.getHeight()) return false;
		
		blobCircleToCheck = img.getSubimage(x, y, wh, wh);	
		//ImageIO.write(blobCircleToCheck, "jpg", new File(".\\output\\blobs_cc\\" + n + ".jpg"));
		
		r = (blobCircleToCheck.getWidth()/2)-1;
		
		ArrayList<int[]> iarrImaginaryCirclePixels = generateImaginaryCirclePixelsArray(r, r, r);
		
		/////////// verify imaginary circles ////////////

		graph.setStroke(new BasicStroke(1));
		graph.setColor(Color.RED);
		
		for (int i = 0; i < iarrImaginaryCirclePixels.size(); i++) {
			int x1 = iarrImaginaryCirclePixels.get(i)[0]+x;
			int y1 = iarrImaginaryCirclePixels.get(i)[1]+y;
			graph.drawLine(x1, y1, x1, y1);
		}
//		iImageNo++;
		/////////////////////////////////////////////////
		
		return passedCircleTest(r, blobCircleToCheck, iarrImaginaryCirclePixels,dLineThresholdValue, dBadPixelThresholdValue);
		
	}
	
	/**
	 * detect circle like structures based on pixel color and generate a distance/error array
	 * then selects best matches based on error threshold
	 * @param r radius of the circle
	 * @param squaredCircleToCheck BufferedImage of the circle image
	 * @param iarrImaginaryCirclePixels pixels of the imaginary circle
	 * @return boolean value to indicate this is a circle based on a given threshold
	 */
	private boolean passedCircleTest(int r, BufferedImage squaredCircleToCheck, ArrayList<int[]> iarrImaginaryCirclePixels, double dLineThresholdValue, double dBadPixelThresholdValue) {
		
//		Graphics2D graphics = squaredCircleToCheck.createGraphics();
//		graphics.setColor(Color.RED);
//		for (int i = 0; i < iarrImaginaryCirclePixels.size(); i++) {
//			int x1 = iarrImaginaryCirclePixels.get(i)[0];
//			int y1 = iarrImaginaryCirclePixels.get(i)[1];
//			graphics.drawLine(x1, y1, x1, y1);
//		}
//		graphics.setColor(Color.GREEN);
		
		iarrErrors = new ArrayList<Integer>();
		iBadPixels = new ArrayList<Integer>();
		int width  = squaredCircleToCheck.getWidth();
		int height = squaredCircleToCheck.getHeight();
		Point pCenter = new Point(r, r);
		//int iSumOfError = 0;
		int iPixelError = -1;
		
		double dLineThreshold = 0.0;
		double dBadPixelThreshold = 0.0;
		double circumference = (double) 2 * Math.PI * r;
		
		
		for (int i = 0; i < iarrImaginaryCirclePixels.size(); i++) {
			int x1 = iarrImaginaryCirclePixels.get(i)[0];
			int y1 = iarrImaginaryCirclePixels.get(i)[1];
			
			ArrayList<int[]> iarrPixelsOnTheLine_InsideCircle  = getPixelsOnTheLineBetweenTwoPoints (new int[]{pCenter.x, pCenter.y}, new int[]{x1, y1});
			ArrayList<int[]> iarrPixelsOnTheLine_OutsideCircle = getNextPixelsOnTheLineOutsideCircle(new int[]{pCenter.x, pCenter.y}, new int[]{x1, y1}, width, height);    
			
			for (int j = (iarrPixelsOnTheLine_InsideCircle.size() - 1), k = 0; j >= 0; j--, k++) {
				int xj = iarrPixelsOnTheLine_InsideCircle.get(j)[0];
				int yj = iarrPixelsOnTheLine_InsideCircle.get(j)[1];
				if (!isBlackPixel(xj, yj, squaredCircleToCheck)) {//check inside the circle portion
					iarrErrors.add(new Integer(k));
					iPixelError = k;
					break;
				}
				if (k < iarrPixelsOnTheLine_OutsideCircle.size()) {//check outside the circle portion
					int xk = iarrPixelsOnTheLine_OutsideCircle.get(k)[0];
					int yk = iarrPixelsOnTheLine_OutsideCircle.get(k)[1];
					if (!isBlackPixel(xk, yk, squaredCircleToCheck)) {
						iarrErrors.add(new Integer(k));
						iPixelError = k;
						break;
					}
				}
			}
			
			if (iPixelError == -1) 
				iBadPixels.add(new Integer(iPixelError));
			else {
				dLineThreshold = (double) ((double)iPixelError) / r;
				//System.out.println("dLineThreshold: " + dLineThreshold);
				if (dLineThreshold > dLineThresholdValue)//0.2
					iBadPixels.add(new Integer(iPixelError));
			}
		}
		
		
		dBadPixelThreshold = iBadPixels.size() / circumference;
		//System.out.println(iBadPixels.size()+"    "+dBadPixelThreshold);
		if (dBadPixelThreshold <= dBadPixelThresholdValue) { //0.01
			
//			try {
//				graphics.dispose();
//				ImageIO.write(squaredCircleToCheck, "jpg", new File(".\\output\\blobs\\" + iImageNo + ".jpg"));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			return true;
		}
		
		return false;
		
	}
	
	
	/**
	 * first implementation
	 * @param r
	 * @param squaredCircleToCheck
	 * @param iarrImaginaryCirclePixels
	 * @return
	 */
//	private boolean passedCircleTest_OLD(int r, BufferedImage squaredCircleToCheck, ArrayList<int[]> iarrImaginaryCirclePixels) {
//		iarrErrors = new ArrayList<Integer>();
//		int width  = squaredCircleToCheck.getWidth();
//		int height = squaredCircleToCheck.getHeight();
//		Point pCenter = new Point(r, r);
//		int iSumOfError = 0;
//		
//		for (int i = 0; i < iarrImaginaryCirclePixels.size(); i++) {
//			int x1 = iarrImaginaryCirclePixels.get(i)[0];
//			int y1 = iarrImaginaryCirclePixels.get(i)[1];
//			
//			ArrayList<int[]> iarrPixelsOnTheLine_InsideCircle  = getPixelsOnTheLineBetweenTwoPoints (new int[]{pCenter.x, pCenter.y}, new int[]{x1, y1});
//			ArrayList<int[]> iarrPixelsOnTheLine_OutsideCircle = getNextPixelsOnTheLineOutsideCircle(new int[]{pCenter.x, pCenter.y}, new int[]{x1, y1}, width, height);    
//			
//			for (int j = (iarrPixelsOnTheLine_InsideCircle.size() - 1), k = 0; j >= 0; j--, k++) {
//				int xj = iarrPixelsOnTheLine_InsideCircle.get(j)[0];
//				int yj = iarrPixelsOnTheLine_InsideCircle.get(j)[1];
//				if (isBlackPixel(xj, yj, squaredCircleToCheck)) {//inside the circle portion
//					iarrErrors.add(new Integer(k));
//					break;
//				}
//				if (k < iarrPixelsOnTheLine_OutsideCircle.size()) {//outside the circle portion
//					int xk = iarrPixelsOnTheLine_OutsideCircle.get(k)[0];
//					int yk = iarrPixelsOnTheLine_OutsideCircle.get(k)[1];
//					if (isBlackPixel(xk, yk, squaredCircleToCheck)) {
//						iarrErrors.add(new Integer(k));
//						break;
//					}
//				}
//			}
//		}
//		
//		//sum of errors
//		for (int i = 0; i < iarrErrors.size(); i++) {
//			int errorValue = iarrErrors.get(i);
//			iSumOfError = iSumOfError + errorValue;
//		}
//		//calculate error T
//		double dErrorThreshold = (double) iSumOfError / ((double)iarrErrors.size());
//		setErrorThreshold(dErrorThreshold);
//		
//		if (dErrorThreshold <= 0.8) 
//			return true;
//		
//		return false;
//	}
	
	/**
	 * return true if the pixel is white color
	 * @param x x cordinates of the pixel
	 * @param y y cordinates of the pixel
	 * @param squaredCircleToCheck buffered-image object of the circle segment
	 * @return true if the pixel is white color
	 */
	private boolean isBlackPixel(int x , int y, BufferedImage squaredCircleToCheck) {
		try {
			int rgb = squaredCircleToCheck.getRGB(x, y);
			int r = (rgb >> 16) & 0xFF;
			int g = (rgb >> 8) & 0xFF;
			int b = (rgb & 0xFF);
			int gray = (r + g + b) / 3;
			//System.out.println("isBlackPixel - gray level: " + gray);
			if (gray < 5) { //almost black pixel //if (gray < 100) {
				//System.out.println("isBlackPixel: " + gray);
				return true;
			}
		} catch (ArrayIndexOutOfBoundsException arrex) {
			System.out.println("x: " + x + " y: " + y + " W: " + squaredCircleToCheck.getWidth() + " H: " + squaredCircleToCheck.getHeight());
			arrex.printStackTrace();
		}
		return false;
	}
	
	/**
	 * this function predicts pixels beyond the line until it reaches the image boundaries
	 * @param startpixelxy start pixel or the center of the circle
	 * @param circlepixelxy pixel on the circle
	 * @param width width of the image 
	 * @param height height of the image 
	 * @return array list with the pixel x,y coordinates 
	 */
	private ArrayList<int[]> getNextPixelsOnTheLineOutsideCircle(int startpixelxy[], int circlepixelxy[], int width, int height) {
		int iExtraPixels = 20;
		//Bresenham Algorithm
		ArrayList<int[]> iarrPixelsOutLine = new ArrayList<int[]>();
		int x  = startpixelxy[0],  y  = startpixelxy[1];
		int x2 = circlepixelxy[0],    y2 = circlepixelxy[1];
		
		int w = x2 - x;
	    int h = y2 - y;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
	    if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
	    if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
	    if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
	    int longest = Math.abs(w);
	    int shortest = Math.abs(h);
	    
	    if (!(longest > shortest)) {
	        longest = Math.abs(h);
	        shortest = Math.abs(w) ;
	        if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
	        dx2 = 0;            
	    }
	    
	    int numerator = longest >> 1;
		
	    for (int i = 0; i <= (longest+iExtraPixels); i++) {
	    	if ( (x >= (width)) || (y >= (height)) ) break;
	    	if ( (x < 0) || (y < 0) ) break;
	    	if (i > longest)
	    		iarrPixelsOutLine.add(new int[]{x, y});
	        
	        numerator += shortest;
	        if (!(numerator < longest)) {
	            numerator -= longest;
	            x += dx1;
	            y += dy1;
	        } else {
	            x += dx2;
	            y += dy2;
	        }
	    }
	    
		return iarrPixelsOutLine;
		
	}
	
	/**
	 * get the pixel locations between two pixels
	 * @param startpixelxy start point of the first pixel
	 * @param endpixelxy end point of the second pixel
	 * @return pixel array between two points
	 */
	private ArrayList<int[]> getPixelsOnTheLineBetweenTwoPoints(int startpixelxy[], int endpixelxy[]) {
		//Bresenham Algorithm
		ArrayList<int[]> iarrPixelsInLine = new ArrayList<int[]>();
		int x  = startpixelxy[0],  y  = startpixelxy[1];
		int x2 = endpixelxy[0],    y2 = endpixelxy[1];
		
		int w = x2 - x;
	    int h = y2 - y;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
	    if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
	    if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
	    if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
	    int longest = Math.abs(w);
	    int shortest = Math.abs(h);
	    
	    if (!(longest > shortest)) {
	        longest = Math.abs(h);
	        shortest = Math.abs(w) ;
	        if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
	        dx2 = 0;            
	    }
	    
	    int numerator = longest >> 1;
		
	    for (int i = 0; i <= (longest); i++) {
	        iarrPixelsInLine.add(new int[]{x, y});
	        numerator += shortest;
	        if (!(numerator < longest)) {
	            numerator -= longest;
	            x += dx1;
	            y += dy1;
	        } else {
	            x += dx2;
	            y += dy2;
	        }
	    }
	    
		return iarrPixelsInLine;
	}
	
	
	/**
	 * generate pixels on the circle based on center point and radius 
	 * @param centerX center X coordinates of the circle
	 * @param centerY center Y coordinates of the circle
	 * @param r radius of the circle
	 * @return pixel array of the circle
	 */
	private ArrayList<int[]> generateImaginaryCirclePixelsArray(int centerX, int centerY, int r) {
		//if 0,0
		ArrayList<int[]> iarrImaginaryCirclePixels = new ArrayList<int[]>();
		
		if (r < 2) return null;
		
		int d = (5 - r * 4)/4;
		int x = 0;
		int y = r;
 
		do {
			iarrImaginaryCirclePixels.add(new int[]{centerX + x, centerY + y});
			iarrImaginaryCirclePixels.add(new int[]{centerX + x, centerY - y});
			iarrImaginaryCirclePixels.add(new int[]{centerX - x, centerY + y});
			iarrImaginaryCirclePixels.add(new int[]{centerX - x, centerY - y});
			
			iarrImaginaryCirclePixels.add(new int[]{centerX + y, centerY + x});
			iarrImaginaryCirclePixels.add(new int[]{centerX + y, centerY - x});
			iarrImaginaryCirclePixels.add(new int[]{centerX - y, centerY + x});
			iarrImaginaryCirclePixels.add(new int[]{centerX - y, centerY - x});
			
			if (d < 0) {
				d += 2 * x + 1;
			} else {
				d += 2 * (x - y) + 1;
				y--;
			}
			x++;
		} while (x <= y);
		
		return iarrImaginaryCirclePixels;
	}

	
	//////////////// getters and setters for T //////////////////////
	public double getErrorThreshold() {
		return dErrorThreshold;
	}

	public void setErrorThreshold(double dErrorThreshold) {
		this.dErrorThreshold = dErrorThreshold;
	}
	
	
}



