package shoeFittingAlgorithm;

import java.io.Serializable;

/**
 * Attributes for each node, hold distance calculations
 * @author nimesha
 *
 */
public class AttributesMatrix implements Serializable {

	private double pdistance    = -1.0;
	private double rdistance    = -1.0;
	private double rorientation = -1.0;
	private String sShapeMethod = "-1";
	
	public double[] getDistanceOrientationArray() {
		return new double[]{pdistance, rdistance, rorientation};
	}

	public double getPdistance() {
		return pdistance;
	}

	public void setPdistance(double pdistance) {
		this.pdistance = pdistance;
	}

	public double getRdistance() {
		return rdistance;
	}

	public void setRdistance(double rdistance) {
		this.rdistance = rdistance;
	}

	public double getRorientation() {
		return rorientation;
	}

	public void setRorientation(double rorientation) {
		this.rorientation = rorientation;
	}

	public String getsShapeMethod() {
		return sShapeMethod;
	}

	public void setsShapeMethod(String sShapeMethod) {
		this.sShapeMethod = sShapeMethod;
	}
	
	
	

}