package shoeFittingAlgorithm;

import java.util.ArrayList;

public class CompareRows_T implements Runnable {
	
	//private ArrayList<Thread> arrCompareSingleRow_T = null;
	
	private AttributesMatrix[][] newMatrix           = null;
	private AttributesMatrix[][] existingMatrix = null;
	
	private ArrayList<Integer>  iarrMatchedRows      = new ArrayList<Integer>();
	private ArrayList<Integer>  iarrMatchedColumns   = null; //matched elements
	
	private int iStart = -1;
	private int iEnd   = -1;
	
	public CompareRows_T(int iStart, int iEnd, AttributesMatrix[][] newMatrix, AttributesMatrix[][] existingMatrix, ArrayList<Integer> iarrMatchedRows) {
		this.iStart = iStart;
		this.iEnd   = iEnd;
		this.newMatrix           = newMatrix;
		this.existingMatrix = existingMatrix;
		this.iarrMatchedRows = iarrMatchedRows;
		
	}

	@Override
	public void run() {
		
		for (int a = iStart; a < iEnd; a++) {
			if (a >= existingMatrix.length) break;
			AttributesMatrix[] rowOfCurrentMatrix   = newMatrix[a];
			
			for (int b = 0; b < existingMatrix.length; b++) {
				
				if (iarrMatchedRows.contains(new Integer(b))) continue;
				
				AttributesMatrix[] rowOfPreviousMatrix  = existingMatrix[b];
				iarrMatchedColumns = new ArrayList<Integer>();
				
				for (int i = 0; i < rowOfCurrentMatrix.length; i++) {//starting from 1 since 0th position has '-1'
					for (int j = 0; j < rowOfPreviousMatrix.length; j++) {
						
						if ( iarrMatchedColumns.contains(new Integer(j)) )
							continue;
						
						if ( !( (b == i) || (b == j) ) ) 
						{
							if (    (rowOfCurrentMatrix[i].getsShapeMethod().equalsIgnoreCase("L2L")) && (rowOfPreviousMatrix[j].getsShapeMethod().equalsIgnoreCase("L2L")) ||
									(rowOfCurrentMatrix[i].getsShapeMethod().equalsIgnoreCase("C2C")) && (rowOfPreviousMatrix[j].getsShapeMethod().equalsIgnoreCase("C2C")) ||
									(rowOfCurrentMatrix[i].getsShapeMethod().equalsIgnoreCase("C2L")) && (rowOfPreviousMatrix[j].getsShapeMethod().equalsIgnoreCase("C2L")) ||
									(rowOfCurrentMatrix[i].getsShapeMethod().equalsIgnoreCase("L2C")) && (rowOfPreviousMatrix[j].getsShapeMethod().equalsIgnoreCase("L2C"))  
							   ) {

								double pddiff = Math.abs( Math.abs(rowOfCurrentMatrix[i].getPdistance())    - Math.abs(rowOfPreviousMatrix[j].getPdistance())    );
								double rddiff = Math.abs( Math.abs(rowOfCurrentMatrix[i].getRdistance())    - Math.abs(rowOfPreviousMatrix[j].getRdistance())    );
								double ordiff = Math.abs( Math.abs(rowOfCurrentMatrix[i].getRorientation()) - Math.abs(rowOfPreviousMatrix[j].getRorientation()) );
								//TODO:
								//Play with these parameters
								if ( (pddiff < 20) && (rddiff < 20) && (ordiff < 20) ) { //&& (ordiff < 10)
									
									iarrMatchedColumns.add(new Integer(j)); 
									break;
								}
							}
						}
					}
				}
				
				double percentage = (double) ( (iarrMatchedColumns.size() * 100.0) / ( (double) (newMatrix.length-1)) );
				//System.out.println("iMPoints: " + iMPoints + " rowOfCurrentMatrix.length: " + rowOfCurrentMatrix.length + " %: " + percentage);
				
				if (percentage > 60.0) {
					iarrMatchedRows.add( new Integer(b) ); //a match found
					break;
				}
				
			}
			//System.out.print(a + ", ");
		}
					
	}

}
