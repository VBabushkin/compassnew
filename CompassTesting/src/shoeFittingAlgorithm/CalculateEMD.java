package shoeFittingAlgorithm;

import java.util.Arrays;
import org.apache.commons.math3.ml.distance.EarthMoversDistance;

/**
 * calculate earth movers distance of two double arrays
 * @author nimesha
 *
 */
public class CalculateEMD {

	public static int iCount = 0;
	
	public double simpleEMD(double[] dArray1, double[] dArray2) {
		EarthMoversDistance emd = new EarthMoversDistance();
		return emd.compute(dArray1, dArray2);
	}

	public static int EMD(double [][] matrix,double [][] dPreviousMatrix) {

		//System.out.println("\n!!!!!!!!!!!!!! Earth Movers Distance !!!!!!!!!!!!!!!!!!!!!"); 
		
		iCount = 0;
		EarthMoversDistance emd = new EarthMoversDistance();
		//double [][] dPreviousMatrix = null; //new SaveAndReadRelationalGraphData().readFromDisk(sPath);
		
		if (matrix.length > dPreviousMatrix.length)
			matrix = Arrays.copyOf(matrix, dPreviousMatrix.length);
		
		//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ matrix.length x dPreviousMatrix.length: " + matrix.length + " ## " + dPreviousMatrix.length);
		//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ matrix[].length x dPreviousMatrix[].length: " + matrix[1].length + " ## " + dPreviousMatrix[1].length);
		
		for (int i=0; i < matrix.length; i++) {
			for (int j=0; j < dPreviousMatrix.length; j++) {
				
				if (matrix[i].length > dPreviousMatrix[i].length)
					matrix[i] = Arrays.copyOf(matrix[i], dPreviousMatrix[i].length);
				
				double dd = emd.compute(matrix[i], dPreviousMatrix[j]);
				//System.out.println("EarthMoversDistance: i = " + i + " , j = " + j + " : " + dd);
				if (dd < 100) {
					iCount++;
					
				}
			}
		}
		
		//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MATCH: " + ( (iCount * 100)/(relativedistances_l2l.length) ) + "%");
		System.out.println("~~~~~~~~~~~~~~ iCount ~~~~~~~~~~~~~~~: " + iCount);
		return iCount;
	}
}
