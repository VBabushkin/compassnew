package shoeFittingAlgorithm;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractorMOG2;

public class TestContourExtractionGrabcut {
	
	
	final static int DIFF = 10;
	final static int LOWER_CONTOUR_AREA_THRESHOLD=6000;//20000
	final static int UPPER_CONTOUR_AREA_THRESHOLD=100000;//50000
	static String initialImagePath=".//RawPhotos//";//".//results//in.jpg";
	static String processedImagesPath=".//RawPhotos//";
	static String resultImagesPath=".//RawPhotos//";
	static String bckgImage =".//RawPhotos//background.jpg";//".//results//bckg.jpg";
	
	
	
	public static void main(String args[]){
		int userID= 18;
		int angle=924;
		//names after undistorting is performed
		String filename= "shoes_"+userID+"_"+angle;
		String backgroundFileName="bckg_"+userID;
		String initPath=initialImagePath+filename+".jpg";
		String bckgImagePath=initialImagePath+backgroundFileName+".jpg";
		
		//Split camera image by two
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		
		//read image
		Mat source = Highgui.imread(initPath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(source),"Initial Image");
		
		//read the background image
		Mat bckg = Highgui.imread(bckgImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bckg),"bckg");
		

		//with smoothing
		Mat initialImage =source.clone();
		Mat initialBck = bckg.clone();
		
//		// 2. blur
//	    //Imgproc.blur(initialImage, initialImage, new Size(10,10));
//	    //Imgproc.blur(initialBck, initialBck, new Size(10,10));
//	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(initialImage),"BLURRED");
//	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(initialBck),"BLURRED");
//	
//		
//		//applying background remover
//		 BackgroundSubtractorMOG2 bg=new BackgroundSubtractorMOG2();	
//		 Mat fgmask = new Mat();
//		 
//		 
//		 double learningRate = 0.05;
//		 bg.apply(initialBck, initialImage.clone(),learningRate);
//		 bg.apply(initialImage.clone(), fgmask,learningRate);
//		    
//		 ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(fgmask),"REMOVED   "+learningRate);
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

	    Rect rectangle = new Rect(0, 0, 320, 240);
	    Mat result = new Mat();
	    Mat bgdModel = new Mat();
	    Mat fgdModel = new Mat();
	    
	    Mat sourceInit = new Mat(1, 1, CvType.CV_8U, new Scalar(3));
	    Imgproc.grabCut(initialImage, result, rectangle, bgdModel, fgdModel, 8, Imgproc.GC_INIT_WITH_RECT);
	    Core.compare(result, sourceInit, result, Core.CMP_EQ);
	    Mat foreground = new Mat(initialImage.size(), CvType.CV_8UC3, new Scalar(255, 255, 255));
	    initialImage.copyTo(foreground, result);
	    
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(foreground),"REMOVED  Background ");
		
		   
	}
}
