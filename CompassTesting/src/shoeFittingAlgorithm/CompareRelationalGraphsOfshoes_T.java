package shoeFittingAlgorithm;

import java.util.ArrayList;


/**
 * invoke two threads to match pair of shoes individually
 * @author nimesha
 *
 */
public class CompareRelationalGraphsOfshoes_T implements Runnable {
	
	private ShoesInfo shoesInfo = null;
	private AttributesMatrix[][] attributesMatrix  = null;
	ArrayList<double[]> arrTotalMatch   = null;
	ArrayList<double[]> arrPartialMatch = null;
	
	
	public CompareRelationalGraphsOfshoes_T(ShoesInfo shoesInfo, AttributesMatrix[][] attributesMatrix, ArrayList<double[]> arrTotalMatch, ArrayList<double[]> arrPartialMatch) {
		this.shoesInfo              = shoesInfo;
		this.attributesMatrix  = attributesMatrix;
		this.arrTotalMatch          = arrTotalMatch;
		this.arrPartialMatch        = arrPartialMatch;
		
	}

	@Override
	public void run() {

		//compare left
		//compareRelationalGraphs(attributesMatrix_Left, shoesInfo.getARGmatrixLeft());
		CompareRelationalGraphs_T compareRelationalGraphs= new CompareRelationalGraphs_T(shoesInfo.getId());
		compareRelationalGraphs.setNewMatrix(attributesMatrix);
		compareRelationalGraphs.setExistingMatrix(shoesInfo.getArgmatrix());
		Thread threadCurrentShoe = new Thread(compareRelationalGraphs);
		threadCurrentShoe.start();
		
		
		
		
		try {
			threadCurrentShoe.join();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		//both shoes are matching
		if ( (compareRelationalGraphs.getMatchingPercentage() > 50) ) {
			arrTotalMatch.add(new double[]{shoesInfo.getId(), compareRelationalGraphs.getMatchingPercentage()});
			//continue;
		}
		//TODO:
		//Change logic
//		//either one of them are matching
		else if ( (compareRelationalGraphs.getMatchingPercentage() < 50) ) {
			arrPartialMatch.add(new double[]{shoesInfo.getId(), compareRelationalGraphs.getMatchingPercentage()});
			//continue;
		}
		
	}

}
