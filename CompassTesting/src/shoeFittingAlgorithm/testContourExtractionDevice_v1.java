package shoeFittingAlgorithm;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractorMOG2;

public class testContourExtractionDevice_v1 {
	final static int DIFF = 10;
	final static int LOWER_CONTOUR_AREA_THRESHOLD=6000;//20000
	final static int UPPER_CONTOUR_AREA_THRESHOLD=100000;//50000
	static String initialImagePath=".//RawPhotos//";//".//results//in.jpg";
	static String processedImagesPath=".//RawPhotos//";
	static String resultImagesPath=".//RawPhotos//";
	static String bckgImage =".//RawPhotos//background.jpg";//".//results//bckg.jpg";
	
	public static void main(String args[]){
		int userID= 18;
		int angle=924;
		//names after undistorting is performed
		String filename= "shoes_"+userID+"_"+angle;
		String backgroundFileName="bckg_"+userID;
		String initPath=initialImagePath+filename+".jpg";
		String bckgImagePath=initialImagePath+backgroundFileName+".jpg";
		
		//Split camera image by two
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		
		//read image
		Mat source = Highgui.imread(initPath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(source),"Initial Image");
		
		//read the background image
		Mat bckg = Highgui.imread(bckgImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bckg),"bckg");
		

		//with smoothing
		Mat initialImage =source.clone();
		Mat initialBck = bckg.clone();
		
		// 2. blur
	    Imgproc.blur(initialImage, initialImage, new Size(10,10));
	    Imgproc.blur(initialBck, initialBck, new Size(10,10));
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(initialImage),"BLURRED");
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(initialBck),"BLURRED");
	
		
		
		
		
		
		
		//applying background remover
		 BackgroundSubtractorMOG2 bg=new BackgroundSubtractorMOG2();	
		 Mat fgmask = new Mat();
		 
		 
		 double learningRate = 0.05;
		 bg.apply(initialBck, initialImage.clone(),learningRate);
		 bg.apply(initialImage.clone(), fgmask,learningRate);
		    
		 ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(fgmask),"REMOVED   "+learningRate);
		   
		//4. equalize histograms
		Imgproc.equalizeHist(fgmask, fgmask);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(fgmask),"after equalization");	
//			
		
//		//eroding and dilating
//		
		Imgproc.erode(fgmask, fgmask, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
		Imgproc.dilate(fgmask, fgmask, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
	    
		
		
		 //thresholding
		 Mat thresholdedBinary=new Mat();
		 Imgproc.threshold(fgmask, thresholdedBinary, 125, 255, Imgproc.THRESH_BINARY);
	    
		 ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(thresholdedBinary),"thresholdedBinary ");
		   
		 
		 Mat finalResultingImage = thresholdedBinary;
		 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
		 //contour extraction
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////
		 //to store the recognized contours
		 List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

		 //	recognize contours
		 Imgproc.findContours(finalResultingImage.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);

		 
		 Mat destination = drawShoeRectangles(contours, finalResultingImage.clone());
		 ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"Shoes only with contours without morphological transform");
		 Highgui.imwrite(initialImagePath+"out_"+filename+".jpg", destination);
			
		 
	}
	
	
	/**
	 * Draw bounding rectangles for determined shoes on the picture
	 * @param contours
	 * @param image
	 * @return
	 */
	public static Mat drawShoeRectangles(List <MatOfPoint> contours, Mat image){
		Mat result =  new Mat();
		Imgproc.cvtColor(image, result, Imgproc.COLOR_GRAY2BGR);
		ArrayList<RotatedRect> shoesRectangle = new ArrayList<RotatedRect>();
		for(int i=0;i<contours.size()-1;i++){
	    	 
	    	 MatOfPoint2f approx =  new MatOfPoint2f();
	    	 
	    	 //get current contour elements
	    	 MatOfPoint tempContour=contours.get(i);
	    	 
	    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
	    	 
	    	 int contourSize = (int)tempContour.total();
	    	 
	    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
	    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
	    	 Float[] tempArray=new Float[2];

	    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
	    		 	
	    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
	    		 	
	    		 	shoesRectangle.add(r);
	    		 	
	    		 	//DRAW THE BOUNDING RECTANGLE
	    		 	
 			   		//area of the approximated polygon
 			   		double area = Imgproc.contourArea(newMat);
 			   		Imgproc.drawContours(result, contours,i, new Scalar(0, 0, 250),2);
 			   		Point points[] = new Point[4];
 			   		r.points(points);
 			   		
 			   		if(r.size.height>r.size.width){
 			   			
 			   			tempArray[0]=(float) r.size.height;
 			   			tempArray[1]=(float) r.size.width;
 			   			
 			   		}
 			   		else{
 			   		
 			   			tempArray[0]=(float) r.size.width;
 			   			tempArray[1]=(float) r.size.height;
 			   			
 			   		}
 			   	
 			   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
 			   		String textH=" h = "+String.format("%.2f",tempArray[1]);
 			   		
 			   		
 			   		
 			   		for(int pt=0; pt<4; ++pt){
 			   			Core.line(result, points[pt], points[(pt+1)%4], new Scalar(0,255,0),2);
 			   			}
 			   		Core.putText(result, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
 			   		Core.putText(result, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
	    	 		}//end of if (checking the contours area)

 			   		
 			}//end of for loop
	    
		return result;
	}

}
