package shoeFittingAlgorithm;

import blobDetection.BlobDetection;
import static org.bytedeco.javacpp.opencv_core.CV_FILLED;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.bytedeco.javacpp.opencv_core.Point2f;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.video.BackgroundSubtractorMOG;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opencv.imgproc.Imgproc;

import com.mysql.jdbc.PreparedStatement;

import blobDetection.Blob;
import blobDetection.BlobDetection;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;

public class Main2 {
	final static int EDGE_LENGTH=20;
	
	//threshold to set the minimum area of contours that can be considered as shoes
	//TODO: needs to be analyzed and selected the optimal value
	final static int LOWER_CONTOUR_AREA_THRESHOLD=10000;
	final static int UPPER_CONTOUR_AREA_THRESHOLD=120000;
	final static int SHOE_SIZE_AREA_DIFFERENCE=4000;
	static String initialImagePath=".//CollectedShoePictures//";//".//results//in.jpg";
	static String processedImagesPath=".//CollectedShoePictures//Processed//";
	static String resultImagesPath=".//CollectedShoePictures//SizeExtracted//";
	static String bckgImage =".//CollectedShoePictures//background.jpg";//".//results//bckg.jpg";
	
	
	
	static BlobDetection theBlobDetection;
	static float threshold= 0.88f;
	
	
	
	/**
	 * 
	 * @param filename -- name of the file
	 * @param shoeSize -- ArrayList of shoe sizes
	 * TODO: other parameters may be added later
	 */
	public static void saveToDB(int id, String filename,int shoeHeight, int shoeWidth, int numOfLines, int  numOfCircles){
		Connection con = null;
	    PreparedStatement pst = null;

	    String url = "jdbc:mysql://localhost:3306/testdb";
        String user = "testuser";
        String password = "test623";

        try {
            con = DriverManager.getConnection(url, user, password);
            
            //FILL CORRESPONDING FIELDS IN TABLE 

            pst = (PreparedStatement) con.prepareStatement("INSERT INTO test1(userID,fileID,shoeSize_H,shoeSize_W, nlines, ncircles) VALUES(?, ?, ?, ?, ?, ?)");
            pst.setInt(1, id);
            pst.setString(2, filename);
            pst.setFloat(3, shoeHeight);
            pst.setFloat(4, shoeWidth);
            pst.setFloat(5, numOfLines);
            pst.setFloat(6, numOfCircles);
            
            pst.executeUpdate();
            
            

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main2.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
            	if (pst != null) {
                    pst.close();
            	}
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Main2.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		
	}
	
	
	
	
	
	
	
	/**
	 * to print the whole DB
	 * @param filename -- name of the file
	 * @param shoeSize -- ArrayList of shoe sizes
	 * TODO: other parameters may be added later
	 */
	public static void outputDB(){
		Connection con = null;
		Statement st = null;
	    ResultSet rs = null;
	    PreparedStatement pst = null;

	    String url = "jdbc:mysql://localhost:3306/testdb";
        String user = "testuser";
        String password = "test623";

        try {
            con = DriverManager.getConnection(url, user, password);
            
            //FILL CORRESPONDING FIELDS IN TABLE 

           
            
            pst = (PreparedStatement) con.prepareStatement("SELECT * FROM test1");
            rs = pst.executeQuery();
            
            //Print the headers of the table
            System.out.print(String.format("%-5s" , "ID"));
            System.out.print("| ");
            System.out.print(String.format("%-5s" , "userID"));
            System.out.print("| ");
            System.out.print(String.format("%-15s" , "fileID"));
            System.out.print("| ");
            System.out.print(String.format("%-10s" , "shoeHeight"));
            System.out.print("| ");
            System.out.print(String.format("%-11s" , "shoeWidth"));
            System.out.print("| ");
            System.out.print(String.format("%-11s" , "numOfLines"));
            System.out.print("| ");
            System.out.println(String.format("%-11s" , "numOfCircles"));

            System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
            //print the content of the table
            while (rs.next()) {
            	System.out.print(String.format("%-5d" , rs.getInt(1)));
                System.out.print("| ");
                System.out.print(String.format("%-6d" , rs.getInt(2)));
                System.out.print("| ");
                System.out.print(String.format("%-15s" , rs.getString(3)));
                System.out.print("| ");
                System.out.print(String.format("%-11d" , rs.getInt(4)));
                System.out.print("| ");
                System.out.print(String.format("%-11d" , rs.getInt(5)));
                System.out.print("| ");
                System.out.print(String.format("%-12d" , rs.getInt(6)));
                System.out.print("| ");
                System.out.println(String.format("%-12d" , rs.getInt(7)));
               
                
            }

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main2.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
            	if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Main2.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
		
	}
	
	
	/**
	 * 
	 * @param image -- a processed image with removed background
	 * @return an ArrayList of 2D arays containing shoes ROI
	 */
	public static ArrayList<RotatedRect> extractShoesOnly(Mat image){
		Mat onlyShoes=new Mat();
		//remove black color of background 
		//TODO: needs to be parametrized and checked for different gray background color ranges
		org.opencv.core.Core.inRange(image, new Scalar(0, 0, 0), new Scalar(15, 15, 15), onlyShoes);
		Mat onlyShoesCopy = onlyShoes.clone();
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoesCopy),"Shoes only");
		
		
		//Morphologic transformation operations before applying edge detector:
		//Apply morphological closing to remove small objects (it is assumed that the objects are bright on a dark foreground)
		//http://docs.opencv.org/trunk/doc/tutorials/imgproc/opening_closing_hats/opening_closing_hats.html
		
		
		//dilation
		//Imgproc.dilate(onlyShoes, onlyShoes, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5)));
		//erosion
		Imgproc.erode(onlyShoes, onlyShoes, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15,15)));        
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoes),"After morphological closing");
		
		 //to store the recognized contours
		 List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		 
		 //recognize contours
	     Imgproc.findContours(onlyShoes.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
	     
	     //keep the destination to draw the extracted rectangles on it
	     Mat destination=onlyShoes.clone();
	     
	     Imgproc.cvtColor(destination, destination, Imgproc.COLOR_GRAY2BGR);
	     
	     Imgproc.drawContours(destination, contours, -1, new Scalar(0,0,255));
	     
	     //store extracted sizes in the list of arrays of two elements [height][width]
	     //height is bigger then width by assumption 
	     ArrayList<RotatedRect> shoesOnly =new ArrayList<RotatedRect>();
	  
	     for(int i=0;i<contours.size()-1;i++){
	    	 
	    	 MatOfPoint2f approx =  new MatOfPoint2f();
	    	 
	    	 //get current contour elements
	    	 MatOfPoint tempContour=contours.get(i);
	    	 
	    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
	    	 
	    	 int contourSize = (int)tempContour.total();
	    	 
	    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
	    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
	    	 Float[] tempArray=new Float[2];
	    	 
	    	 //System.out.println("CURRENT CONTOUR AREA: "+Math.abs(Imgproc.contourArea(tempContour)));
	    	//if contour is big enough
	    	 //TODO check the threshold for min area
	    	//System.out.println("Countour "+i+ " area is "+Math.abs(Imgproc.contourArea(tempContour)));
	    	//Imgproc.drawContours(destination, contours, i, new Scalar(10*i,10*i,0),3);
	    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
	    		 	
	    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
	    		 	shoesOnly.add(r);
	    		 	
	    		 	//DRAW THE BOUNDING RECTANGLE
	    		 	
  			   		//area of the approximated polygon
  			   		double area = Imgproc.contourArea(newMat);
  			   		Imgproc.drawContours(destination, contours,i, new Scalar(0, 0, 250),2);
  			   		Point points[] = new Point[4];
  			   		r.points(points);
  			   		System.out.println();
  			   		System.out.println("Size of the shoe is:\nWidth "+ r.size.width+"\nHeight "+r.size.height);
  			   		
  			   		if(r.size.height>r.size.width){
  			   			
  			   			tempArray[0]=(float) r.size.height;
  			   			tempArray[1]=(float) r.size.width;
  			   			
  			   		}
  			   		else{
  			   		
  			   			tempArray[0]=(float) r.size.width;
  			   			tempArray[1]=(float) r.size.height;
  			   			
  			   		}
  			   	
  			   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
  			   		String textH=" h = "+String.format("%.2f",tempArray[1]);
  			   		
  			   		
  			   		
  			   		for(int pt=0; pt<4; ++pt){
  			   			Core.line(destination, points[pt], points[(pt+1)%4], new Scalar(0,255,0),2);
  			   			}
  			   		Core.putText(destination, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
  			   		Core.putText(destination, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
	    	 		}//end of if (checking the contours area)

  			   		
  			}//end of for loop
	     
	     System.out.println("shoesOnly size: "+shoesOnly.size());
	     //if cannot find any suitable contours after morphologic transforms -- just try to find contours from original shoesOnly image
	     if(shoesOnly.size()==0 || shoesOnly.size()==1){
	    	 Imgproc.findContours(onlyShoesCopy.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
	    	 for(int i=0;i<contours.size()-1;i++){
		    	 
		    	 MatOfPoint2f approx =  new MatOfPoint2f();
		    	 
		    	 //get current contour elements
		    	 MatOfPoint tempContour=contours.get(i);
		    	 
		    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
		    	 
		    	 int contourSize = (int)tempContour.total();
		    	 
		    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
		    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.05, true);
		    	 Float[] tempArray=new Float[2];
		    	 
		    	 //System.out.println("CURRENT CONTOUR AREA: "+Math.abs(Imgproc.contourArea(tempContour)));
		    	//if contour is big enough
		    	 //TODO check the threshold for min area
		    	 //System.out.println("CONTOUR AREA: "+Math.abs(Imgproc.contourArea(tempContour)));
		    	 if((Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD)){
		    		 	
		    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
		    		 	shoesOnly.add(r);
		    	 }//end of if
	    	 }//end of for loop

	     }
	     System.out.println("Contours found "+contours.size());
	     ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"Shoes only with contours");
	     //ImageIO.write(ProcessImages.Mat2BufferedImage(destination), "JPG", new File(extractedSizeFilePath));
	     System.out.println("Size of Shoes Only "+ shoesOnly.size());
	     return shoesOnly;
	     
	}
	
	/**
	 * 
	 * @param image -- a processed image with removed background
	 * @return an ArrayList of 2D arays containing shoes ROI
	 */
	public static ArrayList<RotatedRect> extractShoesOnly_v1(Mat image){
		Mat onlyShoes=new Mat();
		//remove black color of background 
		//TODO: needs to be parametrized and checked for different gray background color ranges
		org.opencv.core.Core.inRange(image, new Scalar(0, 0, 0), new Scalar(14, 14, 14), onlyShoes);
		Mat onlyShoesCopy = onlyShoes.clone();
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoesCopy),"Shoes only");
		
		
		 //to store the recognized contours
		 List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		 
		 
		 //FIRST CHECK IF TWO CONTOURS ARE CLEARLY SEPARATED FROM INITIAL IMAGE:
		 
		 //recognize contours
	     Imgproc.findContours(onlyShoes.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
	     
	     //keep the destination to draw the extracted rectangles on it
	     Mat destination=onlyShoes.clone();
	     
	     Imgproc.cvtColor(destination, destination, Imgproc.COLOR_GRAY2BGR);
	     
	     Imgproc.drawContours(destination, contours, -1, new Scalar(0,0,255));
	     
	     //store extracted sizes in the list of arrays of two elements [height][width]
	     //height is bigger then width by assumption 
	     ArrayList<RotatedRect> shoesOnly =new ArrayList<RotatedRect>();
	     
	     //to save contour areas for further analysis
	     ArrayList<Double> contourAreas = new ArrayList<Double>();
	     
	     for(int i=0;i<contours.size()-1;i++){
	    	 
	    	 MatOfPoint2f approx =  new MatOfPoint2f();
	    	 
	    	 //get current contour elements
	    	 MatOfPoint tempContour=contours.get(i);
	    	 
	    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
	    	 
	    	 int contourSize = (int)tempContour.total();
	    	 
	    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
	    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
	    	 Float[] tempArray=new Float[2];
	    	 
	    	 //System.out.println("CURRENT CONTOUR AREA: "+Math.abs(Imgproc.contourArea(tempContour)));
	    	//if contour is big enough
	    	 //TODO check the threshold for min area
	    	//System.out.println("Countour "+i+ " area is "+Math.abs(Imgproc.contourArea(tempContour)));
	    	 contourAreas.add(Math.abs(Imgproc.contourArea(tempContour)));
	    	//Imgproc.drawContours(destination, contours, i, new Scalar(10*i,10*i,0),3);
	    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
	    		 	
	    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
	    		 	shoesOnly.add(r);
	    		 	
	    		 	//DRAW THE BOUNDING RECTANGLE
	    		 	
  			   		//area of the approximated polygon
  			   		double area = Imgproc.contourArea(newMat);
  			   		Imgproc.drawContours(destination, contours,i, new Scalar(0, 0, 250),2);
  			   		Point points[] = new Point[4];
  			   		r.points(points);
  			   		System.out.println();
  			   		System.out.println("Size of the shoe is:\nWidth "+ r.size.width+"\nHeight "+r.size.height);
  			   		
  			   		if(r.size.height>r.size.width){
  			   			
  			   			tempArray[0]=(float) r.size.height;
  			   			tempArray[1]=(float) r.size.width;
  			   			
  			   		}
  			   		else{
  			   		
  			   			tempArray[0]=(float) r.size.width;
  			   			tempArray[1]=(float) r.size.height;
  			   			
  			   		}
  			   	
  			   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
  			   		String textH=" h = "+String.format("%.2f",tempArray[1]);
  			   		
  			   		
  			   		
  			   		for(int pt=0; pt<4; ++pt){
  			   			Core.line(destination, points[pt], points[(pt+1)%4], new Scalar(0,255,0),2);
  			   			}
  			   		Core.putText(destination, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
  			   		Core.putText(destination, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
	    	 		}//end of if (checking the contours area)

  			   		
  			}//end of for loop
	     
	    	     
	     ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"Shoes only with contours without morphological transform");
	     System.out.println("Without morphological transform found contours areas: ");
	     
	     Collections.sort(contourAreas);
	     
//	     for(int j=0;j<contourAreas.size();j++)
//	    	 System.out.println("THE CONTOUR AREA IS: "+ contourAreas.get(j));
	     
	     System.out.println("NUMBER OF CONTOURS FOUND WITHOUT MORPHOLOGICAL TRANSFORM "+shoesOnly.size());

	     //if one contour found but it's area is within the allowed limits --just return it to split by 2
	     if(shoesOnly.size()==1 && shoesOnly.get(0).size.width*shoesOnly.get(0).size.height>6000 &&shoesOnly.get(0).size.width*shoesOnly.get(0).size.height <120000){
	    	 return shoesOnly;
	     }
	    
	     
	     
	     
	     
	     //Apply morphological transform for better detection
	     
	   //to save contour areas for further analysis
	     ArrayList<Double> contourAreas_1 = new ArrayList<Double>();
	     
	     //Add something here to discriminate based on areas
	     //if 0 or more then 2 contours found...
	     //or if the sizes of found two contours are different (areas differ by difference value)
	     //TODO:
	     int difference =SHOE_SIZE_AREA_DIFFERENCE;
	     if(shoesOnly.size()==0 || shoesOnly.size()>2 || (shoesOnly.size()==2 && (shoesOnly.get(0).size.width*shoesOnly.get(0).size.height-shoesOnly.get(1).size.width*shoesOnly.get(1).size.height> difference))){
	    	 contours = new ArrayList<MatOfPoint>();
	    	 shoesOnly =new ArrayList<RotatedRect>();
	    	 Imgproc.erode(onlyShoes, onlyShoes, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15,15)));
	    	 ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(onlyShoes),"After morphological closing");
	    	 //TODO: MAKE IT AS FUNCTION
	    	 
	    	//recognize contours
		     Imgproc.findContours(onlyShoes.clone(), contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
		     
		     //keep the destination to draw the extracted rectangles on it
		     destination=onlyShoes.clone();
		     
		     Imgproc.cvtColor(destination, destination, Imgproc.COLOR_GRAY2BGR);
		     
		     Imgproc.drawContours(destination, contours, -1, new Scalar(0,0,255));
		     
		     //store extracted sizes in the list of arrays of two elements [height][width]
		     //height is bigger then width by assumption 
		     
		  
		     for(int i=0;i<contours.size()-1;i++){
		    	 
		    	 MatOfPoint2f approx =  new MatOfPoint2f();
		    	 
		    	 //get current contour elements
		    	 MatOfPoint tempContour=contours.get(i);
		    	 
		    	 MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
		    	 
		    	 int contourSize = (int)tempContour.total();
		    	 
		    	 //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
		    	 Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
		    	 Float[] tempArray=new Float[2];
		    	 
		    	 //System.out.println("CURRENT CONTOUR AREA: "+Math.abs(Imgproc.contourArea(tempContour)));
		    	//if contour is big enough
		    	 //TODO check the threshold for min area
		    	//System.out.println("Countour "+i+ " area is "+Math.abs(Imgproc.contourArea(tempContour)));
		    	 
		    	 contourAreas_1.add(Math.abs(Imgproc.contourArea(tempContour)));
		    	//Imgproc.drawContours(destination, contours, i, new Scalar(10*i,10*i,0),3);
		    	 if(Math.abs(Imgproc.contourArea(tempContour))>LOWER_CONTOUR_AREA_THRESHOLD && Math.abs(Imgproc.contourArea(tempContour))<UPPER_CONTOUR_AREA_THRESHOLD){
		    		 	
		    		 	RotatedRect r = Imgproc.minAreaRect(newMat);
		    		 	shoesOnly.add(r);
		    		 	
		    		 	//DRAW THE BOUNDING RECTANGLE
		    		 	
	  			   		//area of the approximated polygon
	  			   		double area = Imgproc.contourArea(newMat);
	  			   		Imgproc.drawContours(destination, contours,i, new Scalar(0, 0, 250),2);
	  			   		Point points[] = new Point[4];
	  			   		r.points(points);
	  			   		System.out.println();
	  			   		System.out.println("Size of the shoe is:\nWidth "+ r.size.width+"\nHeight "+r.size.height);
	  			   		
	  			   		if(r.size.height>r.size.width){
	  			   			
	  			   			tempArray[0]=(float) r.size.height;
	  			   			tempArray[1]=(float) r.size.width;
	  			   			
	  			   		}
	  			   		else{
	  			   		
	  			   			tempArray[0]=(float) r.size.width;
	  			   			tempArray[1]=(float) r.size.height;
	  			   			
	  			   		}
	  			   	
	  			   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
	  			   		String textH=" h = "+String.format("%.2f",tempArray[1]);
	  			   		
	  			   		
	  			   		
	  			   		for(int pt=0; pt<4; ++pt){
	  			   			Core.line(destination, points[pt], points[(pt+1)%4], new Scalar(0,255,0),2);
	  			   			}
	  			   		Core.putText(destination, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
	  			   		Core.putText(destination, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
		    	 		}//end of if (checking the contours area)

	  			   		
	  			}
	    	 
	     }
	    
	     
	     
	     
	     
	     ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination),"Shoes only with contours after morphological transform");
	     //ImageIO.write(ProcessImages.Mat2BufferedImage(destination), "JPG", new File(extractedSizeFilePath));
	     System.out.println("Size of Shoes Only "+ shoesOnly.size());
	     
	     
	     System.out.println();
	     System.out.println();
	     Collections.sort(contourAreas_1);
	     
//	     for(int j=0;j<contourAreas_1.size();j++)
//	    	 System.out.println("THE CONTOUR AREA IS: "+ contourAreas_1.get(j));
	     
	     
	     return shoesOnly;
	     
	}
	
	
	
	
	
	
	
	/**
	 * 
	 * @param index
	 * @param masks
	 * @param image
	 * @return
	 */
	public static ArrayList<Mat> cropShoesFromImage(Mat image){
		ArrayList<Mat> result=new ArrayList<Mat>();
		ArrayList<RotatedRect> masks= extractShoesOnly_v1(image);
		for(int i=0;i<masks.size();i++){
			RotatedRect rect= masks.get(i);
			Mat src=image.clone();
			// matrices we'll use
			Mat M=image.clone();
			Mat rotated=new Mat();
			Mat cropped=new Mat();
			// get angle and size from the bounding box
			float angle = (float) rect.angle;
			Size rect_size = rect.size;
			// thanks to http://felix.abecassis.me/2011/10/opencv-rotation-deskewing/
			if (rect.angle < -45.) {
			    angle += 90.0;
			    rect_size=swap(rect_size.width, rect_size.height);
			}
			// get the rotation matrix
			M = org.opencv.imgproc.Imgproc.getRotationMatrix2D(rect.center, angle, 1.0);
			// perform the affine transformation on your image in src,
			// the result is the rotated image in rotated. I am doing
			// cubic interpolation here
			org.opencv.imgproc.Imgproc.warpAffine(src, rotated, M, src.size(), org.opencv.imgproc.Imgproc.INTER_CUBIC);
			// crop the resulting image, which is then given in cropped
			org.opencv.imgproc.Imgproc.getRectSubPix(rotated, rect_size, rect.center, cropped);
			result.add(cropped);
			ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(cropped),"Cropped");
		}
		
		return result;
		
	}
	/**
	 * 
	 * @param w
	 * @param h
	 * @return
	 */
	public static Size  swap(double w, double h){
		Size rect_size=new Size(h,w);
		return rect_size;

	}
	
	
	
	public static int[] getSize(Mat image){
		int result[] = new int[2];
		if(image.height()> image.width()){
			result[0]=image.height();
			result[1]=image.width();
		}
		else{
			result[1]=image.height();
			result[0]=image.width();
		}
		return result;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * iterator for sorting the hash map by values
	 * @param unsortMap
	 * @return
	 */
	private static Map<Integer, Double> sortByComparator(Map<Integer,Double> unsortMap) {
		 
		// Convert Map to List
		List<Map.Entry<Integer,Double>> list = 
			new LinkedList<Map.Entry<Integer,Double>>(unsortMap.entrySet());
 
		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<Integer,Double>>() {
			public int compare(Map.Entry<Integer,Double> o1,
                                           Map.Entry<Integer,Double> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});
 
		// Convert sorted map back to a Map
		Map<Integer,Double> sortedMap = new LinkedHashMap<Integer,Double>();
		for (Iterator<Map.Entry<Integer,Double>> it = list.iterator(); it.hasNext();) {
			Map.Entry<Integer,Double> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
	
	/**
	 * to print the map
	 * @param map
	 */
	public static void printMap(Map<Integer,Double> map) {
		for (Map.Entry<Integer,Double> entry : map.entrySet()) {
			System.out.println("[Key] : " + entry.getKey() 
                                      + " [Value] : " + entry.getValue());
		}
	}
	
	/**
	 * 
	 * @param filename -- name of the file
	 * @param shoeSize -- ArrayList of shoe sizes
	 * TODO: other parameters may be added later
	 */
	public static Map<Integer, Double> compareEntry(int height, int width, int numOfLines, int numOfCircles){
		ArrayList<double[]> result = new ArrayList<double[]>();
		
		Map<Integer, Double> res = new HashMap<>();
		Connection con = null;
		Statement st = null;
	    ResultSet rs = null;
	    PreparedStatement pst = null;

	    String url = "jdbc:mysql://localhost:3306/testdb";
        String user = "testuser";
        String password = "test623";

        try {
            con = DriverManager.getConnection(url, user, password);
            
            
            pst = (PreparedStatement) con.prepareStatement("SELECT * FROM test1");
            rs = pst.executeQuery();
            
           //print the content of the table
            while (rs.next()) {
            	
                
                int tmpHeight= rs.getInt(4);
                int tmpWidth=rs.getInt(5);
                int tmpNumOfLines = rs.getInt(6);
                int tmpNumOfCircles=rs.getInt(7);
               
                
                double distance= Math.sqrt((tmpHeight-height)*(tmpHeight-height)+(tmpWidth-width)*(tmpWidth-width)+(tmpNumOfLines-numOfLines)*(tmpNumOfLines-numOfLines)+(tmpNumOfCircles-numOfCircles)*(tmpNumOfCircles-numOfCircles));
//                System.out.println("Distance at id "+rs.getInt(1) + " is "+distance);
//                System.out.println("tmpHeight "+tmpHeight);
//                System.out.println("height "+height);
//                System.out.println("tmpWidth "+tmpWidth);
//                System.out.println("width "+width);
//                System.out.println("tmpNumOfLines "+tmpNumOfLines);
//                System.out.println("numOfLines "+numOfLines);
//                System.out.println("tmpNumOfCircles "+tmpNumOfCircles);
//                System.out.println("numOfCircles "+numOfCircles);
                
                //double tempArray[]={rs.getInt(1),distance};
                //result.add(tempArray);
                res.put(rs.getInt(1)-1,distance);
                
            }

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main2.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
            	if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Main2.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
        return res;
		
	}
	
	
	
	
	
	/**
	 * retrieves info about record with given ID
	 * @param filename -- name of the file
	 * @param shoeSize -- ArrayList of shoe sizes
	 * TODO: other parameters may be added later
	 */
	public static void searchDBforID (int id){
		ArrayList<double[]> result = new ArrayList<double[]>();
		
		Map<Integer, Double> res = new HashMap<>();
		Connection con = null;
		Statement st = null;
	    ResultSet rs = null;
	    PreparedStatement pst = null;

	    String url = "jdbc:mysql://localhost:3306/testdb";
        String user = "testuser";
        String password = "test623";

        try {
            con = DriverManager.getConnection(url, user, password);
            
            
            pst = (PreparedStatement) con.prepareStatement("SELECT * FROM test1");
            rs = pst.executeQuery();
            
           //print the content of the table
            while (rs.next()) {
            	
                
                int currentID= rs.getInt(1);
                if((id+1) == currentID){
                	//Print the headers of the table
                	System.out.print(String.format("%-5s" , "ID"));
                    System.out.print("| ");
                    System.out.print(String.format("%-5s" , "userID"));
                    System.out.print("| ");
                    System.out.print(String.format("%-15s" , "fileID"));
                    System.out.print("| ");
                    System.out.print(String.format("%-10s" , "shoeHeight"));
                    System.out.print("| ");
                    System.out.print(String.format("%-11s" , "shoeWidth"));
                    System.out.print("| ");
                    System.out.print(String.format("%-11s" , "numOfLines"));
                    System.out.print("| ");
                    System.out.println(String.format("%-11s" , "numOfCircles"));
                    System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
                    //print the content of the table
                    
                    System.out.print(String.format("%-5d" , rs.getInt(1)));
                    System.out.print("| ");
                    System.out.print(String.format("%-6d" , rs.getInt(2)));
                    System.out.print("| ");
                    System.out.print(String.format("%-15s" , rs.getString(3)));
                    System.out.print("| ");
                    System.out.print(String.format("%-11d" , rs.getInt(4)));
                    System.out.print("| ");
                    System.out.print(String.format("%-11d" , rs.getInt(5)));
                    System.out.print("| ");
                    System.out.print(String.format("%-12d" , rs.getInt(6)));
                    System.out.print("| ");
                    System.out.println(String.format("%-12d" , rs.getInt(7)));
                        
                   
                        break;
                        
                   
                }
              
                
            }//end of while loop
            //System.out.println("No record for given ID is found");

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main2.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
            	if (pst != null) {
                    pst.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(Main2.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
       
		
	}
	
	
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String args[]) throws IOException{
		//TODO:
		//devise a logic to determie whether the taken pisture valid
		//devise a function to sort records according to shoe sizes categorization -- small, medium, large 
		//make everything tidy and accurate
		//create a relational DB to store all records
		//devise a mechanism to calculate a distance between new and old records and find a better match
		//create an interface may be with parametrized values to simplify the analysis
		//
		//
		//TODO: test
		int userID=7;
		String filename= "shoes_"+userID+"_90";
		//String filename= "shoes_"+userID+"_half_"+3;
		String backgroundFileName="background"+userID;
		String initPath=initialImagePath+filename+".jpg";
		String bckgImagePath=initialImagePath+backgroundFileName+".jpg";
		String backgroundRemovedFilePath=processedImagesPath+"proc_"+filename+".jpg";
		String extractedSizeFilePath=resultImagesPath+"size_"+filename+".jpg";
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		
		//read image
		Mat source = Highgui.imread(initPath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//crop image
		Mat source0= ProcessImages.cropImageHorizontal(source,EDGE_LENGTH);
		
		//show initial image
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(source),"Initial Image");
		
		//read the background image
		Mat bckg0 = Highgui.imread(bckgImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//crop the background image
		Mat bckg=ProcessImages.cropImageHorizontal(bckg0,EDGE_LENGTH);
		
		//show the background image
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bckg),"bckg");
		
		//to store image with removed background
		Mat backgroundRemoved = new Mat();
		
		//REMOVE BACKGROUND
		org.opencv.core.Core.subtract(source0.clone(), bckg, backgroundRemoved);
		
		//show image with background removed 
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved),"difference btw main image and background using subtract");
		
		//save it to file (optional)
		ImageIO.write(ProcessImages.Mat2BufferedImage(backgroundRemoved), "JPG", new File(backgroundRemovedFilePath));
		
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//							Croping the extracted shoe regions only
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		ArrayList<Mat> croppedImages = cropShoesFromImage(backgroundRemoved);
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("NUMBER OF IMAGES EXTRACTED: "+croppedImages.size());
		
		
		ArrayList<Mat> correctCroppedImages=new ArrayList<Mat>();
		//TODO: Correct the logic
		//
//		
		
		if(croppedImages.size()==1){ //only one contour is extracted -- split it by half
			Mat tempCropped=croppedImages.get(0);
			//System.out.println("HEIGHT "+tempCropped.height());
			//System.out.println("WIDTH "+tempCropped.width());
			int w;
			int h;
			if(tempCropped.width()> tempCropped.height()){
//				w=tempCropped.height();
//				h=tempCropped.width();
//			}
//			else{
//				w=tempCropped.width();
//				h=tempCropped.height();
				Core.transpose(tempCropped, tempCropped);
				Core.flip(tempCropped, tempCropped, 1);
			}
			w=tempCropped.width();
			h=tempCropped.height();
			if(w>100 && w<400 && h>120 && h<500){
				
				Mat uncropped = tempCropped.clone(); // if you want to rotate by 90 degrees
				
//				if(uncropped.width()>uncropped.height()){
//					
//				}
				
				
				ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(uncropped),"ROTATED");
				//uncropped=dist.clone();
				//split by two images
				Rect roiOne = new Rect(0, 0, uncropped.cols()/2, uncropped.rows());
				Rect roiTwo = new Rect(uncropped.cols()/2, 0, uncropped.cols()/2, uncropped.rows());
				
				//System.out.println("uncropped.cols()   "+uncropped.cols());
				//System.out.println("uncropped.rows()   "+uncropped.rows());
				
				correctCroppedImages.add(new Mat(uncropped.clone(), roiOne));
				correctCroppedImages.add(new Mat(uncropped.clone(), roiTwo));
				
			}//internal if
		}
		else{ // if more than 1 rectangle is returned
			for(int idx=0;idx<croppedImages.size();idx++){
				Mat tempCropped=croppedImages.get(idx);
				if(tempCropped.width()> tempCropped.height()){
					Core.transpose(tempCropped, tempCropped);
					Core.flip(tempCropped, tempCropped, 1);
				}
				correctCroppedImages.add(tempCropped);
				//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempCropped),"Cropped Correctly  "+idx);
			}//end of for
		}//end of else
			

		
		//System.out.println("SIZE "+correctCroppedImages.size());
		
		
		ArrayList<int[]> shoeSizes=new ArrayList<int[]>();
		ArrayList<Integer> numOfLines=new ArrayList<Integer>();
		ArrayList<Integer> numOfCircles=new ArrayList<Integer>();
		//print all correctly cropped images
		for(int index=0;index< correctCroppedImages.size();index++){
			System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////");
			System.out.println("PROCESSING IMAGE # "+index);
			
			//current image
			Mat tempImage= correctCroppedImages.get(index);
			ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempImage),"Shoe number "+index);
			//Save image size:
			int[] tempSize=new int[2];
			
			if(tempImage.height()>tempImage.width()){
				tempSize[1]=tempImage.height();
				tempSize[0]=tempImage.width();
			}
			else{
				tempSize[0]=tempImage.height();
				tempSize[1]=tempImage.width();
			}
				
			shoeSizes.add(tempSize);
			
			
			//optimal parameters for Canny edge extractor
			//TODO:
			//find optimal parameters
			int lowThreshold=50;
			int highThreshold=50;
			int kernelSize=3;
			
			Mat destination0=tempImage.clone();//imgToProcess1.clone();
			//extract edges
			Mat cannyFilteredSourceMat= AnalyzeImage.applyCannyEdgeDetectorOpenCV_Mat(destination0,lowThreshold, highThreshold, kernelSize);
			//display the image with extracted edges
			ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(cannyFilteredSourceMat),"Canny edge extracting algorith applied");
			
			//Extract circles
			Mat destination1=cannyFilteredSourceMat.clone();
			
			Mat destination2=cannyFilteredSourceMat.clone();

			Mat destination3=cannyFilteredSourceMat.clone();
			
			BufferedImage destinationBuffered = ProcessImages.Mat2BufferedImage(destination2);
			ArrayList<Rectangle>listOfDetectedCircleDimensions =new ArrayList<Rectangle>();
		    
			
			int currentBlobDetectionThresholdValue=15;
			int currentLineDetectionThresholdValue=20;
			int currentbadPixelThresholdValue=2;
			int heightThresholdValue=10;
			int widthThresholdValue =10;
			int differenceValue=10;
		    //detect circles
			listOfDetectedCircleDimensions=newDetectBlobs.detectBlobsAddCirclesToARG1(destinationBuffered,(float) (currentBlobDetectionThresholdValue/100.0),currentLineDetectionThresholdValue/100.0, currentbadPixelThresholdValue/100.0,heightThresholdValue,  widthThresholdValue,  differenceValue);//usualy 0.15f
			
//			//draw detected circles on the destination image
//			destination1=drawCirclesArrayList(destination1,listOfDetectedCircleDimensions);
//			
//			//display image with detected circles
//			ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination1),"Circles detected");
//			

			//remove detected circles
			//select (0,0,0) for black, (255,255,255) for white
			destinationBuffered=AnalyzeImage.removeDetectedCircles(destinationBuffered,listOfDetectedCircleDimensions,new Scalar(0,0,0));
			
			ProcessImages.displayImage(destinationBuffered,"Circles detected are removed from the image");
			
			//now detect lines on the image
			
			ArrayList<int[][]>listOfDetectedLinesCoordinates =new ArrayList<int[][]>();
			
			//specify the length of shortest detected line in pixels
		    int currentLengthThresholdValue = 10;
		    listOfDetectedLinesCoordinates=PixelBasedLineDetection_T.detectLinesAndAddToARG_v2(destinationBuffered,currentLengthThresholdValue);
		   
			//draw detected lines and circles
		    
		    destination3=AnalyzeImage.drawLinesCirclesArrayList(destination1,listOfDetectedLinesCoordinates,listOfDetectedCircleDimensions);
		    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination3),"Lines and circles detected for shoe "+index);
			
		    
		    //number of detected circles
			int numOfDetectedCircles = listOfDetectedCircleDimensions.size();
			//System.out.println(numOfDetectedCircles + " circles detected");
			numOfCircles.add(numOfDetectedCircles);
			//number of detected lines
			int numOfDetectedLines = AnalyzeImage.removeLinesOfZeroLength(listOfDetectedLinesCoordinates).size();
			//System.out.println(numOfDetectedLines + " lines detected");
			numOfLines.add(numOfDetectedLines);
			
			int currentHeight = tempSize[0];
			int currentWidth=tempSize[1];
			int currentNumOfLines=numOfDetectedLines;
			int currentNumOfCircles = numOfDetectedCircles;
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//output the parameters of processed image before comparing:
			//
			System.out.println();
			System.out.println("FOUND FEATURES:");
        	System.out.print(String.format("%-5s" , "ID"));
            System.out.print("| ");
            System.out.print(String.format("%-5s" , "userID"));
            System.out.print("| ");
            System.out.print(String.format("%-15s" , "fileID"));
            System.out.print("| ");
            System.out.print(String.format("%-10s" , "shoeHeight"));
            System.out.print("| ");
            System.out.print(String.format("%-11s" , "shoeWidth"));
            System.out.print("| ");
            System.out.print(String.format("%-11s" , "numOfLines"));
            System.out.print("| ");
            System.out.println(String.format("%-11s" , "numOfCircles"));
            System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
            //print the content of the table
            
            System.out.print(String.format("%-5s" , "null"));
            System.out.print("| ");
            System.out.print(String.format("%-6d" , userID));
            System.out.print("| ");
            System.out.print(String.format("%-15s" , filename));
            System.out.print("| ");
            System.out.print(String.format("%-11d" , currentHeight));
            System.out.print("| ");
            System.out.print(String.format("%-11d" , currentWidth));
            System.out.print("| ");
            System.out.print(String.format("%-12d" , currentNumOfLines));
            System.out.print("| ");
            System.out.println(String.format("%-12d" , currentNumOfCircles));
            System.out.println();
            System.out.println();
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			
			
			//hash map to store the differences
			Map<Integer, Double> res = new HashMap<>();
			//
			//populate the hashmap
			res=compareEntry(currentHeight, currentWidth, currentNumOfLines,currentNumOfCircles);
			//if the DB is empty -- add shoes to DB
			if(res.isEmpty()){
				System.out.println();
				System.out.println("The DB is empty");
				System.out.println("ADDING TO DATABASE....");
				System.out.println();
				saveToDB(userID,filename,currentHeight, currentWidth, currentNumOfLines,currentNumOfCircles);
			}
			else{
				//Calculate distances to all shoes in DB, save results in HashMap, sort this map and select the top (smallest) distance
//				System.out.println();
//				tmpNumOfCircles
				
//				System.out.println("Unsort Map......");
//				printMap(res);
//				
//				System.out.println();
//				
//				System.out.println("\nSorted Map......");
				Map<Integer, Double> sortedMap = sortByComparator(res);
//				printMap(sortedMap);
				
				
				System.out.println("THE CLOSES RECORD IN DB HAS AN ID: "+((int)sortedMap.keySet().toArray()[0]+1));
				
				int closesID=(int)sortedMap.keySet().toArray()[0];
				
				double closestValue=(double) sortedMap.values().toArray()[0];
				System.out.println("The closest distance is "+closestValue);
				System.out.println();
				System.out.println("THE CLOSEST SHOE FEATURES in DB: ");
				//Search DB for closest shoe and output its records
				searchDBforID(closesID);
				System.out.println();
				//if the distance is greater then a specified threshold it might be a new shoe -- add it as a new entry
				//TODO: analyze threshold
				if(closestValue > 25.0){
					System.out.println();
					System.out.println("ADDING TO DATABASE....");
					System.out.println();
					saveToDB(userID,filename, currentHeight, currentWidth, currentNumOfLines,currentNumOfCircles);
				}
				
			}//end of else

		}//END OF FOR LOOP
		
	
		
		////Sprint the whole DB (keep commented)
		System.out.println("/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////");
		System.out.println("DATABASE");
		outputDB();
		
		
		
		
//		//to get the first value
//		System.out.println(sortedMap.values().toArray()[0]);
//		//To get the value of the "first" key:
//		System.out.println(sortedMap.get(sortedMap.keySet().toArray()[0]));
		//To get the very first key
		
//		for (Entry<Integer, Double> entry : sortedMap.entrySet()) {
//            if (entry.getValue().equals(sortedMap.values().toArray()[0])) {
//                System.out.println(entry.getKey());
//            }
//        }
		//

	}//end of main
	
}//end of class



