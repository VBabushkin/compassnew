package shoeFittingAlgorithm;

import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Calculated edge attributes corresponding to other nodes 
 * which includes Orientation, Relative, and Perpendicular distances 
 * @author nimesha
 *
 */
public class CalculateNodeAndEdgeAttributes {


	private AttributesMatrix[][] distanceArray         = null; //r c
	
	public CalculateNodeAndEdgeAttributes( AttributesMatrix[][] _distanceArray ) {
		distanceArray = _distanceArray;
	}
	
	/**
	 * 
	 * line   = x1, y1, x2, y2, error, 0
	 * circle = x, y, w, h, error, 1
	 * 
	 * L2L & C2C - calculate line to line perpendicular distance, relative distance, and orientations //array elements: n, x, y, w, h
	 * @param strips array-list with all the strips
	 * @param relativedistances_l2l 2D array matrix calculated with relative distances
	 * @param perpendiculardistances_l2l  2D array matrix calculated with perpendicular distances
	 * @param relativeorientations_l2l  2D array matrix calculated with relative orientations
	 */
	public void calculate_OrientationRelativeAndPerpendicularDistances(ArrayList<double[]> arrshapes) {

		Point mainMidPoint = null;
		Point subMidPoint  = null;
		Point A  = null;
		Point B  = null;
		String sMainShape  = "";
		String sSubShape   = "";	
		String datalines   = "";

		for (int i = 0; i < arrshapes.size(); i++) { //primary loop
			double [] mainstripinfo = arrshapes.get(i);
			
			//calculate mid point of main shape
			if (mainstripinfo[5] == 0) { // line = x1, y1, x2, y2, error, 0
				int x1 = (int)mainstripinfo[0]; int y1 = (int)mainstripinfo[1];
				int x2 = (int)mainstripinfo[2]; int y2 = (int)mainstripinfo[3];
				mainMidPoint = new Point ( ((x1 + x2) / 2), ((y1 + y2) / 2) );
				sMainShape  = "L";
			}
			else if (mainstripinfo[5] == 1) { //circle = x, y, r, 1, 1, 1
				int x1 = (int)mainstripinfo[0]; int y1 = (int)mainstripinfo[1];
				int r1 = (int)mainstripinfo[2]; 
				mainMidPoint = new Point (x1, y1);
				sMainShape  = "C";
			}
			
			for (int j = 0; j < arrshapes.size(); j++) {  //secondary loop
				double [] comparestripinfo = arrshapes.get(j);
				
				if (i == j) {
					distanceArray[i][j] =  new AttributesMatrix();
				}
				else {
					
					//calculate mid point 
					if (comparestripinfo[5] == 0) { // line = x1, y1, x2, y2, error, 0
						int x1 = (int)comparestripinfo[0]; int y1 = (int)comparestripinfo[1];
						int x2 = (int)comparestripinfo[2]; int y2 = (int)comparestripinfo[3];
						subMidPoint = new Point ( ((x1 + x2) / 2), ((y1 + y2) / 2) );
						A = new Point(x1, y1); //starting point of the line
						B = new Point(x2, y2); //end point of the line
						sSubShape   = "L";
					}
					else if (comparestripinfo[5] == 1) { //circle = x, y, r, 1, 1, 1
						int x1 = (int)comparestripinfo[0]; int y1 = (int)comparestripinfo[1];
						int r1 =(int)comparestripinfo[2]; 
						subMidPoint = new Point (x1, y1);
						A = new Point( (x1-r1), y1 );
						B = new Point( (x1 + r1), y1 );
						sSubShape   = "C";
					}
					
					//length of second line
					double normalLength = Math.sqrt((B.x-A.x)*(B.x-A.x)+(B.y-A.y)*(B.y-A.y));
					
					//calculate distances
					double pdistance    = Math.abs((mainMidPoint.x-A.x)*(B.y-A.y)-(mainMidPoint.y-A.y)*(B.x-A.x)) / normalLength;
					double rdistance    = mainMidPoint.distance(subMidPoint);
					double rorientation = GetAngleBetweenTwoPoints(mainMidPoint, subMidPoint, "radians");
					
					pdistance    = Math.round(pdistance    * 100) / 100.0;   //perpendicular distance
					rdistance    = Math.round(rdistance    * 100) / 100.0;   //relative distance
					rorientation = Math.round(rorientation * 100) / 100.0;   //relative orientation
					
					AttributesMatrix distances = new AttributesMatrix();
					distances.setPdistance(pdistance);
					distances.setRdistance(rdistance);
					distances.setRorientation(rorientation);
					distances.setsShapeMethod(sMainShape + "2" + sSubShape);
					
					distanceArray[i][j] =  distances;
					
					//					//normalize
					//					pdistance = pdistance / (comparestripinfo[3] + mainstripinfo[3]);
					//					rdistance = rdistance / (comparestripinfo[3] + mainstripinfo[3]);
					//					
					//					double normalizedRelativeDistance      = 2 * ( ( 1 / (1 + Math.pow(Math.E, -rdistance)) ) - 0.5);
					//					double normalizedPerpendicularDistance = 2 * ( ( 1 / (1 + Math.pow(Math.E, -pdistance)) ) - 0.5);
					
					//System.out.println("rdistance:           " + rdistance);
					//System.out.println("normalizedrdistance: " + normalizedrdistance);
					
					//					//fill array with distances
					//					relationalGraphMatrix[iCurrentRow][j]              = Math.round(pdistance    * 1000) / 1000.0;   //perpendicular distance
					//					relationalGraphMatrix[relativedistances_row][j]    = Math.round(rdistance    * 1000) / 1000.0;   //relative distance
					//					relationalGraphMatrix[relativeorientations_row][j] = Math.round(rorientation * 1000) / 1000.0;   //relative orientation
					
					



				}
			}
			
			//////////////////////////// saving ARG to be saved in text files /////////////////////////////////////
			try {
				String sShapeName = "";
				int iShapeSize = 0;
				int iOrientation = 0;
				
				if (sMainShape.equalsIgnoreCase("L")) {
					sShapeName = "Line";
					
					int x1 = (int)mainstripinfo[0]; int y1 = (int)mainstripinfo[1];
					int x2 = (int)mainstripinfo[2]; int y2 = (int)mainstripinfo[3];
					Point P1 = new Point(x1, y1);
					Point P2 = new Point(x2, y2);

					iShapeSize = (int) P1.distance(P2); //getPixelsOfTheIdealLineBetweenTwoPoints(new int[]{x1, y1}, new int[]{x2, y2});

					iOrientation = (int)GetAngleBetweenTwoPoints(P1, P2, "degrees");
					//System.out.println(GetAngleBetweenTwoPoints(new Point(0,0), new Point(3,2), "degrees"));
					
				}
				if (sMainShape.equalsIgnoreCase("C")) {
					sShapeName = "Circle";
					iShapeSize = (int)mainstripinfo[2];

				}
				
				String dataline = i + "," + sShapeName + "," + mainMidPoint.x + "," + mainMidPoint.y + "," + iShapeSize + "," + iOrientation + "\r\n";
				datalines = datalines + dataline;
			} catch (Exception ex){
				ex.printStackTrace();
			}
			///////////////////////////////////////////////////////////////////////////////////////////////
			
		}
		
		//////////////////////////// saving ARG data in text files /////////////////////////////
		try {
			String sFilename = "";
			sFilename = "ARG.txt";
//			if (sLeftOrRight.equalsIgnoreCase("Left"))  sFilename = "L.txt";
//			if (sLeftOrRight.equalsIgnoreCase("Right")) sFilename = "R.txt";

			File file = new File(".\\output\\txtdata\\" + sFilename); 

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(datalines);
			bw.flush();
			bw.close();
		} catch (Exception ex){
			ex.printStackTrace();
		}
		//////////////////////////////////////////////////////////////////////////////////////////


		

	}
	
	
	
	
	/**
	 * calculate angle from the Y axis to second point - mid point of second object //array elements: n, x, y, w, h
	 * @param p1 source point - root
	 * @param p2 secondary point - relative to root
	 * @return degrees from Y axis
	 */
	private double GetAngleBetweenTwoPoints(Point p1, Point p2, String sMeasurement) { 
		double xDiff = p2.x - p1.x; 
		double yDiff = p2.y - p1.y; 
		double radians  = Math.atan2(yDiff, xDiff);
		double degrees = Math.toDegrees(radians); 
		
		//From the Y Axis
		double ydegrees = -1;

		if ( (degrees <= 90) && (degrees >= 0) ) {
			ydegrees = 90-degrees;
		}
		if ( (degrees <= 180) && (degrees > 90) ) {
			ydegrees = (180 - degrees) + 90; //360 - degrees + 90;
		}
		if ( (degrees < 0) && (degrees >= -90) ) {
			ydegrees = 90 + Math.abs(degrees);
		}
		if ( (degrees <= -90) && (degrees > -180) ) {
			ydegrees = (90 + Math.abs(degrees) ) - 180;
		}
		
		if (sMeasurement.equalsIgnoreCase("radians"))
			return radians;
		if (sMeasurement.equalsIgnoreCase("degrees"))
			return ydegrees;
		return 0;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//////////////////////////////OLD VERSION - V2 ///////////////////////
	
	
	
//	///////////////////////// L2L line and C2C circle attributes /////////////////////////////////////////////
//
//	/**
//	 * L2L & C2C - calculate line to line relative size differences //array elements: n, x, y, w, h
//	 * @param strips arraylist with all the strips
//	 * @param relativesizes_l2l 2D array matrix calculated with relative sizes
//	 */
//	public void calculate_RelativeSizes_L2LC2CR2R(ArrayList<int[]> stripsorcircles) {
//
//		for (int i = 0; i < stripsorcircles.size(); i++) {
//			int [] mainstripinfo = stripsorcircles.get(i);
//			iCurrentRow++;
//			
//			for (int j = 0; j < stripsorcircles.size(); j++) {
//				int [] comparestripinfo = stripsorcircles.get(j);
//
//				if (i == j) relationalGraphMatrix[iCurrentRow][j] = -1;
//				else {
//					double nrsize = (double) mainstripinfo[3] / (comparestripinfo[3] + mainstripinfo[3]);
//					relationalGraphMatrix[iCurrentRow][j] = Math.round(nrsize * 1000) / 1000.0;
//				}
//			}
//		}
//
//	}
//	
//	
//	/**
//	 * C2L - calculate circle to line relative size differences //array elements: n, x, y, w, h
//	 * @param strips arraylist with all the strips
//	 * @param circles  arraylist with all the circles
//	 * @param relativesizes_c2l 2D array matrix calculated with relative sizes - C2L
//	 */
//	public void calculate_RelativeSizes_C2LorL2C(ArrayList<int[]> strips, ArrayList<int[]> circles) {
//
//		for (int i = 0; i < circles.size(); i++) {
//			int [] maincircleinfo = circles.get(i);
//			iCurrentRow++;
//
//			for (int j = 0; j < strips.size(); j++) {
//				int [] comparestripinfo = strips.get(j);
//
//				if (i == j) relationalGraphMatrix[iCurrentRow][j] = -1;
//				else {
//					double nrsize = (double) maincircleinfo[3] / (comparestripinfo[3] + maincircleinfo[3]);
//					relationalGraphMatrix[iCurrentRow][j] = Math.round(nrsize * 1000) / 1000.0;
//				}
//			}
//		}
//
//		/*System.out.println("relatives#############");
//		for (int i = 0; i < relativesizes_l2l.length; i++) {
//			System.out.println();
//			for (int j = 0; j < relativesizes_l2l.length; j++) {
//				System.out.print(relativesizes_l2l[i][j] + ", ");
//			}
//		}*/
//
//	}
//	
//	
//	/**
//	 * L2L & C2C - calculate line to line perpendicular distance, relative distance, and orientations //array elements: n, x, y, w, h
//	 * @param strips array-list with all the strips
//	 * @param relativedistances_l2l 2D array matrix calculated with relative distances
//	 * @param perpendiculardistances_l2l  2D array matrix calculated with perpendicular distances
//	 * @param relativeorientations_l2l  2D array matrix calculated with relative orientations
//	 */
//	public void calculate_OrientationRelativeAndPerpendicularDistances_L2LC2CR2R(ArrayList<int[]> stripsorcircles, double [][] relativedistances_array, double [][] perpendiculardistances_array, double [][] relativeorientations_array) {
//
//		//int perpendiculardistances_row = iCurrentRow;
//		int relativedistances_row      = iCurrentRow + stripsorcircles.size();
//		int relativeorientations_row   = relativedistances_row + stripsorcircles.size();
//		
//		for (int i = 0; i < stripsorcircles.size(); i++) {
//			int [] mainstripinfo = stripsorcircles.get(i);
//			iCurrentRow++; relativedistances_row++; relativeorientations_row++;
//			
//			for (int j = 0; j < stripsorcircles.size(); j++) {
//				int [] comparestripinfo = stripsorcircles.get(j);
//
//				if (i == j) {
//					relationalGraphMatrix[iCurrentRow][j]               = -1;
//					relationalGraphMatrix[relativedistances_row][j]     = -1;
//					relationalGraphMatrix[relativeorientations_row][j]  = -1;
//				}
//				else {
//					//distance from R to AB line 
//					//Point R1 = new Point( (mainstripinfo[1]), (mainstripinfo[2]) );
//					//Point R2 = new Point( (mainstripinfo[1] + mainstripinfo[3]), (mainstripinfo[2] + mainstripinfo[4]) );
//					//mid point of R1-R2
//					Point R = new Point( (mainstripinfo[1] + mainstripinfo[3]/2) , (mainstripinfo[2] + mainstripinfo[4]/2) ); 
//
//					//second line
//					Point A = new Point( (comparestripinfo[1]), (comparestripinfo[2]) );
//					Point B = new Point( (comparestripinfo[1] + comparestripinfo[3]), (comparestripinfo[2] + comparestripinfo[4]) );
//					//mid point of A-B
//					Point C = new Point( (comparestripinfo[1] + comparestripinfo[3]/2) , (comparestripinfo[2] + comparestripinfo[4]/2) ); 
//
//					//length of root line
//					//double rootLength = Math.sqrt((R2.x-R1.x)*(R2.x-R1.x)+(R2.y-R1.y)*(R2.y-R1.y));
//					//length of second line
//					double normalLength = Math.sqrt((B.x-A.x)*(B.x-A.x)+(B.y-A.y)*(B.y-A.y));
//
//					//calculate distances
//					double pdistance    = Math.abs((R.x-A.x)*(B.y-A.y)-(R.y-A.y)*(B.x-A.x))/normalLength;
//					double rdistance    = Math.sqrt((R.x-C.x)*(R.x-C.x)+(R.y-C.y)*(R.y-C.y));
//					double rorientation = GetAngleBetweenTwoPoints(R, C);
//					
//					//normalize
//					pdistance = pdistance / (comparestripinfo[3] + mainstripinfo[3]);
//					rdistance = rdistance / (comparestripinfo[3] + mainstripinfo[3]);
//					
//					double normalizedRelativeDistance      = 2 * ( ( 1 / (1 + Math.pow(Math.E, -rdistance)) ) - 0.5);
//					double normalizedPerpendicularDistance = 2 * ( ( 1 / (1 + Math.pow(Math.E, -pdistance)) ) - 0.5);
//					
//					//System.out.println("rdistance:           " + rdistance);
//					//System.out.println("normalizedrdistance: " + normalizedrdistance);
//					
//					//fill array with distances
//					relationalGraphMatrix[iCurrentRow][j]              = Math.round(pdistance    * 1000) / 1000.0;   //perpendicular distance
//					relationalGraphMatrix[relativedistances_row][j]    = Math.round(rdistance    * 1000) / 1000.0;   //relative distance
//					relationalGraphMatrix[relativeorientations_row][j] = Math.round(rorientation * 1000) / 1000.0;   //relative orientation
//				}
//			}
//		}
//
//		iCurrentRow = relativeorientations_row;
//		
//		/*System.out.println("\n\nrelatives#############");
//		for (int i = 0; i < relativeorientations_array.length; i++) {
//			System.out.println();
//			for (int j = 0; j < relativeorientations_array.length; j++) {
//				System.out.print(relativeorientations_array[i][j] + ", ");
//			}
//		}*/
//
//	}
//
//
//	/**
//	 * C2L - calculate circle to line perpendicular distance, relative distance, and orientations // //array elements: n, x, y, w, h
//	 * @param circles arraylist with all the circles
//	 * @param strips arraylist with all the strips
//	 * @param relativedistances_c2l 2D array matrix calculated with relative distances
//	 * @param perpendiculardistances_c2l  2D array matrix calculated with perpendicular distances
//	 * @param relativeorientations_c2l  2D array matrix calculated with relative orientations
//	 */
//	public void calculate_OrientationRelativeAndPerpendicularDistances_C2L(ArrayList<int[]> circles, ArrayList<int[]> strips, double [][] relativedistances_c2l, double [][] perpendiculardistances_c2l, double [][] relativeorientations_c2l) {
//
//		for (int i = 0; i < circles.size(); i++) {
//			int [] maincircleinfo = circles.get(i);
//
//			for (int j = 0; j < strips.size(); j++) {
//				int [] comparestripinfo = strips.get(j);
//
//				if (i == j) {
//					perpendiculardistances_c2l[i][j] = -1;
//					relativedistances_c2l[i][j]      = -1;
//					relativeorientations_c2l[i][j]   = -1;
//				}
//				else {
//					//distance from R to AB line 
//					//Point R1 = new Point( (mainstripinfo[1]), (mainstripinfo[2]) );
//					//Point R2 = new Point( (mainstripinfo[1] + mainstripinfo[3]), (mainstripinfo[2] + mainstripinfo[4]) );
//					//mid point of R1-R2
//					Point R = new Point( (maincircleinfo[1] + maincircleinfo[3]/2) , (maincircleinfo[2] + maincircleinfo[4]/2) ); 
//
//					//second line
//					Point A = new Point( (comparestripinfo[1]), (comparestripinfo[2]) );
//					Point B = new Point( (comparestripinfo[1] + comparestripinfo[3]), (comparestripinfo[2] + comparestripinfo[4]) );
//					//mid point of A-B
//					Point C = new Point( (comparestripinfo[1] + comparestripinfo[3]/2) , (comparestripinfo[2] + comparestripinfo[4]/2) );
//
//					//length of root line
//					//double rootLength = Math.sqrt((R2.x-R1.x)*(R2.x-R1.x)+(R2.y-R1.y)*(R2.y-R1.y));
//					//length of second line
//					double normalLength = Math.sqrt((B.x-A.x)*(B.x-A.x)+(B.y-A.y)*(B.y-A.y));
//
//					//calculate distances
//					double pdistance    = Math.abs((R.x-A.x)*(B.y-A.y)-(R.y-A.y)*(B.x-A.x))/normalLength;
//					double rdistance    = Math.sqrt((R.x-C.x)*(R.x-C.x)+(R.y-C.y)*(R.y-C.y));
//					double rorientation = GetAngleBetweenTwoPoints(R, C);
//					
//					//normalize
////					pdistance = pdistance / maincircleinfo[3]/2;
////					rdistance = rdistance / maincircleinfo[3]/2;
////					
////					double normalizedRelativeDistance      = rdistance / (rdistance + (10/rdistance));
//
//					//fill array with distances
//					relativedistances_c2l[i][j]      = Math.round(rdistance    * 1000) / 1000.0;   //relative distance
//					perpendiculardistances_c2l[i][j] = Math.round(pdistance    * 1000) / 1000.0;   //perpendicular distance
//					relativeorientations_c2l[i][j]   = Math.round(rorientation * 1000) / 1000.0;   //relative orientation
//				}
//			}
//		}
//		
////		System.out.println("\n\nrelatives #############");
////		for (int i = 0; i < relativedistances_c2l.length; i++) {
////			System.out.println();
////			for (int j = 0; j < relativedistances_c2l.length; j++) {
////				System.out.print(relativedistances_c2l[i][j] + ", ");
////			}
////		}
//
//	}
//
//
//	/**
//	 * L2C - calculate line to circle perpendicular distance, relative distance, and orientations // //array elements: n, x, y, w, h
//	 * @param circles arraylist with all the circles
//	 * @param strips arraylist with all the strips
//	 * @param relativedistances_l2c 2D array matrix calculated with relative distances
//	 * @param perpendiculardistances_l2c  2D array matrix calculated with perpendicular distances
//	 * @param relativeorientations_l2c  2D array matrix calculated with relative orientations
//	 */
//	public void calculate_OrientationRelativeAndPerpendicularDistances_L2C(ArrayList<int[]> circles, ArrayList<int[]> strips, double [][] relativedistances_l2c, double [][] perpendiculardistances_l2c, double [][] relativeorientations_l2c) {
//
//		for (int i = 0; i < strips.size(); i++) {
//			int [] mainstripinfo = strips.get(i);
//
//			for (int j = 0; j < circles.size(); j++) {
//				int [] comparecircleinfo = circles.get(j);
//
//				if (i == j) {
//					perpendiculardistances_l2c[i][j] = -1;
//					relativedistances_l2c[i][j]      = -1;
//					relativeorientations_l2c[i][j]   = -1;
//				}
//				else {
//					
//					//second line
//					Point A = new Point( (mainstripinfo[1]), (mainstripinfo[2]) );
//					Point B = new Point( (mainstripinfo[1] + mainstripinfo[3]), (mainstripinfo[2] + mainstripinfo[4]) );
//					//mid point of A-B
//					Point C = new Point( (mainstripinfo[1] + mainstripinfo[3]/2) , (mainstripinfo[2] + mainstripinfo[4]/2) );
//					
//					//distance from R to AB line 
//					//Point R1 = new Point( (mainstripinfo[1]), (mainstripinfo[2]) );
//					//Point R2 = new Point( (mainstripinfo[1] + mainstripinfo[3]), (mainstripinfo[2] + mainstripinfo[4]) );
//					//mid point of R1-R2
//					Point R = new Point( (comparecircleinfo[1] + comparecircleinfo[3]/2) , (comparecircleinfo[2] + comparecircleinfo[4]/2) ); 
//
//					//length of root line
//					//double rootLength = Math.sqrt((R2.x-R1.x)*(R2.x-R1.x)+(R2.y-R1.y)*(R2.y-R1.y));
//					//length of second line
//					double normalLength = Math.sqrt((B.x-A.x)*(B.x-A.x)+(B.y-A.y)*(B.y-A.y));
//					
//					//calculate distances
//					double pdistance    = Math.abs((A.x-R.x)*(A.y-B.y)-(A.y-R.y)*(A.x-B.x))/normalLength;
//					double rdistance    = Math.sqrt((C.x-R.x)*(C.x-R.x)+(C.y-R.y)*(C.y-R.y));
//					double rorientation = GetAngleBetweenTwoPoints(C, R);
//					
//					//normalize
////					pdistance = pdistance / maincircleinfo[3]/2;
////					rdistance = rdistance / maincircleinfo[3]/2;
////					
////					double normalizedRelativeDistance      = rdistance / (rdistance + (10/rdistance));
//
//					//fill array with distances
//					relativedistances_l2c[i][j]      = Math.round(rdistance    * 1000) / 1000.0;   //relative distance
//					perpendiculardistances_l2c[i][j] = Math.round(pdistance    * 1000) / 1000.0;   //perpendicular distance
//					relativeorientations_l2c[i][j]   = Math.round(rorientation * 1000) / 1000.0;   //relative orientation
//				}
//			}
//		}
//		
////		System.out.println("\n\nrelatives #############");
////		for (int i = 0; i < relativedistances_c2l.length; i++) {
////			System.out.println();
////			for (int j = 0; j < relativedistances_c2l.length; j++) {
////				System.out.print(relativedistances_c2l[i][j] + ", ");
////			}
////		}
//
//	}
//
//
//	/**
//	 * calculate angle from the Y axis to second point - mid point of second object //array elements: n, x, y, w, h
//	 * @param p1 source point - root
//	 * @param p2 secondary point - relative to root
//	 * @return degrees from Y axis
//	 */
//	private double GetAngleBetweenTwoPoints(Point p1, Point p2) { 
//		double xDiff = p2.x - p1.x; 
//		double yDiff = p2.y - p1.y; 
//		double radians  = Math.atan2(yDiff, xDiff);
//		double degrees = Math.toDegrees(radians); 
//		
//		//From the Y Axis
//		double ydegrees = -1;
//
//		if ( (degrees <= 90) && (degrees >= 0) ) {
//			ydegrees = degrees + (90-degrees) + (90-degrees) + 90;
//		}
//		if ( (degrees <= 180) && (degrees > 90) ) {
//			ydegrees = 360 - degrees + 90;
//		}
//		if ( (degrees < 0) && (degrees >= -90) ) {
//			ydegrees = 90 + Math.abs(degrees);
//		}
//		if ( (degrees <= -90) && (degrees > -180) ) {
//			ydegrees = 90 + Math.abs(degrees);
//		}
//		
//		return radians;
//	}

	
	
	
	
	

//////////////////////////////  OLD VERSION - V1 ///////////////////////
	
	
//	///////////////////////// L2L line and C2C circle attributes /////////////////////////////////////////////
//
//	/**
//	 * L2L & C2C - calculate line to line relative size differences //array elements: n, x, y, w, h
//	 * @param strips arraylist with all the strips
//	 * @param relativesizes_l2l 2D array matrix calculated with relative sizes
//	 */
//	public void calculate_RelativeSizes_L2LC2CR2R(ArrayList<int[]> stripsorcircles, double[][] relativesizes_array) {
//
//		for (int i = 0; i < stripsorcircles.size(); i++) {
//			int [] mainstripinfo = stripsorcircles.get(i);			
//			for (int j = 0; j < stripsorcircles.size(); j++) {
//				int [] comparestripinfo = stripsorcircles.get(j);
//
//				if (i == j) relativesizes_array[i][j] = -1;
//				else {
//					double nrsize = (double) mainstripinfo[3] / (comparestripinfo[3] + mainstripinfo[3]);
//					relativesizes_array[i][j] = Math.round(nrsize * 1000) / 1000.0;
//				}
//			}
//		}
//
//	}
//	
//	
//	/**
//	 * C2L - calculate circle to line relative size differences //array elements: n, x, y, w, h
//	 * @param strips arraylist with all the strips
//	 * @param circles  arraylist with all the circles
//	 * @param relativesizes_c2l 2D array matrix calculated with relative sizes - C2L
//	 */
//	public void calculate_RelativeSizes_C2L(ArrayList<int[]> strips, ArrayList<int[]> circles, double[][] relativesizes_c2l) {
//
//		for (int i = 0; i < circles.size(); i++) {
//			int [] maincircleinfo = circles.get(i);
//
//			for (int j = 0; j < strips.size(); j++) {
//				int [] comparestripinfo = strips.get(j);
//
//				if (i == j) relativesizes_c2l[i][j] = -1;
//				else {
//					double nrsize = (double) maincircleinfo[3] / (comparestripinfo[3] + maincircleinfo[3]);
//					relativesizes_c2l[i][j] = Math.round(nrsize * 1000) / 1000.0;
//				}
//			}
//		}
//
//		/*System.out.println("relatives#############");
//		for (int i = 0; i < relativesizes_l2l.length; i++) {
//			System.out.println();
//			for (int j = 0; j < relativesizes_l2l.length; j++) {
//				System.out.print(relativesizes_l2l[i][j] + ", ");
//			}
//		}*/
//
//	}
//	
//	
//	/**
//	 * L2C - calculate line to circle relative size differences //array elements: n, x, y, w, h
//	 * @param strips arraylist with all the strips
//	 * @param circles  arraylist with all the circles
//	 * @param relativesizes_l2c 2D array matrix calculated with relative sizes - L2C
//	 */
//	public void calculate_RelativeSizes_L2C(ArrayList<int[]> strips, ArrayList<int[]> circles, double[][] relativesizes_l2c) {
//		
//		for (int i = 0; i < strips.size(); i++) {
//			int [] comparestripinfo = strips.get(i);
//			
//			for (int j = 0; j < circles.size(); j++) {
//				int [] maincircleinfo = circles.get(j);
//
//				if (i == j) relativesizes_l2c[i][j] = -1;
//				else {
//					double nrsize = (double) comparestripinfo[3] / (comparestripinfo[3] + maincircleinfo[3]);
//					relativesizes_l2c[i][j] = Math.round(nrsize * 1000) / 1000.0;
//				}
//			}
//		}
//
//		/*System.out.println("relatives#############");
//		for (int i = 0; i < relativesizes_l2l.length; i++) {
//			System.out.println();
//			for (int j = 0; j < relativesizes_l2l.length; j++) {
//				System.out.print(relativesizes_l2l[i][j] + ", ");
//			}
//		}*/
//
//	}
//	
//	
//	/**
//	 * L2L & C2C - calculate line to line perpendicular distance, relative distance, and orientations // //array elements: n, x, y, w, h
//	 * @param strips arraylist with all the strips
//	 * @param relativedistances_l2l 2D array matrix calculated with relative distances
//	 * @param perpendiculardistances_l2l  2D array matrix calculated with perpendicular distances
//	 * @param relativeorientations_l2l  2D array matrix calculated with relative orientations
//	 */
//	public void calculate_OrientationRelativeAndPerpendicularDistances_L2LC2CR2R(ArrayList<int[]> stripsorcircles, double [][] relativedistances_array, double [][] perpendiculardistances_array, double [][] relativeorientations_array) {
//
//		for (int i = 0; i < stripsorcircles.size(); i++) {
//			int [] mainstripinfo = stripsorcircles.get(i);
//
//			for (int j = 0; j < stripsorcircles.size(); j++) {
//				int [] comparestripinfo = stripsorcircles.get(j);
//
//				if (i == j) {
//					perpendiculardistances_array[i][j] = -1;
//					relativedistances_array[i][j]      = -1;
//					relativeorientations_array[i][j]   = -1;
//				}
//				else {
//					//distance from R to AB line 
//					//Point R1 = new Point( (mainstripinfo[1]), (mainstripinfo[2]) );
//					//Point R2 = new Point( (mainstripinfo[1] + mainstripinfo[3]), (mainstripinfo[2] + mainstripinfo[4]) );
//					//mid point of R1-R2
//					Point R = new Point( (mainstripinfo[1] + mainstripinfo[3]/2) , (mainstripinfo[2] + mainstripinfo[4]/2) ); 
//
//					//second line
//					Point A = new Point( (comparestripinfo[1]), (comparestripinfo[2]) );
//					Point B = new Point( (comparestripinfo[1] + comparestripinfo[3]), (comparestripinfo[2] + comparestripinfo[4]) );
//					//mid point of A-B
//					Point C = new Point( (comparestripinfo[1] + comparestripinfo[3]/2) , (comparestripinfo[2] + comparestripinfo[4]/2) ); 
//
//					//length of root line
//					//double rootLength = Math.sqrt((R2.x-R1.x)*(R2.x-R1.x)+(R2.y-R1.y)*(R2.y-R1.y));
//					//length of second line
//					double normalLength = Math.sqrt((B.x-A.x)*(B.x-A.x)+(B.y-A.y)*(B.y-A.y));
//
//					//calculate distances
//					double pdistance    = Math.abs((R.x-A.x)*(B.y-A.y)-(R.y-A.y)*(B.x-A.x))/normalLength;
//					double rdistance    = Math.sqrt((R.x-C.x)*(R.x-C.x)+(R.y-C.y)*(R.y-C.y));
//					double rorientation = GetAngleBetweenTwoPoints(R, C);
//					
//					//normalize
//					pdistance = pdistance / (comparestripinfo[3] + mainstripinfo[3]);
//					rdistance = rdistance / (comparestripinfo[3] + mainstripinfo[3]);
//					
//					double normalizedRelativeDistance      = 2 * ( ( 1 / (1 + Math.pow(Math.E, -rdistance)) ) - 0.5);
//					double normalizedPerpendicularDistance = 2 * ( ( 1 / (1 + Math.pow(Math.E, -pdistance)) ) - 0.5);
//					
//					//System.out.println("rdistance:           " + rdistance);
//					//System.out.println("normalizedrdistance: " + normalizedrdistance);
//					
//					//fill array with distances
//					relativedistances_array[i][j]      = Math.round(rdistance    * 1000) / 1000.0;   //relative distance
//					perpendiculardistances_array[i][j] = Math.round(pdistance    * 1000) / 1000.0;   //perpendicular distance
//					relativeorientations_array[i][j]   = Math.round(rorientation * 1000) / 1000.0;   //relative orientation
//				}
//			}
//		}
//
//		/*System.out.println("\n\nrelatives#############");
//		for (int i = 0; i < relativeorientations_array.length; i++) {
//			System.out.println();
//			for (int j = 0; j < relativeorientations_array.length; j++) {
//				System.out.print(relativeorientations_array[i][j] + ", ");
//			}
//		}*/
//
//	}
//
//
//	/**
//	 * C2L - calculate circle to line perpendicular distance, relative distance, and orientations // //array elements: n, x, y, w, h
//	 * @param circles arraylist with all the circles
//	 * @param strips arraylist with all the strips
//	 * @param relativedistances_c2l 2D array matrix calculated with relative distances
//	 * @param perpendiculardistances_c2l  2D array matrix calculated with perpendicular distances
//	 * @param relativeorientations_c2l  2D array matrix calculated with relative orientations
//	 */
//	public void calculate_OrientationRelativeAndPerpendicularDistances_C2L(ArrayList<int[]> circles, ArrayList<int[]> strips, double [][] relativedistances_c2l, double [][] perpendiculardistances_c2l, double [][] relativeorientations_c2l) {
//
//		for (int i = 0; i < circles.size(); i++) {
//			int [] maincircleinfo = circles.get(i);
//
//			for (int j = 0; j < strips.size(); j++) {
//				int [] comparestripinfo = strips.get(j);
//
//				if (i == j) {
//					perpendiculardistances_c2l[i][j] = -1;
//					relativedistances_c2l[i][j]      = -1;
//					relativeorientations_c2l[i][j]   = -1;
//				}
//				else {
//					//distance from R to AB line 
//					//Point R1 = new Point( (mainstripinfo[1]), (mainstripinfo[2]) );
//					//Point R2 = new Point( (mainstripinfo[1] + mainstripinfo[3]), (mainstripinfo[2] + mainstripinfo[4]) );
//					//mid point of R1-R2
//					Point R = new Point( (maincircleinfo[1] + maincircleinfo[3]/2) , (maincircleinfo[2] + maincircleinfo[4]/2) ); 
//
//					//second line
//					Point A = new Point( (comparestripinfo[1]), (comparestripinfo[2]) );
//					Point B = new Point( (comparestripinfo[1] + comparestripinfo[3]), (comparestripinfo[2] + comparestripinfo[4]) );
//					//mid point of A-B
//					Point C = new Point( (comparestripinfo[1] + comparestripinfo[3]/2) , (comparestripinfo[2] + comparestripinfo[4]/2) );
//
//					//length of root line
//					//double rootLength = Math.sqrt((R2.x-R1.x)*(R2.x-R1.x)+(R2.y-R1.y)*(R2.y-R1.y));
//					//length of second line
//					double normalLength = Math.sqrt((B.x-A.x)*(B.x-A.x)+(B.y-A.y)*(B.y-A.y));
//
//					//calculate distances
//					double pdistance    = Math.abs((R.x-A.x)*(B.y-A.y)-(R.y-A.y)*(B.x-A.x))/normalLength;
//					double rdistance    = Math.sqrt((R.x-C.x)*(R.x-C.x)+(R.y-C.y)*(R.y-C.y));
//					double rorientation = GetAngleBetweenTwoPoints(R, C);
//					
//					//normalize
////					pdistance = pdistance / maincircleinfo[3]/2;
////					rdistance = rdistance / maincircleinfo[3]/2;
////					
////					double normalizedRelativeDistance      = rdistance / (rdistance + (10/rdistance));
//
//					//fill array with distances
//					relativedistances_c2l[i][j]      = Math.round(rdistance    * 1000) / 1000.0;   //relative distance
//					perpendiculardistances_c2l[i][j] = Math.round(pdistance    * 1000) / 1000.0;   //perpendicular distance
//					relativeorientations_c2l[i][j]   = Math.round(rorientation * 1000) / 1000.0;   //relative orientation
//				}
//			}
//		}
//		
////		System.out.println("\n\nrelatives #############");
////		for (int i = 0; i < relativedistances_c2l.length; i++) {
////			System.out.println();
////			for (int j = 0; j < relativedistances_c2l.length; j++) {
////				System.out.print(relativedistances_c2l[i][j] + ", ");
////			}
////		}
//
//	}
//
//
//	/**
//	 * L2C - calculate line to circle perpendicular distance, relative distance, and orientations // //array elements: n, x, y, w, h
//	 * @param circles arraylist with all the circles
//	 * @param strips arraylist with all the strips
//	 * @param relativedistances_l2c 2D array matrix calculated with relative distances
//	 * @param perpendiculardistances_l2c  2D array matrix calculated with perpendicular distances
//	 * @param relativeorientations_l2c  2D array matrix calculated with relative orientations
//	 */
//	public void calculate_OrientationRelativeAndPerpendicularDistances_L2C(ArrayList<int[]> circles, ArrayList<int[]> strips, double [][] relativedistances_l2c, double [][] perpendiculardistances_l2c, double [][] relativeorientations_l2c) {
//
//		for (int i = 0; i < strips.size(); i++) {
//			int [] mainstripinfo = strips.get(i);
//
//			for (int j = 0; j < circles.size(); j++) {
//				int [] comparecircleinfo = circles.get(j);
//
//				if (i == j) {
//					perpendiculardistances_l2c[i][j] = -1;
//					relativedistances_l2c[i][j]      = -1;
//					relativeorientations_l2c[i][j]   = -1;
//				}
//				else {
//					
//					//second line
//					Point A = new Point( (mainstripinfo[1]), (mainstripinfo[2]) );
//					Point B = new Point( (mainstripinfo[1] + mainstripinfo[3]), (mainstripinfo[2] + mainstripinfo[4]) );
//					//mid point of A-B
//					Point C = new Point( (mainstripinfo[1] + mainstripinfo[3]/2) , (mainstripinfo[2] + mainstripinfo[4]/2) );
//					
//					//distance from R to AB line 
//					//Point R1 = new Point( (mainstripinfo[1]), (mainstripinfo[2]) );
//					//Point R2 = new Point( (mainstripinfo[1] + mainstripinfo[3]), (mainstripinfo[2] + mainstripinfo[4]) );
//					//mid point of R1-R2
//					Point R = new Point( (comparecircleinfo[1] + comparecircleinfo[3]/2) , (comparecircleinfo[2] + comparecircleinfo[4]/2) ); 
//
//					//length of root line
//					//double rootLength = Math.sqrt((R2.x-R1.x)*(R2.x-R1.x)+(R2.y-R1.y)*(R2.y-R1.y));
//					//length of second line
//					double normalLength = Math.sqrt((B.x-A.x)*(B.x-A.x)+(B.y-A.y)*(B.y-A.y));
//					
//					//calculate distances
//					double pdistance    = Math.abs((A.x-R.x)*(A.y-B.y)-(A.y-R.y)*(A.x-B.x))/normalLength;
//					double rdistance    = Math.sqrt((C.x-R.x)*(C.x-R.x)+(C.y-R.y)*(C.y-R.y));
//					double rorientation = GetAngleBetweenTwoPoints(C, R);
//					
//					//normalize
////					pdistance = pdistance / maincircleinfo[3]/2;
////					rdistance = rdistance / maincircleinfo[3]/2;
////					
////					double normalizedRelativeDistance      = rdistance / (rdistance + (10/rdistance));
//
//					//fill array with distances
//					relativedistances_l2c[i][j]      = Math.round(rdistance    * 1000) / 1000.0;   //relative distance
//					perpendiculardistances_l2c[i][j] = Math.round(pdistance    * 1000) / 1000.0;   //perpendicular distance
//					relativeorientations_l2c[i][j]   = Math.round(rorientation * 1000) / 1000.0;   //relative orientation
//				}
//			}
//		}
//		
////		System.out.println("\n\nrelatives #############");
////		for (int i = 0; i < relativedistances_c2l.length; i++) {
////			System.out.println();
////			for (int j = 0; j < relativedistances_c2l.length; j++) {
////				System.out.print(relativedistances_c2l[i][j] + ", ");
////			}
////		}
//
//	}



} //end class
