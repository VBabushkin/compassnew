package shoeFittingAlgorithm;


import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import com.mysql.jdbc.PreparedStatement;

/**
 * handles all the DB connections and data read and write operations
 * @author nimesha
 *
 */
public class DatabaseReadWrite implements Runnable {
	private int id=1;
	private String filename = null;
	private int[] shoeSize  = null;
	private int inLines     = -1;
	private int inCircles   = -1;
	private AttributesMatrix[][] distances  = null;


	
	

	/**
	 * make the db connection and returns the connection
	 * @return
	 */
	private Connection getConnection() {
		Connection con = null;
//		String url = "jdbc:mysql://localhost:3306/";
//		String db = "compass";
//		String driver = "com.mysql.jdbc.Driver";
//		String user = "compass";
//		String pass = "compass";
		
		String url = "jdbc:mysql://localhost:3306/";
		String db = "testdb";
        String user = "testuser";
        String pass = "test623";
		try{
			//Class.forName(driver);
			con = DriverManager.getConnection(url+db, user, pass);
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return con;
	}
	
	/**
	 * update the db with the data if the shoes are detected for the first time
	 */
	@Override
	public void run() {
		try {
			this.writeShoeInfoToDB();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * set shoe info in the DB, if it did not match with existing records
	 * @param shoeSize
	 * @param rightShoeSize
	 * @param inLines
	 * @param inCircles
	 * @param inLinesRight
	 * @param inCirclesRight
	 * @param distances
	 * @param distances_right
	 * @param sDirection
	 * @param iBlackPlainDetected
	 * @throws SQLException
	 * @throws IOException
	 */
	public void setShoeInfoWriteToDB(int id, String filename, int[] shoeSize, int inLines, int inCircles, AttributesMatrix[][] distances) throws SQLException, IOException {
		this.id = id;
		this.filename = filename;
		this.shoeSize    = shoeSize;
		this.inLines     = inLines;
		this.inCircles   = inCircles;
		this.distances      = distances;

	}

	/**
	 * to print the whole DB
	 * @param filename -- name of the file
	 * @param shoeSize -- ArrayList of shoe sizes
	 * TODO: other parameters may be added later
	 * @throws SQLException 
	 */
	public  void outputDB() throws SQLException{
		Connection con = getConnection();
		ResultSet rs = null;
		PreparedStatement stmt = (PreparedStatement) con.prepareStatement("SELECT id, userID, fileID,shoesize_W,shoesize_H,nlines, ncircles FROM shoesCompassTest");
		rs =stmt.executeQuery();

    		
            
            //Print the headers of the table
            System.out.print(String.format("%-5s" , "ID"));
            System.out.print("| ");
            System.out.print(String.format("%-5s" , "userID"));
            System.out.print("| ");
            System.out.print(String.format("%-15s" , "fileID"));
            System.out.print("| ");
            System.out.print(String.format("%-10s" , "shoesize_W"));
            System.out.print("| ");
            System.out.print(String.format("%-11s" , "shoesize_H"));
            System.out.print("| ");
            System.out.print(String.format("%-11s" , "nlines"));
            System.out.print("| ");
            System.out.println(String.format("%-10s" , "ncircles"));
            System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
            //print the content of the table
            while (rs.next()) {
            	System.out.print(String.format("%-5d" , rs.getInt(1)));
                System.out.print("| ");
                System.out.print(String.format("%-6d" , rs.getInt(2)));
                System.out.print("| ");
                System.out.print(String.format("%-15s" , rs.getString(3)));
                System.out.print("| ");
                System.out.print(String.format("%-11d" , rs.getInt(4)));
                System.out.print("| ");
                System.out.print(String.format("%-11d" , rs.getInt(5)));
                System.out.print("| ");
                System.out.print(String.format("%-12d" , rs.getInt(6)));
                System.out.print("| ");
                System.out.println(String.format("%-12d" , rs.getInt(7)));
                
                
            }
            stmt.close();
    		con.close();
        
		
	}
	
	
	/**
	 * write data into the compass table
	 * @throws SQLException
	 * @throws IOException
	 */
	public void writeShoeInfoToDB() throws SQLException, IOException {
		Connection con = getConnection();

		ByteArrayOutputStream bosl = new ByteArrayOutputStream();
		ObjectOutput outl = new ObjectOutputStream(bosl);
		outl.writeObject(distances);
		byte barrDistances[] = bosl.toByteArray();

		

		PreparedStatement stmt = (PreparedStatement) con.prepareStatement("INSERT INTO shoesCompassTest (userID, fileID, shoesize_W, shoesize_H, nlines, ncircles, distances) " + "VALUES (?, ?, ?, ?, ?, ?, ?)");
		
		stmt.setInt(1, id);
		stmt.setString(2, filename);
		stmt.setInt(3, shoeSize[0]);
		stmt.setInt(4, shoeSize[1]);
		
		stmt.setInt(5, inLines);
		stmt.setInt(6, inCircles);
	
		stmt.setBytes(7, barrDistances);
	
		stmt.execute();

		stmt.close();
		con.close();
	}
	
	
//	/**
//	 * update if new destination is selected
//	 * @param iShoeID
//	 * @param NewDestination
//	 * @throws SQLException
//	 * @throws IOException
//	 */
//	public void writeNewDestinationToDB(int iShoeID, String NewDestination) throws SQLException, IOException {
//		Connection con = getConnection();
//
//		PreparedStatement stmt = con.prepareStatement("UPDATE shoes SET direction=?" + " WHERE shoeid=?");
//		stmt.setString(1, NewDestination);
//		stmt.setInt(2, iShoeID);
//		stmt.execute();
//
//		stmt.close();
//		con.close();
//	}

	

	/**
	 * read data from compass table and returns an arraylist with shoe info
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public ArrayList<ShoesInfo> readShoeInfoFromDB() throws SQLException, IOException, ClassNotFoundException {

		String sDirection = "";
		int[] iarrlinecircleinfo = new int[4];
		
		ArrayList<ShoesInfo> arrShoesinfo = new ArrayList<ShoesInfo>();
		Connection con = getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM shoesCompassTest");

		while (rs.next())
		{
			int id = rs.getInt("id");

			byte[] bmatrix = rs.getBytes("distances");
			ByteArrayInputStream inbmatrix = new ByteArrayInputStream(bmatrix);
			ObjectInputStream isbmatrix    = new ObjectInputStream(inbmatrix);
			AttributesMatrix[][] argmatrix = (AttributesMatrix[][]) isbmatrix.readObject();

			
			//sDirection = rs.getString("direction");
			
			iarrlinecircleinfo[0] = rs.getInt("shoesize_W");
			iarrlinecircleinfo[1] = rs.getInt("shoesize_H");
			
			
			iarrlinecircleinfo[2] = rs.getInt("nlines");
			iarrlinecircleinfo[3] = rs.getInt("ncircles");
			//System.out.println(id+" ARG matrix read "+argmatrix.length+ "  "+argmatrix[0].length+" "+iarrlinecircleinfo[2]+" "+iarrlinecircleinfo[3]);
			ShoesInfo shoeinfo = new ShoesInfo(id, argmatrix, iarrlinecircleinfo);
			arrShoesinfo.add(shoeinfo);
		}

		st.close();
		con.close();

		return arrShoesinfo;
	}

//	/**
//	 * get the direction from db for a given shoeid
//	 * @param shoeID
//	 * @return
//	 * @throws SQLException
//	 */
//	public String readDirectionFromDB(int shoeID) throws SQLException {
//		String sDirection = "";
//		Connection con = getConnection();
//		Statement st = con.createStatement();
//		//ResultSet rs = st.executeQuery("SELECT * FROM shoes");
//		ResultSet rs = st.executeQuery("SELECT direction FROM shoes WHERE shoeid='" + shoeID + "'");
//		rs.next();
//		sDirection = rs.getString("direction");
//		
//		st.close();
//		con.close();
//
//		return sDirection;
//	}

	/**
	 * read no of lines and circles from DB for a given shoeid
	 * @param shoeID
	 * @return int array with info {nlines_left, ncircles_left, nlines_right, ncircles_right}
	 * @throws SQLException
	 */
	public int[] readLinesCirclesAndShoeSizeInfoFromDB(int shoeID) throws SQLException {
		Connection con = getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM shoesCompassTest");
		int[] iarrlinecircleinfo = new int[4];

		while (rs.next())
		{
			int id = rs.getInt("id");

			if (id == shoeID) {
				
				iarrlinecircleinfo[0] = rs.getInt("shoesize_W");
				iarrlinecircleinfo[1] = rs.getInt("shoesize_H");
				iarrlinecircleinfo[2] = rs.getInt("nlines");
				iarrlinecircleinfo[3] = rs.getInt("ncircles");

			}
		}
		
		st.close();
		con.close();
		
		return iarrlinecircleinfo;
	}
	
//	/**
//	 * converts an image into a byte[]
//	 * @param sPath
//	 * @return
//	 * @throws IOException
//	 */
//	private byte[] getImageByteStream(String sPath) throws IOException {
//		BufferedImage imgCanny = ImageIO.read(new File(sPath));
//		ByteArrayOutputStream bosImg = new ByteArrayOutputStream();
//		ImageIO.write(imgCanny, "PNG", bosImg);
//		byte barrCanny[] = bosImg.toByteArray();
//		return barrCanny;
//	}
	
	
//	/////////////////////////// allshoes operations //////////////////////////////////
//	
	/**
	 * write data into the compass allshoes table
	 * @param imgCanny
	 * @param distances
	 * @param sDiscription
	 * @throws SQLException
	 * @throws IOException
	 */
	public int writeAllShoesInfoToDB(int id, String filename, int[] shoeSize, int inLines, int inCircles, AttributesMatrix[][] distances) throws SQLException, IOException {
		
		Connection con = getConnection();

		ByteArrayOutputStream bosl = new ByteArrayOutputStream();
		ObjectOutput outl = new ObjectOutputStream(bosl);
		outl.writeObject(distances);
		byte barrDistances[] = bosl.toByteArray();

		

		PreparedStatement stmt = (PreparedStatement) con.prepareStatement("INSERT INTO shoesCompassTest (userID, fileID, shoesize_W, shoesize_H, nlines, ncircles, distances) " + "VALUES (?, ?, ?, ?, ?, ?, ?)",Statement.RETURN_GENERATED_KEYS);
		
		stmt.setInt(1, id);
		stmt.setString(2, filename);
		stmt.setInt(3, shoeSize[0]);
		stmt.setInt(4, shoeSize[1]);
		
		stmt.setInt(5, inLines);
		stmt.setInt(6, inCircles);
	
		stmt.setBytes(7, barrDistances);
	
		stmt.execute();

		ResultSet rss = stmt.getGeneratedKeys();
		
		int iAllShoeID = 0;
		while(rss.next())
			iAllShoeID = rss.getInt(1);

		stmt.close();
		con.close();
		
		return iAllShoeID;
	}
//	
//	
//	/**
//	 * update if new destination is selected
//	 * @param iShoeID
//	 * @param NewDestination
//	 * @throws SQLException
//	 * @throws IOException
//	 */
//	public void writeFilenamesToAllShoesDB(int iAllShoeID, String sOriginalImagePath, String sCannyImagePath) throws SQLException, IOException {
//		Connection con = getConnection();
//
//		PreparedStatement stmt = con.prepareStatement("UPDATE allshoes SET originalcapture_path=?, cannyshoe_path =? " + " WHERE shoeid=?");
//		stmt.setString(1, sOriginalImagePath);
//		stmt.setString(2, sCannyImagePath);
//		stmt.setInt(3, iAllShoeID);
//		stmt.execute();
//
//		stmt.close();
//		con.close();
//	}
//	
//	/**
//	 * update time and date, if match found
//	 * @param iAllShoeID
//	 * @throws SQLException
//	 * @throws IOException
//	 */
//	public void writeMatchingTimestampToAllShoesDB(int iAllShoeID) throws SQLException, IOException {
//		Connection con = getConnection();
//		
//		java.util.Date dt = new java.util.Date();
//		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		String currentTime = sdf.format(dt);
//
//		PreparedStatement stmt = con.prepareStatement("UPDATE allshoes SET matchedtimestamp=?" + " WHERE shoeid=?");
//		stmt.setString(1, currentTime);
//		stmt.setInt(2, iAllShoeID);
//		stmt.execute();
//
//		stmt.close();
//		con.close();
//	}


}








