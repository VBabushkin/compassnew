package shoeFittingAlgorithm;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;

/**
 * back up shoe information each-time it detects a new pair
 * @author nimesha
 *
 */
public class BackupData implements Runnable {
	private DatabaseReadWrite dbcon = null;
	private int id=1;
	private String filename = null;
	private int[] shoeSize  = null;
	private int inLines     = -1;
	private int inCircles   = -1;
	private AttributesMatrix[][] distances  = null;
	
	public BackupData() {
		dbcon = new DatabaseReadWrite();
	}
	
	@Override
	public void run() {
		try {
			int allshoesid = dbcon.writeAllShoesInfoToDB(id,  filename, shoeSize,  inLines,  inCircles,  distances);
			//moveOriginalAndCannyImagesAndUpdateDB(allshoesid);
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * update DB with extracted shoe sole info
	 * @param leftShoeSize
	 * @param rightShoeSize
	 * @param inLinesLeft
	 * @param inCirclesLeft
	 * @param inLinesRight
	 * @param inCirclesRight
	 * @param distances_left
	 * @param distances_right
	 * @param sDirection
	 * @param iBlackPlainDetected
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	public void backupDataInAllShoesInfoDB(int id, String filename, int[] shoeSize, int inLines, int inCircles, AttributesMatrix[][] distances) throws SQLException, IOException {
		
		this.id = id;
		this.filename = filename;
		this.shoeSize    = shoeSize;
		this.inLines     = inLines;
		this.inCircles   = inCircles;
		this.distances      = distances;
		
		//int allshoesid = dbcon.writeAllShoesInfoToDB(leftShoeSize, rightShoeSize, inLinesLeft, inCirclesLeft, inLinesRight, inCirclesRight, distances_left, distances_right, sDirection, iBlackPlainDetected);
		//return allshoesid;
	}
	
//	/**
//	 * save original image and canny edge detected image in the "\\output\\all-shoes-backup\\" folder
//	 * and update DB record with the path+filename
//	 * @param allshoesid
//	 * @throws IOException
//	 * @throws SQLException
//	 */
//	private void moveOriginalAndCannyImagesAndUpdateDB(int allshoesid) throws IOException, SQLException {
//		sFilenameOriginal = new String(".\\output\\all-shoes-backup\\" + allshoesid + "_original.jpg");
//		sFilenameCanny    = new String(".\\output\\all-shoes-backup\\" + allshoesid + "_canny.jpg");
//		
//		Files.copy(new File(Properties.sRawImagePath).toPath(), new File(sFilenameOriginal).toPath() );
//		Files.copy(new File(Properties.cannyEdgeDetectedImagePath).toPath(), new File(sFilenameCanny).toPath() );
//		
//		dbcon.writeFilenamesToAllShoesDB(allshoesid, sFilenameOriginal, sFilenameCanny);
//		
//	}

//	/**
//	 * if the current shoe matched with existing record in the DB, update the time
//	 * @param allshoesid
//	 * @throws SQLException
//	 * @throws IOException
//	 */
//	public void updateMatchedTimestamp(int allshoesid) throws SQLException, IOException {
//		dbcon.writeMatchingTimestampToAllShoesDB(allshoesid);
//	}
	
	
}
