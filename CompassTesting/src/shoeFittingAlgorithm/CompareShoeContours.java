package shoeFittingAlgorithm;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

public class CompareShoeContours {
	
	final static double D_LINE_THRESHOLD_VALUE=0.02;
	final static double  D_BAD_PIXEL_THRESHOLD_VALUE=0.4;
	
	
	public static boolean contoursMatched(shoesInfo2 shoe1, shoesInfo2 shoe2) throws IOException{//(Mat image1, Mat image2){
		Mat image1=shoe1.shoeOutline;
		Mat image2=shoe2.shoeOutline;
		
		MatOfPoint tempContourShoe1 =  ExtractShoeContours.extractLargestContour(image1);
		MatOfPoint tempContourShoe2 =  ExtractShoeContours.extractLargestContour(image2);
		
		Point[] outlinePointsShoe1 = tempContourShoe1.toArray();
		Point[] outlinePointsShoe2 = tempContourShoe2.toArray();
//		System.out.println(outlinePointsShoe1.length);
//		System.out.println(outlinePointsShoe2.length);
		//Draw the outline on the white background
		Mat outlinePointsOnlyShoe1 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat imageShoe1 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat outlinePointsOnlyShoe2 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		
		for(int i=0;i<outlinePointsShoe1.length-1; i++){
			Point start=outlinePointsShoe1[i];
			Point end= outlinePointsShoe1[i+1];
			Core.line(outlinePointsOnlyShoe1, start, end, new Scalar(0,255,0), 1);
		}
		
		imageShoe1=outlinePointsOnlyShoe1.clone();
		
		for(int i=0;i<outlinePointsShoe2.length-1; i++){
			Point start=outlinePointsShoe2[i];
			Point end= outlinePointsShoe2[i+1];
			Core.line(outlinePointsOnlyShoe2, start, end, new Scalar(0,255,0), 1);
		}
		
		
		
		Point center1=findGravityCenter(tempContourShoe1);
		Point center2=findGravityCenter(tempContourShoe2);
		
		Core.circle(outlinePointsOnlyShoe1, center1, 3, new Scalar(0,0,255));
		Core.circle(outlinePointsOnlyShoe2, center2, 3, new Scalar(0,0,255));
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe1),"Outline of tempContourShoe1 ");
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe2),"Outline of tempContourShoe2 ");
		
		
		
		
		//ImageIO.write(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe1), "JPG", new File("./output/result/outlinePointsOnlyShoe1.jpg"));
		//ImageIO.write(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe2), "JPG", new File("./output/result/outlinePointsOnlyShoe2.jpg"));
		
		
		Mat overlayImage= new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat shiftedImageShoe2= new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		
		Imgproc.cvtColor(overlayImage, overlayImage, Imgproc.COLOR_GRAY2BGR);
		for(int i=0;i<outlinePointsShoe1.length-1; i++){
			Point start=outlinePointsShoe1[i];
			Point end= outlinePointsShoe1[i+1];
			Core.line(overlayImage, start, end, new Scalar(255,0,0), 1);
		}
		
		//shift contour so their centers of gravity coincide
		Point[] outlinePointsShoe2Shifted = tempContourShoe2.toArray();
		int diffX= (int) (center1.x-center2.x);
		int diffY=(int) (center1.y-center2.y);
		for(int i=0;i<outlinePointsShoe2.length;i++){
			outlinePointsShoe2Shifted[i].x=outlinePointsShoe2[i].x+diffX;
			outlinePointsShoe2Shifted[i].y=outlinePointsShoe2[i].y+diffY;
		}
		
		//to get the diagonal
		RotatedRect minAreaRectShoe2=org.opencv.imgproc.Imgproc.minAreaRect(new MatOfPoint2f( outlinePointsShoe2Shifted ));
		Point[] pts=new Point[4];
				
		minAreaRectShoe2.points(pts);
		
		double diagonal = Math.sqrt((pts[0].x-pts[2].x)*(pts[0].x-pts[2].x)+(pts[0].y-pts[2].y)*(pts[0].y-pts[2].y));
		
		
		
		//draw the shifted image on the overlay background
		for(int i=0;i<outlinePointsShoe2Shifted.length-1; i++){
			Point start=outlinePointsShoe2Shifted[i];
			Point end= outlinePointsShoe2Shifted[i+1];
			Core.line(overlayImage, start, end, new Scalar(0,255,0), 1);
			Core.line(shiftedImageShoe2, start, end, new Scalar(0,0,0), 1);
		}
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imageShoe1),"imageShoe1 ");
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(shiftedImageShoe2),"shiftedImageShoe2");
		
		
		//add centers to the image
		Core.circle(overlayImage, center1, 2, new Scalar(255,255,0));
		Core.circle(overlayImage, new Point(center2.x+diffX, center2.y+diffY), 3, new Scalar(0,0,255));
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(overlayImage),"Two contours overlayed ");

		//now convert both contour images to binary arrays

		//convert it to binary data
		int[][] binArrayShoe1=ExtractShoeFeatures.extractBinaryDataFromImage(imageShoe1);
		//int[][] binArrayShoe2=ExtractShoeFeatures.extractBinaryDataFromImage(shiftedImageShoe2);
		//BufferedImage squaredCircleToCheck = ProcessImages.Mat2BufferedImage(shiftedImageShoe2);
	
		int circumference=(int) Imgproc.arcLength(new MatOfPoint2f( outlinePointsShoe2Shifted ), true);
		
		int width  = binArrayShoe1.length;// squaredCircleToCheck.getWidth();
		int height = binArrayShoe1[0].length;//squaredCircleToCheck.getHeight();
	
		
		
		ArrayList<Integer> iarrErrors = new ArrayList<Integer>();
		ArrayList<Integer> iBadPixels = new ArrayList<Integer>();
		Point pCenter = center1;//the center of the common contour (first shoe's gravity center)
		
		int iPixelError = -1;
		
		double dLineThreshold = 0.0;
		double dBadPixelThreshold = 0.0;
		
		boolean match=false;
		
		//set thresholds:
		
	
		
		
		for (int i = 0; i < outlinePointsShoe2Shifted.length; i++) {
			int x1 = (int) outlinePointsShoe2Shifted[i].x;
			int y1 = (int) outlinePointsShoe2Shifted[i].y;
			ArrayList<int[]> iarrPixelsOnTheLine_InsideCircle  = getPixelsOnTheLineBetweenTwoPoints (new int[]{(int) pCenter.x, (int) pCenter.y}, new int[]{x1, y1});
			ArrayList<int[]> iarrPixelsOnTheLine_OutsideCircle = getNextPixelsOnTheLineOutsideContour(new int[]{(int) pCenter.x, (int) pCenter.y}, new int[]{x1, y1}, width, height);    
			
			
			
			
			for (int j = (iarrPixelsOnTheLine_InsideCircle.size() - 1), k = 0; j >= 0; j--, k++) {
				int xj = iarrPixelsOnTheLine_InsideCircle.get(j)[0];
				int yj = iarrPixelsOnTheLine_InsideCircle.get(j)[1];
				Core.circle(overlayImage, new Point(xj, yj), 1,new Scalar(255,0,0), 1);
				//System.out.println("xj "+xj +" yj "+yj);
				if (xj>=0 && xj<width && yj>=0 && yj<height && binArrayShoe1[xj][yj]==0 ) {//check inside the circle portion TODO: Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 500
					iarrErrors.add(new Integer(k));
					iPixelError = k;
					break;
				}
				if (k < iarrPixelsOnTheLine_OutsideCircle.size()) {//check outside the circle portion
					int xk = iarrPixelsOnTheLine_OutsideCircle.get(k)[0];
					int yk = iarrPixelsOnTheLine_OutsideCircle.get(k)[1];
					
					Core.circle(overlayImage, new Point(xk, yk), 1,new Scalar(0,255,255), 1);
					
					if (binArrayShoe1[xk][yk]==0) {
						iarrErrors.add(new Integer(k));
						iPixelError = k;
						break;
					}
				}
			}
			
			if (iPixelError == -1) 
				iBadPixels.add(new Integer(iPixelError));
			else {
				dLineThreshold = (double) 2.0*((double)iPixelError)/ diagonal;
				//System.out.println("dLineThreshold: " + dLineThreshold);
				if (dLineThreshold > D_LINE_THRESHOLD_VALUE)//0.2
					iBadPixels.add(new Integer(iPixelError));
			}
		}
		
		
		dBadPixelThreshold = (double)iBadPixels.size() / circumference;
		//System.out.println("Circumference "+  circumference);
		System.out.println("iBadPixels.size() = "+iBadPixels.size()+"   dBadPixelThreshold = "+dBadPixelThreshold);
		
		
		Core.line(overlayImage, pts[0], pts[2], new Scalar(255,0,255));
		
		Core.putText(overlayImage, shoe1.fileID+" vs "+shoe2.fileID, new Point(10,30), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,0,255));
		
		//ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(overlayImage),"Two contours overlayed ");
		
		
		if (dBadPixelThreshold <= D_BAD_PIXEL_THRESHOLD_VALUE) { //0.01
			
//			try {
//				graphics.dispose();
//				ImageIO.write(squaredCircleToCheck, "jpg", new File(".\\output\\blobs\\" + iImageNo + ".jpg"));
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			Core.putText(overlayImage,"TRUE ", new Point(10,480), Core.FONT_ITALIC,new Double(0.7), new Scalar(0,0,255));
			ImageIO.write(ProcessImages.Mat2BufferedImage(overlayImage), "JPG", new File("./output/ShoesCompared/"+shoe1.fileID+"_vs_"+shoe2.fileID+".jpg"));
			return true;
			
		}
		Core.putText(overlayImage,"FALSE ", new Point(10,480), Core.FONT_ITALIC,new Double(0.7), new Scalar(0,0,255));
		ImageIO.write(ProcessImages.Mat2BufferedImage(overlayImage), "JPG", new File("./output/ShoesCompared/"+shoe1.fileID+"_vs_"+shoe2.fileID+".jpg"));
		return false;
		
	}//END OF contoursMatched
	
	
	
	
	
	/**
	 * 
	 * @param image1
	 * @param image2
	 * @return
	 * @throws IOException
	 */
	public static double contoursMatchedDistanceValues(Mat image1, Mat image2) throws IOException{//(Mat image1, Mat image2){
		
		
		MatOfPoint tempContourShoe1 =  ExtractShoeContours.extractLargestContour(image1);
		MatOfPoint tempContourShoe2 =  ExtractShoeContours.extractLargestContour(image2);
		
		Point[] outlinePointsShoe1 = tempContourShoe1.toArray();
		Point[] outlinePointsShoe2 = tempContourShoe2.toArray();
//		System.out.println(outlinePointsShoe1.length);
//		System.out.println(outlinePointsShoe2.length);
		//Draw the outline on the white background
		Mat outlinePointsOnlyShoe1 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat imageShoe1 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat outlinePointsOnlyShoe2 = new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		
		for(int i=0;i<outlinePointsShoe1.length-1; i++){
			Point start=outlinePointsShoe1[i];
			Point end= outlinePointsShoe1[i+1];
			Core.line(outlinePointsOnlyShoe1, start, end, new Scalar(0,255,0), 1);
		}
		
		imageShoe1=outlinePointsOnlyShoe1.clone();
		
		for(int i=0;i<outlinePointsShoe2.length-1; i++){
			Point start=outlinePointsShoe2[i];
			Point end= outlinePointsShoe2[i+1];
			Core.line(outlinePointsOnlyShoe2, start, end, new Scalar(0,255,0), 1);
		}
		
		
		
		Point center1=findGravityCenter(tempContourShoe1);
		Point center2=findGravityCenter(tempContourShoe2);
		
		Core.circle(outlinePointsOnlyShoe1, center1, 3, new Scalar(0,0,255));
		Core.circle(outlinePointsOnlyShoe2, center2, 3, new Scalar(0,0,255));
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe1),"Outline of tempContourShoe1 ");
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe2),"Outline of tempContourShoe2 ");
		
		
		
		
		//ImageIO.write(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe1), "JPG", new File("./output/result/outlinePointsOnlyShoe1.jpg"));
		//ImageIO.write(ProcessImages.Mat2BufferedImage(outlinePointsOnlyShoe2), "JPG", new File("./output/result/outlinePointsOnlyShoe2.jpg"));
		
		
		Mat overlayImage= new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		Mat shiftedImageShoe2= new Mat(500,300, CvType.CV_8U, new Scalar(255,255,255));
		
		Imgproc.cvtColor(overlayImage, overlayImage, Imgproc.COLOR_GRAY2BGR);
		for(int i=0;i<outlinePointsShoe1.length-1; i++){
			Point start=outlinePointsShoe1[i];
			Point end= outlinePointsShoe1[i+1];
			Core.line(overlayImage, start, end, new Scalar(255,0,0), 1);
		}
		
		//shift contour so their centers of gravity coincide
		Point[] outlinePointsShoe2Shifted = tempContourShoe2.toArray();
		int diffX= (int) (center1.x-center2.x);
		int diffY=(int) (center1.y-center2.y);
		for(int i=0;i<outlinePointsShoe2.length;i++){
			outlinePointsShoe2Shifted[i].x=outlinePointsShoe2[i].x+diffX;
			outlinePointsShoe2Shifted[i].y=outlinePointsShoe2[i].y+diffY;
		}
		
		//to get the diagonal
		RotatedRect minAreaRectShoe2=org.opencv.imgproc.Imgproc.minAreaRect(new MatOfPoint2f( outlinePointsShoe2Shifted ));
		Point[] pts=new Point[4];
				
		minAreaRectShoe2.points(pts);
		
		double diagonal = Math.sqrt((pts[0].x-pts[2].x)*(pts[0].x-pts[2].x)+(pts[0].y-pts[2].y)*(pts[0].y-pts[2].y));
		
		
		
		//draw the shifted image on the overlay background
		for(int i=0;i<outlinePointsShoe2Shifted.length-1; i++){
			Point start=outlinePointsShoe2Shifted[i];
			Point end= outlinePointsShoe2Shifted[i+1];
			Core.line(overlayImage, start, end, new Scalar(0,255,0), 1);
			Core.line(shiftedImageShoe2, start, end, new Scalar(0,0,0), 1);
		}
		
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imageShoe1),"imageShoe1 ");
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(shiftedImageShoe2),"shiftedImageShoe2");
		
		
		//add centers to the image
		Core.circle(overlayImage, center1, 2, new Scalar(255,255,0));
		Core.circle(overlayImage, new Point(center2.x+diffX, center2.y+diffY), 3, new Scalar(0,0,255));
//		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(overlayImage),"Two contours overlayed ");

		//now convert both contour images to binary arrays

		//convert it to binary data
		int[][] binArrayShoe1=ExtractShoeFeatures.extractBinaryDataFromImage(imageShoe1);
		//int[][] binArrayShoe2=ExtractShoeFeatures.extractBinaryDataFromImage(shiftedImageShoe2);
		//BufferedImage squaredCircleToCheck = ProcessImages.Mat2BufferedImage(shiftedImageShoe2);
	
		int circumference=(int) Imgproc.arcLength(new MatOfPoint2f( outlinePointsShoe2Shifted ), true);
		
		int width  = binArrayShoe1.length;// squaredCircleToCheck.getWidth();
		int height = binArrayShoe1[0].length;//squaredCircleToCheck.getHeight();
	
		
		
		ArrayList<Integer> iarrErrors = new ArrayList<Integer>();
		ArrayList<Integer> iBadPixels = new ArrayList<Integer>();
		Point pCenter = center1;//the center of the common contour (first shoe's gravity center)
		
		int iPixelError = -1;
		
		double dLineThreshold = 0.0;
		double dBadPixelThreshold = 0.0;
		
		
		
		//set thresholds:
		
	
		
		
		for (int i = 0; i < outlinePointsShoe2Shifted.length; i++) {
			int x1 = (int) outlinePointsShoe2Shifted[i].x;
			int y1 = (int) outlinePointsShoe2Shifted[i].y;
			ArrayList<int[]> iarrPixelsOnTheLine_InsideCircle  = getPixelsOnTheLineBetweenTwoPoints (new int[]{(int) pCenter.x, (int) pCenter.y}, new int[]{x1, y1});
			ArrayList<int[]> iarrPixelsOnTheLine_OutsideCircle = getNextPixelsOnTheLineOutsideContour(new int[]{(int) pCenter.x, (int) pCenter.y}, new int[]{x1, y1}, width, height);    
			
			
			
			
			for (int j = (iarrPixelsOnTheLine_InsideCircle.size() - 1), k = 0; j >= 0; j--, k++) {
				int xj = iarrPixelsOnTheLine_InsideCircle.get(j)[0];
				int yj = iarrPixelsOnTheLine_InsideCircle.get(j)[1];
				Core.circle(overlayImage, new Point(xj, yj), 1,new Scalar(255,0,0), 1);
				//System.out.println("xj "+xj +" yj "+yj);
				if (xj>=0 && xj<width && yj>=0 && yj<height && binArrayShoe1[xj][yj]==0 ) {//check inside the circle portion TODO: Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 500
					iarrErrors.add(new Integer(k));
					iPixelError = k;
					break;
				}
				if (k < iarrPixelsOnTheLine_OutsideCircle.size()) {//check outside the circle portion
					int xk = iarrPixelsOnTheLine_OutsideCircle.get(k)[0];
					int yk = iarrPixelsOnTheLine_OutsideCircle.get(k)[1];
					
					Core.circle(overlayImage, new Point(xk, yk), 1,new Scalar(0,255,255), 1);
					
					if (binArrayShoe1[xk][yk]==0) {
						iarrErrors.add(new Integer(k));
						iPixelError = k;
						break;
					}
				}
			}
			
			if (iPixelError == -1) 
				iBadPixels.add(new Integer(iPixelError));
			else {
				dLineThreshold = (double) 2.0*((double)iPixelError)/ diagonal;
				//System.out.println("dLineThreshold: " + dLineThreshold);
				if (dLineThreshold > D_LINE_THRESHOLD_VALUE)//0.2
					iBadPixels.add(new Integer(iPixelError));
			}
		}
		
		
		dBadPixelThreshold = (double)iBadPixels.size() / circumference;
		//System.out.println("Circumference "+  circumference);
		System.out.println("iBadPixels.size() = "+iBadPixels.size()+"   dBadPixelThreshold = "+dBadPixelThreshold);
		return dBadPixelThreshold;
		
		
		
	}//END OF contoursMatchedDistanceValues
	
	
	
	/**
	 * 
	 * @param contour
	 * @return
	 */
	public static Point findGravityCenter(MatOfPoint contour){
		//calculate moments, center of gravity and axis tilt
				Moments m = Imgproc.moments(contour, true);
				// center of gravity
				
				double m00 =m.get_m00();
				double m10 = m.get_m10();
				double m01 =m.get_m01();
				
				int xCenter =0;
				int yCenter=0;
				
				if (m00 != 0) {   // calculate center
					xCenter = (int) Math.round(m10/m00);
					yCenter = (int) Math.round(m01/m00);
					return new Point(xCenter,yCenter);
					}
				else 
					return null;		
	}
	
	/**
	 * transpose int[][] matrix of binary image data
	 * @param m
	 * @return
	 */
	public static int[][] transposeMatrix(int [][] m){
		int[][] temp = new int[m[0].length][m.length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[0].length; j++)
                temp[j][i] = m[i][j];
        return temp;
    }
	
	
	
	
	/**
	 * get the pixel locations between two pixels
	 * @param startpixelxy start point of the first pixel
	 * @param endpixelxy end point of the second pixel
	 * @return pixel array between two points
	 */
	private static ArrayList<int[]> getPixelsOnTheLineBetweenTwoPoints(int startpixelxy[], int endpixelxy[]) {
		//Bresenham Algorithm
		ArrayList<int[]> iarrPixelsInLine = new ArrayList<int[]>();
		int x  = startpixelxy[0],  y  = startpixelxy[1];
		int x2 = endpixelxy[0],    y2 = endpixelxy[1];
		
		int w = x2 - x;
	    int h = y2 - y;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
	    if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
	    if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
	    if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
	    int longest = Math.abs(w);
	    int shortest = Math.abs(h);
	    
	    if (!(longest > shortest)) {
	        longest = Math.abs(h);
	        shortest = Math.abs(w) ;
	        if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
	        dx2 = 0;            
	    }
	    
	    int numerator = longest >> 1;
		
	    for (int i = 0; i <= (longest); i++) {
	        iarrPixelsInLine.add(new int[]{x, y});
	        numerator += shortest;
	        if (!(numerator < longest)) {
	            numerator -= longest;
	            x += dx1;
	            y += dy1;
	        } else {
	            x += dx2;
	            y += dy2;
	        }
	    }
	    
		return iarrPixelsInLine;
	}
	
	
	
	/**
	 * this function predicts pixels beyond the line until it reaches the image boundaries
	 * @param startpixelxy start pixel or the center of the circle
	 * @param circlepixelxy pixel on the circle
	 * @param width width of the image 
	 * @param height height of the image 
	 * @return array list with the pixel x,y coordinates 
	 */
	private static ArrayList<int[]> getNextPixelsOnTheLineOutsideContour(int startpixelxy[], int circlepixelxy[], int width, int height) {
		int iExtraPixels = 40;
		//Bresenham Algorithm
		ArrayList<int[]> iarrPixelsOutLine = new ArrayList<int[]>();
		int x  = startpixelxy[0],  y  = startpixelxy[1];
		int x2 = circlepixelxy[0],    y2 = circlepixelxy[1];
		
		int w = x2 - x;
	    int h = y2 - y;
	    int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
	    if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
	    if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
	    if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
	    int longest = Math.abs(w);
	    int shortest = Math.abs(h);
	    
	    if (!(longest > shortest)) {
	        longest = Math.abs(h);
	        shortest = Math.abs(w) ;
	        if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
	        dx2 = 0;            
	    }
	    
	    int numerator = longest >> 1;
		
	    for (int i = 0; i <= (longest+iExtraPixels); i++) {
	    	if ( (x >= (width)) || (y >= (height)) ) break;
	    	if ( (x < 0) || (y < 0) ) break;
	    	if (i > longest)
	    		iarrPixelsOutLine.add(new int[]{x, y});
	        
	        numerator += shortest;
	        if (!(numerator < longest)) {
	            numerator -= longest;
	            x += dx1;
	            y += dy1;
	        } else {
	            x += dx2;
	            y += dy2;
	        }
	    }
	    
		return iarrPixelsOutLine;
		
	}
	
	

	
}//END OF CLASS

