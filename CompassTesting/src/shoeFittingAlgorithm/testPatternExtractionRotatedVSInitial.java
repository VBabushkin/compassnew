package shoeFittingAlgorithm;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class testPatternExtractionRotatedVSInitial {
	final static int LOWER_CONTOUR_AREA_THRESHOLD=10000;
	final static int UPPER_CONTOUR_AREA_THRESHOLD=150000;
	final static int SHOE_SIZE_AREA_DIFFERENCE=14000;
	final static int SHORTEST_LINE_LENGTH=13;
	final static double UPPER_RATIO_VALUE=2.8;
	final static double LOWER_RATIO_VALUE=2.1;
	final static int EDGE_LENGTH=20;
	final static int DELTA=40; //80 pixels square around the center of gravity
	
	static String initialImagePath=".//CollectedShoePictures//";//".//results//in.jpg";
	static String processedImagesPath=".//CollectedShoePictures//Processed//";
	static String resultImagesPath=".//CollectedShoePictures//SizeExtracted//";
	static String bckgImage =".//CollectedShoePictures//background.jpg";//".//results//bckg.jpg";
	
	
	
	
	public static void main(String args[]) throws IOException{
		
		int userID=22;//53;
		int angle=135;

		
//		for(int userID=1;userID<=1;userID++){
//			for(int angle=0;angle<=315;angle+=45){
		
		//String filename= "shoes_"+userID+"_"+angle;
		String filename= "shoes_22_half_4";
		String backgroundFileName="background"+userID;
		String initPath=initialImagePath+filename+".jpg";
		String bckgImagePath=initialImagePath+backgroundFileName+".jpg";
		
		
		
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		//read image
		Mat source = Highgui.imread(initPath, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//crop image
		Mat source0= ProcessImages.cropImageHorizontal(source,EDGE_LENGTH);
				
		//show initial image
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(source),"Initial Image");
		
	  //read the background image
	  	Mat bckg0 = Highgui.imread(bckgImagePath, Highgui.CV_LOAD_IMAGE_COLOR);
	  		
	  	//crop the background image
	  	Mat bckg=ProcessImages.cropImageHorizontal(bckg0,EDGE_LENGTH);
	  		
	  	//show the background image
	  	ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(bckg),"bckg");
	  		
	  	//to store image with removed background
	  	Mat backgroundRemoved = new Mat();
	  		
	  	//REMOVE BACKGROUND
	  	Core.subtract(source0.clone(), bckg, backgroundRemoved);
	  		
	  	//show image with background removed 
	  	ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved),"difference btw main image and background using subtract");

	  	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  	//process initial image and extract patterns without rotating it
	  	
	  	
	  	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		Mat finalImage=extractLinesCircles(backgroundRemoved);
		
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImage),"Lines and circles detected for shoe ");
		//save to file
		ImageIO.write(ProcessImages.Mat2BufferedImage(finalImage), "JPG", new File("./compareRotationResults/initialImgSupplied/"+filename+"_initial.jpg"));
		
		
	  	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//now rotate the image, extract shoes one-by-one and extract pattens
	  	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		List<shoesInfo2> shoesOutlineContours = new ArrayList<shoesInfo2>();
		
		//Split camera image by two
		ArrayList<Mat>correctCroppedImages=ExtractShoeContours.extractCorrectlyCroppedImages(initPath, bckgImagePath);
		if(correctCroppedImages.size()==0){
			System.out.println("NOTHING HAS BEEN DETECTED");
		}
		else{
			for(int index=0;index<2;index++){//ASSUMING ONLY 2 CONTOURS ARE EXTRACTED!!!
				//current image
				Mat tempImage= correctCroppedImages.get(index);
				Mat copyOfTempImage=tempImage.clone();
				
//				ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(tempImage),"Shoe number "+index);
				
				//extract shoe outline
				Mat processedImage= ExtractShoeContours.processImageForContoursDetection(tempImage);
				ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(processedImage),"Outline extracted "+index);
				
				
				//get a contour corresponding the extracted shoe outline
				MatOfPoint tempContour= ExtractShoeContours.extractLargestContour(processedImage);
				
				//find the area of this contour
				double area=Math.abs(Imgproc.contourArea(tempContour));
				System.out.println("THE AREA OF LARGEST CONTOUR FOR SHOE "+index+" DETECTED: "+ area);
				if(area<1000){
					System.out.println("NOTHING HAS BEEN DETECTED");
					continue;
				}
				else{
					//shoesInfo2 currentShoe=ExtractShoeFeatures.determineLeftOrRight(tempContour, processedImage);
					
					//String whichShoe=currentShoe.label;
					
					//Mat finalImgForProcessing = currentShoe.shoeOutline;
					
				   	ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(copyOfTempImage),"Shoe number "+index);
				   	
					Mat preFinalImageForProcessing=ExtractShoeFeatures.finalizeImgForPatternDetection(copyOfTempImage);

					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(preFinalImageForProcessing),"Rotated Image "+index);
					
					Mat preFinalImagePatternExtracted=extractLinesCircles(preFinalImageForProcessing);
					
					ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(preFinalImagePatternExtracted),"Lines and circles detected for rotated shoe "+index);
					
					//shoesOutlineContours.add(new shoesInfo2(whichShoe+filename, userID, whichShoe, finalImgForProcessing));
					ImageIO.write(ProcessImages.Mat2BufferedImage(preFinalImagePatternExtracted), "JPG", new File("./compareRotationResults/initialImgSupplied/"+filename+"_"+index+".jpg"));
					
				}
			}
		}//end of else case if number of extracted images is non zero
		
		
		
//			}//end of inner for iteration over all angles
//		}//end of outer for iteration over all user IDs
		
		
		
	}//end of main

	
	/**
	 * Detect lines and circles
	 * @param image
	 * @return
	 * @throws IOException
	 */
	static Mat extractLinesCircles(Mat image) throws IOException{
		//optimal parameters for Canny edge extractor
				//TODO:
				//find optimal parameters
				int lowThreshold=50;
				int highThreshold=50;
				int kernelSize=3;
				
				Mat destination0=image.clone();//imgToProcess1.clone();
				//extract edges
				Mat cannyFilteredSourceMat= AnalyzeImage.applyCannyEdgeDetectorOpenCV_Mat(destination0,lowThreshold, highThreshold, kernelSize);
				//display the image with extracted edges
				ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(cannyFilteredSourceMat),"Canny edge extracting algorith applied");
				
				//Extract circles
				Mat destination1=cannyFilteredSourceMat.clone();
				
				Mat destination2=cannyFilteredSourceMat.clone();

				Mat destination3=cannyFilteredSourceMat.clone();
				
				BufferedImage destinationBuffered = ProcessImages.Mat2BufferedImage(destination2);
				ArrayList<Rectangle>listOfDetectedCircleDimensions =new ArrayList<Rectangle>();
			    
				
				int currentBlobDetectionThresholdValue=15;
				int currentLineDetectionThresholdValue=20;
				int currentbadPixelThresholdValue=2;
				int heightThresholdValue=10;
				int widthThresholdValue =10;
				int differenceValue=10;
			    //detect circles
				listOfDetectedCircleDimensions=newDetectBlobs.detectBlobsAddCirclesToARG1(destinationBuffered,(float) (currentBlobDetectionThresholdValue/100.0),currentLineDetectionThresholdValue/100.0, currentbadPixelThresholdValue/100.0,heightThresholdValue,  widthThresholdValue,  differenceValue);//usualy 0.15f
				
//				//draw detected circles on the destination image
//				destination1=drawCirclesArrayList(destination1,listOfDetectedCircleDimensions);
//				
//				//display image with detected circles
				ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination1),"Circles detected");
//				

				//remove detected circles
				//select (0,0,0) for black, (255,255,255) for white
				destinationBuffered=AnalyzeImage.removeDetectedCircles(destinationBuffered,listOfDetectedCircleDimensions,new Scalar(0,0,0));
				
//				ProcessImages.displayImage(destinationBuffered,"Circles detected are removed from the image");
				
				
				//now detect lines on the image
				
				ArrayList<int[][]>listOfDetectedLinesCoordinates =new ArrayList<int[][]>();
				
				//specify the length of shortest detected line in pixels
			    int currentLengthThresholdValue = SHORTEST_LINE_LENGTH;
			    listOfDetectedLinesCoordinates=PixelBasedLineDetection_T.detectLinesAndAddToARG_v2(destinationBuffered,currentLengthThresholdValue);
			   
				//draw detected lines and circles
			    
			    destination3=AnalyzeImage.drawLinesCirclesArrayList(destination1,listOfDetectedLinesCoordinates,listOfDetectedCircleDimensions);
//			    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(destination3),"Lines and circles detected for shoe ");
				
			    
			    //number of detected circles
				int numOfDetectedCircles = listOfDetectedCircleDimensions.size();
				System.out.println(numOfDetectedCircles + " circles detected");
				
				//number of detected lines
				int numOfDetectedLines = AnalyzeImage.removeLinesOfZeroLength(listOfDetectedLinesCoordinates).size();
				System.out.println(numOfDetectedLines + " lines detected");
				
				
				
				//for output only
				Core.putText(destination3, "CIRCLES "+numOfDetectedCircles, new Point(10,30), Core.FONT_ITALIC,new Double(0.6), new Scalar(0,0,255));
				Core.putText(destination3, "LINES "+numOfDetectedLines, new Point(10,50), Core.FONT_ITALIC,new Double(0.6), new Scalar(0,0,255));
//			   	ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(finalImgForProcessing), "Determined");
//				ImageIO.write(ProcessImages.Mat2BufferedImage(finalImgForProcessing), "JPG", new File("./output/result/"+whichShoe+"/"+filename+"_"+index+" "+whichShoe+".jpg"));
//				ImageIO.write(ProcessImages.Mat2BufferedImage(finalImgForProcessing), "JPG", new File("./output/result/"+filename+"_"+index+" "+whichShoe+".jpg"));
				
				
				
				return destination3;

	}
	
	
	
}
