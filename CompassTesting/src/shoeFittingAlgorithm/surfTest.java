package shoeFittingAlgorithm;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.bytedeco.javacpp.opencv_core.Point2f;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.Scalar;
import 	org.opencv.features2d.*;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

public class surfTest {
	
	static String initialImagePath=".//CollectedShoePictures//";
	
	public static void main(String args[]){
		int userID1=8;
		int angle1=90;
		
		int userID2=8;
		int angle2=135;
		
		String filename1= "shoes_"+userID1+"_"+angle1;
		String backgroundFileName1="background"+userID1;
		String initPath1=initialImagePath+filename1+".jpg";
		String bckgImagePath1=initialImagePath+backgroundFileName1+".jpg";
		
		String filename2= "shoes_"+userID2+"_"+angle2;
		String backgroundFileName2="background"+userID2;
		String initPath2=initialImagePath+filename2+".jpg";
		String bckgImagePath2=initialImagePath+backgroundFileName2+".jpg";
		
		//load library
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME );
		//read image
		Mat source1 = Highgui.imread(initPath1, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//read the background image
		Mat bckg1 = Highgui.imread(bckgImagePath1, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//to store image with removed background
		Mat backgroundRemoved1 = new Mat();
				
		//REMOVE BACKGROUND
		org.opencv.core.Core.subtract(source1.clone(), bckg1, backgroundRemoved1);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved1),"Image 1 without  background");
		
		
		Highgui.imwrite("./CollectedShoePictures/withoutBckg_8_90.jpg", backgroundRemoved1);
		
		//read image
		Mat source2 = Highgui.imread(initPath2, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//read the background image
		Mat bckg2 = Highgui.imread(bckgImagePath2, Highgui.CV_LOAD_IMAGE_COLOR);
		
		//to store image with removed background
		Mat backgroundRemoved2 = new Mat();
				
		//REMOVE BACKGROUND
		org.opencv.core.Core.subtract(source2.clone(), bckg2, backgroundRemoved2);
		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(backgroundRemoved2),"Image 2 without  background");
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//PLAYING WITH FEATURE DESCRIPTORS
		
		
		FeatureDetector detector = FeatureDetector.create(FeatureDetector.SURF);
        DescriptorExtractor extractor = DescriptorExtractor.create(DescriptorExtractor.SURF);
        DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE);
        
        //Mat imgTrain = backgroundRemoved1.clone();
        Mat imgTrain = Highgui.imread("./CollectedShoePictures/left_8_90.jpg", Highgui.CV_LOAD_IMAGE_COLOR);
        Mat imgQuery =backgroundRemoved2.clone();//imgTrain.submat(new Range(0, imgTrain.rows() - 100), Range.all());//

        MatOfKeyPoint trainKeypoints = new MatOfKeyPoint();
        MatOfKeyPoint queryKeypoints = new MatOfKeyPoint();

        detector.detect(imgTrain, trainKeypoints);
        detector.detect(imgQuery, queryKeypoints);
        
        Mat trainDescriptors = new Mat();
        Mat queryDescriptors = new Mat();

        extractor.compute(imgTrain, trainKeypoints, trainDescriptors);
        extractor.compute(imgQuery, queryKeypoints, queryDescriptors);

        MatOfDMatch matches = new MatOfDMatch();

        matcher.add(Arrays.asList(trainDescriptors));
        matcher.match(queryDescriptors, matches);

        DMatch adm[] = matches.toArray();
        
        DMatch[] matchesArr = matches.toArray();//same as adm[]
        double max_dist = 0;
        double min_dist = 100;

        // -- Quick calculation of max and min distances between keypoints
        for (int i = 0; i < matchesArr.length; i++) {
            double dist = matchesArr[i].distance;
            if (dist < min_dist)
                min_dist = dist;
            if (dist > max_dist)
                max_dist = dist;
        }

        System.out.printf("-- Max dist : %f \n", max_dist);
        System.out.printf("-- Min dist : %f \n", min_dist);
        
        
     // -- Draw only "good" matches (i.e. whose distance is less than
        // 2*min_dist,
        // -- or a small arbitary value ( 0.02 ) in the event that min_dist is
        // very
        // -- small)
        // -- PS.- radiusMatch can also be used here.
        MatOfDMatch good_matches = new MatOfDMatch();

        for (int i = 0; i < matchesArr.length; i++) {
            if (matchesArr[i].distance <= Math.max(2 * min_dist, 0.01)) {
                good_matches.push_back(matches.row(i));
            }
        }

        System.out.println("NUMBER OF GOOD MATCHES FOUND "+good_matches.size().height);
        
        // -- Draw only "good" matches
        Mat img_matches = new Mat();
        Features2d.drawMatches(imgQuery, queryKeypoints,  imgTrain, trainKeypoints, good_matches, img_matches);
        //, Scalar.all(-1), Scalar.all(-1),
        //null, Features2d.NOT_DRAW_SINGLE_POINTS);

        
        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(img_matches),"GOOD MATCHES ONLY");
        
        
        //end of drawing good matches only
        
        
        List<Point> lp1 = new ArrayList<Point>(adm.length);
        List<Point> lp2 = new ArrayList<Point>(adm.length);
        KeyPoint tkp[] = trainKeypoints.toArray();
        KeyPoint qkp[] = queryKeypoints.toArray();
        for (int i = 0; i < adm.length; i++) {
            DMatch dm = adm[i];
            lp1.add(tkp[dm.trainIdx].pt);
            lp2.add(qkp[dm.queryIdx].pt);
        }

        MatOfPoint2f points1 = new MatOfPoint2f(lp1.toArray(new Point[0]));
        MatOfPoint2f points2 = new MatOfPoint2f(lp2.toArray(new Point[0]));

        Mat hmg = Calib3d.findHomography(points1, points2, Calib3d.RANSAC, 3);

        
        //assertMatEqual(Mat.eye(3, 3, CvType.CV_64F), hmg);

        Mat outimg = new Mat();
        Features2d.drawMatches(imgQuery, queryKeypoints, imgTrain, trainKeypoints, matches, outimg);
        
        //System.out.println("NUMBER OF MATCHED POINTS FOUND "+adm.length);
        
        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outimg),"OUTPUT");
        
      //-- Get the corners from the image_1 ( the object to be "detected" )
        List<Point> obj_corners = new ArrayList<Point>();
        obj_corners.add(new Point(0,0)); 
        obj_corners.add(new Point(imgTrain.cols(), 0));
        obj_corners.add(new Point(imgTrain.cols(), imgTrain.rows()));
        obj_corners.add(new Point(0, imgTrain.rows()));
        
        List<Point> scene_corners = new ArrayList<Point>();
       
        Mat startM = Converters.vector_Point2f_to_Mat(obj_corners);
        Mat endM = Converters.vector_Point2f_to_Mat(scene_corners);
        
        Core.perspectiveTransform( startM, endM, hmg);
        
        
        List<Point> new_scene_corners = new ArrayList<Point>();
        Converters.Mat_to_vector_Point(endM, new_scene_corners);
        
        System.out.println(new_scene_corners.size()+" "+new_scene_corners.get(0).x+"     "+new_scene_corners.get(0).y);
      
        
       //-- Draw lines between the corners (the mapped object in the scene - image_2 )
       //TODO: Correctly specify the matched region
       Core.line(img_matches, new Point(new_scene_corners.get(0).x + imgTrain.cols(),new_scene_corners.get(0).y), new Point(new_scene_corners.get(1).x + imgTrain.cols(),new_scene_corners.get(1).y), new  Scalar(0, 255, 0), 4 );
       Core.line(img_matches, new Point(new_scene_corners.get(1).x + imgTrain.cols(),new_scene_corners.get(1).y), new Point(new_scene_corners.get(2).x + imgTrain.cols(),new_scene_corners.get(2).y), new  Scalar(0, 255, 0), 4 );
       Core.line(img_matches, new Point(new_scene_corners.get(2).x + imgTrain.cols(),new_scene_corners.get(2).y), new Point(new_scene_corners.get(3).x + imgTrain.cols(),new_scene_corners.get(3).y), new  Scalar(0, 255, 0), 4 );
       Core.line(img_matches, new Point(new_scene_corners.get(0).x + imgTrain.cols(),new_scene_corners.get(0).y), new Point(new_scene_corners.get(3).x + imgTrain.cols(),new_scene_corners.get(3).y), new  Scalar(0, 255, 0), 4 );
//        //-- Draw lines between the corners (the mapped object in the scene - image_2 )
//        line( img_matches, scene_corners[0] + Point2f( img_object.cols, 0), scene_corners[1] + Point2f( img_object.cols, 0), Scalar(0, 255, 0), 4 );
//        line( img_matches, scene_corners[1] + Point2f( img_object.cols, 0), scene_corners[2] + Point2f( img_object.cols, 0), Scalar( 0, 255, 0), 4 );
//        line( img_matches, scene_corners[2] + Point2f( img_object.cols, 0), scene_corners[3] + Point2f( img_object.cols, 0), Scalar( 0, 255, 0), 4 );
//        line( img_matches, scene_corners[3] + Point2f( img_object.cols, 0), scene_corners[0] + Point2f( img_object.cols, 0), Scalar( 0, 255, 0), 4 );

       ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(img_matches),"GOOD MATCHES ONLY");       
//        String outputPath="./results/homography.jpg";
//        Highgui.imwrite(outputPath, hmg);
       
	}
	
	
}
