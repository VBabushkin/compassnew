package shoeFittingAlgorithm;

import java.util.ArrayList;

/**
 * Compare relational graphs of shoes bin by bin to be a given accuracy and set matching percentage
 * @author nimesha
 *
 */
public class CompareRelationalGraphs_T implements Runnable {

	private ArrayList<Thread> arrCompareRows_T = null;
	
	private AttributesMatrix[][] newMatrix = null;
	private AttributesMatrix[][] existingMatrix = null;
	private int iShoeID = 0;
	
	private double dMatchingPercentage = 0.0;
	
	
	
	public CompareRelationalGraphs_T(int id) {
		this.iShoeID = id;
		
	}

	@Override
	public void run() {
		try{
			compareRelationalGraphsOfShoeSoles();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * compare node attributed bin by bin
	 * compare different shapes individually (L2L, C2C, L2C, C2L)
	 * @throws InterruptedException 
	 */
	private void compareRelationalGraphsOfShoeSoles() throws InterruptedException {
		
		int iNoOfRowsPerThread = 3;
		ArrayList <Integer> iarrMatchedRows    = new ArrayList<Integer>();
		
		arrCompareRows_T = new ArrayList<Thread>();
		
		CompareRows_T compareRows_T = null;
		Thread tCompareRows_T = null;
		
		for(int i = 0; i < newMatrix.length; i=(i+iNoOfRowsPerThread)) {
			int start = i;
			int end   = i + (iNoOfRowsPerThread - 1);
			if (end > (newMatrix.length-1) ) end = newMatrix.length-1;
			compareRows_T  = new CompareRows_T(start, end, newMatrix, existingMatrix, iarrMatchedRows);
			tCompareRows_T = new Thread(compareRows_T);
			tCompareRows_T.start();
			arrCompareRows_T.add(tCompareRows_T);
		}
		
		//System.out.println("$$$$$$$$$$$##########: no of threads" + arrCompareRows_T.size() + " arrCompareRows_T.size(): " + arrCompareRows_T.size());
		
		for (int i = 0; i < arrCompareRows_T.size(); i++) {
			Thread tCompareSingleRow_T = arrCompareRows_T.get(i);
			tCompareSingleRow_T.join();
		}
		
		dMatchingPercentage = (double) ( (iarrMatchedRows.size() * 100.0) / ( (double) (newMatrix.length)) );
		//System.out.println("$$$$$$$$$$$########## dMatchingPercentage: " + dMatchingPercentage);
	}

	///////getters and setters of matrices /////////////
	public AttributesMatrix[][] getNewMatrix() {
		return newMatrix;
	}

	public void setNewMatrix(AttributesMatrix[][] newMatrix) {
		this.newMatrix = newMatrix;
	}

	public AttributesMatrix[][] getExistingMatrix() {
		return existingMatrix;
	}

	public void setExistingMatrix(AttributesMatrix[][] existingMatrix) {
		this.existingMatrix = existingMatrix;
	}

	public double getMatchingPercentage() {
		return dMatchingPercentage;
	}

	public void setMatchingPercentage(double dMatchingPercentage) {
		this.dMatchingPercentage = dMatchingPercentage;
	}

}
