
Folder structure:

libs:
contains libraries for blob detection, gabor (Haralick) descriptor, and gabor matlab code exported as exe

marvin:
Marvin library used for different filters and image processing cannot be moved to libs
since we need to preserve the folder structure.

oldOutput:
old images - ignore

output:
output images of the algorithm
please see seperate readme file inside expplaining images


Code:

 * different classes and their functions as follows,
 * 
 * Compass:         main entry point of the app, running a loop to capture and process images continuously 
 * ImageOperations: contains basic image operations such as crop and scale
 * ImageAnalysis:   main class for image background removing functions and filter application 
 * GaborTests:      contains two test functions for gabor library tests
 * ApplyImageFilters: to apply filters and save different outputs in output folder
 * BlobDetector:    detect blobs and edges and draw them in the output
 * 
 * Arduino: calls arduino exe and send data
 * Camera:  calls camera exe and grab camera frames
 * ExecuteMatlabFunction: execute the matlab code by dr yi-exported as exe







Galaxy Tab GMail account:

gmail account:
username: compass.nyuad@gmail.com
password: compass123


