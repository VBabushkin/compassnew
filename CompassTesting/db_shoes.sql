CREATE TABLE shoes(
   shoeid INT NOT NULL AUTO_INCREMENT,
   cannyshoe LONGBLOB NOT NULL,
   shoesize_left varchar(10) NOT NULL,
   shoesize_right varchar(10) NOT NULL,
   nlines_left varchar(10) NOT NULL,
   ncircles_left varchar(10) NOT NULL,
   nlines_right varchar(10) NOT NULL,
   ncircles_right varchar(10) NOT NULL,
   ARG_Matrix_Left LONGBLOB NOT NULL,
   ARG_Matrix_Right LONGBLOB NOT NULL,
   direction varchar(50) NOT NULL,
   black_plain INT NOT NULL,
   PRIMARY KEY ( shoeid )
);


CREATE TABLE shoes(
   shoeid INT NOT NULL AUTO_INCREMENT,
   originalcapture LONGBLOB NOT NULL,
   cannyshoe LONGBLOB NOT NULL,
   shoesize_W_left INT NOT NULL,
   shoesize_H_left INT NOT NULL,
   shoesize_W_right INT NOT NULL,
   shoesize_H_right INT NOT NULL,
   nlines_left INT NOT NULL,
   ncircles_left INT NOT NULL,
   nlines_right INT NOT NULL,
   ncircles_right INT NOT NULL,
   ARG_Matrix_Left LONGBLOB NOT NULL,
   ARG_Matrix_Right LONGBLOB NOT NULL,
   direction varchar(50) NOT NULL,
   black_plain INT NOT NULL,
   PRIMARY KEY ( shoeid )
);