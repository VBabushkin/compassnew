%% Detect and Measure Circular Objects in an Image
% This example shows how to use |imfindcircles| to automatically detect
% circles or circular objects in an image. It also shows the use of
% |viscircles| to visualize the detected circles.

% Copyright 2012-2013 The MathWorks, Inc.

%f = 'canny_left_now1.png';
rgb = imread(f);
[centers, radius] = imfindcircles(rgb,[3 75],'ObjectPolarity','dark')
%figure
%imshow(rgb)
%h = viscircles(centers,radius);















