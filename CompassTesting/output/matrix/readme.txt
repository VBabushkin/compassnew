File naming conventions:

R.jpg = Register image
R-L.txt = data for left shoe (Registration image)
R-R.txt = data for right shoe (Registration image)

T.jpg = Test image
T-L.txt = data for left shoe (Test image)
T-R.txt = data for right shoe (Test image)