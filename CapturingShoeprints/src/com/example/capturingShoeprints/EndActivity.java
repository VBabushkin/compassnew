package com.example.capturingShoeprints;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class EndActivity extends MyBaseActivity{
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_end);
		int secondsDelayed = 10;
        new Handler().postDelayed(new Runnable() {
                public void run() {
                	Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
        }, secondsDelayed * 1000);
	}//end onCreate
	
		// Initiating Menu XML file (menu.xml)
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu)
	    {
	        MenuInflater menuInflater = getMenuInflater();
	        menuInflater.inflate(R.layout.menu, menu);
	        return true;
	    }
		
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        switch(item.getItemId()) {
	        case R.id.menu_ip:
	            Intent intent = new Intent(this, MenuActivity.class);
	            this.startActivity(intent);
	            break;
	        default:
	            return super.onOptionsItemSelected(item);
	        }

	        return true;
	    }
	    
	    @SuppressLint("NewApi") @Override
	    public void onBackPressed() {
	            super.onBackPressed();
	            this.finish();
	    }
	    
//	    public void onPause()
//	    {
//	       super.onPause();
//	       this.finish();
//	    }
	    public void onDestroy()
	    {
	       super.onDestroy();
	       this.finish();
	    }
}
