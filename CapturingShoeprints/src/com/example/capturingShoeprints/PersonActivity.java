package com.example.capturingShoeprints;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.StringTokenizer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class PersonActivity extends MyBaseActivity {
	private Handler handler;
	private Runnable runnable;
    private Button button_next;
    private Button button_back;
    private EditText personFirstName;
    //private EditText personSecondName;
    String CurrentString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_person);
        
      //set timeout for 1 minute -- if this activity is idle for one minute -- return to main activity
      
        
        
        button_next = (Button) findViewById(R.id.button_next);
        button_back = (Button) findViewById(R.id.button_back);
        personFirstName=(EditText) findViewById(R.id.personFirstName);
        //personSecondName=(EditText) findViewById(R.id.personSecondName);
        
        button_next.setEnabled(false);
        button_next.setClickable(false);
     // set listeners
        personFirstName.addTextChangedListener(mTextWatcher);
        //personSecondName.addTextChangedListener(mTextWatcher);

        // run once to disable if empty
        checkFieldsForEmptyValues(); 
        
       
        
        
        /**
         * 
         */
        button_next.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                try {
                	Date dt = new Date();
                    long timestamp = dt.getTime(); 
                    
                    String CurrentString=personFirstName.getText().toString();
//                  
                    String[] separated = CurrentString.split("\\s+");
                    
                    String combinedName="";
                    
                    for(String word: separated){
                    	combinedName+=word;
                    }

                    combinedName=combinedName+timestamp;//personSecondName.getText().toString()+timestamp;
                   
                    
                	//Toast.makeText(PersonActivity.this, combinedName,3000).show();
                	
                	Bundle ePzl= new Bundle();
                	ePzl.putString("key", combinedName);
                	
                	Intent i = new Intent(PersonActivity.this,SignActivity.class);
                	i.putExtras(ePzl);
                	startActivity(i);
                	finish();

                	
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(PersonActivity.this, "Nothing to next",3000).show();
                }
            }
        });
        
        
        /**
         * 
         */
        button_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                try {
                	
                	Intent i = new Intent(PersonActivity.this,MainActivity.class);
                	startActivity(i);
                	finish();

                	
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        
    }//end onCreate
    
    /**
     * to watch whether the name is written in text field then activate next button
     */
    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // check Fields For Empty Values
            checkFieldsForEmptyValues();
        }
    };

    /**
     * 
     */
    void checkFieldsForEmptyValues(){
        Button button_next = (Button) findViewById(R.id.button_next);

        String s1 = personFirstName.getText().toString();
        //String s2 = personSecondName.getText().toString();

        if(s1.equals("")){//|| s2.equals("")){
        	button_next.setEnabled(false);
        } else {
        	button_next.setEnabled(true);
        }
    }
    
    
    
    /**
     * 
     */
    public void onDestroy()
    {
       super.onDestroy();
       this.finish();
    }
   
}