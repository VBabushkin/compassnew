package com.example.capturingShoeprints;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


@TargetApi(Build.VERSION_CODES.DONUT) public class SignActivity extends MyBaseActivity {
	private Button button_save;
    private Button button_clear;
    private GestureOverlayView gesture;
    private EditText personName;
    private Handler handler;
	private Runnable runnable;

    @TargetApi(Build.VERSION_CODES.DONUT) @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_sign);
        

        gesture = (GestureOverlayView) findViewById(R.id.gestures);
        button_save = (Button) findViewById(R.id.save_button);
        button_clear = (Button) findViewById(R.id.clear_button);
       
        
        button_clear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                try {
                	gesture.cancelClearAnimation();
                	gesture.clear(true);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(SignActivity.this, "Nothing to clear",
                            3000).show();
                }
            }
        });
    
        
        
        button_save.setOnClickListener(new OnClickListener() {
            @TargetApi(Build.VERSION_CODES.FROYO) @Override
            public void onClick(View arg0) {
                try {
                	
            		Bitmap gestureImg = gesture.getGesture().toBitmap(300, 300, 8, Color.BLACK);
            		

            		ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    gestureImg.compress(Bitmap.CompressFormat.PNG, 100, bos);
                    
                    
                    byte[] bArray = bos.toByteArray();
                    
                    ByteArrayInputStream imageStreamClient = new ByteArrayInputStream(bArray);
                    Bitmap bmOriginal = BitmapFactory.decodeStream(imageStreamClient);
                    
                    
                    String fullPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() +"/Img/";

//                   
                    
                    
                    Bundle p = getIntent().getExtras();
                    String CurrentString =p.getString("key");
                    
                    
                                       
                    String filename =CurrentString+".png" ;
                    //Toast.makeText(SignActivity.this, filename,3000).show();
                    saveFile(bmOriginal,fullPath,filename);
                    
                    Intent intent = new Intent(getApplicationContext(), StartButtonActivity.class);
                    startActivity(intent); 
                    finish();
                

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(SignActivity.this, "Please sign with your fingertip ",3000).show();
                }
            }
        });
    }//end onCreate
    
    
    /**
     * 
     * @param context
     * @param b
     * @param picName
     */
    public  void saveFile(Bitmap b, String fullPath, String picName){ 
 	
    	try {
    			File dir = new File(fullPath);
    			if (!dir.exists()) {
    				dir.mkdirs();
    			}
    		} catch (Exception e) {
    			e.printStackTrace(); 
    		}

		OutputStream fOut = null;
		File file = new File(fullPath, picName);
		
		try {
			file.createNewFile();
		
			fOut = new FileOutputStream(file);
			// 100 means no compression, the lower you go, the stronger the compression
        	
			b.compress(Bitmap.CompressFormat.PNG, 100, fOut);
			
			fOut.flush();
			fOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
    }
    
    
   
    
    
    @SuppressLint("NewApi") @Override
    public void onBackPressed() {
            super.onBackPressed();
            this.finish();
    }
    
//    public void onPause()
//    {
//       super.onPause();
//       this.finish();
//    }
    public void onDestroy()
    {
       super.onDestroy();
       this.finish();
    }
    

}