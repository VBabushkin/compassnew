package com.example.capturingShoeprints;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class StartButtonActivity extends MyBaseActivity {
	static String ipAddress=MainActivity.ipAddress;//="10.224.44.28";//
	
	//EditText textOut;
	private Handler handler;
	private Runnable runnable;
	ImageButton  buttonSend;
	
	
	
	private final Handler mHandler = new Handler();
    /** Called when the activity is first created. */
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        
        buttonSend = (ImageButton)findViewById(R.id.send);
        TextView textMessage=(TextView)findViewById(R.id.textViewStartActivity);
        textMessage.setText("Push start and then walk across the platform");
        
        buttonSend.setEnabled(true);
		buttonSend.setClickable(true);
        buttonSend.setAdjustViewBounds(true);
        buttonSend.setMaxHeight(600);
        buttonSend.setMaxWidth(600);
        buttonSend.setOnClickListener(buttonSendOnClickListener);
       
//        int secondsDelayed = 35;
//        new Handler().postDelayed(new Runnable() {
//                public void run() {
//                	Intent intent = new Intent(getApplicationContext(), EndActivity.class);
//                    startActivity(intent);
//                    finish();
//                }
//        }, secondsDelayed * 1000);

        
        
    }
    
    
    
    
    ImageButton.OnClickListener buttonSendOnClickListener = new ImageButton.OnClickListener(){

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Socket socket = null;
			DataOutputStream dataOutputStream = null;
			DataInputStream dataInputStream = null;
			
			try {
				socket = new Socket(ipAddress.toString(), 8888);
				//Toast.makeText(StartButtonActivity.this, "Connected to 8888",3000).show();
				dataOutputStream = new DataOutputStream(socket.getOutputStream());
				dataInputStream = new DataInputStream(socket.getInputStream());
				dataOutputStream.writeUTF("1");//textOut.getText().toString());
				//textIn.setText(dataInputStream.readUTF());
				Thread.sleep(25000);
				Intent intent = new Intent(getApplicationContext(), EndActivity.class);
				startActivity(intent);
				finish();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{
				if (socket != null){
					try {
						socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if (dataOutputStream != null){
					try {
						dataOutputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if (dataInputStream != null){
					try {
						dataInputStream.close();
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			
		}};
		
    
    /////////////////////////////
    
 
		
	    
	    
//	    public void onPause()
//	    {
//	       super.onPause();
//	       this.finish();
//	    }
	    
	    /**
	     * 
	     */
	    public void connectToSocket(){
			// TODO Auto-generated method stub
			Socket socket = null;
			DataOutputStream dataOutputStream = null;
			DataInputStream dataInputStream = null;
			
			try {
				socket = new Socket(ipAddress.toString(), 8888);
				dataOutputStream = new DataOutputStream(socket.getOutputStream());
				dataInputStream = new DataInputStream(socket.getInputStream());
				dataOutputStream.writeUTF("1");

				
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			finally{
				if (socket != null){
					try {
						socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if (dataOutputStream != null){
					try {
						dataOutputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if (dataInputStream != null){
					try {
						dataInputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
	    }
	    
}
