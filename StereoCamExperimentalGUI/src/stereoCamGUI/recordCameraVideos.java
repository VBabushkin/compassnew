package stereoCamGUI;


import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class recordCameraVideos {

	private static String initFilesFolder="D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\output\\";

	public static void recordVideo(int currentNumber, int experimentalIndex) throws IOException, InterruptedException{
		
		String mainDirectory  ="./images/"+currentNumber;
		
		String resDirectory = mainDirectory+"/"+currentNumber+"_"+experimentalIndex+"/";
		
		
		System.out.println("Executing the exe... ");
		Process p = Runtime.getRuntime().exec("D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\Compass.exe", null, new File("D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\"));
		System.out.println("Waiting for exe file...");
		//Thread.sleep(40000);
		int processComplete = p.waitFor();
		System.out.println("Process complete "+processComplete);

		System.out.println("Done... ");
		
		//create main directory 
		
		File dir = new File(mainDirectory);
		if (!dir.exists()) {
			if (dir.mkdir()) {
				System.out.println(mainDirectory + "     Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
		
		
		//create sub directory with current experimental index 
		File file = new File(resDirectory);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println(resDirectory + "     Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
		
		//move to newly created dir
		
		File mainDir=new File(initFilesFolder);
		
		File[] allImageFiles=mainDir.listFiles();
		
		for (File  source : allImageFiles) {
	    	  
	    	  File dest = new File(resDirectory+source.getName());
	    	  try {
	    	      FileUtils.copyFile(source, dest);
	    	      } 
	    	  catch (IOException e) {
	    	    	  e.printStackTrace();
	    	      }
	    	  Thread.sleep(500);
	  		
	    	  
	      }
		System.out.println("Data is recorded...");
		
	}

}
