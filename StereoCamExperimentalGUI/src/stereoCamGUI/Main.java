package stereoCamGUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

public class Main extends JFrame{
	JPanel p = new JPanel(new GridLayout(6, 1));
	
	JPanel mainPane = new JPanel();
	GridLayout layout = new GridLayout(6, 1);
	JLabel l1 = new JLabel("Current Number");
	JTextField textField = new JTextField(30);
	JButton button1 = new JButton("First Round");
	JButton button2 = new JButton("Second Round");
	JButton button3 = new JButton("Third Round");
	JButton button4= new JButton("Fourth Round");
	TitledBorder title = BorderFactory.createTitledBorder("Experiment Control Panel");
	ImageIcon img = new ImageIcon("ic_launcher.png");
	//constructor
	public Main() {
		super("DTL Lab");
		setIconImage(img.getImage());
		textField.setFont(new java.awt.Font("Arial", Font.ITALIC | Font.BOLD, 25));
	
		
		p.setBorder(title);
		p.add(l1);
		p.add(textField);
		p.add(button1);
		p.add(button2);
		p.add(button3);
		p.add(button4);
		
		add(p);
		
		setSize(300, 400);		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);		
		setVisible(true);
		
		
		if(textField.getText().equals("")){
			button1.setEnabled(false);
			button2.setEnabled(false);
			button3.setEnabled(false);
			button4.setEnabled(false);
		}
		
		// adds key event listener
		textField.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent event) {
				String currentNumberStr = textField.getText();
				
				if (currentNumberStr.equals(null)) {
					button1.setEnabled(false);
				} else {
					button1.setEnabled(true);
				}
			}			
		});
		
		
		
		// adds action listener for the button
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				button1.setBackground(Color.red);
				int currentNumber = Integer.parseInt(textField.getText());
				System.out.println("currentNumber button 1   "+currentNumber);
				try {
					recordCameraVideos.recordVideo(currentNumber, 1);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				button2.setEnabled(true);
			}
		});
		
		
		// adds action listener for the button
		button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				button2.setBackground(Color.red);
				int currentNumber = Integer.parseInt(textField.getText());
				System.out.println("currentNumber button 2   "+currentNumber);
				try {
					recordCameraVideos.recordVideo(currentNumber, 2);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				button3.setEnabled(true);
			}
		});
		
		// adds action listener for the button
		button3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				button3.setBackground(Color.red);
				int currentNumber = Integer.parseInt(textField.getText());
				System.out.println("currentNumber button 3   "+currentNumber);
				try {
					recordCameraVideos.recordVideo(currentNumber, 3);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				button4.setEnabled(true);
			}
		});
		
		
		// adds action listener for the button
		button4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				button4.setBackground(Color.red);
				int currentNumber = Integer.parseInt(textField.getText());
				System.out.println("currentNumber button 4   "+currentNumber);
				try {
					recordCameraVideos.recordVideo(currentNumber, 4);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		
	}//end of constructor
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Main();
			}
		});
	}
	
	
	
	
	
}
