package openCVCamCapture;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class undistortStereoFrames {
	 static String initialImagePath=".//camPictures//leftRightImages130//";
	 static String dataPath =".//camPictures//calibData130//";
	 public static void main(String args[]){
		
		 System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		   int num=49130;
		   
		   String leftFile = ".//camPictures//left"+num+".jpg";
		   String rightFile = ".//camPictures//right"+num+".jpg";
		   
		   Mat imgleft=Highgui.imread(leftFile, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
		   Mat imgright=Highgui.imread(rightFile, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
		   
		   TaFileStorage storage = new TaFileStorage();
		   //storage.create(dataPath+"dataStorage.xml");
		   storage.open(dataPath+"dataStorage.xml");
		   Mat distCoeff1 = storage.readMat("distCoeff1");
		   Mat distCoeff2 = storage.readMat("distCoeff2");
		   Mat cameraMatrix1 = storage.readMat("cameraMatrix1");
		   Mat cameraMatrix2 = storage.readMat("cameraMatrix2");
		   
		   System.out.println(cameraMatrix2.toString());
		   
		   
		   Mat undistored1 = new Mat();
		   Imgproc.undistort(imgleft, undistored1, cameraMatrix1, distCoeff1);
//		   drawEpiLines(undistored1, lines1);
		   ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(undistored1),"undistored left");
		   
		   
		   Mat undistored2 = new Mat();
		   Imgproc.undistort(imgright, undistored2, cameraMatrix2, distCoeff2);
		   ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(undistored2),"undistored right");
		   //stereoRectify????
		   Highgui.imwrite(".//camPictures//undistLeft"+num+".jpg", undistored1);
		   Highgui.imwrite(".//camPictures//undistRight"+num+".jpg", undistored2);
//		    
		    System.out.println("Done");
	 }

	 
	
	
	
}
