package openCVCamCapture;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;


public class SegmentKinectFrames {
	static String currentFolder ="GRAY_2_15";
	static String initialImagePath=".//videos//"+currentFolder+"//";
	static String savePath=".//videoFrames//"+currentFolder+"//";
	final static int MIN_AREA=4000;
	final static int MAX_AREA=20000;
	
	public static void main(String args[]) throws IOException{
		int index = 162;//36
		
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		Mat currentDepthFrame=Highgui.imread(savePath+"frame_"+"outDepth_"+index+".jpg", Highgui.CV_LOAD_IMAGE_GRAYSCALE);
		
		Mat currentIRFrame=Highgui.imread(savePath+"frame_"+"outIR_"+index+".jpg", Highgui.CV_LOAD_IMAGE_COLOR);

		Mat currentColorFrame=Highgui.imread(savePath+"frame_"+"outVideo_"+index+".jpg", Highgui.CV_LOAD_IMAGE_COLOR);
		
		Mat currentRegisteredFrame=Highgui.imread(savePath+"frame_"+"outRegistred_"+index+".jpg", Highgui.CV_LOAD_IMAGE_COLOR);
	   	
		Mat source = currentIRFrame.clone();
		
		displayImage(Mat2BufferedImage(currentDepthFrame), "Depth Image");
		
		Mat segmentedDepthFrame = new Mat();
		//blur (no need)
		//Imgproc.blur(currentDepthFrame, currentDepthFrame, new Size(10,10));
		
		//without light
		org.opencv.core.Core.inRange(currentDepthFrame, new Scalar(52, 52, 52), new Scalar(55, 55, 55), segmentedDepthFrame);
		
		
		//with light

		//org.opencv.core.Core.inRange(currentDepthFrame, new Scalar(25, 25, 25), new Scalar(29, 29, 29), segmentedDepthFrame);
		
		
		displayImage(Mat2BufferedImage(segmentedDepthFrame), "Segmented depth Image");
		
		//smooth the extracted image
//		Mat smoothedSegmentedFrame = segmentedDepthFrame.clone();
		//eroding and dilating
//		
//		Imgproc.erode(smoothedSegmentedFrame, smoothedSegmentedFrame, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
//		Imgproc.dilate(smoothedSegmentedFrame, smoothedSegmentedFrame, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
//		 ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(smoothedSegmentedFrame), "Smoothed");
	        
		//extract contours
		//get a contour corresponding the extracted shoe outline
		MatOfPoint tempContour= extractLargestContour_v1(segmentedDepthFrame);
		
		//find the area of this contour
		double area=Math.abs(Imgproc.contourArea(tempContour));
		System.out.println("THE AREA OF LARGEST CONTOUR FOR SHOE "+index+" DETECTED: "+ area);
		
		//extract points from contour and draw them on image:
		
		Point[] outlinePoints = tempContour.toArray();
		
		//Draw the outline on the white background
		Mat outlinePointsOnly = new Mat(segmentedDepthFrame.rows(),segmentedDepthFrame.cols(), CvType.CV_8U, new Scalar(255,255,255));
		
		Imgproc.cvtColor(outlinePointsOnly, outlinePointsOnly, Imgproc.COLOR_GRAY2BGR);
		
		for(int i=0;i<outlinePoints.length-1; i++){
			Point start=outlinePoints[i];
			Point end= outlinePoints[i+1];
			Core.line(outlinePointsOnly, start, end, new Scalar(0,255,0), 1);
		}
		
		
		
        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnly), "Outline Contour Extracted for processing");
        
        MatOfPoint2f approx =  new MatOfPoint2f();
        MatOfPoint2f newMat = new MatOfPoint2f( tempContour.toArray() );
        RotatedRect r = Imgproc.minAreaRect(newMat);
        int contourSize = (int)tempContour.total();
        //approximate with polygon contourSize*0.05 -- 5% of curve length, curve is closed -- true
   	 	Imgproc.approxPolyDP(newMat, approx, contourSize*0.5, true);
   	 	Float[] tempArray=new Float[2];
	 	
	 	//DRAW THE BOUNDING RECTANGLE
	 	
	   
        
	   	//double area = Imgproc.contourArea(newMat);
	   		//Imgproc.drawContours(destination, contours, i, new Scalar(0, 0, 250),2);
	   		Point points[] = new Point[4];
	   		r.points(points);
	   		System.out.println();
	   		System.out.println("Size of the shoe is:\nWidth "+ r.size.width+"\nHeight "+r.size.height);
	   		
	   		if(r.size.height>r.size.width){
	   			
	   			tempArray[0]=(float) r.size.height;
	   			tempArray[1]=(float) r.size.width;
	   			
	   		}
	   		else{
	   		
	   			tempArray[0]=(float) r.size.width;
	   			tempArray[1]=(float) r.size.height;
	   			
	   		}
	   	
	   		String textW= "w = "+ String.format("%.2f",tempArray[0]);
	   		String textH=" h = "+String.format("%.2f",tempArray[1]);
	   		
	   		
	   		//draw the vertices of rectangle:
	   		for(int pt=0; pt<4; ++pt){
	   			//System.out.println(points[pt].toString());
	   			Core.line(outlinePointsOnly, points[pt], points[(pt+1)%4], new Scalar(255,255,0),2);
	   			Core.line(currentIRFrame, points[pt], points[(pt+1)%4], new Scalar(255,255,0),2);
	   			
	   			}
//	   		Core.putText(segmentedDepthFrame, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
//	   		Core.putText(segmentedDepthFrame, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
	   		
	   		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outlinePointsOnly), "Bounding rectangle");
	        
//	   		Core.putText(currentIRFrame, textW, points[2], Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
//	   		Core.putText(currentIRFrame, textH, new Point(points[2].x, points[2].y+15), Core.FONT_ITALIC,new Double(0.4), new Scalar(0,255,0));
//	   		ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(currentIRFrame), "Segmented shoe ");
	        
	   		
	   		
	   		//extract regions of the segmented contours
	   		//http://stackoverflow.com/questions/10176184/with-opencv-try-to-extract-a-region-of-a-picture-described-by-arrayofarrays
	   		//ArrayList<Mat> subregions = new ArrayList<Mat>();
	   		
	   		Mat subregion = new Mat();
	   	// Get bounding box for contour
	        Rect roi = Imgproc.boundingRect(tempContour); 
	   		
	     // Create a mask for each contour to mask out that region from image.
	        Mat mask = new Mat(source.rows(), source.cols(), CvType.CV_8UC1);
	        mask.setTo(new Scalar(0,0,0)) ;
	        List <MatOfPoint> largestDetectedContours = new ArrayList<MatOfPoint>();
	        largestDetectedContours.add(tempContour);
	        //place the white filled contour on the mask
	        Imgproc.drawContours(mask,  largestDetectedContours, -1, new Scalar(255,0,0),-1); 
	        
	        //erode and dilate the mask to remove sharp edges
	        Imgproc.dilate(mask, mask, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
	        Imgproc.erode(mask, mask, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3,3)));
			Imgproc.dilate(mask, mask, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(7,7)));
			Imgproc.blur(mask, mask, new Size(5,5));
	        
	        
	        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(mask), "Masked shoe ");
	        
	     // At this point, mask has value of 255 for pixels within the contour and value of 0 for those not in contour.

	        // Extract region using mask for region
	        Mat imageROI= new Mat();
	        source.copyTo(imageROI, mask); 
	        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imageROI), "Segmented shoe ");
	        
	        //mask registred
	        Mat imageROIRegistred= new Mat();
	        currentRegisteredFrame.copyTo(imageROIRegistred, mask); 
	        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imageROIRegistred), "Segmented Registred shoe ");
	        
	        Highgui.imwrite("./images/RGBMask0.jpg",mask);
	        
	        
	        
	        
	   		//correction to color image

	        int x=0;
	        int y=0;
	        
	        System.out.println(mask.rows());
	        System.out.println(mask.cols());
	        
	        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(mask), "Masked shoe 2");
	        
	        
	        
	        Mat T = new Mat(3, 3, CvType.CV_64F);
	        T.put(0, 0, 0.3584*0.94);//0.95
	        T.put(0, 1, 0.0089);
	        T.put(0, 2, 0.0000);
	        T.put(1, 0, 0.0031);
	        T.put(1, 1, 0.3531*1.01);
	        T.put(1, 2, 0.0001);
	        T.put(2, 0, -101.5934);
	        T.put(2, 1, 13.6311*3);
	        T.put(2, 2, 0.9914*10);
	        
	        
	        
	        Mat T_inverse = T.inv();
	        
	        System.out.println(T.dump());
	        System.out.println(T_inverse.dump());
	        
	        //Mat colorMask = new Mat(1080, 1920, CvType.CV_8UC1);
	        
	        //colorMask.setTo(new Scalar(0,0,0)) ;
	        Mat colorMask=transformMat(mask.clone(), T_inverse, 300,50,new Size(1920,1080));
	        
//	        
	         
	        Highgui.imwrite("./images/RGBMask.jpg",colorMask);
	        
	        double alpha=0.7;
	        double beta = 1.0 -alpha;
	        double gamma = 0.0;
	        Mat dst = new Mat();
	        //ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(currentColorFrame), "Mask for RGB data");
	        
	        Imgproc.cvtColor(colorMask, colorMask, Imgproc.COLOR_GRAY2BGR);
	        System.out.println(currentColorFrame.toString());
	        System.out.println(colorMask.toString());
	        
	        Core.addWeighted(currentColorFrame, alpha, colorMask, beta, gamma, dst);
	        
	        //resize
	        Mat resizeimage = new Mat();
	        Size sz = new Size(960,540);
	        Imgproc.resize( dst, resizeimage, sz );
	        
	        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(resizeimage), "Mask for RGB data");
	        Highgui.imwrite("./images/RGBMask1.jpg",dst);
	        
	        
	        
	        
	        //convert RGB to IR:
	        
	       
	        /*
	        R
	        [ 9.9984628826577793e-01, 1.2635359098409581e-03,
	        -1.7487233004436643e-02, -1.4779096108364480e-03,
	        9.9992385683542895e-01, -1.2251380107679535e-02,
	        1.7470421412464927e-02, 1.2275341476520762e-02,
	        9.9977202419716948e-01 ]
	        */
	        Mat R = new Mat(3, 3, CvType.CV_64F);
	        R.put(0, 0, 9.9984628826577793e-01);//0.95
	        R.put(0, 1, 1.2635359098409581e-03);
	        R.put(0, 2, -1.7487233004436643e-02);
	        R.put(1, 0, -1.4779096108364480e-03);
	        R.put(1, 1, 9.9992385683542895e-01);
	        R.put(1, 2, -1.2251380107679535e-02);
	        R.put(2, 0, 1.7470421412464927e-02);
	        R.put(2, 1, 1.2275341476520762e-02);
	        R.put(2, 2, 9.9977202419716948e-01);
	        
	        System.out.println(R.dump());
	        
	       
	        
	        Mat colorMask_1 = new Mat(1080, 1920, CvType.CV_8UC1);
	        
	        colorMask_1=transformMat(mask.clone(),R,0,0,new Size(1920,1080 ));
	        
	        Highgui.imwrite("./images/RGBMask2.jpg",colorMask_1);
	        
	        ///////////////////////////////////////////////////////////////////////////////////////////////////
	       
	        // RGB->IR
	        T = new Mat(3, 3, CvType.CV_64F);
	        T.put(0, 0, 0.3584);//0.95
	        T.put(0, 1, 0.0089);
	        T.put(0, 2, 0.0000);
	        T.put(1, 0, 0.0031);
	        T.put(1, 1, 0.3531);
	        T.put(1, 2, 0.0001);
	        T.put(2, 0, -101.5934);
	        T.put(2, 1, 13.6311);
	        T.put(2, 2, 0.9914);
	        
	        
	       // Mat irMask=transformMat(currentColorFrame.clone(), T, 0,0,new Size( 512,424));
	        Mat irMask = new Mat(new Size(512,424), CvType.CV_8UC1);
	        
	        //colorMask.setTo(new Scalar(0,0,0)) ;
	        Mat RGB_coord = new Mat(3, 1, CvType.CV_64F);
	        Mat IR_coord = new Mat(3, 1, CvType.CV_64F);
	        
	        
	        Imgproc.cvtColor(currentColorFrame, currentColorFrame, Imgproc.COLOR_BGR2GRAY);
	        
	        System.out.println(currentColorFrame.toString());
	        /*
	        //TODO:
	        //Perform backward transform (RGB->IR) with T matrix ()
	        //Take into account nonlinearity of the shifts (IR_width/RGB_width), (IR_height/RGB_height)  
	        
	        Mat resizedColorFrame = new Mat();
	        Size sz1 = new Size(512,424);
	        Imgproc.resize( currentColorFrame, resizedColorFrame, sz );
	        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(currentColorFrame), "resizedColorFrame");
	        
	        for(y=0;y<currentColorFrame.rows();y++){
	        	for(x=0;x<currentColorFrame.cols();x++){
	        		//double[] maskData = mask.get(y, x);
	        		//if(maskData[0]==255.0){
	        			RGB_coord.put(0, 0, x);//x
	        			RGB_coord.put(1, 0, y);//y
	        			RGB_coord.put(2, 0, 1.0);
	        	        Core.gemm(T,RGB_coord,1,new Mat(),0, IR_coord);
	        	        double X[]=IR_coord.get(0, 0);
	        	        double Y[]=IR_coord.get(1, 0);
	        	        //System.out.println("X = "+X[0]+", Y = "+Y[0]);
	        	        int ax=(int) X[0];
	        	        int ay=(int) Y[0];
	        	        irMask.put(ay,ax, currentColorFrame.get(x, y));
	        	       
	        		//}
	        	}
	        }
	        System.out.println(IR_coord.dump());
	         
	        Highgui.imwrite("./images/irMask.jpg",irMask);
	        
	        
	       */
	        
	        
	        
	}
	
	/**
	 * 
	 * @param mask
	 * @param T
	 * @param vShift
	 * @param hShift
	 * @return
	 */
	public static Mat transformMat(Mat mask, Mat T, int hShift, int vShift, Size size){
		Mat colorMask = new Mat(size, CvType.CV_8UC1);
        
        colorMask.setTo(new Scalar(0,0,0)) ;
		Mat RGB_coord = new Mat(3, 1, CvType.CV_64F);
        Mat IR_coord = new Mat(3, 1, CvType.CV_64F);
        
        
        //TODO:
        //Perform backward transform (RGB->IR) with T matrix ()
        //Take into account nonlinearity of the shifts (IR_width/RGB_width), (IR_height/RGB_height)  
        
        for(int y=0;y<mask.rows();y++){
        	for(int x=0;x<mask.cols();x++){
        		double[] maskData = mask.get(y, x);
        		if(maskData[0]==255.0){
        			IR_coord.put(0, 0, x);//x
        	        IR_coord.put(1, 0, y);//y
        	        IR_coord.put(2, 0, 1.0);
        	        Core.gemm(T, IR_coord,1,new Mat(),0, RGB_coord);
        	        double X[]=RGB_coord.get(0, 0);
        	        double Y[]=RGB_coord.get(1, 0);
        	        //System.out.println("X = "+X[0]+", Y = "+Y[0]);
        	        int ax=(int) X[0];
        	        int ay=(int) Y[0];
        	        if(ay-vShift-1>0 && ax+hShift+1<=colorMask.cols()){
        	        colorMask.put(ay-vShift, ax+hShift, 255.0);
        	        colorMask.put(ay-vShift-1, ax+hShift, 255.0);
        	        colorMask.put(ay-vShift+1, ax+hShift, 255.0);
        	        colorMask.put(ay-vShift, ax+hShift-1, 255.0);
        	        colorMask.put(ay-vShift, ax+hShift+1, 255.0);
        	        colorMask.put(ay-vShift-1, ax+hShift-1, 255.0);
        	        colorMask.put(ay-vShift-1, ax+hShift+1, 255.0);
        	        colorMask.put(ay-vShift+1, ax+hShift-1, 255.0);
        	        colorMask.put(ay-vShift+1, ax+hShift+1, 255.0);
        	        }
        		}
        	}
        }
        return colorMask;
	}
	
	/**
	 * 
	 * @param image
	 * @return
	 */
	public static MatOfPoint extractLargestContour_v1(Mat image){
		//to store the recognized contours
		 List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		//recognize contours
		 Imgproc.findContours(image, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
		 MatOfPoint largestContour = contours.get(0);
		 int largestAreaContourID=0;
			//TODO: ADD logic for selecting of proper contour
			for(MatOfPoint contour: contours){
				if(Math.abs(Imgproc.contourArea(contour))>MIN_AREA && Math.abs(Imgproc.contourArea(contour))<MAX_AREA && Math.abs(Imgproc.contourArea(contour))> Math.abs(Imgproc.contourArea(largestContour))){
					largestContour=contour;
				}
					
			}
			return largestContour;
	}//end of extractShoeContour
	
	
	/**
	 * 
	 * @param image
	 * @return
	 */
	public static MatOfPoint extractLargestContour(Mat image){
		//to store the recognized contours
		 List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		//recognize contours
		 Imgproc.findContours(image, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.RETR_LIST);
		  
		 int largestAreaContourID=0;
			//TODO: ADD logic for selecting of proper contour
			for(int cntID=0; cntID<contours.size();cntID++){
				if(Math.abs(Imgproc.contourArea(contours.get(cntID)))>MIN_AREA && Math.abs(Imgproc.contourArea(contours.get(cntID)))<MAX_AREA && Math.abs(Imgproc.contourArea(contours.get(cntID)))> Math.abs(Imgproc.contourArea(contours.get(largestAreaContourID)))){
					largestAreaContourID=cntID;
					break;
				}
					
			}
			
			MatOfPoint resultingContour= contours.get(largestAreaContourID);

			return resultingContour;
	}//end of extractShoeContour
	
	/**
	 * to display images overloaded function to take BufferedImage as argument
	 * modified from initial source http://answers.opencv.org/question/31505/how-load-and-display-images-with-java-using-opencv/
	 * @param img2 -- image to display
	 * @param str -- description of image will appear as title of image frame
	 */
	public static void displayImage(BufferedImage img2, String str)
	{   
		MarvinImagePanel	imagePanel;
		imagePanel = new MarvinImagePanel();
	    JFrame frame=new JFrame();      
	    frame.setSize(img2.getWidth()+20, img2.getHeight()+50);     
	    frame.add(imagePanel, BorderLayout.CENTER);
	    frame.setVisible(true);
	    frame.setTitle(str);
	    imagePanel.setImage(new MarvinImage(img2));
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * BufferedImage2Mat
	 * @param BufferedImage image
	 * @return Mat
	 */
	public static Mat BufferedImage2Mat(BufferedImage image){
		//source: http://stackoverflow.com/questions/18581633/fill-in-and-detect-contour-rectangles-in-java-opencv
        byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        Mat mat = new Mat(image.getHeight(),image.getWidth(), CvType.CV_8U);
        mat.put(0, 0, data);
        return mat;
    }
	
	/**
	 * Mat2BufferedImage
	 * @param Mat m
	 * @return BufferedImage
	 */
	
	public static BufferedImage Mat2BufferedImage(Mat m){
		// source: http://answers.opencv.org/question/10344/opencv-java-load-image-to-gui/
		// Fastest code
		// The output can be assigned either to a BufferedImage or to an Image

		    int type = BufferedImage.TYPE_BYTE_GRAY;
		    if ( m.channels() > 1 ) {
		        type = BufferedImage.TYPE_3BYTE_BGR;
		    }
		    int bufferSize = m.channels()*m.cols()*m.rows();
		    byte [] b = new byte[bufferSize];
		    m.get(0,0,b); // get all the pixels
		    BufferedImage image = new BufferedImage(m.cols(),m.rows(), type);
		    final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		    System.arraycopy(b, 0, targetPixels, 0, b.length);  
		    return image;

		}
}
