package openCVCamCapture;
import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;


public class processPicture {
	static String savePath= ".//camPictures";
	private static String savedImageFromCamera="C:\\Users\\Wild\\Desktop\\MyTests\\CompassCamera\\bin\\Release\\output\\now1.jpg";
	private static String exePath = "C:\\Users\\Wild\\Desktop\\MyTests\\CompassCamera\\bin\\Release\\Compass.exe";
	private static String exeFolder="C:\\Users\\Wild\\Desktop\\MyTests\\CompassCamera\\bin\\Release\\";
	private static StringBuffer sb;
	
	private static String exePath_1 ="C:\\Program Files (x86)\\OptiTrack\\Camera SDK\\bin\\visualtest.exe";
	private static String exeFolder_1="C:\\Program Files (x86)\\OptiTrack\\Camera SDK\\bin\\";
			
	public static void main(String[] args) throws IOException, InterruptedException {
		
		long startTime = System.currentTimeMillis();
		long currentTime=System.currentTimeMillis()-startTime;
		while(currentTime<75000){	//run for 75 seconds to match the video duration
			if(currentTime%1600==0){
				//first run the native Optitrack SDK to take shoes pictures
				//Runtime.getRuntime().exec(exePath_1, null, new File(exeFolder_1)).waitFor();
				Runtime.getRuntime().exec(exePath, null, new File(exeFolder));
				Thread.sleep(1000);
				System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
				String filename=savedImageFromCamera;
				Mat source = Highgui.imread(filename, Highgui.CV_LOAD_IMAGE_COLOR);
				BufferedImage sourceBuffer = Mat2BufferedImage(source.clone());
				//displayImage(sourceBuffer,"Captured Image");

				String outputName ="img_"+currentTime;
				//String outputName ="shoes_"+number+"_half_3";
				
				ImageIO.write(sourceBuffer, "JPG", new File(savePath+"\\"+ outputName +".jpg"));
				Thread.sleep(200);
				deletefile(savedImageFromCamera); 
			}
			currentTime=System.currentTimeMillis()-startTime;
		}
		long duration=System.currentTimeMillis()-startTime;  
		System.out.println("Time elapsed "+duration);

	}
	
	/**
	 * To delete now1.jpg picture taken by camera
	 * @param fileName
	 */
	private static void deletefile(String fileName) {
		File file = new File(fileName);
		boolean success = file.delete();
		if (!success) {
			System.out.println(fileName + " Deletion failed.");
		} else {
			System.out.println(fileName + " File deleted.");
		}
	}
	
	/**
	 * to display images overloaded function to take BufferedImage as argument
	 * modified from initial source http://answers.opencv.org/question/31505/how-load-and-display-images-with-java-using-opencv/
	 * @param img2 -- image to display
	 * @param str -- description of image will appear as title of image frame
	 */
	public static void displayImage(BufferedImage img2, String str)
	{   
		MarvinImagePanel	imagePanel;
		imagePanel = new MarvinImagePanel();
	    JFrame frame=new JFrame();      
	    frame.setSize(img2.getWidth()+20, img2.getHeight()+50);     
	    frame.add(imagePanel, BorderLayout.CENTER);
	    frame.setVisible(true);
	    frame.setTitle(str);
	    imagePanel.setImage(new MarvinImage(img2));
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Mat2BufferedImage
	 * @param Mat m
	 * @return BufferedImage
	 */
	
	public static BufferedImage Mat2BufferedImage(Mat m){
		// source: http://answers.opencv.org/question/10344/opencv-java-load-image-to-gui/
		// Fastest code
		// The output can be assigned either to a BufferedImage or to an Image

		    int type = BufferedImage.TYPE_BYTE_GRAY;
		    if ( m.channels() > 1 ) {
		        type = BufferedImage.TYPE_3BYTE_BGR;
		    }
		    int bufferSize = m.channels()*m.cols()*m.rows();
		    byte [] b = new byte[bufferSize];
		    m.get(0,0,b); // get all the pixels
		    BufferedImage image = new BufferedImage(m.cols(),m.rows(), type);
		    final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		    System.arraycopy(b, 0, targetPixels, 0, b.length);  
		    return image;

		}
}
