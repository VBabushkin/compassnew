package openCVCamCapture;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;



public class TakeChessboardPictures {
	static String savePath= ".//camPictures//leftRightImages130//";
	private static String leftImageName="D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\output\\now1.jpg";
	private static String rightImageName="D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\output\\now2.jpg";
	private static String exePath = "D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\CompassOld.exe";
	private static String exeFolder="D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\";
	private static StringBuffer sb;
	
	private static String exePath_1 ="C:\\Program Files (x86)\\OptiTrack\\Camera SDK\\bin\\visualtest.exe";
	private static String exeFolder_1="C:\\Program Files (x86)\\OptiTrack\\Camera SDK\\bin\\";
			
	public static void main(String[] args) throws IOException, InterruptedException {
		
		for(int number =49130; number <=49130; number ++){
			//first run the native Optitrack SDK to arrange shoes
			Runtime.getRuntime().exec(exePath_1, null, new File(exeFolder_1)).waitFor();
			
			
			Runtime.getRuntime().exec(exePath, null, new File(exeFolder));
			Thread.sleep(3500);
			System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
			String fileLeft=leftImageName;
			Mat sourceLeft = Highgui.imread(fileLeft, Highgui.CV_LOAD_IMAGE_COLOR);
			BufferedImage sourceBufferLeft = ProcessImages.Mat2BufferedImage(sourceLeft.clone());
			
			String fileRight=rightImageName;
			Mat sourceRight = Highgui.imread(fileRight, Highgui.CV_LOAD_IMAGE_COLOR);
			BufferedImage sourceBufferRight = ProcessImages.Mat2BufferedImage(sourceRight.clone());

			String outputNameLeft ="left"+number;
			String outputNameRight ="right"+number;
		
			
			ImageIO.write(sourceBufferLeft, "JPG", new File(savePath+"\\"+ outputNameLeft +".jpg"));
			ImageIO.write(sourceBufferRight, "JPG", new File(savePath+"\\"+ outputNameRight +".jpg"));
			Thread.sleep(500);
			deletefile(leftImageName); 
			deletefile(rightImageName);
			Thread.sleep(1500);
		}//end of for
		
		
		
	}//end of main
	/**
	 * 
	 * @param fileName
	 */
	private static void deletefile(String fileName) {
		File file = new File(fileName);
		boolean success = file.delete();
		if (!success) {
			System.out.println(fileName + " Deletion failed.");
		} else {
			System.out.println(fileName + " File deleted.");
		}
	}
}
