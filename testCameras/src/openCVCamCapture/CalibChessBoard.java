package openCVCamCapture;

//http://stackoverflow.com/questions/29388289/opencv-triangulation-output-seems-twisted
//http://stackoverflow.com/questions/24130884/opencv-stereo-camera-calibration-image-rectification
//http://www.jayrambhia.com/blog/stereo-calibration/
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point;
import org.opencv.core.Point3;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.bytedeco.javacpp.Pointer;
import org.bytedeco.javacpp.opencv_calib3d;
import org.bytedeco.javacpp.opencv_core.FileStorage;
public class CalibChessBoard implements Serializable{

	static String leftfilename;
	static String rightfilename;
    static String initialImagePath=".//camPictures//leftRightImages130//";
    static String dataPath =".//camPictures//calibData130//";
	private static final TermCriteria CRITERIA = new TermCriteria(
			TermCriteria.EPS + TermCriteria.COUNT, 30, 0.1);
	static int CHESSBOARD_WIDTH = 9;//21; //input width of  chessboard
    static int CHESSBOARD_HEIGHT = 6;//14; //input height of chessboard
    double squareSize = 1.8;//2.5; //input size of a side of a single square in chessboard
    static Size winSize = new Size(5, 5);
    static Size zoneSize = new Size(-1, -1);
	
	
	public static void main(String args[]) throws IOException{
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
	    Size patternSize=new Size(CHESSBOARD_WIDTH,CHESSBOARD_HEIGHT);
		List<Mat> objectPoints = new ArrayList<Mat>();
		
		Size imageSize=null;
		List<Mat> imagePoints1 = new ArrayList<Mat>();
		MatOfPoint2f corners1 = new MatOfPoint2f();
		
		List<Mat> imagePoints2 = new ArrayList<Mat>();
		MatOfPoint2f corners2 = new MatOfPoint2f();
		List<Mat> leftImages =new ArrayList<Mat>();
		List<Mat> rightImages =new ArrayList<Mat>();
		int counter=(new File(initialImagePath).listFiles().length)/2;
		//int i=1;
		for(int i=1;i<=counter; i++){
			objectPoints.add(getCorner3f(patternSize));
			leftfilename=initialImagePath+"left"+i+".jpg";
		    rightfilename=initialImagePath+"right"+i+".jpg";
		    
		    Mat imgleft_frame=Highgui.imread(leftfilename, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
		    Mat imgright_frame=Highgui.imread(rightfilename, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
		    imageSize=imgleft_frame.size();
			Mat images[] = new Mat[2];
			images[0]=imgleft_frame;
			images[1]=imgright_frame;
			
			leftImages.add(imgleft_frame);
			rightImages.add(imgright_frame);
			
			boolean found1=getCorners(images[0],patternSize,winSize,zoneSize, corners1);
		    if ( ! found1) {
		        System.out.println("No corner detected for left image");
		    }
		    imagePoints1.add(corners1);
	
		    Imgproc.cvtColor(imgleft_frame, imgleft_frame, Imgproc.COLOR_GRAY2BGR);
		    Calib3d.drawChessboardCorners(imgleft_frame, patternSize,corners1, found1);
		    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgleft_frame),"Corners found right image # "+i);
		
		    boolean found2=getCorners(images[1], patternSize,winSize,zoneSize, corners2);
		    if ( ! found2) {
		    	System.out.println("No corner detected for right image");
		    }
		    
		    imagePoints2.add(corners2);
		  
		    Imgproc.cvtColor(imgright_frame, imgright_frame, Imgproc.COLOR_GRAY2BGR);
		    Calib3d.drawChessboardCorners(imgright_frame, patternSize,corners2, found2); 
		    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgright_frame),"Corners found right image # "+i);
		}
	    
	     Mat cameraMatrix1 = new Mat();
	     Mat cameraMatrix2 = new Mat();
	    cameraMatrix1.eye(3, 3, CvType.CV_64FC1);
	    cameraMatrix2.eye(3, 3, CvType.CV_64FC1);
	     Mat distCoeff1 = new Mat();
	     Mat distCoeff2 = new Mat();
	    Mat R = new Mat();
	    Mat T = new Mat();
	    Mat E= new Mat();
	    Mat F = new Mat();
	    double errReproj=Calib3d.stereoCalibrate(
	        objectPoints,
	        imagePoints1,
	        imagePoints2,
	        cameraMatrix1,
	        distCoeff1,
	        cameraMatrix2,
	        distCoeff2,
	        imageSize,
	        R,
	        T,
	        E,
	        F,CRITERIA,Calib3d.CALIB_FIX_ASPECT_RATIO +
			+Calib3d.CALIB_ZERO_TANGENT_DIST +Calib3d.CALIB_SAME_FOCAL_LENGTH +
			+Calib3d.CALIB_RATIONAL_MODEL +Calib3d.CALIB_FIX_K3 +
			Calib3d.CALIB_FIX_K4 + Calib3d.CALIB_FIX_K5
	    );
	    
	    System.out.println("Done");
	    System.out.println("cameraMatrix1 = \n" + cameraMatrix1.dump());
	    System.out.println("cameraMatrix2 = \n" + cameraMatrix2.dump());
	    System.out.println("distCoeff1 = \n" + distCoeff1.dump());
	    System.out.println("distCoeff2 = \n" + distCoeff2.dump());
	    System.out.println("R = \n" + R.dump());
	    System.out.println("T = \n" + T.dump());
	    System.out.println("E = \n" + E.dump());
	    System.out.println("F = \n" + F.dump());
	    System.out.println("errReproj = " + errReproj);
	    
//	    //save intrinsics
	   
	    
	    System.out.println(distCoeff1.toString());
	    
	    TaFileStorage storage = new TaFileStorage();
	    storage.create(dataPath+"dataStorage.xml");
	    storage.writeMat("distCoeff1",distCoeff1);
	    storage.writeMat("distCoeff2",distCoeff2);
	    storage.writeMat("cameraMatrix1",cameraMatrix1);
	    storage.writeMat("cameraMatrix2",cameraMatrix2);
	    storage.release();
		
		
		
//		ObjectOutputStream d2oos = new ObjectOutputStream(new FileOutputStream(dataPath+"distCoeff2.ser"));   
//		d2oos.writeObject((Object)distCoeff2);
//		d2oos.flush();
//		d2oos.close();
//	    
//	    FileOutputStream m1out = new FileOutputStream(dataPath+"cameraMatrix1.ser");
//		ObjectOutputStream m1oos = new ObjectOutputStream(m1out);   
//		m1oos.writeObject((Object)cameraMatrix1);
//		m1oos.close();
//		
//		FileOutputStream m2out = new FileOutputStream(dataPath+"cameraMatrix2.ser");
//		ObjectOutputStream m2oos = new ObjectOutputStream(m2out);   
//		m2oos.writeObject((Object)cameraMatrix2);
//		m2oos.close();
		
		
		
		System.out.println("Done");
		
		//Rectification Works!!!
//	    Mat rectification1 = new Mat();
//	    Mat rectification2 = new Mat();
//	    Mat projectionMatrix1 = new Mat();
//	    Mat projectionMatrix2 = new Mat();
//	    Mat dtdMatrix = new Mat();
//	    
//	    Calib3d.stereoRectify(
//	        cameraMatrix1,
//	        distCoeff1,
//	        cameraMatrix2,
//	        distCoeff2,
//	        imageSize,
//	        R,
//	        T,
//	        rectification1,
//	        rectification2,
//	        projectionMatrix1,
//	        projectionMatrix2,
//	        dtdMatrix
//	    ); 
	    
	   //TODO:
	   //Draws no epipolar lines...
//	   Mat lines1 = new Mat();
	   // Imgproc.undistortPoints(corners1, corners1, cameraMatrix1, distCoeff1, dtdMatrix, projectionMatrix1);
//	   Calib3d.computeCorrespondEpilines(corners1, 1, rectification2, lines1);
	    
	    
	   //TODO: 
	   //Try with undistortPoints() as in http://stackoverflow.com/questions/24130884/opencv-stereo-camera-calibration-image-rectification
	   Mat undistored1 = new Mat();
	   
	   int num=49130;
	   
	   String leftFile = ".//camPictures//left"+num+".jpg";
	   String rightFile = ".//camPictures//right"+num+".jpg";
	   
	   Mat imgleft=Highgui.imread(leftFile, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
	   Mat imgright=Highgui.imread(rightFile, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
	   
	   Imgproc.undistort(imgleft, undistored1, cameraMatrix1, distCoeff1);
//	   drawEpiLines(undistored1, lines1);
	   ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(undistored1),"undistored left");
	   
	   
	   Mat undistored2 = new Mat();
	   Imgproc.undistort(imgright, undistored2, cameraMatrix2, distCoeff2);
	   ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(undistored2),"undistored right");
	   //stereoRectify????
	   Highgui.imwrite(".//camPictures//undistLeft"+num+".jpg", undistored1);
	   Highgui.imwrite(".//camPictures//undistRight"+num+".jpg", undistored2);
//	    
	    System.out.println("Done");
	    
	    

	}//end of main()
	
	/**
	 * 
	 * @param patternSize
	 * @return
	 */
	public static MatOfPoint3f getCorner3f(final Size patternSize) {
		final MatOfPoint3f corners3f = new MatOfPoint3f();
		final double squareSize = 25;
		final Point3[] vp = new Point3[(int) (patternSize.height * patternSize.width)];
		int cnt = 0;
		for (int i = 0; i < patternSize.height; ++i) {
			for (int j = 0; j < patternSize.width; ++j, cnt++) {
				vp[cnt] = new Point3(j * squareSize, i * squareSize, 0.0d);
			}
		}
		corners3f.fromArray(vp);
		return corners3f;
	}

	/**
	 * Find chess board corners.
	 * 
	 * @param gray
	 *            Gray image.
	 * @param patternSize
	 *            Chess board pattern size.
	 * @param winSize
	 *            Window size.
	 * @param zoneSize
	 *            Zone size.
	 * @param corners
	 *            This value is modified by JNI code.
	 * @return Value of findChessboardCorners.
	 */
	public static boolean getCorners(final Mat gray, final Size patternSize,
			final Size winSize, final Size zoneSize, final MatOfPoint2f corners) {
		boolean found = false;
		if (Calib3d.findChessboardCorners(gray, patternSize, corners)) {
			Imgproc.cornerSubPix(gray, corners, winSize, zoneSize, CRITERIA);
			found = true;
		}
		return found;
	}
	
	/**
	 * Draw Epipolar Lines
	 * @param outImg
	 * @param epilinesSrc
	 * @return
	 */
	public static Mat drawEpiLines(Mat outImg, Mat epilinesSrc ) {

        int epiLinesCount = epilinesSrc.rows();

        double a, b, c;

        for (int line = 0; line < epiLinesCount; line++) {
            a = epilinesSrc.get(line, 0)[0];
            b = epilinesSrc.get(line, 0)[1];
            c = epilinesSrc.get(line, 0)[2];

            int x0 = 0;
            int y0 = (int) (-(c + a * x0) / b);
            int x1 = outImg.cols() / 2;
            int y1 = (int) (-(c + a * x1) / b);

            Point p1 = new Point(x0, y0);
            Point p2 = new Point(x1, y1);
            Scalar color = new Scalar(255, 255, 255);
            Core.line(outImg, p1, p2, color);

        }

//        for (int line = 0; line < epiLinesCount; line++) {
//            a = epilinesDst.get(line, 0)[0];
//            b = epilinesDst.get(line, 0)[1];
//            c = epilinesDst.get(line, 0)[2];
//
//            int x0 = outImg.cols() / 2;
//            int y0 = (int) (-(c + a * x0) / b);
//            int x1 = outImg.cols();
//            int y1 = (int) (-(c + a * x1) / b);
//
//            Point p1 = new Point(x0, y0);
//            Point p2 = new Point(x1, y1);
//            Scalar color = new Scalar(255, 255, 255);
//            Core.line(outImg, p1, p2, color);
//
//        }
        ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(outImg),"Epipolar Lines");
        return outImg;
    }
	
}
