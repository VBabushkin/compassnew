package openCVCamCapture;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.FileUtils;

public class recordCameraVideos_1 {
	
	
	private static String initFilesFolder="D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\output\\";
	private static String mainDirectory="./images/";
	
	static int currentNum=21;
	
	public static String resDirectory = mainDirectory+currentNum+"_1/";
	
	
	public static void main(String args[]) throws IOException, InterruptedException{
		System.out.println("Executing the exe... ");
		Process p = Runtime.getRuntime().exec("D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\Compass.exe", null, new File("D:\\WorkspaceEclipse\\CompassCamera\\bin\\Release\\"));
		System.out.println("Waiting for exe file...");
		//Thread.sleep(40000);
		int processComplete = p.waitFor();
		System.out.println("Process complete "+processComplete);

		System.out.println("Done... ");
		
		//create a random directory to store the video
		//long currentTime = System.currentTimeMillis();
		
		File file = new File(resDirectory);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println(resDirectory + "     Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
		
		//move to newly created dir
		
		File mainDir=new File(initFilesFolder);
		
		File[] allImageFiles=mainDir.listFiles();
		
		for (File  source : allImageFiles) {
	    	  
	    	  File dest = new File(resDirectory+source.getName());
	    	  try {
	    	      FileUtils.copyFile(source, dest);
	    	      } 
	    	  catch (IOException e) {
	    	    	  e.printStackTrace();
	    	      }
	    	  Thread.sleep(500);
	  		
	    	  
	      }
		System.out.println("Data is recorded...");
		
	}

}
