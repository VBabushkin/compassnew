package openCVCamCapture;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point3;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.highgui.Highgui;

public class StereoCameraCalibration {
	static String initialImagePath=".//camPictures//leftRightImages//";
	public static void main(String args[]){
		
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		
		int CHESSBOARD_WIDTH = 9; //input width of  chessboard
	    int CHESSBOARD_HEIGHT = 6; //input height of chessboard
	    double squareSize = 3.96; //input size of a side of a single square in chessboard
	    Size corner=new Size(CHESSBOARD_WIDTH,CHESSBOARD_HEIGHT);
	    int counter =30;
	    int nimages=24;
	    Size imageSize;
	    int capturing=0;
	    int calibrated=1;
	    int mode=capturing;
	    String leftfilename;
	    String rightfilename;
	    List<Mat>  imagePoints1 = new Vector<Mat>();
	    List<Mat>  imagePoints2 = new Vector<Mat>();
	    //std::vector<std::vector<cv::Point3f>> objectPoints;
	    Point3[] vp = new Point3[(int) (corner.height * corner.width)];
	    List<Mat> objectPoints = new ArrayList<Mat>();
	    boolean found1=false;
	    boolean found2=false;
	    int counter2=0;
	    //Mat pointBuf1= new Mat(54,2,CvType.CV_32FC1);
	    MatOfPoint2f pointBuf1=new MatOfPoint2f();
	    MatOfPoint2f pointBuf2=new MatOfPoint2f();//new Mat(54,2,CvType.CV_32FC1);
	    
	    int i=2;
	    
	    leftfilename=initialImagePath+"left"+i+".jpg";
	    rightfilename=initialImagePath+"right"+i+".jpg";
	    
	    Mat imgleft_frame=Highgui.imread(leftfilename, Highgui.CV_LOAD_IMAGE_COLOR);
	    Mat imgright_frame=Highgui.imread(rightfilename, Highgui.CV_LOAD_IMAGE_COLOR);
	    
	    
	    imageSize=imgleft_frame.size();
	    
	    found1 = Calib3d.findChessboardCorners(imgleft_frame, corner,pointBuf1,Calib3d.CALIB_CB_ADAPTIVE_THRESH |Calib3d.CALIB_CB_FAST_CHECK | Calib3d.CALIB_CB_NORMALIZE_IMAGE);
	    Calib3d.drawChessboardCorners(imgleft_frame, corner,  pointBuf1, found1);
	    ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgleft_frame),"Corners found left image");
	    
	    
	   found2 = Calib3d.findChessboardCorners(imgright_frame, corner, pointBuf2, Calib3d.CALIB_CB_ADAPTIVE_THRESH |Calib3d.CALIB_CB_FAST_CHECK | Calib3d.CALIB_CB_NORMALIZE_IMAGE);
	   Calib3d.drawChessboardCorners(imgright_frame, corner,  pointBuf2, found2);
	   ProcessImages.displayImage(ProcessImages.Mat2BufferedImage(imgright_frame),"Corners found right image");
	    
	    
	    
	    
	    if(found1&&found2)
	    {         
	    	imagePoints1.add(pointBuf1);
		    imagePoints2.add(pointBuf2);
		    //sprintf(leftfilename,"newleftcheck%d.jpg",s.counter);
		    //sprintf(rightfilename,"newrightcheck%d.jpg",s.counter);
		    //Highgui.imwrite(initialImagePath+" corners_"+leftfilename,imgleft_frame);
		    //Highgui.imwrite(initialImagePath+" corners_"+rightfilename,imgright_frame);
		    
		    counter2=counter2+1;
		    System.out.println(counter2);
	    }
	    
	    nimages=counter2;
//	    //objectPoints.resize(nimages);
//	    MatOfPoint3f corners3f =new MatOfPoint3f();
//	    //vp.setSize(nimages);
//	    System.out.println("Size of objectPoints "+vp.length);
//	    for( i = 0; i <nimages; i++ )
//	    {
//	    	int cnt = 0;
//	        for( int j = 0; j < CHESSBOARD_HEIGHT; j++ )
//	            for( int k = 0; k < CHESSBOARD_WIDTH; k++,cnt++ )
//	            	vp[cnt]= new Point3(j*squareSize, k*squareSize, 0.0d);
//	        System.out.println("vp[1] "+vp[52]);
//	       corners3f.fromArray(vp); 
//	       objectPoints.add(corners3f);
//	    }
//	    
//	    
//	    System.out.println("objectPoints   "+objectPoints.toString());
	    
	    
	    Mat cameraMatrix[]=new Mat[2];
	    Mat distCoeffs[] = new Mat[2];
	    cameraMatrix[0].eye(3, 3, CvType.CV_64FC1);
	    cameraMatrix[1].eye(3, 3, CvType.CV_64FC1);
	    Mat R = new Mat();
	    Mat T = new Mat();
	    Mat E= new Mat();
	    Mat F = new Mat();
	    
	    System.out.println("Size of objectPoints "+objectPoints.size());
	    System.out.println("Size of imagePoints1 "+imagePoints1.size());
	    
//	    if(imagePoints1.size()==imagePoints2.size())
//	    	System.out.println("Same size");
//	    
//	    if(imagePoints1.size()>=nimages ){
//	    	//Calib3d.stereoCalibrate(objectPoints, imagePoints1, imagePoints2, cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, imageSize, R, T, E, F, criteria, flags)
//	    	double rms =Calib3d.stereoCalibrate(objectPoints, imagePoints1, imagePoints2, 
//	    			cameraMatrix[0], distCoeffs[0], cameraMatrix[1], distCoeffs[1], 
//	    			imageSize, R, T, E, F, 
//	    			new TermCriteria(TermCriteria.COUNT+TermCriteria.EPS, 100, 1e-5), 
//	    			Calib3d.CALIB_FIX_ASPECT_RATIO +
//	    			+Calib3d.CALIB_ZERO_TANGENT_DIST +Calib3d.CALIB_SAME_FOCAL_LENGTH +
//	    			+Calib3d.CALIB_RATIONAL_MODEL +Calib3d.CALIB_FIX_K3 +
//	    			Calib3d.CALIB_FIX_K4 + Calib3d.CALIB_FIX_K5);
//	    }
	    
	    
	    
	   
	    
	    
	    
	    
	    
	    
	    
	}//end of main
	
    
    
}
