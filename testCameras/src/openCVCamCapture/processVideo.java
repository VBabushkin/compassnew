package openCVCamCapture;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;

import org.bytedeco.javacpp.opencv_highgui.VideoCapture;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Size;
import org.bytedeco.javacpp.opencv_highgui;
import org.bytedeco.javacpp.opencv_imgproc;
import org.opencv.core.MatOfByte;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class processVideo {
	static String currentFolder ="GRAY_2_15";
	static String initialImagePath=".//videos//"+currentFolder+"//";
	static String savePath=".//videoFrames//";
	public static void main(String args[]) throws IOException{
		
		File directory = new File(savePath+currentFolder);
		if(!directory.exists()){
			if(directory.mkdir())
				System.out.println(directory.getPath()+" is created");
			else
				System.out.println("Failed to create a directory...");
		}
		String name = "outVideo";
		String filename=initialImagePath+name+".mp4";
		VideoCapture capture=new VideoCapture();
		capture.open(filename);
		int length=(int) capture.get(7);
		System.out.println(length);
		Mat frame=new Mat();
		long startTime = System.currentTimeMillis();
		        
		int i=0;
		while(i<length){
		   capture.read(frame); //reads captured frame into the Mat image
		   System.out.println("Frame # " +i);
			   //if(i%100==0){
				   //displayImage(frame.getBufferedImage(),"frame "+i);
				   String outputName ="frame_"+name+"_"+i;
				   ImageIO.write(frame.getBufferedImage(), "JPG", new File(savePath+currentFolder+"//"+ outputName +".jpg"));
				
			   //} 
			   i++;
		}
		long duration=System.currentTimeMillis()-startTime;  
		System.out.println("Time elapsed "+duration);
	}


	/**
	 * to display images overloaded function to take BufferedImage as argument
	 * modified from initial source http://answers.opencv.org/question/31505/how-load-and-display-images-with-java-using-opencv/
	 * @param img2 -- image to display
	 * @param str -- description of image will appear as title of image frame
	 */
	public static void displayImage(BufferedImage img2, String str)
	{   
		MarvinImagePanel	imagePanel;
		imagePanel = new MarvinImagePanel();
	    JFrame frame=new JFrame();      
	    frame.setSize(img2.getWidth()+20, img2.getHeight()+50);     
	    frame.add(imagePanel, BorderLayout.CENTER);
	    frame.setVisible(true);
	    frame.setTitle(str);
	    imagePanel.setImage(new MarvinImage(img2));
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    

	}
	
}
